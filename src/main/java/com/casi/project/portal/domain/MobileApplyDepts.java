package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 应用与部门关系表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MobileApplyDepts implements Serializable {
    private String applyId;
    private String deptId;
}
