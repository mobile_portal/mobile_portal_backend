package com.casi.project.portal.controller;

import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.ServletUtils;
import com.casi.common.utils.StringUtils;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.security.LoginUser;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.ApplyDetails;
import com.casi.project.portal.domain.ApplyVersion;
import com.casi.project.portal.domain.UserAndRole;
import com.casi.project.portal.service.impl.ApplyDetailsServiceImpl;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.mapper.SysUserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * 应用明细接口等
 *
 * @author 18732
 */
@Api(tags = { "应用详情管理" })
@RestController
@RequestMapping("/apply/applyDetails")
public class ApplyDetailsController extends BaseController {

    @Resource
    private ApplyDetailsServiceImpl applyService;

    @Autowired
    private TokenService tokenService;

    @Resource
    private SysUserMapper userMapper;


    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);


    /**
     * 获取的应用的app_key和app_secret
     *
     * @return
     */
    @ApiOperation(value = "获取的应用的app_key和app_secret")
    @PreAuthorize("@ss.hasPermi('apply:detail:add')")
    @GetMapping("/getApplyKey")
    public AjaxResult getApplyKey() {
        HashMap<String, Object> hashMap = new HashMap<>();
        try {
            hashMap.put("appKey", UUID.randomUUID().toString().toUpperCase().replace("-", ""));
            hashMap.put("appSecret", UUID.randomUUID().toString().toUpperCase().replace("-", ""));
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("生成失败");
        }
        return AjaxResult.success("生成成功", hashMap);
    }

    /**
     * 新增应用具体详细
     *
     * @param applyDetails
     * @return
     */
    @ApiOperation(value = "新增应用详情")
    @Log(title = "新增应用详情", businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermi('apply:detail:add')")
    @PostMapping(value = "/addApplyDetails")
    public AjaxResult addApplyDetails(@RequestBody ApplyDetails applyDetails) {
        logger.info("===============新增应用具体详细=============");
        return applyService.addApplyDetails(applyDetails);
    }

    /**
     * 回显应用明细
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "回显应用详情")
    @PreAuthorize("@ss.hasPermi('apply:detail:echo')")
    @GetMapping("/findApplyDetailsId")
    public AjaxResult findApplyDetailsId(String id) {
        logger.info("===============根据id回显应用明细具体详情=============");
        return applyService.findApplyDetailsId(id);
    }

    /**
     * 修改应用明细
     *
     * @param applyDetails
     * @return
     */
    @ApiOperation(value = "修改应用明细")
    @Log(title = "修改应用明细", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('apply:detail:edit')")
    @PostMapping(value = "/updateApplyDetails")
    public AjaxResult updateApplyDetails(@RequestBody ApplyDetails applyDetails) {
        logger.info("===============修改应用具体详细=============");
        return applyService.updateApplyDetails(applyDetails);
    }

    /**
     * 删除应用明细
     *
     * @param
     * @return
     */
    @ApiOperation(value = "删除应用明细")
    @Log(title = "删除应用明细", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermi('apply:detail:remove')")
    @GetMapping("/deleteApplyDetails")
    public AjaxResult deleteApplyDetails(String id) {
        logger.info("===============删除应用具体详细=============");
        return applyService.deleteApplyDetails(id);
    }


    /**
     * 禁用启用应用明细
     *
     * @return
     */
    @ApiOperation(value = "禁用启用应用明细")
    @Log(title = "禁用启用应用明细", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('apply:detail:disable')")
    @GetMapping("/disableEnabledDetails")
    public AjaxResult disableEnabledDetails(String[] id, Integer type) {
        logger.info("===============禁用应用明细=============");
        return applyService.disableEnabledDetails(id, type);
    }

    /**
     * 查询应用管理列表和条件模糊查询
     *
     * @param menuButtonName
     * @return
     */
    @ApiOperation(value = "查询应用管理列表和条件模糊查询")
    @PreAuthorize("@ss.hasPermi('apply:detail:list')")
    @GetMapping(value = "/getApplyList")
    public AjaxResult getApplyList(String menuButtonName, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);
        logger.info("===============查询应用管理列表和条件模糊查询=============");
        List<Map<String, Object>> applyList = new ArrayList<>();
        try {
            SysUser loginUser = SecurityUtils.getLoginUser().getUser();
            if (StringUtils.isNull(loginUser)) {
                return AjaxResult.error("没有当前登录用户");
            }
            if (loginUser.isAdmin()) {
                applyList = applyService.getApplyList(menuButtonName);
            } else {
                //查询用户和部门的应用id
                List<HashMap<String, Object>> applyId = applyService.getApplyId(loginUser.getUserId(), loginUser.getDeptId());
                if (applyId == null) {
                    return AjaxResult.success();
                }
                String value = "";
                for (HashMap<String, Object> hashMap : applyId) {
                    Set<Map.Entry<String, Object>> entries = hashMap.entrySet();
                    for (Map.Entry<String, Object> entry : entries) {
                        String value2 = (String) entry.getValue();
                        value += value2 + ",";
                    }
                }
                String[] split = value.split(",");
                for (int i = 0; i < split.length; i++) {
                    Map<String, Object> applyListId = applyService.getApplyListId(menuButtonName, split[i]);
                    int rows = applyService.selectIsBlackList(loginUser.getUserId(), split[i]);
                    if (applyListId != null && rows < 1) {
                        applyList.add(applyListId);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");
        }
        logger.info("查询成功" + applyList);
        PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(applyList);
        return AjaxResult.success("查询成功", mapPageInfo);
    }


    /**
     * 审核应用
     *
     * @param id 应用id
     * @return
     */
    @ApiOperation(value = "审核应用")
    @Log(title = "审核应用", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('apply:detail:check')")
    @GetMapping("/checkApplyStatus")
    public AjaxResult checkApplyStatus(String id, Integer type) {
        logger.info("==========审核应用==========");
        return applyService.checkApplyStatus(id, type);
    }

    /**
     * 上线应用
     *
     * @return
     */
    @GetMapping("/upLineApply")
    public AjaxResult upLineApply(String id) {
        logger.info("=========上线应用==========");
        AjaxResult ajaxResult = null;
        try {
            ajaxResult = applyService.upLineApply(id);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return ajaxResult;
    }

    /**
     * 新增轻应用服务号版本
     *
     * @return
     */
    @PostMapping("/addApplyVersion")
    public AjaxResult addApplyVersion(@RequestBody ApplyVersion applyVersion) {
        logger.info("===============新增应用版本=============");
        return applyService.addApplyVersion(applyVersion);
    }

    /**
     * 应用指定开发者账户——应用列表展示
     */
    @GetMapping("/queryAllApply")
    public AjaxResult queryAllApply(Integer pageNum, Integer pageSize) {
        return applyService.queryAllApply(pageNum, pageSize);
    }

    /**
     * 指定应用开发管理员
     *
     * @param
     * @param
     * @return
     */
    @GetMapping("/addApplyDevelopAdminIsTraTor")
    public AjaxResult addApplyDevelopAdminIsTraTor(@RequestBody UserAndRole userAndRole) {
        return applyService.addApplyDevelopAdminIsTraTor(userAndRole);
    }

    /**
     * 根据应用appKey和appSecret获得应用的资源
     *
     * @param hashMap
     * @return
     */
    @ApiOperation(value = "根据应用appKey和appSecret获得应用的资源")
    @PostMapping("/getApplyResource")
    public AjaxResult getApplyResource(@RequestBody HashMap<String, Object> hashMap) {
        return applyService.getApplyResource(hashMap);
    }

    /**
     * 根据应用appKey和appSecret获得应用的用户
     *
     * @param hashMap
     * @return
     */
    @ApiOperation(value = "根据应用appKey和appSecret获得应用的用户")
    @PostMapping("/getApplyUser")
    public AjaxResult getApplyUser(@RequestBody HashMap<String, Object> hashMap) {
        return applyService.getApplyUser(hashMap);
    }

    /**
     * 根据应用appKey和appSecret获得应用的所有角色
     */
    @ApiOperation(value = "根据应用appKey和appSecret获得应用的所有角色")
    @GetMapping("/queryRolesByAppKeyAndAppSecret")
    public AjaxResult queryRolesByAppKeyAndAppSecret(@RequestBody ApplyDetails applyDetails) {
        return applyService.queryRolesByAppKeyAndAppSecret(applyDetails);
    }

    /**
     * 根据应用appKey和appSecret获得应用的所有组织机构
     */
    @ApiOperation(value = "根据应用appKey和appSecret获得应用的所有组织机构")
    @GetMapping("/queryDeptsByAppKeyAndAppSecret")
    public AjaxResult queryDeptsByAppKeyAndAppSecret(@RequestBody ApplyDetails applyDetails) {
        return applyService.queryDeptsByAppKeyAndAppSecret(applyDetails);
    }

    /**
     * 根据应用appKey和appSecret和用户token获得用户的角色
     */
    @ApiOperation(value = "根据应用appKey和appSecret和用户token获得用户的角色")
    @GetMapping("/queryRoleByAppKeyAndAppSecretAndToken")
    public AjaxResult queryRoleByAppKeyAndAppSecretAndToken(@RequestBody ApplyDetails applyDetails) {
        return applyService.queryRoleByAppKeyAndAppSecretAndToken(applyDetails);
    }

    /**
     * 根据应用appKey和appSecret和token获得用户的用户权限
     *
     * @param hashMap
     * @return
     */
    @ApiOperation(value = "根据应用appKey和appSecret和token获得用户的用户权限")
    @PostMapping("/getApplyUserPermission")
    public AjaxResult getApplyUserPermission(@RequestBody HashMap<String, Object> hashMap) {
        return applyService.getApplyUserPermission(hashMap);
    }
}
