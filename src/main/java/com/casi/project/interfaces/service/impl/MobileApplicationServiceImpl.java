package com.casi.project.interfaces.service.impl;

import com.alibaba.fastjson.JSON;
import com.casi.common.constant.Constants;
import com.casi.common.utils.StringUtils;
import com.casi.common.utils.http.HttpUtils;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.interfaces.domain.Text;
import com.casi.project.interfaces.service.MobileApplicationService;
import com.casi.project.interfaces.utils.AccessTokenUtils;
import com.casi.project.interfaces.utils.HttpRequest;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.mapper.SysUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class MobileApplicationServiceImpl implements MobileApplicationService {
    private static Logger logger = LoggerFactory.getLogger(MobileApplicationServiceImpl.class);

    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private AccessTokenUtils accessTokenUtils;

    /**
     * @author: liyuan   AccessTokenUtils
     * @date: 2020/5/21 15:32
     * @Description:  单点登录（移动商店调用门户平台，登录）
     */
    @Override
    public AjaxResult login(String userName) {
        if(StringUtils.isEmpty(userName)){
            return AjaxResult.error(9999,"用户名不能为空");
        }
        SysUser sysUser = sysUserMapper.selectByUserName(userName);
        if(StringUtils.isNull(sysUser)){
            return AjaxResult.error(9999,"该用户不存在");
        }
        //获取access_token
        String accessToken = accessTokenUtils.getAccessToken();
        if(StringUtils.isEmpty(accessToken)){
            return AjaxResult.error(9999,"系统错误，请稍候再试");
        }
        //根据access_token和userID去云信获取用户信息
        String s = HttpUtils.sendGet(Constants.url, "");
        if(StringUtils.isEmpty(s)){
            return AjaxResult.error(9999,"该用户不存在");
        }
        return AjaxResult.success(200,"成功");
    }
    /**
     * @author: liyuan
     * @date: 2020/5/22 9:57
     * @Description:  推送消息接口（移动商店调用门户平台，推送消息）
     */
    @Override
    public AjaxResult sendMsg(Map<String, Object> params) {
        if(StringUtils.isEmpty(params)){
            return AjaxResult.error("缺失参数");
        }
        if(StringUtils.isEmpty(String.valueOf(params.get("idToken")))){
            return AjaxResult.error(999,"idToken不能为空!");
        }
        /*try {
            LoginUser loginUser = tokenService.parsingToken(String.valueOf(params.get("idToken")));
        }catch (Exception e){
            return AjaxResult.error(999,"idToken认证失败");
        }*/
        String agentId = String.valueOf(params.get("agentId"));
        if(StringUtils.isEmpty(agentId)){
            return AjaxResult.error(999,"企业应用Id不能为空");
        }
        String msgType = String.valueOf(params.get("msgType"));
        if(StringUtils.isEmpty(msgType)){
            return AjaxResult.error(999,"消息类型不能为空");
        }
        if(!msgType.equals("text") && !msgType.equals("image") && !msgType.equals("voice") && !msgType.equals("video") && !msgType.equals("file") && !msgType.equals("textcard") && !msgType.equals("news")){
            return AjaxResult.error(999,"消息类型错误");
        }
        //获取access_token
        String accessToken = accessTokenUtils.getAccessToken();
        if(StringUtils.isEmpty(accessToken)){
            return AjaxResult.error(999,"系统错误，请稍候再试");
        }
        String userId =(String)params.get("userId");
        Map<String,Object> param = new HashMap<String,Object>();
        if(StringUtils.isEmpty(userId)){
            //如果消息接收人的id为空，那么就是向所有人推送消息推送人的id变为@all
            if(msgType.equals("text")){
                String msg = String.valueOf(params.get("msg"));
                if(StringUtils.isEmpty(msg)){
                    return AjaxResult.error(999,"消息内容不能为空");
                }
                //文本消息
                //封装请求数据
                param.put("touser", "@all");
                param.put("msgtype", msgType);
                param.put("agentid", Integer.parseInt(agentId));
                Text text = new Text();
                text.setContent(msg);
                param.put("text", text);
            }else if(msgType.equals("image")){
                //图片消息
            }else if(msgType.equals("voice")){
                //语音消息
            }else if(msgType.equals("video")){
                //视频消息
            }else if(msgType.equals("file")){
                //文件消息
            }else if(msgType.equals("textcard")){
                //文本卡片消息
            }else if(msgType.equals("news")){
                //图文消息
            }
            //调用云信  发送消息
            logger.info("推送地址消息的地址======"+Constants.WX_SEND_MSG_URL);
            logger.info("云信应用对应的accessToken======"+accessToken);
            String message = JSON.toJSONString(param);
            logger.info("推送消息携带的Json参数======"+message);
            HttpRequest.sendPost(Constants.WX_SEND_MSG_URL+"?access_token="+accessToken, message);
            return null;
        }else{
            //反之userId不为空则是像这个人推送消息
            if(msgType.equals("text")){
                String msg = String.valueOf(params.get("msg"));
                if(StringUtils.isEmpty(msg)){
                    return AjaxResult.error(999,"消息内容不能为空");
                }
                //文本消息
                //封装请求数据
                param.put("touser", userId);
                param.put("msgtype", msgType);
                param.put("agentid", Integer.parseInt(agentId));
                Text text = new Text();
                text.setContent(msg);
                param.put("text", text);
            }else{

            }
            //调用云信  发送消息
            String message = JSON.toJSONString(param);
            logger.info("个人推送消息的内容message++++"+message);
            HttpRequest.sendPost(Constants.WX_SEND_MSG_URL+"?access_token="+accessToken, message);
            return null;
        }
    }
}
