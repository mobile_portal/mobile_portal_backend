package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@NoArgsConstructor
@AllArgsConstructor
@Data
public class MediaBase {



  /**
   * 作者
   */
  private String author;

  /**
   * 开始时间
   */
  private Date onTime;
  /**
   * 结束时间
   */
  private Date offTime;


  /**
   * 主键id
   */
  private Long id;
  /**
   * 素材主键id
   */
  private Long mediaId;
  /**
   * 素材类型（img:图片，voice:音频，video:视频，news:图文，file:文件，web:网页）
   */
  private String mediaType;
  /**
   * 状态（1：待提交，2：审核中，3：已审核，4：已发布，5：已退回）
   */
  private String status;
  /**
   * 审批流程（a:党群信息审批流程，b:精神宣贯审批流程，c:通知公告审批流程）
   */
  private String approveProcess;
  /**
   * 终端类别（1：微信公众，2：移动平台服务号，3：其他软件终端，4：硬件终端）
   */
  private String terminalType;
  /**
   * 微信公众号id集合
   */
  private String wxList;
  /**
   * 移动平台服务号id集合
   */
  private String ydList;
  /**
   * 其他软件终端id集合
   */
  private String otherList;
  /**
   * 硬件终端id集合
   */
  private String hardwareList;
  /**
   * 备注
   */
  private String remark;
  /**
   * 提交人
   */
  private String submitUser;
  /**
   * 提交时间
   */
  private Date submitTime;
  /**
   * 创建时间
   */
  private Date createTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 素材具体信息
   */
  private MediaFile mediaFile;
  /**
   * 素材具体信息
   */
  private MediaWeb mediaWeb;
  /**
   * 素材具体信息
   */
  private MediaNews mediaNews;
  /**
   * 当前页数
   */
  private int pageNum;
  /**
   * 页面记录数
   */
  private int pageSize;
  /**
   * 名称
   */
  private String name;
  /**
   * 网页地址
   */
  private String url;
  /**
   * 文件id字符串集合
   */
  private String fileIdList;
  /**
   * 文件id
   */
  private Long fileId;


  /**
   * 栏目id
   */
  private Long  columnId;


  /**
   * 视频标题
   */
  private String title;
  /**
   * 视频描述
   */
  private String introduction;
  /**
   * newsUrl
   */
  private String newsUrl;

}
