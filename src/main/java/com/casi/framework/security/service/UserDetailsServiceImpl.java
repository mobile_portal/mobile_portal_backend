package com.casi.framework.security.service;

import com.casi.project.system.domain.SysDept;
import com.casi.project.system.service.ISysDeptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.casi.common.enums.UserStatus;
import com.casi.common.exception.BaseException;
import com.casi.common.utils.StringUtils;
import com.casi.framework.security.LoginUser;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.service.ISysUserService;

/**
 * 用户验证处理
 *
 * @author lzb
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService
{
    private static final Logger log = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysDeptService deptService;

    @Autowired
    private SysPermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException
    {
        SysUser user = userService.selectUserByUserName(username);
        if (StringUtils.isNull(user))
        {
            log.info("登录用户：{} 不存在.", username);
            throw new UsernameNotFoundException("登录用户：" + username + " 不存在");
        }
        else if (UserStatus.DELETED.getCode().equals(user.getDelFlag()))
        {
            log.info("登录用户：{} 已被删除.", username);
            throw new BaseException("对不起，您的账号：" + username + " 已被删除");
        }
        else if (UserStatus.DISABLE.getCode().equals(user.getStatus()))
        {
            log.info("登录用户：{} 已被停用.", username);
            throw new BaseException("对不起，您的账号：" + username + " 已停用");
        }

        if (!"1".equals(user.getUserId())){
            SysDept sysDept = deptService.selectDeptById(user.getDeptId());
            if (StringUtils.isNull(sysDept)){
                log.info("登录用户：{} 所在的部门不存在.", username);
                throw new UsernameNotFoundException("登录用户：" + username + " 所在的部门不存在");
            }
            else if ("1".equals(sysDept.getStatus()))
            {
                log.info("登录用户：{} 所在部门已停用.", username);
                throw new BaseException("对不起，您的账号：" + username + " 所在部门已停用");
            }
        }

        return createLoginUser(user);
    }

    public UserDetails createLoginUser(SysUser user)
    {
        return new LoginUser(user, permissionService.getMenuPermission(user));
    }
}
