package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.MobileNotice;
import com.casi.project.portal.domain.vo.NoticeVO;
import org.apache.ibatis.annotations.Param;

import javax.xml.crypto.Data;
import java.util.*;

public interface NoticePcMapper {

    /**
     * 查询公告列表
     *
     * @return
     */
    public ArrayList<NoticeVO> getNoticeList(NoticeVO noticeVO);


    /**
     * 添加公告
     *
     * @param mobileNotice
     * @return
     */
    int addNotice(MobileNotice mobileNotice);

    /**
     * 公告修改数据回显接口
     */
    public NoticeVO getNoticeId(@Param("id") String id);

    /**
     * 审核公告数据回显接口
     */
    public Map<String, Object> getNoticeById(@Param("id") String id);

    /**
     * 根据ID查询公告具体详情
     *
     * @param id
     * @return
     */
    public Map<String, Object> getNoticeId1(@Param("id") String id);


    /**
     * 修改公告
     *
     * @param mobileNotice
     * @return
     */
    int updateNotice(MobileNotice mobileNotice);

    /**
     * 删除公告
     *
     * @param id
     * @return
     */
    int deleteNotice(String id);

    /**
     * 审核公告
     *
     * @param
     * @return
     */
    int checkNoticeStatus(@Param("id") String id, @Param("type") Integer type, @Param("userName") String userName, @Param("pushTime") Date pushTime);

    /**
     * 查询公告是否被审核
     *
     * @param id
     * @return
     */
    List<HashMap<String, Integer>> selectIsAudit(@Param("id") String id);

    /**
     * 发布公告
     *
     * @param id
     * @return
     */
    int publishNoticeStatus(@Param("id") String id, @Param("type") Integer type, @Param("userName") String userName);

    /**
     * 根据公告id查询这条公告
     */
    public Map<String, Object> selectMobileNoticeById(String id);
}
