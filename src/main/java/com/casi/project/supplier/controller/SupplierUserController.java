package com.casi.project.supplier.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.supplier.domain.SupplierUser;
import com.casi.project.supplier.service.ISupplierUserService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 供应商账号表Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"供应商账号表"})
@RestController
@RequestMapping("/supplier/user")
public class SupplierUserController extends BaseController
{
    @Autowired
    private ISupplierUserService supplierUserService;

    /**
     * 查询供应商账号表列表
     */
    @ApiOperation(value = "查询供应商账号表列表")
    @PreAuthorize("@ss.hasPermi('supplier:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(SupplierUser supplierUser)
    {
        startPage();
        List<SupplierUser> list = supplierUserService.selectSupplierUserList(supplierUser);
        return getDataTable(list);
    }

    /**
     * 导出供应商账号表列表
     */
    @PreAuthorize("@ss.hasPermi('supplier:user:export')")
    @Log(title = "供应商账号表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SupplierUser supplierUser)
    {
        List<SupplierUser> list = supplierUserService.selectSupplierUserList(supplierUser);
        ExcelUtil<SupplierUser> util = new ExcelUtil<SupplierUser>(SupplierUser.class);
        return util.exportExcel(list, "user");
    }

    /**
     * 获取供应商账号表详细信息
     */
    @ApiOperation(value = "获取供应商账号表详细信息")
    @PreAuthorize("@ss.hasPermi('supplier:user:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(supplierUserService.selectSupplierUserById(uuid));
    }

    /**
     * 新增供应商账号表
     */
    @ApiOperation(value = "新增供应商账号表")
    @PreAuthorize("@ss.hasPermi('supplier:user:add')")
    @Log(title = "供应商账号表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SupplierUser supplierUser)
    {
        return toAjax(supplierUserService.insertSupplierUser(supplierUser));
    }

    /**
     * 修改供应商账号表
     */
    @ApiOperation(value = "修改供应商账号表")
    @PreAuthorize("@ss.hasPermi('supplier:user:edit')")
    @Log(title = "供应商账号表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SupplierUser supplierUser)
    {
        return toAjax(supplierUserService.updateSupplierUser(supplierUser));
    }

    /**
     * 删除供应商账号表
     */
    @ApiOperation(value = "删除供应商账号表")
    @PreAuthorize("@ss.hasPermi('supplier:user:remove')")
    @Log(title = "供应商账号表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(supplierUserService.deleteSupplierUserByIds(uuids));
    }
}
