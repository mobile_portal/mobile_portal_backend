package com.casi.project.tenant.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.tenant.domain.TenantAppPermission;
import com.casi.project.tenant.service.ITenantAppPermissionService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 租户自定义应用角色权限Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"租户自定义应用角色权限"})
@RestController
@RequestMapping("/tenant/appPermission")
public class TenantAppPermissionController extends BaseController
{
    @Autowired
    private ITenantAppPermissionService tenantAppPermissionService;

    /**
     * 查询租户自定义应用角色权限列表
     */
    @ApiOperation(value = "查询租户自定义应用角色权限列表")
    @PreAuthorize("@ss.hasPermi('tenant:appPermission:list')")
    @GetMapping("/list")
    public TableDataInfo list(TenantAppPermission tenantAppPermission)
    {
        startPage();
        List<TenantAppPermission> list = tenantAppPermissionService.selectTenantAppPermissionList(tenantAppPermission);
        return getDataTable(list);
    }

    /**
     * 导出租户自定义应用角色权限列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:appPermission:export')")
    @Log(title = "租户自定义应用角色权限", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TenantAppPermission tenantAppPermission)
    {
        List<TenantAppPermission> list = tenantAppPermissionService.selectTenantAppPermissionList(tenantAppPermission);
        ExcelUtil<TenantAppPermission> util = new ExcelUtil<TenantAppPermission>(TenantAppPermission.class);
        return util.exportExcel(list, "appPermission");
    }

    /**
     * 获取租户自定义应用角色权限详细信息
     */
    @ApiOperation(value = "获取租户自定义应用角色权限详细信息")
    @PreAuthorize("@ss.hasPermi('tenant:appPermission:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(tenantAppPermissionService.selectTenantAppPermissionById(uuid));
    }

    /**
     * 新增租户自定义应用角色权限
     */
    @ApiOperation(value = "新增租户自定义应用角色权限")
    @PreAuthorize("@ss.hasPermi('tenant:appPermission:add')")
    @Log(title = "租户自定义应用角色权限", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TenantAppPermission tenantAppPermission)
    {
        return toAjax(tenantAppPermissionService.insertTenantAppPermission(tenantAppPermission));
    }

    /**
     * 修改租户自定义应用角色权限
     */
    @ApiOperation(value = "修改租户自定义应用角色权限")
    @PreAuthorize("@ss.hasPermi('tenant:appPermission:edit')")
    @Log(title = "租户自定义应用角色权限", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TenantAppPermission tenantAppPermission)
    {
        return toAjax(tenantAppPermissionService.updateTenantAppPermission(tenantAppPermission));
    }

    /**
     * 删除租户自定义应用角色权限
     */
    @ApiOperation(value = "删除租户自定义应用角色权限")
    @PreAuthorize("@ss.hasPermi('tenant:appPermission:remove')")
    @Log(title = "租户自定义应用角色权限", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(tenantAppPermissionService.deleteTenantAppPermissionByIds(uuids));
    }
}
