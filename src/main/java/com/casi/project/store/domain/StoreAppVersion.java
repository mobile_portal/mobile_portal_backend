package com.casi.project.store.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 应用版本对象 store_app_version
 * 
 * @author casi
 * @date 2020-06-02
 */
public class StoreAppVersion extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private String appId;

    /** 版本 */
    @Excel(name = "版本")
    private String version;

    /** 更新内容 */
    @Excel(name = "更新内容")
    private String updateContent;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setVersion(String version) 
    {
        this.version = version;
    }

    public String getVersion() 
    {
        return version;
    }
    public void setUpdateContent(String updateContent) 
    {
        this.updateContent = updateContent;
    }

    public String getUpdateContent() 
    {
        return updateContent;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("appId", getAppId())
            .append("version", getVersion())
            .append("updateContent", getUpdateContent())
            .append("updateTime", getUpdateTime())
            .append("status", getStatus())
            .toString();
    }
}
