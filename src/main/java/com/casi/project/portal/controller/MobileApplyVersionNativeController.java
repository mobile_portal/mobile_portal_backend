package com.casi.project.portal.controller;

import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.MobileType;
import com.casi.project.portal.domain.param.MobileApplyVersionNativeParam;
import com.casi.project.portal.mapper.MobileTypeMapper;
import com.casi.project.portal.service.MobileApplyVersionNativeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static com.casi.project.portal.util.UnNullCheck.getAjaxResult;

/**
 * @Auther shp
 * @data2020/4/219:57
 * @description
 */


@RestController
@Api(tags = "新增原生应用版相关接口")
@RequestMapping("/portal/mavnc")
public class MobileApplyVersionNativeController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(UpFileController.class);

    @Autowired
    private MobileApplyVersionNativeService service;

    @Autowired
    private MobileTypeMapper mapper;


    @PostMapping("/addAppVersion")
    @ApiOperation("添加原生应用版本")
    public AjaxResult addApp(MobileApplyVersionNativeParam m) throws IllegalAccessException {
        //进行非空校验
        if (getAjaxResult(m) != null) {
            return getAjaxResult(m);
        }
        return service.addApp(m);
    }


    @GetMapping("/findType")
    @ApiOperation("获取系统应用下拉选")
    List<MobileType> findType() {
        Integer softId = 1;
        return mapper.findType(softId);
    }


}
