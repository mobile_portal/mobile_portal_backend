package com.casi.project.tenant.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.tenant.domain.TenantAppResources;
import com.casi.project.tenant.service.ITenantAppResourcesService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 租户应用资源Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"租户应用资源"})
@RestController
@RequestMapping("/tenant/resources")
public class TenantAppResourcesController extends BaseController
{
    @Autowired
    private ITenantAppResourcesService tenantAppResourcesService;

    /**
     * 查询租户应用资源列表
     */
    @ApiOperation(value = "查询租户应用资源列表")
    @PreAuthorize("@ss.hasPermi('tenant:resources:list')")
    @GetMapping("/list")
    public TableDataInfo list(TenantAppResources tenantAppResources)
    {
        startPage();
        List<TenantAppResources> list = tenantAppResourcesService.selectTenantAppResourcesList(tenantAppResources);
        return getDataTable(list);
    }

    /**
     * 导出租户应用资源列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:resources:export')")
    @Log(title = "租户应用资源", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TenantAppResources tenantAppResources)
    {
        List<TenantAppResources> list = tenantAppResourcesService.selectTenantAppResourcesList(tenantAppResources);
        ExcelUtil<TenantAppResources> util = new ExcelUtil<TenantAppResources>(TenantAppResources.class);
        return util.exportExcel(list, "resources");
    }

    /**
     * 获取租户应用资源详细信息
     */
    @ApiOperation(value = "获取租户应用资源详细信息")
    @PreAuthorize("@ss.hasPermi('tenant:resources:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(tenantAppResourcesService.selectTenantAppResourcesById(uuid));
    }

    /**
     * 新增租户应用资源
     */
    @ApiOperation(value = "新增租户应用资源")
    @PreAuthorize("@ss.hasPermi('tenant:resources:add')")
    @Log(title = "租户应用资源", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TenantAppResources tenantAppResources)
    {
        return toAjax(tenantAppResourcesService.insertTenantAppResources(tenantAppResources));
    }

    /**
     * 修改租户应用资源
     */
    @ApiOperation(value = "修改租户应用资源")
    @PreAuthorize("@ss.hasPermi('tenant:resources:edit')")
    @Log(title = "租户应用资源", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TenantAppResources tenantAppResources)
    {
        return toAjax(tenantAppResourcesService.updateTenantAppResources(tenantAppResources));
    }

    /**
     * 删除租户应用资源
     */
    @ApiOperation(value = "删除租户应用资源")
    @PreAuthorize("@ss.hasPermi('tenant:resources:remove')")
    @Log(title = "租户应用资源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(tenantAppResourcesService.deleteTenantAppResourcesByIds(uuids));
    }
}
