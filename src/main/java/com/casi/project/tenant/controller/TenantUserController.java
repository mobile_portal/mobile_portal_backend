package com.casi.project.tenant.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.tenant.domain.TenantUser;
import com.casi.project.tenant.service.ITenantUserService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 租户账号Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"租户账号"})
@RestController
@RequestMapping("/tenant/user")
public class TenantUserController extends BaseController
{
    @Autowired
    private ITenantUserService tenantUserService;

    /**
     * 查询租户账号列表
     */
    @ApiOperation(value = "查询租户账号列表")
    @PreAuthorize("@ss.hasPermi('tenant:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(TenantUser tenantUser)
    {
        startPage();
        List<TenantUser> list = tenantUserService.selectTenantUserList(tenantUser);
        return getDataTable(list);
    }

    /**
     * 导出租户账号列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:user:export')")
    @Log(title = "租户账号", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TenantUser tenantUser)
    {
        List<TenantUser> list = tenantUserService.selectTenantUserList(tenantUser);
        ExcelUtil<TenantUser> util = new ExcelUtil<TenantUser>(TenantUser.class);
        return util.exportExcel(list, "user");
    }

    /**
     * 获取租户账号详细信息
     */
    @ApiOperation(value = "获取租户账号详细信息")
    @PreAuthorize("@ss.hasPermi('tenant:user:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(tenantUserService.selectTenantUserById(uuid));
    }

    /**
     * 新增租户账号
     */
    @ApiOperation(value = "新增租户账号")
    @PreAuthorize("@ss.hasPermi('tenant:user:add')")
    @Log(title = "租户账号", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TenantUser tenantUser)
    {
        return toAjax(tenantUserService.insertTenantUser(tenantUser));
    }

    /**
     * 修改租户账号
     */
    @ApiOperation(value = "修改租户账号")
    @PreAuthorize("@ss.hasPermi('tenant:user:edit')")
    @Log(title = "租户账号", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TenantUser tenantUser)
    {
        return toAjax(tenantUserService.updateTenantUser(tenantUser));
    }

    /**
     * 删除租户账号
     */
    @ApiOperation(value = "删除租户账号")
    @PreAuthorize("@ss.hasPermi('tenant:user:remove')")
    @Log(title = "租户账号", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(tenantUserService.deleteTenantUserByIds(uuids));
    }
}
