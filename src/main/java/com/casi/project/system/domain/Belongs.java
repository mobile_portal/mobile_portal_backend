package com.casi.project.system.domain;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class Belongs {
    private String ouDirectory;
    private String belongOuUuid;
    private String rootNode;

    public String getRootNode() {
        return rootNode;
    }

    public void setRootNode(String rootNode) {
        this.rootNode = rootNode;
    }

    public String getBelongOuUuid() {
        return belongOuUuid;
    }

    public void setBelongOuUuid(String belongOuUuid) {
        this.belongOuUuid = belongOuUuid;
    }

    public String getOuDirectory() {
        return ouDirectory;
    }

    public void setOuDirectory(String ouDirectory) {
        this.ouDirectory = ouDirectory;
    }

    @Override
    public String toString() {
        return "Belongs{" +
                "ouDirectory='" + ouDirectory + '\'' +
                ", belongOuUuid='" + belongOuUuid + '\'' +
                ", rootNode='" + rootNode + '\'' +
                '}';
    }
}
