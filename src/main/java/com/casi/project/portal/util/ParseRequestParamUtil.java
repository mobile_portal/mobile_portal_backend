package com.casi.project.portal.util;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

public class ParseRequestParamUtil {

	/*
	 * 将请求的参数转换为Map
	 **/
	public static Map<String,Object> parseRequestParam(HttpServletRequest request){
		Map<String,Object> paramMap=new HashMap<String,Object>();
		Enumeration<String> paramNames=request.getParameterNames();
		while(paramNames.hasMoreElements()){
			String key=paramNames.nextElement();
			String value=request.getParameter(key);
			paramMap.put(key,value);
		}
		return paramMap;
	}
}
