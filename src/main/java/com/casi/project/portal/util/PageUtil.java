package com.casi.project.portal.util;

import java.util.Map;

public class PageUtil {
    public static Map<String, Object> getPageParam(Map<String, Object> param) {
        int page = Integer.parseInt(param.get("page").toString());
        int rows = Integer.parseInt(param.get("rows").toString());
        int start = (page - 1) * rows + 1;
        int end = start + rows - 1;
        param.put("start", start);
        param.put("end", end);
        return param;
    }
}
