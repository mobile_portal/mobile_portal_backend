package com.casi.project.portal.service;


import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.ApplyDetails;
import com.casi.project.portal.domain.ApplyVersion;
import com.casi.project.portal.domain.UserAndRole;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 应用明细接口等
 *
 * @author 18732
 */
public interface ApplyDetailsService {

    /**
     * 新增应用具体详细
     *
     * @param applyDetails
     * @return
     */
    AjaxResult addApplyDetails(ApplyDetails applyDetails);

    /**
     * 回显应用明细
     *
     * @param id
     * @return
     */
    AjaxResult findApplyDetailsId(String id);

    /**
     * 修改应用明细
     *
     * @param applyDetails
     * @return
     */
    AjaxResult updateApplyDetails(ApplyDetails applyDetails);

    /**
     * 删除应用明细
     *
     * @param id
     * @return
     */
    AjaxResult deleteApplyDetails(String id);

    /**
     * 禁用启用应用明细
     *
     * @param id
     * @param type
     * @return
     */
    AjaxResult disableEnabledDetails(String[] id, Integer type);

    /**
     * 查询应用管理列表和条件模糊查询
     *
     * @param menuButtonName
     * @return
     */
    List<Map<String, Object>> getApplyList(@Param("menuButtonName") String menuButtonName);

    /**
     * 查询指定人员的应用列表
     *
     * @param menuButtonName
     * @param applyId
     * @return
     */
    Map<String, Object> getApplyListId(@Param("menuButtonName") String menuButtonName, @Param("applyId") String applyId);

    /**
     * 审核应用
     *
     * @param id
     * @param type
     * @return
     */
    AjaxResult checkApplyStatus(String id, Integer type);

    /**
     * 上线应用
     *
     * @param id
     * @return
     */
    AjaxResult upLineApply(String id);

    /**
     * 新增轻应用服务号版本
     *
     * @param applyVersion
     * @return
     */
    public AjaxResult addApplyVersion(ApplyVersion applyVersion);

    /**
     * 应用指定开发者账户——应用列表展示
     */
    public AjaxResult queryAllApply(Integer pageNum, Integer pageSize);


    /**
     * 指定应用开发管理员
     *
     * @param
     * @param
     * @return
     */
    AjaxResult addApplyDevelopAdminIsTraTor(UserAndRole userAndRole);

    /**
     * 查询部门和应用的并集
     *
     * @param userId
     * @param deptId
     * @return
     */
    List<HashMap<String, Object>> getApplyId(String userId, String deptId);

    /**
     * 查询用户是否在黑名单中
     *
     * @param userId
     * @param applyId
     * @return
     */
    int selectIsBlackList(String userId, String applyId);

    /**
     * 根据应用appKey和appSecret获得应用的资源
     *
     * @param hashMap
     * @return
     */
    AjaxResult getApplyResource(HashMap<String, Object> hashMap);

    /**
     * 根据应用appKey和appSecret获得应用的用户
     *
     * @param hashMap
     * @return
     */
    AjaxResult getApplyUser(HashMap<String, Object> hashMap);


    /**
     * 根据应用appKey，appSecret,token获得用户的用户权限
     *
     * @param hashMap
     * @return
     */
    AjaxResult getApplyUserPermission(HashMap<String, Object> hashMap);

    /**
     * 根据应用appKey，appSecret 获得应用的所有角色
     */
    public AjaxResult queryRolesByAppKeyAndAppSecret(ApplyDetails applyDetails);

    /**
     * 根据应用appKey，appSecret   获得应用的所有组织机构
     */
    public AjaxResult queryDeptsByAppKeyAndAppSecret(ApplyDetails applyDetails);

    /**
     * 根据应用appKey，appSecret   和用户token获得用户的角色
     */
    public AjaxResult queryRoleByAppKeyAndAppSecretAndToken(ApplyDetails applyDetails);


}
