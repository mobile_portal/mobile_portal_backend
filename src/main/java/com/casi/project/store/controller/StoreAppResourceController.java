package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreAppResource;
import com.casi.project.store.service.IStoreAppResourceService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用资源Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用资源"})
@RestController
@RequestMapping("/store/resource")
public class StoreAppResourceController extends BaseController
{
    @Autowired
    private IStoreAppResourceService storeAppResourceService;

    /**
     * 查询应用资源列表
     */
    @ApiOperation(value = "查询应用资源列表")
    @PreAuthorize("@ss.hasPermi('store:resource:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreAppResource storeAppResource)
    {
        startPage();
        List<StoreAppResource> list = storeAppResourceService.selectStoreAppResourceList(storeAppResource);
        return getDataTable(list);
    }

    /**
     * 导出应用资源列表
     */
    @PreAuthorize("@ss.hasPermi('store:resource:export')")
    @Log(title = "应用资源", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreAppResource storeAppResource)
    {
        List<StoreAppResource> list = storeAppResourceService.selectStoreAppResourceList(storeAppResource);
        ExcelUtil<StoreAppResource> util = new ExcelUtil<StoreAppResource>(StoreAppResource.class);
        return util.exportExcel(list, "resource");
    }

    /**
     * 获取应用资源详细信息
     */
    @ApiOperation(value = "获取应用资源详细信息")
    @PreAuthorize("@ss.hasPermi('store:resource:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppResourceService.selectStoreAppResourceById(uuid));
    }

    /**
     * 新增应用资源
     */
    @ApiOperation(value = "新增应用资源")
    @PreAuthorize("@ss.hasPermi('store:resource:add')")
    @Log(title = "应用资源", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreAppResource storeAppResource)
    {
        return toAjax(storeAppResourceService.insertStoreAppResource(storeAppResource));
    }

    /**
     * 修改应用资源
     */
    @ApiOperation(value = "修改应用资源")
    @PreAuthorize("@ss.hasPermi('store:resource:edit')")
    @Log(title = "应用资源", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreAppResource storeAppResource)
    {
        return toAjax(storeAppResourceService.updateStoreAppResource(storeAppResource));
    }

    /**
     * 删除应用资源
     */
    @ApiOperation(value = "删除应用资源")
    @PreAuthorize("@ss.hasPermi('store:resource:remove')")
    @Log(title = "应用资源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppResourceService.deleteStoreAppResourceByIds(uuids));
    }
}
