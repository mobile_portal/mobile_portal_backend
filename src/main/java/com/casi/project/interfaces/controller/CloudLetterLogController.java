package com.casi.project.interfaces.controller;

import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.controller.NoticeController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.HashMap;

/**
 * @ClassName CloudLetterLogController
 * @Description:云信日志接口
 * @Author:ghk
 * @Date 2020/5/21 14:38
 * @Version 1.0
 **/
@SuppressWarnings("all")
@RestController
@RequestMapping("/cloudLog")
public class CloudLetterLogController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);

//
//    @Value("${cloudlog.url.informationUrl}")
//    private String informationUrl;
//
//    @Value("${cloudlog.url.noticeUrl}")
//    private String noticeUrl;

    /**
     * @author ghk
     * @Description:获取资讯信息
     * @date 2020/5/21 15:26
     */
    @GetMapping("/getInformation")
    public AjaxResult getInformation() throws IOException, GeneralSecurityException {
//        /**
//         * 构造器，创建sdk对象，首次使用初始化
//         *
//         * @param url        本地部署航天云信openapi服务基地址
//         */
//        WeWorkLocalSdk weWorkLocalSdk = new WeWorkLocalSdk(informationUrl);

//        /**
//         * 初始化函数
//         *
//         * @param  corpId     企业ID，格式如：wl33fd99d5c5，可以在航天云信管理站点--我的单位--单位信息查看
//         * @param  secret     日志及数据导出应用的Secret，可以在航天云信管理站点--管理工具--日志及数据导出查看
//         */
//        weWorkLocalSdk.init("wl0698e4442a", "SgolXb2QiFDOl-npStjH4GkWzLvOltNLSEYN-o0KQcg");
//        /**
//         * 设置解密日志RSA私钥文件路径，仅当有调用getLogList时，需先调用此接口
//         * @param rsaPriKeyFile    RSA私钥文件存储绝对路径，确保程序有读权限
//         * @throws IOException 失败（比如文件不存在，文件内容非法等）时抛出
//         */
//        weWorkLocalSdk.setRsaPrivateKeyPath();

//        /**
//         * 分页拉取业务日志
//         *
//         * @param  featureId    业务数据ID，参看前述定义
//         * @param  startTime    起始时间，时间戳，拉取这个时间（包含）之后的日志数据
//         * @param  endTime        结束时间，时间戳，拉取截止到这个时间（包含）的日志数据，且必须和start_time在同一天
//         * @param  startIndex    分页拉取的起始位置，首次拉取为0
//         * @param  limit        单次拉取日志最大条数限制，最大值不超过1000
//         *
//         * @return 日志数据列表
//         * @throws WeWorkLocalException,IOException,GeneralSecurityException 获取日志失败（请求服务器失败、解密失败）时抛出
//         */
//        ArrayList<LogInfo> logList = weWorkLocalSdk.getLogList(90000034, 2019 - 10, 2020 - 5, 0, 1);
        return AjaxResult.success("调用成功", new HashMap<>());
    }

    /**
     * @author ghk
     * @Description:获取公告信息
     * @date 2020/5/21 15:26
     */
    @GetMapping("/getNotice")
    public AjaxResult getNotice() {

        return AjaxResult.success("调用成功", new HashMap<>());
    }
}
