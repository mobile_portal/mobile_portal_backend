package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreAppTenant;
import com.casi.project.store.service.IStoreAppTenantService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用租户授权Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用租户授权"})
@RestController
@RequestMapping("/store/tenant")
public class StoreAppTenantController extends BaseController
{
    @Autowired
    private IStoreAppTenantService storeAppTenantService;

    /**
     * 查询应用租户授权列表
     */
    @ApiOperation(value = "查询应用租户授权列表")
    @PreAuthorize("@ss.hasPermi('store:tenant:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreAppTenant storeAppTenant)
    {
        startPage();
        List<StoreAppTenant> list = storeAppTenantService.selectStoreAppTenantList(storeAppTenant);
        return getDataTable(list);
    }

    /**
     * 导出应用租户授权列表
     */
    @PreAuthorize("@ss.hasPermi('store:tenant:export')")
    @Log(title = "应用租户授权", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreAppTenant storeAppTenant)
    {
        List<StoreAppTenant> list = storeAppTenantService.selectStoreAppTenantList(storeAppTenant);
        ExcelUtil<StoreAppTenant> util = new ExcelUtil<StoreAppTenant>(StoreAppTenant.class);
        return util.exportExcel(list, "tenant");
    }

    /**
     * 获取应用租户授权详细信息
     */
    @ApiOperation(value = "获取应用租户授权详细信息")
    @PreAuthorize("@ss.hasPermi('store:tenant:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppTenantService.selectStoreAppTenantById(uuid));
    }

    /**
     * 新增应用租户授权
     */
    @ApiOperation(value = "新增应用租户授权")
    @PreAuthorize("@ss.hasPermi('store:tenant:add')")
    @Log(title = "应用租户授权", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreAppTenant storeAppTenant)
    {
        return toAjax(storeAppTenantService.insertStoreAppTenant(storeAppTenant));
    }

    /**
     * 修改应用租户授权
     */
    @ApiOperation(value = "修改应用租户授权")
    @PreAuthorize("@ss.hasPermi('store:tenant:edit')")
    @Log(title = "应用租户授权", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreAppTenant storeAppTenant)
    {
        return toAjax(storeAppTenantService.updateStoreAppTenant(storeAppTenant));
    }

    /**
     * 删除应用租户授权
     */
    @ApiOperation(value = "删除应用租户授权")
    @PreAuthorize("@ss.hasPermi('store:tenant:remove')")
    @Log(title = "应用租户授权", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppTenantService.deleteStoreAppTenantByIds(uuids));
    }
}
