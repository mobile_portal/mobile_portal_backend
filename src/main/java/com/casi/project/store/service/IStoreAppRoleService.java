package com.casi.project.store.service;

import java.util.List;
import com.casi.project.store.domain.StoreAppRole;

/**
 * 应用角色Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface IStoreAppRoleService 
{
    /**
     * 查询应用角色
     * 
     * @param uuid 应用角色ID
     * @return 应用角色
     */
    public StoreAppRole selectStoreAppRoleById(String uuid);

    /**
     * 查询应用角色列表
     * 
     * @param storeAppRole 应用角色
     * @return 应用角色集合
     */
    public List<StoreAppRole> selectStoreAppRoleList(StoreAppRole storeAppRole);

    /**
     * 新增应用角色
     * 
     * @param storeAppRole 应用角色
     * @return 结果
     */
    public int insertStoreAppRole(StoreAppRole storeAppRole);

    /**
     * 修改应用角色
     * 
     * @param storeAppRole 应用角色
     * @return 结果
     */
    public int updateStoreAppRole(StoreAppRole storeAppRole);

    /**
     * 批量删除应用角色
     * 
     * @param uuids 需要删除的应用角色ID
     * @return 结果
     */
    public int deleteStoreAppRoleByIds(String[] uuids);

    /**
     * 删除应用角色信息
     * 
     * @param uuid 应用角色ID
     * @return 结果
     */
    public int deleteStoreAppRoleById(String uuid);
}
