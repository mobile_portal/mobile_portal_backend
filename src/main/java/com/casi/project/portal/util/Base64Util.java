package com.casi.project.portal.util;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;

public class Base64Util {
	//失之毫厘，谬之千里
	private static final String SIGN_KEY = "LZFGW";
    private static final Logger log = LoggerFactory.getLogger(Base64Util.class);
    /**
     * 方法名称: encode<br>
     * 描述：base64加密(无秘钥)<br>
     * 作者: dingjianwei<br>
     * 修改日期：2017年10月25日下午4:41:57<br>
     * @param data
     * @return
     */
    public static String encode (String data) {
    	String dataBase64=null;
        try {
            byte[] dataBytes = Base64.encodeBase64(data.getBytes("UTF-8"));

            dataBase64 = new String(dataBytes, "UTF-8");
            if (dataBase64 != null) {
                dataBase64 = dataBase64.trim();
            }
        } catch (UnsupportedEncodingException e) {
            log.info("Base64Util.encode UnsupportedEncodingException", e);
        }
        return dataBase64;

    }
    /**
     * 方法名称: decode<br>
     * 描述：base64解密(无秘钥)<br>
     * 作者: dingjianwei<br>
     * 修改日期：2017年10月25日下午4:41:57<br>
     * @param data
     * @return
     */
    public static String decode (String data) {
    	String dataStr=null;
        try {
            byte[] dataBytes = Base64.decodeBase64(data.getBytes("UTF-8"));

            dataStr = new String(dataBytes, "UTF-8");

          
        } catch (UnsupportedEncodingException e) {
            log.info("Base64Util.decode UnsupportedEncodingException", e);
        }
        return dataStr;
    }
    
    
    /**
     * 方法名称: encodeByKey<br>
     * 描述：base64加密(根据秘钥)<br>
     * 作者: dingjianwei<br>
     * 修改日期：2017年10月25日下午4:41:57<br>
     * @param data
     * @return
     */
    public static String encodeByKey (String data) {
        return encode(data+SIGN_KEY);

    }
    /**
     * 方法名称: decodeBykey<br>
     * 描述：base64解密(根据秘钥)<br>
     * 作者: dingjianwei<br>
     * 修改日期：2017年10月25日下午4:41:57<br>
     * @param data
     * @return
     */
    public static String decodeByKey(String data){
    	String strall = decode(data);
    	if(strall!=null&&strall.trim().length()>0){
    		return strall.replace(SIGN_KEY, "");
    	} 
    	return "";
    }
    
    /**
     * 方法名称: encodePassword<br>
     * 描述：加密密码<br>
     * 作者: dingjianwei<br>
     * 修改日期：2017年10月25日下午4:41:57<br>
     * @param data
     * @return
     */
    public static String encodePassword(String password){
    	return encodeByKey(EncryptionUtils.getMD5(password));
    }
   /* public static void main(String[] args) {
		System.out.println(encodePassword("123456"));
	}*/
}
