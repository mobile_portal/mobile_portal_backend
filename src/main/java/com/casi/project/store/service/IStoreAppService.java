package com.casi.project.store.service;

import java.util.List;
import com.casi.project.store.domain.StoreApp;

/**
 * 应用详情Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface IStoreAppService 
{
    /**
     * 查询应用详情
     * 
     * @param uuid 应用详情ID
     * @return 应用详情
     */
    public StoreApp selectStoreAppById(String uuid);

    /**
     * 查询应用详情列表
     * 
     * @param storeApp 应用详情
     * @return 应用详情集合
     */
    public List<StoreApp> selectStoreAppList(StoreApp storeApp);

    /**
     * 新增应用详情
     * 
     * @param storeApp 应用详情
     * @return 结果
     */
    public int insertStoreApp(StoreApp storeApp);

    /**
     * 修改应用详情
     * 
     * @param storeApp 应用详情
     * @return 结果
     */
    public int updateStoreApp(StoreApp storeApp);

    /**
     * 批量删除应用详情
     * 
     * @param uuids 需要删除的应用详情ID
     * @return 结果
     */
    public int deleteStoreAppByIds(String[] uuids);

    /**
     * 删除应用详情信息
     * 
     * @param uuid 应用详情ID
     * @return 结果
     */
    public int deleteStoreAppById(String uuid);
}
