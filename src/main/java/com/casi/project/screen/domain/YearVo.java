package com.casi.project.screen.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther shp
 * @data2020/5/815:53
 * @description
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
public class YearVo {

    private String name;

    private String staus;

    private Double size;

}
