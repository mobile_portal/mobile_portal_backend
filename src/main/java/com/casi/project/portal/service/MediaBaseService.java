package com.casi.project.portal.service;

import com.casi.project.portal.domain.MediaBase;

/**
 * @Description:
 * @Param:
 * @Return:
 */
public interface MediaBaseService {
    /**
     * @Description:更新base类信息
     */
    boolean updateBase(MediaBase base);
}
