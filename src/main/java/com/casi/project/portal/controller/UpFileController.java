package com.casi.project.portal.controller;

import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.service.UpFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Auther shp
 * @data2020/4/1610:09
 * @description
 */
@RestController
@RequestMapping("/upfile")
@CrossOrigin
@Api(tags = "文件上传相关接口")
public class UpFileController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(UpFileController.class);

    @Autowired
    private UpFileService service;

    @PostMapping("/uploadFile")
    @ApiOperation("支持前端上传文件")
    public AjaxResult uploadFile(@RequestParam("file") MultipartFile file) {
        return service.uploadFile(file);
    }


    @PostMapping("/newuploadFile")
    @ApiOperation("支持前端上传文件（新闻专用接口）")
    public AjaxResult newsUploadFile(@RequestParam("file") MultipartFile file) {
        return service.newUploadFile(file);
    }


}

