package com.casi.project.portal.service.impl;

import com.casi.common.utils.SecurityUtils;
import com.casi.project.portal.domain.ApplyDetails;
import com.casi.project.portal.domain.MobileApplyResource;
import com.casi.project.portal.mapper.ApplyDetailsMapper;
import com.casi.project.portal.mapper.ApplyResourcesMapper;
import com.casi.project.portal.service.ApplyResourcesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @author css
 * @date 2020/5/8 17:08
 */
@Service
public class ApplyResourcesServiceImpl implements ApplyResourcesService {

    @Autowired
    private ApplyResourcesMapper applyResourcesMapper;

    @Autowired
    private ApplyDetailsMapper applyDetailsMapper;

    @Override
    public List<MobileApplyResource> selectMenuList(String applyId) {


        List<MobileApplyResource> mobileList = new ArrayList<>();
        List<MobileApplyResource> mobileApplyList = new ArrayList<>();

            mobileApplyList = applyResourcesMapper.selectApplyList(applyId);
            for (MobileApplyResource mobileApplyResource : mobileApplyList) {
                mobileList.add(mobileApplyResource);
            }

        return mobileList;

    }

    /**
     * 新增应用资源
     */
    @Transactional
    @Override
    public int insertResource(MobileApplyResource mobileApplyResource) {
        return applyResourcesMapper.insertResource(mobileApplyResource);

    }

    /**
     * 修改应用资源
     */
    @Override
    public int updateResource(MobileApplyResource mobileApplyResource) {
        return applyResourcesMapper.updateResource(mobileApplyResource);
    }

    /**
     * 删除应用资源
     */
    @Override
    public int deleteMenuById(String mobileApplyId) {
        return applyResourcesMapper.deleteMenuById(mobileApplyId);
    }

    /**
     * 查询用户的应用角色
     */
    @Override
    public String[] selectRoleList(String userId) {
        return applyResourcesMapper.selectRoleList(userId);
    }


    private Map<String, List<MobileApplyResource>> menuList(List<MobileApplyResource> mobileApplyResourceList) {

        Map<String, List<MobileApplyResource>> map = new HashMap<>();
        List<MobileApplyResource> applyResourcesList = new ArrayList<>();
        for (MobileApplyResource mobileApplyResource : mobileApplyResourceList) {
            ApplyDetails applyDetails = applyDetailsMapper.findApplyDetailsId(mobileApplyResource.getMobileApplyId());
            if (applyDetails != null){
                map.put(applyDetails.getMenuButtonName(), new ArrayList<>());
            }
        }
        Set<String> applyDetailsNames = map.keySet();
        for (String applyDetailsName : applyDetailsNames) {
            for (int i = 0; i < mobileApplyResourceList.size(); i++) {
                ApplyDetails applyDetails = applyDetailsMapper.findApplyDetailsId(mobileApplyResourceList.get(i).getMobileApplyId());
                if (applyDetailsName.equals(applyDetails.getMenuButtonName())) {
                    applyResourcesList.add(mobileApplyResourceList.get(i));
                }
                if (i == mobileApplyResourceList.size() - 1) {
                    map.put(applyDetailsName, applyResourcesList);
                }
            }
            applyResourcesList = new ArrayList<>();
        }
        return map;
    }
}
