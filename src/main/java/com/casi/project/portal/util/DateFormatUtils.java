package com.casi.project.portal.util;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 日期工具类
 * @name:DateTransUtils.class
 * @description:
 * @author:丁冬
 * @date:2016年11月17日 下午4:18:24
 * @location:cn.com.casic.util.DateTransUtils.class
 * Copyright 2015 by casic.com. All Right Reserved
 */
public class DateFormatUtils {
	
	//对应时间的时间戳2099-12-31 23:59:59.0
	public static final long TIMEMILLIS_20991231 = 4102415999000l;
	//对应时间的时间戳8888-01-01 00:00:00.0
	public static final long TIMEMILLIS_88880101 = 218310998400000l;
	
	/**
	 * 字符串转util日期格式，日期和格式化格式保持一致
	 * @title: str2UtilDate  
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月17日 下午4:18:35  
	 * @param time
	 * @param format
	 * @return
	 */
	public static Date str2UtilDate(String time,String format){
		String timeNew = null;
		String formatNew = null;
		if(-1==time.indexOf(":")){
			timeNew = time+" 00:00:01";
			formatNew = format +" HH:mm:ss";
		}else{
			timeNew = time;
			formatNew = format;
		}

		SimpleDateFormat sdf = new SimpleDateFormat(formatNew);
		Date date = null;
		try {
			date = sdf.parse(timeNew);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	/**
	 * 字符串转SQL日期格式，字符串日期与格式化格式保持一致
	 * @title: str2SqlDate
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月17日 下午4:19:17
	 * @param time
	 * @param format
	 * @return
	 */
	public static java.sql.Date str2SqlDate(String time,String format){
		Date utilDate = str2UtilDate(time,format);
		java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
		return sqlDate;
	}

	/**
	 * SQL日期转字符串
	 * @title: date2Str
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月17日 下午4:20:05
	 * @param date
	 * @param format
	 * @return
	 */
	public static String date2Str(java.sql.Date date,String format){
		if(date==null) return null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	/**
	 * util日期转字符串
	 * @title: date2Str
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月17日 下午4:20:27
	 * @param date
	 * @param format
	 * @return
	 */
	public static String date2Str(Date date, String format){
		if(date==null) return null;
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(date);
	}

	/**
	 * 获取当前sql时间
	 * @title: sqlNow
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月20日 下午4:48:19
	 * @return
	 */
	public static java.sql.Date sqlNow(){
		return new java.sql.Date(System.currentTimeMillis());
	}
	/**
	 * 获取当前毫秒值
	 * @return
	 */
	public static long currentTimeMillis(){
		return System.currentTimeMillis();
	}

	/**
	 * 获取当前util时间
	 * @title: utilNow
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月20日 下午4:48:38
	 * @return
	 */
	public static Date utilNow(){
		return new Date();
	}

	/**
	 * 在util日期基础上增加或减少天数
	 * @title: modifyUtilDate
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月20日 下午4:46:52
	 * @param date
	 * @param days
	 * @return
	 */
	public static Date modifyUtilDate(Date date, Integer days){
		Calendar calendar =new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(calendar.DATE, days);
		// calendar的time转成java.util.Date格式日期
		Date utilDate = (Date)calendar.getTime();
		return utilDate;
	}

	/**
	 * 在sql日期基础上新增或减少天数
	 * @title: modifySqlDate
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月20日 下午4:47:17
	 * @param date
	 * @param days
	 * @return
	 */
	public static java.sql.Date modifySqlDate(java.sql.Date date,Integer days){
		Calendar calendar =new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(calendar.DATE, days);
		// calendar的time转成java.util.Date格式日期
		Date utilDate = (Date)calendar.getTime();
		//java.util.Date日期转换成转成java.sql.Date格式
		java.sql.Date newDate =new java.sql.Date(utilDate.getTime());
		return newDate;
	}

	/**
	 * 获取当前timestamp时间
	 * @title: timeStampNow
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月22日 下午7:46:34
	 * @return
	 */
	public static Timestamp timeStampNow(){
		Timestamp time = new Timestamp(System.currentTimeMillis());
		return time;
	}

	/**
	 * 对给定的timstamp日期进行天数的增加或减少
	 * @title: modifyTimestamp
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月22日 下午7:46:49
	 * @param date
	 * @param days
	 * @return
	 */
	public static Timestamp modifyTimestamp(Timestamp date,long days){
		Timestamp time = new Timestamp(date.getTime()+days*24*60*60*1000);
		return time;
	}

	public static Timestamp timeStampDateD(long days){
		Timestamp time = new Timestamp(System.currentTimeMillis()+days*24*60*60*1000);
		return time;
	}

	public static Timestamp str2Timestamp(String time){
		Date utilDate = null;
		if(time.indexOf("/")!=-1){
			utilDate = str2UtilDate(time, "yyyy/MM/dd");
		}else if(time.indexOf("-")!=-1){
			utilDate = str2UtilDate(time, "yyyy-MM-dd");
		}else{
			utilDate = new Date();
		}
		return new Timestamp(utilDate.getTime());
	}

	public static String timestamp2Str(Timestamp time){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		return sdf.format(time);
	}

	/**
	 *
	 * @description: 根据时间戳获取日期
	 * @author: DingJianwei
	 * @date:2019年5月27日 下午1:53:54
	 * @param： @param timestamp
	 * @param： @return
	 * @return Date
	 * @throws
	 */
	public static Date getDateFromLong(long timestamp){
		Timestamp time = new Timestamp(timestamp);
		return (Date) time;
	}

	/**
	 *
	 * @description: 根据时间戳和日期格式获取最终展现字符串
	 * @author: DingJianwei
	 * @date:2019年5月27日 下午1:56:13
	 * @param： @param timestamp
	 * @param： @param format
	 * @param： @return
	 * @return String
	 * @throws
	 */
	public static String getDateFromLongByFormat(long timestamp,String format){
		Timestamp time = new Timestamp(timestamp);
		//SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(time);
	}
	/**
	 *
	 * @description: 获取date日期的时间戳数据
	 * @author: DingJianwei
	 * @date:2019年5月27日 下午1:53:28
	 * @param： @param date
	 * @param： @return
	 * @return long
	 * @throws
	 */
	public static long timestamp2Str(Date date){
		return (new Timestamp(date.getTime())).getTime();
	}

	/**
	 * 国际标准时间转化为北京时间
	 * @param UTCStr 国际标准时间
	 * @param format 日期格式
	 * @return
	 */
	public static String UTCToCST(String UTCStr, String format){
		String CSTStr = "";
		try {
			Date date = null;
	        SimpleDateFormat sdf = new SimpleDateFormat(format);
	        date = sdf.parse(UTCStr);
	        //System.out.println("UTC时间: " + date);
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) + 8);
	        //calendar.getTime() 返回的是Date类型，也可以使用calendar.getTimeInMillis()获取时间戳
	        CSTStr = getDateFromLongByFormat(calendar.getTimeInMillis(),"yyyy-MM-dd HH:mm:ss");
		} catch (Exception e) {
			e.printStackTrace();
		}
        return CSTStr;
    }

	/**
	 * 北京时间转化为国际标准时间
	 * @param UTCStr 国际标准时间
	 * @param format 日期格式
	 * @return
	 */
	public static String CSTToUTC(String CSTStr, String format){
		String UTCStr = "";
		try {
			Date date = null;
	        SimpleDateFormat sdf = new SimpleDateFormat(format);
	        date = sdf.parse(CSTStr);
	        //System.out.println("UTC时间: " + date);
	        Calendar calendar = Calendar.getInstance();
	        calendar.setTime(date);
	        calendar.set(Calendar.HOUR, calendar.get(Calendar.HOUR) - 8);
	        //calendar.getTime() 返回的是Date类型，也可以使用calendar.getTimeInMillis()获取时间戳
	        UTCStr = getDateFromLongByFormat(calendar.getTimeInMillis(),"yyyy-MM-dd'T'HH:mm:ss'Z'");
		} catch (Exception e) {
			e.printStackTrace();
		}
        return UTCStr;
    }

	/**
	 * 获取日期的年月日时分的格式
	 * @title: getDateYMDHM
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年11月17日 下午4:18:35
	 * @param time
	 * @param format
	 * @return
	 */
	public static Date getDateYMDHM(String data){
		String format = "yyyy-MM-dd HH:mm";
		Date date = str2UtilDate(data, format);
		return date;
	}
	/**
	 * 判断输入的日期与当前日期的大小(小于为-1，大于为1，等于为0)
	 * @param data
	 * @return
	 */
	public static int isAfterOrBeforCurrent(String data){
		Date input = getDateYMDHM(data);
		Date current = new Date();
		return input.compareTo(current);
	}

	/**
	 * 判断两个输入的日期与当前日期的大小(小于为-1，大于为1，等于为0)
	 * @param data
	 * @return
	 */
	public static int compareBetween(String data1,String data2){
		Date input1 = getDateYMDHM(data1);
		Date input2 = getDateYMDHM(data2);
		return input1.compareTo(input2);
	}
	
	/**
	 * 获取某年某月有多少天
	 * @param year
	 * @param month
	 * @return
	 */
	public static  int getDayOfMonth(int year,int month){
		Calendar c = Calendar.getInstance();
		c.set(year, month, 0); //输入类型为int类型
		return c.get(Calendar.DAY_OF_MONTH);
	}
	
	/**
	 * 查询某一天的后几天的日期
	 * @param day
	 * @param year
	 * @param month
	 * @param day
	 * @param hour
	 * @param minute
	 * @param second
	 * @param format
	 * @return
	 */
	public static String getAfterDaysDate(
			int afterDays, 
			int year, 
			int month, 
			int day, 
			int hour,
			int minute,
			int second,
			String format) {
		 Calendar calendar = Calendar.getInstance();
		 calendar.set(year, month-1, day, hour, minute, second);
		 SimpleDateFormat sdf = new SimpleDateFormat(format);
		 calendar.add(Calendar.DATE, afterDays);
		 String daysAfter = sdf.format(calendar.getTime());
		 return daysAfter;
	}
	
	/*
	public static void main(String[] args) {
		//System.out.println(UTCToCST("2019-07-24T03:16:03.944Z", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
		//System.out.println(CSTToUTC("2019-07-24T03:10:03.944Z", "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"));
		System.out.println(isAfterOrBeforCurrent("2019-08-28 10:25"));
	}*/
}
