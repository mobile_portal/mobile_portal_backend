package com.casi.project.system.mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.casi.project.system.domain.SysRole;
import com.casi.project.system.domain.SysUser;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 角色表 数据层
 *
 * @author lzb
 */
@Repository
public interface SysRoleMapper {
    /**
     * 根据条件分页查询角色数据
     *
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    public List<SysRole> selectRoleList(SysRole role);

    /**
     * 根据用户ID查询角色
     *
     * @param userId 用户ID
     * @return 角色列表
     */
    public List<SysRole> selectRolePermissionByUserId(String userId);

    /**
     * 查询所有系统角色
     *
     * @return 角色列表
     */
    public List<SysRole> selectRoleAll();

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    public List<String> selectRoleListByUserId(String userId);

    /**
     * 根据用户ID获取角色列表
     */
    Set<String> findRoleListByUserId(String userId);

    /**
     * 通过角色ID查询角色系统的角色
     *
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    public SysRole selectRoleById(String roleId);


    SysRole queryApplyRoleOneByRoleId(String roleId);

    /**
     * 根据用户ID查询角色
     *
     * @param userName 用户名
     * @return 角色列表
     */
    public List<SysRole> selectRolesByUserName(String userName);

    /**
     * 校验角色名称是否唯一
     *
     * @param roleName 角色名称
     * @return 角色信息
     */
    public SysRole checkRoleNameUnique(String roleName);

    /**
     * 校验角色编码是否唯一
     */
    public int selectRoleByRoleCode(String roleCode);

    public String selectRoleIdByRoleCode(String roleCode);

    /**
     * 校验角色权限是否唯一
     *
     * @param roleKey 角色权限
     * @return 角色信息
     */
    public SysRole checkRoleKeyUnique(String roleKey);

    /**
     * 修改角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    public int updateRole(SysRole role);

    /**
     * 新增角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    public int insertRole(SysRole role);

    /**
     * 通过角色ID删除系统角色
     *
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleById(String roleId);

    /**
     * 通过角色ID删除应用角色
     *
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteApplyRole(String roleId);

    /**
     * 批量删除角色信息
     *
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    public int deleteRoleByIds(String[] roleIds);

    /**
     * 通过用户ID查询角色
     *
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    public List<SysRole> selectRoleByUserId(String roleId);

    /**
     * 根据用户的id查询这个角色具有什么样的系统角色或应用角色
     */
    public String[] selectRoleIdsByUserId(Map<String, Object> where);

    /**
     * 校验角色名称是否唯一
     */
    int selectRoleByRoleName(String roleName);

    /**
     * 根据用户id查询这个用户具有哪些角色(用做给用户设置角色数据回显)
     */
    public List<Map<String, Object>> selectRolesByUserId(String userId);

    /**
     * 根据应用appKey，appSecret   获得应用的所有角色
     */
    public List<Map<String, Object>> selectRolesByAppKeyAndAppSecret(@Param("appKey") String appKey, @Param("appSecret") String appSecret);

    /**
     * 根据应用apki，apksecret   和用户token获得用户的角色
     */
    public List<Map<String, Object>> selectRoleByAppKeyAndAppSecretAndToken(@Param("appKey") String appKey, @Param("appSecret") String appSecret, @Param("userId") String userId);

    SysRole selectApplyRoleById(String roleId);

    String[] checkApplyResourcesById(String roleId);

    /**
     * 根据角色id 查询应用id
     *
     * @param roleId
     * @return
     */
    String[] selectApplyId(@Param("roleId") String roleId);
}
