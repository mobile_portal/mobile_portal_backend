package com.casi.project.screen.service;

import com.casi.project.screen.domain.*;

import java.util.List;
import java.util.Map;

/**
 * @Auther shp
 * @data2020/5/815:03
 * @description
 */
public interface IYearService {


    YearContractedVolumeVo yearTotal2(String code);


    /**
     * 合同采集监控最上层总数统计
     * @return
     */
    Map<String,Object> conCollectionMonitor();
    /**
     * 合同采集监控明细
     * @return
     */
    List<EdwContractDataMonitor> conCollectionMonitorDe();

    /**
     * 对外提供数据监控
     * @return
     */
    List<EdwDataSupplyMonitor> sideCollection();

    /**
     * 其他数据采集监控
     * @return
     */
    List<EdwDataCollectMonitor> otherCollection();


    List<YearTotalVo> inputTotal(String code);

    List<YearTotalVo> outTotal(String code);

    Object findAddContract(String code);

    List<YearStaus> distribution(String code);

    List<YearStaus> performStaus(String code);

    List<YearTotalVo> findSign(String code);

    List<YearTotalVo> findPayMent(String code);

    Object findAddToContract(String code);

    Object YearNoneyTotal(String CON_CREDDEBRT, String code);

    Object moneyBackTotal(String CON_CREDDEBRT, String code);

    Object answerMoneyTotal(String CON_CREDDEBRT, String code);

    Object findAddToContract2(String con_creddebrt, String code);

    Object findcoreignCurrency(String con_creddebrt, String code);

    Object findCarryForward(String code);

    Object findTotalOverdue(String code);

    List<YearTotalVo> deferredCollection(String code);

    List<YearCompanyVo> findYearCompanyVo(String code);

    List<YearMoneyTotal> ContractTotal(String code);

    List<YearMoneyTotal> newContractTotal(String code);

    List<DimUnit> getOrganize();
}
