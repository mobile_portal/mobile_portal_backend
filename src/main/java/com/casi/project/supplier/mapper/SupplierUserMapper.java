package com.casi.project.supplier.mapper;

import java.util.List;
import com.casi.project.supplier.domain.SupplierUser;

/**
 * 供应商账号表Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface SupplierUserMapper 
{
    /**
     * 查询供应商账号表
     * 
     * @param uuid 供应商账号表ID
     * @return 供应商账号表
     */
    public SupplierUser selectSupplierUserById(String uuid);

    /**
     * 查询供应商账号表列表
     * 
     * @param supplierUser 供应商账号表
     * @return 供应商账号表集合
     */
    public List<SupplierUser> selectSupplierUserList(SupplierUser supplierUser);

    /**
     * 新增供应商账号表
     * 
     * @param supplierUser 供应商账号表
     * @return 结果
     */
    public int insertSupplierUser(SupplierUser supplierUser);

    /**
     * 修改供应商账号表
     * 
     * @param supplierUser 供应商账号表
     * @return 结果
     */
    public int updateSupplierUser(SupplierUser supplierUser);

    /**
     * 删除供应商账号表
     * 
     * @param uuid 供应商账号表ID
     * @return 结果
     */
    public int deleteSupplierUserById(String uuid);

    /**
     * 批量删除供应商账号表
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSupplierUserByIds(String[] uuids);
}
