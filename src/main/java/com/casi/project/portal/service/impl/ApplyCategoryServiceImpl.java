package com.casi.project.portal.service.impl;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.controller.NoticeController;
import com.casi.project.portal.domain.vo.ApplyCategory;
import com.casi.project.portal.mapper.ApplyCategoryMapper;
import com.casi.project.portal.service.ApplyCategoryService;
import com.casi.project.portal.util.Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 应用类别
 *
 * @author 18732
 */
@Service
public class ApplyCategoryServiceImpl implements ApplyCategoryService {

    @Resource
    private ApplyCategoryMapper applyCategoryMapper;


    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);

    /**
     * 新增应用类别
     *
     * @param hashMap
     * @return
     */
    @Override
    public AjaxResult addApplyCategory(HashMap<String, Object> hashMap) {
        if (Tools.isEmpty((String) hashMap.get("menuName"))) {
            return AjaxResult.error("应用类别不能为空");
        }
        int count = applyCategoryMapper.checkMenuNameUnique((String) hashMap.get("menuName"));
        if (count > 0) {
            return AjaxResult.error("新增应用类别'" + (String) hashMap.get("menuName") + "'失败，应用类别已存在");
        }
        int rows = 0;
        try {
            rows = applyCategoryMapper.addApplyCategory(hashMap);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("添加成功") : AjaxResult.error("添加失败");
    }

    /**
     * 回显应用类别
     *
     * @param id
     * @return
     */
    @Override
    public Map<String, Object> findApplyCategoryId(String id) {
        return applyCategoryMapper.findApplyCategoryId(id);
    }

    /**
     * 修改应用类别
     *
     * @param hashMap
     * @return
     */
    @Override
    public AjaxResult updateApplyCategory(HashMap<String, Object> hashMap) {
        if (hashMap.get("id") == null) {
            return AjaxResult.error("请传要修改应用类别的Id");
        }
        if (Tools.isEmpty((String) hashMap.get("menuName"))) {
            return AjaxResult.error("请传要修改应用类别名称");
        }
        HashMap<String, Object> stringObjectHashMap = new HashMap<>();
        stringObjectHashMap.put("id", hashMap.get("id"));
        stringObjectHashMap.put("menuName", hashMap.get("menuName"));
        int rows = 0;
        try {
            rows = applyCategoryMapper.updateApplyCategory(stringObjectHashMap);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("修改成功") : AjaxResult.error("修改失败");
    }

    /**
     * 禁用启用应用类别
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public AjaxResult disableEnabledCategory(String[] id, Integer type) {
        if (id == null) {
            return AjaxResult.error("请传要禁用应用类别的Id");
        }
        if (null == type) {
            return AjaxResult.error("请传要要禁用启用的值 0-启用 1-禁用");
        }
        if (type != 0 && type != 1) {
            return AjaxResult.error("请传0-启用,1-禁用这个值");
        }
        int rows = 0;
        try {
            for (int i = 0; i < id.length; i++) {
                rows = applyCategoryMapper.disableEnabledCategory(id[i], type);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("成功") : AjaxResult.error("失败");
    }

    /**
     * 根据上级ID查询应用类别下拉列表
     *
     * @param id
     * @return
     */
    @Override
    public ArrayList<ApplyCategory> getApplyCategoryId() {
        return applyCategoryMapper.getApplyCategoryId();
    }

    /**
     * 查询应用类别列表
     *
     * @return
     */
    @Override
    public ArrayList<ApplyCategory> getApplyCategoryListPage() {
        return applyCategoryMapper.getApplyCategoryListPage();
    }
}
