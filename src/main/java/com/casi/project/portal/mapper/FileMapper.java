package com.casi.project.portal.mapper;


import com.casi.project.portal.domain.LocalFile;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Author zhongzj
 * @Description:
 * @Create: 2019/11/28
 */

@Mapper
public interface FileMapper {

    int insert(LocalFile file);

    int delete(Long id);

    LocalFile queryById(Long id);

}
