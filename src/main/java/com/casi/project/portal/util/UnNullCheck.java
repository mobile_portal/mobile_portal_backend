package com.casi.project.portal.util;

import com.casi.framework.web.domain.AjaxResult;

import java.lang.reflect.Field;

/**
 * @Auther shp
 * @data2020/4/2118:03
 * @description
 */
public class UnNullCheck {


    /**
     * 传递一个参数对象，对 对象中的值进行非空判断
     * @param m
     * @return
     */
    public static AjaxResult getAjaxResult(Object m) {
        if (m==null){
            return AjaxResult.error("传输内容不能为空");
        }

        AjaxResult ajaxResult = new AjaxResult();

        Field[] fields = m.getClass().getDeclaredFields();
        for (Field field : fields) {
            field.setAccessible(true);
            try {
                if(field.get(m) == null){
                    return AjaxResult.error(" "+field.getName()+" 的属性值为空 -----------------------------------------------");
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
