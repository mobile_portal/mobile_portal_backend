package com.casi.project.interfaces.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class UserTemp implements Serializable {

    private Integer id;

    /**  成员UserID 对应管理端的帐号  */
    private String userId;

    /**  成员名称  */
    private String name;

    /** 所属部门id列表  */
    private String department;

    private Date createTime;

    /**  手机号码  */
    private String mobile;

    /**  性别。0表示未定义，1表示男性，2表示女性  */
    private String gender;

    /**  邮箱  */
    private String email;

    /**  激活状态: 1=已激活，2=已禁用，4=未激活  */
    private String status;

    }