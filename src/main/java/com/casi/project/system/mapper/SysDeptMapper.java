package com.casi.project.system.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.casi.project.system.domain.SysDept;

/**
 * 部门管理 数据层
 * 
 * @author lzb
 */
public interface SysDeptMapper
{
    /**
     * 根据parent来查询父级数据
     * @param parent_id 部门信息
     *
     * @return 单个部门的信息
     */
    public SysDept selectByParentId(String parentId);
    /**
     * 查询部门是否存在
     *
     * @param deptId 部门信息
     * @return 单个部门信息
     */
    public SysDept selectByDeptId(String id);


    /**
     * 查询部门管理数据
     * 
     * @param dept 部门信息
     * @return 部门信息集合
     */
    public List<SysDept> selectDeptList(SysDept dept);

    /**
     * 根据角色ID查询部门树信息
     * 
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    public List<Integer> selectDeptListByRoleId(String roleId);

    /**
     * 根据部门ID查询信息
     * 
     * @param deptId 部门ID
     * @return 部门信息
     */
    public SysDept selectDeptById(String deptId);

    /**
     * 根据ID查询所有子部门
     * 
     * @param deptId 部门ID
     * @return 部门列表
     */
    public List<SysDept> selectChildrenDeptById(String deptId);

    /**
     * 是否存在子节点
     * 
     * @param deptId 部门ID
     * @return 结果
     */
    public int hasChildByDeptId(String deptId);

    /**
     * 查询部门是否存在用户
     * 
     * @param deptId 部门ID
     * @return 结果
     */
    public int checkDeptExistUser(String deptId);

    /**
     * 校验部门名称是否唯一
     * 
     * @param deptName 部门名称
     * @param parentId 父部门ID
     * @return 结果
     */
    public SysDept checkDeptNameUnique(@Param("deptName") String deptName, @Param("parentId") String parentId);

    public int checkDeptNameUniqueNew(@Param("deptName") String deptName);

    /**
     * 校验部门编码是否唯一
     */
    public int selectDeptByDeptCode(String deptCode);

    /**
     * 新增部门信息
     * 
     * @param dept 部门信息
     * @return 结果
     */
    public int insertDept(SysDept dept);

    /**
     * 修改部门信息
     * 
     * @param dept 部门信息
     * @return 结果
     */
    public int updateDept(SysDept dept);

    /**
     * 组织状态修改
     */
    public int changeStatus(@Param("deptIds") String[] deptIds,@Param("status") String status);

    /**
     * 修改所在部门的父级部门状态
     * 
     * @param dept 部门
     */
    public void updateDeptStatus(SysDept dept);

    /**
     * 修改子元素关系
     * 
     * @param depts 子元素
     * @return 结果
     */
    public int updateDeptChildren(@Param("depts") List<SysDept> depts);

    /**
     * 删除部门管理信息
     * 
     * @param deptId 部门ID
     * @return 结果
     */
    public int deleteDeptById(String deptId);

    /**
     * 获取组织列表
     * @param dept
     * @return
     */
    List<SysDept> selectOrganList();


    /**
     * 删除组织信息
     *
     * @param deptId 组织ID
     * @return 结果
     */
    int deleteOrganById(String deptId);
    /**
     * 数据同步根据uid删除信息
     */
    public int deleteSysDetpByUid(String uid);
    /**
     * 数据同步更新根据uid（用户的id）去查询
     */
    public SysDept selectBySysDeptByUid(String uid);
    /**
     * 数据同步删除数据
     */
    public int deleteOne(String organizationUuid);
    /**
     * 根据本组织机构的uuid查询
     */
    public SysDept selectByOrganizationUuid(String organizationUuid);
    /**
     * 根据应用id查询部门
     */
    public List<Map<String,Object>> selectByApplyId(Long applyId);
    /**
     *根据应用appKey，appSecret   获得应用的所有组织机构
     */
    public List<Map<String,Object>> selectDeptsByAppKeyAndAppSecret(@Param("appKey") String appKey,@Param("appSecret") String appSecret);
}
