package com.casi.project.portal.controller;

import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.ApplyIType;
import com.casi.project.portal.domain.vo.ApplyType;
import com.casi.project.portal.service.impl.ApplyTypeServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 应用类型接口等
 * @author 18732
 * 已去掉该功能 5/27
 */
@RestController
@RequestMapping("/apply/applyType")
public class ApplyTypeController extends BaseController {

    @Resource
    private ApplyTypeServiceImpl applyTypeService;

    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);

    /**
     * 新增应用类型
     *
     * @param applyType
     * @return
     */
    @PreAuthorize("@ss.hasPermi('apply:type:add')")
    @PostMapping("/addApplyType")
    public AjaxResult addApplyType(@RequestBody ApplyIType applyType) {
        logger.info("===============新增应用类型=============");
        return applyTypeService.addApplyType(applyType);
    }

    /**
     * 根据id回显应用类型具体详情
     *
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('apply:type:echo')")
    @GetMapping("/getApplyTypeId")
    public AjaxResult getApplyTypeId(String id) {
        logger.info("===============根据id回显应用类型具体详情=============");
        if (id == null) {
            AjaxResult.error("请传要回显的应用类型ID");
        }
        ApplyIType applyIType = null;
        try {
            applyIType = applyTypeService.getApplyTypeId(id);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");
        }
        logger.info("查询成功=======" + applyIType);
        return AjaxResult.success("查询成功", applyIType);
    }

    /**
     * 修改应用类型
     *
     * @param applyType
     * @return
     */
    @PreAuthorize("@ss.hasPermi('apply:type:edit')")
    @PutMapping("/updateApplyType")
    public AjaxResult updateApplyType(@RequestBody ApplyIType applyType) {
        logger.info("===============修改应用类型=============");
        return applyTypeService.updateApplyType(applyType);
    }

    /**
     * 禁用启用应用类型
     *
     * @return
     */
    @PreAuthorize("@ss.hasPermi('apply:type:disable')")
    @GetMapping("/disableEnabledApplyType")
    public AjaxResult disableEnabledApplyType(String[] id, Integer type) {
        logger.info("===============禁用应用类型=============");
        return applyTypeService.disableEnabledApplyType(id, type);
    }

    /**
     * 查询应用类型下拉列表
     *
     * @return
     */
    @PreAuthorize("@ss.hasPermi('apply:type:list')")
    @GetMapping("/getApplyType")
    public AjaxResult getApplyTypeList() {
        logger.info("===============查询应用类型下拉列表=============");
        ArrayList<ApplyType> applyTypeList1 = null;
        try {
            applyTypeList1 = applyTypeService.getApplyTypeList();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");
        }
        logger.info("查询成功=======" + applyTypeList1);
        return AjaxResult.success("查询成功", applyTypeList1);
    }

    /**
     * 查询应用类型列表
     *
     * @return
     */
    @PreAuthorize("@ss.hasPermi('apply:type:list')")
    @GetMapping("/getApplyTypeListPage")
    public AjaxResult getApplyTypeListPage(Integer pageNum, Integer pageSize) {
        logger.info("===============查询应用类型列表=============");
        PageHelper.startPage(pageNum, pageSize);
        ArrayList<ApplyType> applyTypeList1 = null;
        try {
            applyTypeList1 = applyTypeService.getApplyTypeListPage();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");
        }
        logger.info("查询成功=======" + applyTypeList1);
        PageInfo<ApplyType> applyTypePageInfo = new PageInfo<>(applyTypeList1);
        return AjaxResult.success("查询成功", applyTypePageInfo);
    }

    /**
     * 查询首页menu名称下拉列表
     *
     * @return
     */
    @PreAuthorize("@ss.hasPermi('apply:type:list')")
    @GetMapping("/getHomePageList")
    public AjaxResult getHomePageList() {
        logger.info("===============查询应用类型下拉列表=============");
        List<HashMap<String, Object>> hashMap = null;
        try {
            hashMap = applyTypeService.getHomePageList();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");
        }
        logger.info("查询成功=======" + hashMap);
        return AjaxResult.success("查询成功", hashMap);
    }
}
