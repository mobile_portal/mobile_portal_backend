package com.casi.project.store.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppImageMapper;
import com.casi.project.store.domain.StoreAppImage;
import com.casi.project.store.service.IStoreAppImageService;

/**
 * 应用图片Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppImageServiceImpl implements IStoreAppImageService 
{
    @Autowired
    private StoreAppImageMapper storeAppImageMapper;

    /**
     * 查询应用图片
     * 
     * @param uuid 应用图片ID
     * @return 应用图片
     */
    @Override
    public StoreAppImage selectStoreAppImageById(String uuid)
    {
        return storeAppImageMapper.selectStoreAppImageById(uuid);
    }

    /**
     * 查询应用图片列表
     * 
     * @param storeAppImage 应用图片
     * @return 应用图片
     */
    @Override
    public List<StoreAppImage> selectStoreAppImageList(StoreAppImage storeAppImage)
    {
        return storeAppImageMapper.selectStoreAppImageList(storeAppImage);
    }

    /**
     * 新增应用图片
     * 
     * @param storeAppImage 应用图片
     * @return 结果
     */
    @Override
    public int insertStoreAppImage(StoreAppImage storeAppImage)
    {
        return storeAppImageMapper.insertStoreAppImage(storeAppImage);
    }

    /**
     * 修改应用图片
     * 
     * @param storeAppImage 应用图片
     * @return 结果
     */
    @Override
    public int updateStoreAppImage(StoreAppImage storeAppImage)
    {
        return storeAppImageMapper.updateStoreAppImage(storeAppImage);
    }

    /**
     * 批量删除应用图片
     * 
     * @param uuids 需要删除的应用图片ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppImageByIds(String[] uuids)
    {
        return storeAppImageMapper.deleteStoreAppImageByIds(uuids);
    }

    /**
     * 删除应用图片信息
     * 
     * @param uuid 应用图片ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppImageById(String uuid)
    {
        return storeAppImageMapper.deleteStoreAppImageById(uuid);
    }
}
