package com.casi.project.store.mapper;

import java.util.List;
import com.casi.project.store.domain.StoreAppCheck;

/**
 * 应用审核Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface StoreAppCheckMapper 
{
    /**
     * 查询应用审核
     * 
     * @param uuid 应用审核ID
     * @return 应用审核
     */
    public StoreAppCheck selectStoreAppCheckById(String uuid);

    /**
     * 查询应用审核列表
     * 
     * @param storeAppCheck 应用审核
     * @return 应用审核集合
     */
    public List<StoreAppCheck> selectStoreAppCheckList(StoreAppCheck storeAppCheck);

    /**
     * 新增应用审核
     * 
     * @param storeAppCheck 应用审核
     * @return 结果
     */
    public int insertStoreAppCheck(StoreAppCheck storeAppCheck);

    /**
     * 修改应用审核
     * 
     * @param storeAppCheck 应用审核
     * @return 结果
     */
    public int updateStoreAppCheck(StoreAppCheck storeAppCheck);

    /**
     * 删除应用审核
     * 
     * @param uuid 应用审核ID
     * @return 结果
     */
    public int deleteStoreAppCheckById(String uuid);

    /**
     * 批量删除应用审核
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreAppCheckByIds(String[] uuids);
}
