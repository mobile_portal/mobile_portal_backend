package com.casi.project.store.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 应用审核对象 store_app_check
 * 
 * @author casi
 * @date 2020-06-02
 */
public class StoreAppCheck extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private String appId;

    /** 审核意见 */
    @Excel(name = "审核意见")
    private String checkContent;

    /** 审核人 */
    @Excel(name = "审核人")
    private String checkUser;

    /** 审核时间 */
    @Excel(name = "审核时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date checkTime;

    /** 审核状态 */
    @Excel(name = "审核状态")
    private String status;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setCheckContent(String checkContent) 
    {
        this.checkContent = checkContent;
    }

    public String getCheckContent() 
    {
        return checkContent;
    }
    public void setCheckUser(String checkUser) 
    {
        this.checkUser = checkUser;
    }

    public String getCheckUser() 
    {
        return checkUser;
    }
    public void setCheckTime(Date checkTime) 
    {
        this.checkTime = checkTime;
    }

    public Date getCheckTime() 
    {
        return checkTime;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("appId", getAppId())
            .append("checkContent", getCheckContent())
            .append("checkUser", getCheckUser())
            .append("checkTime", getCheckTime())
            .append("status", getStatus())
            .toString();
    }
}
