package com.casi.project.portal.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApplyType implements Serializable {
    private  String id;
    private  String typeName;
    private String createTime;
    private Long isDisable;
}
