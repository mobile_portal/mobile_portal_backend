package com.casi.project.portal.util;

import java.security.MessageDigest;

/**
 * 字符串加密工具类
 * @name:EncryptionUtils.class
 * @description:
 * @author:丁冬
 * @date:2016年12月23日 下午2:31:28
 * @location:cn.com.casic.util.EncryptionUtils.class
 * Copyright 2015 by Jusfoun.com. All Right Reserved
 */
public class EncryptionUtils {
	
	/**
	 * SHA1加密
	 * @title: getSha1  
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年12月23日 下午2:31:46  
	 * @param str
	 * @param charset
	 * @return
	 */
	public static String getSha1(String str,String charset){
		if (str == null || str.length() == 0) {  
            return null;  
        }  
        char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',  
                'a', 'b', 'c', 'd', 'e', 'f' };  
  
        try {  
            MessageDigest mdTemp = MessageDigest.getInstance("SHA-1");  
            mdTemp.update(str.getBytes(charset));  //"UTF-8"
  
            byte[] md = mdTemp.digest();  
            int j = md.length;  
            char buf[] = new char[j * 2];  
            int k = 0;  
            for (int i = 0; i < j; i++) {  
                byte byte0 = md[i];  
                buf[k++] = hexDigits[byte0 >>> 4 & 0xf];  
                buf[k++] = hexDigits[byte0 & 0xf];  
            }  
            return new String(buf);  
        } catch (Exception e) {  
            return null;  
        }  
	}
	
	/**
	 * MD5加密
	 * @title: getMD5  
	 * @description: TODO
	 * @author: 丁冬
	 * @date:2016年12月23日 下午2:32:03  
	 * @param source
	 * @return
	 */
	public static String getMD5(String source) { 
        String s = null; 
        char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };// 用来将字节转换成16进制表示的字符 
        try { 
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(source.getBytes()); 
            byte tmp[] = md.digest();// MD5 的计算结果是一个 128 位的长整数， 
            // 用字节表示就是 16 个字节 
            char str[] = new char[16 * 2];// 每个字节用 16 进制表示的话，使用两个字符， 所以表示成 16 
            // 进制需要 32 个字符 
            int k = 0;// 表示转换结果中对应的字符位置 
            for (int i = 0; i < 16; i++) {// 从第一个字节开始，对 MD5 的每一个字节// 转换成 16 
                // 进制字符的转换 
                byte byte0 = tmp[i];// 取第 i 个字节 
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];// 取字节中高 4 位的数字转换,// >>> 
                // 为逻辑右移，将符号位一起右移 
                str[k++] = hexDigits[byte0 & 0xf];// 取字节中低 4 位的数字转换 
            } 
            s = new String(str);// 换后的结果转换为字符串 
        } catch (Exception e) { 
            e.printStackTrace(); 
        } 
        return s; 
    }
	
	/*public static void main(String[] args) {
		String result_sha1 = getSha1("7274C7E2-0619-1B02-D467-4D30E7A6A7CF","UTF-8");
		String result_md5 = getMD5("7274C7E2-0619-1B02-D467-4D30E7A6A7CF".getBytes());
		System.out.println(result_sha1);
		System.out.println(result_md5);
	}*/

}
