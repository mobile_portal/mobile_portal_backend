package com.casi.project.supplier.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 供应商账号表对象 supplier_user
 * 
 * @author casi
 * @date 2020-06-02
 */
public class SupplierUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编码 */
    private String uuid;

    /** 所属公司 */
    @Excel(name = "所属公司")
    private String supplierId;

    /** 登录账号 */
    @Excel(name = "登录账号")
    private String username;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    /** 盐 */
    private String salt;

    /** 姓名 */
    private String name;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setSupplierId(String supplierId) 
    {
        this.supplierId = supplierId;
    }

    public String getSupplierId() 
    {
        return supplierId;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setSalt(String salt) 
    {
        this.salt = salt;
    }

    public String getSalt() 
    {
        return salt;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("supplierId", getSupplierId())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("salt", getSalt())
            .append("name", getName())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .append("status", getStatus())
            .toString();
    }
}
