package com.casi.project.portal.mapper;


import com.casi.project.portal.domain.MobileApplyDepts;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public interface MobileApplyDeptsMapper {

    /**
     * 给组织授权应用
     */
    public int insertApplyIdsAndDeptIds(@Param("tags") Set<MobileApplyDepts> tags);
    /**
     * 根据组织机构Id和应用Id查询
     */
    public MobileApplyDepts selectOneByApplyIdsAndDeptIds(@Param("applyId") String applyId,@Param("deptId") String deptId);

    /**
     * 回显应用已授权的组织机构用于回显
     * @param applyIds
     * @return
     */
    List<HashMap<String, Object>> findApplyIdsAndDeptIds(String[] applyIds);

    /**
     * 删除应用授权的部门根据应用的id
     */
    public int deleteByApplyId(String applyId);
    /**
     * 通过用户的id查询出部门的id   在根据部门的id和应用的id查询此应用是否授权给这个部门
     */
    public MobileApplyDepts selectByApplyIdsAndDeptIdAndUserId(@Param("applyId") String applyId,@Param("userId") String userId);
    /**
     * 根据应用的id查询应用与部门表查看这个应用授权了哪些部门
     */
    public String[] selectMobileApplyDeptsGetDeptIdsByApplyId(String applyId);
}
