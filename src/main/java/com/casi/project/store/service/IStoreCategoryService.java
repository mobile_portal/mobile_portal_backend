package com.casi.project.store.service;

import java.util.List;
import com.casi.project.store.domain.StoreCategory;

/**
 * 应用分类Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface IStoreCategoryService 
{
    /**
     * 查询应用分类
     * 
     * @param uuid 应用分类ID
     * @return 应用分类
     */
    public StoreCategory selectStoreCategoryById(String uuid);

    /**
     * 查询应用分类列表
     * 
     * @param storeCategory 应用分类
     * @return 应用分类集合
     */
    public List<StoreCategory> selectStoreCategoryList(StoreCategory storeCategory);

    /**
     * 新增应用分类
     * 
     * @param storeCategory 应用分类
     * @return 结果
     */
    public int insertStoreCategory(StoreCategory storeCategory);

    /**
     * 修改应用分类
     * 
     * @param storeCategory 应用分类
     * @return 结果
     */
    public int updateStoreCategory(StoreCategory storeCategory);

    /**
     * 批量删除应用分类
     * 
     * @param uuids 需要删除的应用分类ID
     * @return 结果
     */
    public int deleteStoreCategoryByIds(String[] uuids);

    /**
     * 删除应用分类信息
     * 
     * @param uuid 应用分类ID
     * @return 结果
     */
    public int deleteStoreCategoryById(String uuid);
}
