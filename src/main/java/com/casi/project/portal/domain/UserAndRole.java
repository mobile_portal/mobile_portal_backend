package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author 18732
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserAndRole {
    private String[] userId;
    private String roleId;
}
