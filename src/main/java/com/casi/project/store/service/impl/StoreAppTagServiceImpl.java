package com.casi.project.store.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppTagMapper;
import com.casi.project.store.domain.StoreAppTag;
import com.casi.project.store.service.IStoreAppTagService;

/**
 * 应用标签关联Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppTagServiceImpl implements IStoreAppTagService 
{
    @Autowired
    private StoreAppTagMapper storeAppTagMapper;

    /**
     * 查询应用标签关联
     * 
     * @param uuid 应用标签关联ID
     * @return 应用标签关联
     */
    @Override
    public StoreAppTag selectStoreAppTagById(String uuid)
    {
        return storeAppTagMapper.selectStoreAppTagById(uuid);
    }

    /**
     * 查询应用标签关联列表
     * 
     * @param storeAppTag 应用标签关联
     * @return 应用标签关联
     */
    @Override
    public List<StoreAppTag> selectStoreAppTagList(StoreAppTag storeAppTag)
    {
        return storeAppTagMapper.selectStoreAppTagList(storeAppTag);
    }

    /**
     * 新增应用标签关联
     * 
     * @param storeAppTag 应用标签关联
     * @return 结果
     */
    @Override
    public int insertStoreAppTag(StoreAppTag storeAppTag)
    {
        return storeAppTagMapper.insertStoreAppTag(storeAppTag);
    }

    /**
     * 修改应用标签关联
     * 
     * @param storeAppTag 应用标签关联
     * @return 结果
     */
    @Override
    public int updateStoreAppTag(StoreAppTag storeAppTag)
    {
        return storeAppTagMapper.updateStoreAppTag(storeAppTag);
    }

    /**
     * 批量删除应用标签关联
     * 
     * @param uuids 需要删除的应用标签关联ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppTagByIds(String[] uuids)
    {
        return storeAppTagMapper.deleteStoreAppTagByIds(uuids);
    }

    /**
     * 删除应用标签关联信息
     * 
     * @param uuid 应用标签关联ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppTagById(String uuid)
    {
        return storeAppTagMapper.deleteStoreAppTagById(uuid);
    }
}
