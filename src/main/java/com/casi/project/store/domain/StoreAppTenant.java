package com.casi.project.store.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;
import java.util.Date;

/**
 * 应用租户授权对象 store_app_tenant
 * 
 * @author casi
 * @date 2020-06-02
 */
public class StoreAppTenant extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private String appId;

    /** 租户编号 */
    @Excel(name = "租户编号")
    private String tenantId;

    /** 操作人 */
    @Excel(name = "操作人")
    private String authUserId;

    /** 授权时间 */
    @Excel(name = "授权时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date authTime;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setTenantId(String tenantId) 
    {
        this.tenantId = tenantId;
    }

    public String getTenantId() 
    {
        return tenantId;
    }
    public void setAuthUserId(String authUserId) 
    {
        this.authUserId = authUserId;
    }

    public String getAuthUserId() 
    {
        return authUserId;
    }
    public void setAuthTime(Date authTime) 
    {
        this.authTime = authTime;
    }

    public Date getAuthTime() 
    {
        return authTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("appId", getAppId())
            .append("tenantId", getTenantId())
            .append("authUserId", getAuthUserId())
            .append("authTime", getAuthTime())
            .toString();
    }
}
