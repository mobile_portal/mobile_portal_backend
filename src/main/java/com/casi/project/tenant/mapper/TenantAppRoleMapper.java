package com.casi.project.tenant.mapper;

import java.util.List;
import com.casi.project.tenant.domain.TenantAppRole;

/**
 * 租户自定义应用角色Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface TenantAppRoleMapper 
{
    /**
     * 查询租户自定义应用角色
     * 
     * @param uuid 租户自定义应用角色ID
     * @return 租户自定义应用角色
     */
    public TenantAppRole selectTenantAppRoleById(String uuid);

    /**
     * 查询租户自定义应用角色列表
     * 
     * @param tenantAppRole 租户自定义应用角色
     * @return 租户自定义应用角色集合
     */
    public List<TenantAppRole> selectTenantAppRoleList(TenantAppRole tenantAppRole);

    /**
     * 新增租户自定义应用角色
     * 
     * @param tenantAppRole 租户自定义应用角色
     * @return 结果
     */
    public int insertTenantAppRole(TenantAppRole tenantAppRole);

    /**
     * 修改租户自定义应用角色
     * 
     * @param tenantAppRole 租户自定义应用角色
     * @return 结果
     */
    public int updateTenantAppRole(TenantAppRole tenantAppRole);

    /**
     * 删除租户自定义应用角色
     * 
     * @param uuid 租户自定义应用角色ID
     * @return 结果
     */
    public int deleteTenantAppRoleById(String uuid);

    /**
     * 批量删除租户自定义应用角色
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTenantAppRoleByIds(String[] uuids);
}
