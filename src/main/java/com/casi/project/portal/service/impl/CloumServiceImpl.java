package com.casi.project.portal.service.impl;

import com.casi.common.core.lang.UUID;
import com.casi.project.portal.domain.Cloum;
import com.casi.project.portal.mapper.CloumMapper;
import com.casi.project.portal.service.CloumService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Auther shp
 * @data2020/4/2416:34
 * @description
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class CloumServiceImpl implements CloumService {


    @Autowired
    private CloumMapper mapper;

    @Override
    public Cloum findById(Cloum cloum) {
        return mapper.findById(cloum);
    }

    @Override
    public List<Cloum> findCloum(Cloum cloum) {
        cloum.setIsDel(0);
       return mapper.findCloum(cloum);
    }

    @Override
    public Integer addCloum(Cloum cloum) {

        cloum.setUuid(UUID.randomUUID().toString(true));

        return mapper.addCloum(cloum);
    }

    @Override
    public Integer editCloum(Cloum cloum) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        cloum.setUpdateTime(formatter.format(new Date()));
        return mapper.editCloum(cloum);
    }

    @Override
    public Integer delCloum(Cloum cloum) {
        cloum.setIsDel(1);
        return mapper.delCloum(cloum);
    }
}
