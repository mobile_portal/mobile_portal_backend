package com.casi.project.portal.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesUtils {
	// ����һ�����������ļ��Ķ���
	static Properties prop = new Properties();

	/**
	 * *
	 * 
	 * @param fileName
	 *            ��Ҫ���ص�properties�ļ����ļ���Ҫ����src��Ŀ¼��
	 * @return �Ƿ���سɹ�
	 */
	public static boolean loadFile(String fileName) {
		InputStream in = null;
		try {
			in = PropertiesUtils.class.getClassLoader().getResourceAsStream(fileName);
			prop.load(in);
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}finally{
			if(in !=null ){
				try {
					in.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return true;
	}

	/**
	 * ����KEYȡ����Ӧ��value
	 * 
	 * @param key
	 * @return
	 */
	public static String getPropertyValue(String key) {
		return prop.getProperty(key);
	}
}
