package com.casi.project.portal.service.impl;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.controller.NoticeController;
import com.casi.project.portal.domain.ApplyIType;
import com.casi.project.portal.domain.vo.ApplyType;
import com.casi.project.portal.mapper.ApplyTypeMapper;
import com.casi.project.portal.service.ApplyTypeService;
import com.casi.project.portal.util.Tools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 应用类型
 *
 * @author 18732
 */
@Service
public class ApplyTypeServiceImpl implements ApplyTypeService {

    @Resource
    private ApplyTypeMapper applyTypeMapper;


    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);

    /**
     * 新增应用类型
     *
     * @param applyType
     * @return
     */
    @Override
    public AjaxResult addApplyType(ApplyIType applyType) {
        if (applyType.getHomePageId() == null) {
            return AjaxResult.error("请传首页菜单homePageId");
        }
        if (Tools.isEmpty(applyType.getTypeName())) {
            return AjaxResult.error("请传应用类型名称");
        }
        int count = applyTypeMapper.checkTypeUnique(applyType.getTypeName());
        if (count > 0) {
            return AjaxResult.error("新增应用类型名称'" + applyType.getTypeName() + "'失败，应用类型名称已存在");
        }
        int rows = 0;
        try {
            rows = applyTypeMapper.addApplyType(applyType);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("添加成功") : AjaxResult.error("添加失败");
    }

    /**
     * 根据id回显应用类型具体详情
     *
     * @param id
     * @return
     */
    @Override
    public ApplyIType getApplyTypeId(String id) {
        return applyTypeMapper.getApplyTypeId(id);
    }

    /**
     * 修改应用类型
     *
     * @param applyType
     * @return
     */
    @Override
    public AjaxResult updateApplyType(ApplyIType applyType) {
        if (applyType.getId() == null) {
            return AjaxResult.error("请传要修改应用类型的Id");
        }
        if (Tools.isEmpty(applyType.getTypeName())) {
            return AjaxResult.error("请传要修改应用类型名称");
        }
        int rows = 0;
        try {
            rows = applyTypeMapper.updateApplyType(applyType);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("修改成功") : AjaxResult.error("修改失败");
    }

    /**
     * 禁用启用应用类型
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public AjaxResult disableEnabledApplyType(String[] id, Integer type) {
        if (id == null) {
            return AjaxResult.error("请传要禁用启用应用类型的id");
        }
        if (null == type) {
            return AjaxResult.error("请传要要禁用启用的值 0-启用 1-禁用");
        }
        if (type != 0 && type != 1) {
            return AjaxResult.error("请传0-启用,1-禁用这个值");
        }
        int rows = 0;
        try {
            for (int i = 0; i < id.length; i++) {
                rows = applyTypeMapper.disableEnabledApplyType(id[i], type);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("成功") : AjaxResult.error("失败");
    }

    /**
     * 查询应用类型下拉列表
     *
     * @return
     */
    @Override
    public ArrayList<ApplyType> getApplyTypeList() {
        return applyTypeMapper.getApplyTypeList();
    }

    /**
     * 查询应用类型列表
     *
     * @return
     */
    @Override
    public ArrayList<ApplyType> getApplyTypeListPage() {
        return applyTypeMapper.getApplyTypeListPage();
    }

    /**
     * 查询首页menu名称下拉列表
     *
     * @return
     */
    @Override
    public List<HashMap<String, Object>> getHomePageList() {
        return applyTypeMapper.getHomePageList();
    }
}
