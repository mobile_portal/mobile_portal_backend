package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.MobileType;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Auther shp
 * @data2020/4/2118:33
 * @description
 */

@Mapper
public interface MobileTypeMapper {

    @Select("SELECT m.id,m.type_name typeName,m.type_id typeId,m.type_id typeSort ,m.create_time createTime,m.update_time updateTime from moblie_type m where m.type_id=#{softId};")
    List<MobileType> findType(Integer softId);

}
