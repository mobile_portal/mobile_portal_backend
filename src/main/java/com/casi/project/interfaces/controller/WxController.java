package com.casi.project.interfaces.controller;

import com.casi.common.constant.Constants;
import com.casi.common.utils.StringUtils;
import com.casi.framework.security.LoginUser;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.interfaces.domain.ApiInvok;
import com.casi.project.interfaces.domain.DepartmentVo;
import com.casi.project.interfaces.service.ApiInvokService;
import com.casi.project.interfaces.service.WxUserService;
import com.casi.project.system.domain.SysUser;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RequestMapping("/api/wx")
@Api(tags = "微信接口")
@RestController
public class WxController {

    @Autowired
    private WxUserService wxUserService;
    @Autowired
    private TokenService tokenService;

    @Autowired
    private ApiInvokService apiInvokService;

    private static Logger logger = LoggerFactory.getLogger(WxController.class);

    /**
     * @Description: 获取IdToken
     * @Param:
     * @return:
     * @Author: y_xiaopeng
     * @Date: 2020/5/21
     */
    @GetMapping("/getAuthToken")
    public AjaxResult getAuthToken(@RequestBody Map<String,Object> params){

        AjaxResult ajax = wxUserService.checkParams(params);
        Integer ajaxCode = (Integer)ajax.get("code");
        if (ajaxCode == 200){
            //创建idtoken
            LoginUser userlogin = new LoginUser();
            SysUser  user = new SysUser();
            if(StringUtils.isEmpty(String.valueOf(params.get("username")))){
                return AjaxResult.error(9999,"用户名不能为空！！！");
            }
            user.setUserName(String.valueOf(params.get("username")));
            userlogin.setUser(user);
            String  idtoken = tokenService.createToken(userlogin);
            logger.info("idtoken====================================================="+idtoken);
            return AjaxResult.success(idtoken);
        }

        return ajax;
    }


    @GetMapping("user")
    public AjaxResult getUser(String idToken, String userId){
        if(StringUtils.isEmpty(userId)){
            return AjaxResult.error("userId不能为空");
        }
        AjaxResult ajax= wxUserService.getUserByUserId(idToken,userId);
        return ajax;
    }

    @Scheduled(cron="0 30 * * * ?")
    //@Scheduled(cron="20 * * * * ?")
    public AjaxResult taskGetOrgList(){
        logger.info("这是拉取组织=======taskGetOrgList=======================定时任务，呵呵呵======");

            AjaxResult ajax=  wxUserService.getOrgList(null,null);
            logger.info("ajax=============================="+ajax);



        return AjaxResult.success();
    }

    //同步用户
    @Scheduled(cron="0 0 2 * * ?")
    //@Scheduled(cron="30 * * * * ?")
    public AjaxResult taskGeUserList(){
        logger.info("这是拉取用户=======taskGetOrgList=======================定时任务，呵呵呵======");

            List<DepartmentVo> list = wxUserService.deptList();
            for (DepartmentVo vo : list ) {
                AjaxResult ajax=  wxUserService.getUserList(null,vo.getId());
                logger.info("ajax=============="+ajax);
            }

        return AjaxResult.success();
    }

    /**
     *根据CODE获取用户信息
     * @param code
     * @return
     */
    @GetMapping("getUserCode")
    public String  getUserByCode(String code, HttpServletResponse resp ) throws IOException {

        if(StringUtils.isEmpty(code)){
            return "code没拿到";
        }
        String  token = wxUserService.getUserByCode(code);
        logger.info("url============================idToken="+token);
        String url = Constants.WX_PORTAL_URL +"?idToken=" ;

        if(StringUtils.isNotEmpty(token)){
            //拼接移动门户登录地址
            url = url+token;
            logger.info("url============================url="+url);
            resp.sendRedirect(url);
        }
        return "redirect:"+url;

    }

}
