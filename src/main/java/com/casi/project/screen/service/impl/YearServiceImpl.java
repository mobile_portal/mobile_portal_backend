package com.casi.project.screen.service.impl;

import com.casi.project.screen.domain.*;
import com.casi.project.screen.mapper.YearToalMapper;
import com.casi.project.screen.service.IYearService;
import com.casi.project.screen.util.DefaultLoadUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther shp
 * @data2020/5/815:03
 * @description
 */

@Transactional(rollbackFor = Exception.class)
@Service
public class YearServiceImpl implements IYearService {

    @Autowired
    private YearToalMapper mapper;


    @Override
    public Map<String, Object> conCollectionMonitor() {

        Object totalQuantity = mapper.getTotalQuantity();
        Object totalQuantityToday = mapper.getTotalQuantityToday();
        Object totalQuantityCompany = mapper.getTotalQuantityCompany();
        Object collectionSys = mapper.collectionSys();
        Object collectTotal = mapper.collectTotal();


        Map<String, Object> map = new LinkedHashMap<>();
        map.put("totalQuantity",totalQuantity);
        map.put("totalQuantityToday",totalQuantityToday);
        map.put("totalQuantityCompany",totalQuantityCompany);
        map.put("collectionSys",collectionSys);
        map.put("collectTotal",collectTotal);


        return map;
    }

    @Override
    public List<EdwContractDataMonitor> conCollectionMonitorDe(){
        List<EdwContractDataMonitor> vo = mapper.conCollectionMonitor();
        if(null != vo){

            return vo;
        }
        return null;
    }

    @Override
    public List<EdwDataSupplyMonitor> sideCollection(){
        List<EdwDataSupplyMonitor> model = mapper.sideCollection();
        if(null != model){
            return model;
        }
        return null;
    }

    @Override
    public List<EdwDataCollectMonitor> otherCollection(){
        List<EdwDataCollectMonitor> model = mapper.otherCollection();
        if(null != model){
            return model;
        }
        return null;
    }


    @Override
    public List<YearTotalVo> inputTotal(String code) {
        return mapper.inputTotal(code);
    }

    @Override
    public List<YearTotalVo> outTotal(String code) {
        return mapper.outTotal(code);
    }

    @Override
    public Object findAddContract(String code) {
        return mapper.findAddContract(code);
    }


    @Override
    public List<YearStaus> distribution(String code) {
        return mapper.distribution(code);
    }

    @Override
    public List<YearStaus> performStaus(String code) {
        return mapper.performStaus(code);
    }

    @Override
    public List<YearTotalVo> findSign(String code) {
        return mapper.findSign(code);
    }

    @Override
    public List<YearTotalVo> findPayMent(String code) {
        return mapper.findPayMent(code);
    }

    @Override
    public Object findAddToContract(String code) {
        return mapper.findAddToContract(code);
    }

    @Override
    public Object YearNoneyTotal(String CON_CREDDEBRT,String code) {

        return mapper.YearNoneyTotal(CON_CREDDEBRT,code);
    }

    @Override
    public Object moneyBackTotal(String CON_CREDDEBRT,String code) {
        return mapper.moneyBackTotal(CON_CREDDEBRT,code);
    }

    @Override
    public Object answerMoneyTotal(String CON_CREDDEBRT,String code) {

        return mapper.answerMoneyTotal(CON_CREDDEBRT,code);
    }

    @Override
    public Object findAddToContract2(String con_creddebrt,String code) {
        return mapper.findAddToContract2(con_creddebrt,code);
    }

    @Override
    public Object findcoreignCurrency(String con_creddebrt, String code) {

        return mapper.findcoreignCurrency(con_creddebrt,code);
    }

    @Override
    public Object findCarryForward(String code) {
        return mapper.findCarryForward(code);
    }

    @Override
    public Object findTotalOverdue(String code) {
        return mapper.findTotalOverdue(code);
    }

    @Override
    public List<YearTotalVo> deferredCollection(String code) {
        return mapper.deferredCollection(code);
    }

    @Override
    public List<YearCompanyVo> findYearCompanyVo(String code) {
        return mapper.findYearCompanyVo();
    }

    @Override
    public List<YearMoneyTotal> ContractTotal(String code) {
        return mapper.ContractTotal(code);
    }

    @Override
    public List<YearMoneyTotal> newContractTotal(String code) {
        return mapper.newContractTotal(code);
    }

    @Override
    public List<DimUnit> getOrganize() {
        return mapper.getOrganize();
    }

    @Override
    public YearContractedVolumeVo yearTotal2(String code) {


        YearContractedVolumeVo yearContractedVolumeVo = new YearContractedVolumeVo();
        List<String>  companyList=new ArrayList<>();
        List<YearCompanyVo> yearCompanyVo = mapper.findYearCompanyVo();



        for (YearCompanyVo companyVo : yearCompanyVo) {
            companyList.add(companyVo.getOrgName());
        }

        List<List<Object>> companyDetailed=new ArrayList<>();

        companyDetailed.add(mapper.modular2(code));
        companyDetailed.add(mapper.modular3(code));
        companyDetailed.add(mapper.modular5(code));
        companyDetailed.add(mapper.modular6(code));

        /**
         * 进行单位转换
         */
        List<List<Object>> listList=new ArrayList<>();

        for (List<Object> list : companyDetailed) {

            List<Object> list1 = new ArrayList<>();

            for (Object o : list) {
                double d = DefaultLoadUtil.toMillion(Long.valueOf(String.valueOf(o)).longValue());
                list1.add(d);
            }

            listList.add(list1);
        }
        listList.add(mapper.modular4(code));
        listList.add(mapper.modular1(code));

        Map<String,List<Object>> map=new LinkedHashMap();
        map.put("contract_quantity",listList.get(5));
        map.put("revenue_contract_amount",listList.get(0));
        map.put("expenditure_contract_amount",listList.get(1));
        map.put("new_contract_year",listList.get(4));
        map.put("accumulated_receivable_amount",listList.get(2));
        map.put("contract_cumulative",listList.get(3));


        yearContractedVolumeVo.setCompanyList(companyList);
        yearContractedVolumeVo.setCompanyDetailed(map);
        return yearContractedVolumeVo;
    }

}
