package com.casi.project.system.service;

import java.util.List;
import java.util.Set;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.system.domain.SysRole;
import com.casi.project.system.domain.SysUser;

/**
 * 角色业务层
 * 
 * @author lzb
 */
public interface ISysRoleService
{
    /**
     * 根据条件分页查询角色数据
     * 
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    public List<SysRole> selectRoleList(SysRole role);


    /**
     *  根据登录用户获取角色列表
     */
    public List<SysRole> roleListByLogin();

    /**
     * 根据用户ID查询角色
     * 
     * @param userId 用户ID
     * @return 权限列表
     */
    public Set<String> selectRolePermissionByUserId(String userId);

    /**
     * 查询系统所有角色
     * 
     * @return 角色列表
     */
    public List<SysRole> selectRoleAll();

    /**
     * 根据用户ID获取角色选择框列表
     * 
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    public List<String> selectRoleListByUserId(String userId);

    /**
     * 通过角色ID查询角色
     * 
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    public SysRole selectRoleById(String roleId);

    SysRole queryApplyRoleOneByRoleId(String roleId);

    /**
     * 校验角色名称是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    public String checkRoleNameUnique(SysRole role);

    /**
     * 校验角色编码是否唯一
     */
    public String checkRoleCodeUnique(String roleCode);

    /**
     * 校验角色名称是否唯一
     */
    public String checkRoleNameUnique(String roleName);

    /**
     * 校验角色权限是否唯一
     * 
     * @param role 角色信息
     * @return 结果
     */
    public String checkRoleKeyUnique(SysRole role);

    /**
     * 校验角色是否允许操作
     * 
     * @param role 角色信息
     */
    public void checkRoleAllowed(SysRole role);

    public AjaxResult checkRoleAllowedAjax(SysRole role);

    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int countUserRoleByRoleId(String roleId);

    /**
     * 新增保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int insertRole(SysRole role);

    public AjaxResult addDeveloper(String[] userIds);

    /**
     * 新增应用角色
     */
    public int addApplyRole(SysRole role);

    /**
     * 修改保存角色信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int updateRole(SysRole role);

    /**
     * 修改应用角色
     * @param role
     * @return
     */
    public int editApplyRole(SysRole role);



    /**
     * 修改角色状态
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int updateRoleStatus(SysRole role);

    /**
     * 修改数据权限信息
     * 
     * @param role 角色信息
     * @return 结果
     */
    public int authDataScope(SysRole role);

    /**
     * 通过角色ID删除角色
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleById(String roleId);



    /**
     * 批量删除角色信息
     * 
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    public int deleteRoleByIds(String[] roleIds);

    int addRole(SysRole role,String deptId);

    /**
     * 判断用户是否有权限操作当前
     */
    void isHavePerm(SysUser sysUser);
    void isHavePerm(String[] userIds);
    void isHavePerm(SysRole sysRole);
    AjaxResult isHavePermByRoleId(String[] roleIds);

    AjaxResult usersLinkedRole(String roleId,String[] userIds);

    SysRole selectApplyRoleById(String roleId);

    String[] checkApplyResourcesById(String roleId);

    /**
     * 根据角色的id查询应用id
     *
     * @param roleId
     * @return
     */
    String[] selectApplyId(String roleId);

    /**
     * 根据角色的id查询菜单的id
     */
    public String[] selectByRoleId(String roleId);

}
