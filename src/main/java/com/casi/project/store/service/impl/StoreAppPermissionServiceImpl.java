package com.casi.project.store.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppPermissionMapper;
import com.casi.project.store.domain.StoreAppPermission;
import com.casi.project.store.service.IStoreAppPermissionService;

/**
 * 应用权限Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppPermissionServiceImpl implements IStoreAppPermissionService 
{
    @Autowired
    private StoreAppPermissionMapper storeAppPermissionMapper;

    /**
     * 查询应用权限
     * 
     * @param uuid 应用权限ID
     * @return 应用权限
     */
    @Override
    public StoreAppPermission selectStoreAppPermissionById(String uuid)
    {
        return storeAppPermissionMapper.selectStoreAppPermissionById(uuid);
    }

    /**
     * 查询应用权限列表
     * 
     * @param storeAppPermission 应用权限
     * @return 应用权限
     */
    @Override
    public List<StoreAppPermission> selectStoreAppPermissionList(StoreAppPermission storeAppPermission)
    {
        return storeAppPermissionMapper.selectStoreAppPermissionList(storeAppPermission);
    }

    /**
     * 新增应用权限
     * 
     * @param storeAppPermission 应用权限
     * @return 结果
     */
    @Override
    public int insertStoreAppPermission(StoreAppPermission storeAppPermission)
    {
        return storeAppPermissionMapper.insertStoreAppPermission(storeAppPermission);
    }

    /**
     * 修改应用权限
     * 
     * @param storeAppPermission 应用权限
     * @return 结果
     */
    @Override
    public int updateStoreAppPermission(StoreAppPermission storeAppPermission)
    {
        return storeAppPermissionMapper.updateStoreAppPermission(storeAppPermission);
    }

    /**
     * 批量删除应用权限
     * 
     * @param uuids 需要删除的应用权限ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppPermissionByIds(String[] uuids)
    {
        return storeAppPermissionMapper.deleteStoreAppPermissionByIds(uuids);
    }

    /**
     * 删除应用权限信息
     * 
     * @param uuid 应用权限ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppPermissionById(String uuid)
    {
        return storeAppPermissionMapper.deleteStoreAppPermissionById(uuid);
    }
}
