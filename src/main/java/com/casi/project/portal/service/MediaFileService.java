package com.casi.project.portal.service;


import com.casi.project.portal.domain.MediaBase;
import com.casi.project.portal.domain.vo.MediaBaseForFileVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Description:
 */
public interface MediaFileService {

    /**
     * @Description: 素材列表获取
     * @param mediaBase
     * @return boolean
     */
    public List<MediaBaseForFileVo> queryList(MediaBase mediaBase);

    /**
     * @Description: 素材详细信息获取
     * @param id
     * @return boolean
     */
    public MediaBase queryDetail(Long id);

    /**
     * @Description: 保存素材
     * @param mediaBase 素材信息， type 保存或提交 ，request 登录用户信息
     * @return boolean
     */
    public boolean add(MediaBase mediaBase, HttpServletRequest request, String type) throws Exception;

    /**
     * @Description: 删除素材
     * @param
     * @return boolean
     */
    public boolean delete(MediaBase mediaBase) throws Exception;

    /**
     * @Description: 添加备注
     * @param
     * @return boolean
     */
    public boolean remark(MediaBase mediaBase);

}
