package com.casi.project.screen.domain;

import lombok.Data;

/**
 * @Auther shp
 * @data2020/5/1023:49
 * @description
 */

@Data
public class YearMoneyTotal {

    private String conUnitcode;
    private String orgName;
    private Long total;
    private Long conConvalue;
    private Long actAmount;


}
