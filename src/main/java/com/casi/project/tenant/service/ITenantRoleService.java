package com.casi.project.tenant.service;

import java.util.List;
import com.casi.project.tenant.domain.TenantRole;

/**
 * 租户角色Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface ITenantRoleService 
{
    /**
     * 查询租户角色
     * 
     * @param uuid 租户角色ID
     * @return 租户角色
     */
    public TenantRole selectTenantRoleById(String uuid);

    /**
     * 查询租户角色列表
     * 
     * @param tenantRole 租户角色
     * @return 租户角色集合
     */
    public List<TenantRole> selectTenantRoleList(TenantRole tenantRole);

    /**
     * 新增租户角色
     * 
     * @param tenantRole 租户角色
     * @return 结果
     */
    public int insertTenantRole(TenantRole tenantRole);

    /**
     * 修改租户角色
     * 
     * @param tenantRole 租户角色
     * @return 结果
     */
    public int updateTenantRole(TenantRole tenantRole);

    /**
     * 批量删除租户角色
     * 
     * @param uuids 需要删除的租户角色ID
     * @return 结果
     */
    public int deleteTenantRoleByIds(String[] uuids);

    /**
     * 删除租户角色信息
     * 
     * @param uuid 租户角色ID
     * @return 结果
     */
    public int deleteTenantRoleById(String uuid);
}
