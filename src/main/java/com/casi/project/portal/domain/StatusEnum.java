package com.casi.project.portal.domain;

/**
 * @Description:状态枚举值
 * @author zhongzj
 * @date 2019/11/19
 */

public enum StatusEnum {
	/**
	 * 待提交
	 */
	TO_SUBMIT("1"),//TS
	/**
	 * 审核中
	 */
	UNDER_REVIEW("2"),
	/**
	 * 已审核
	 */
	PASS_REVIEW("3"),
	/**
	 * 已发布
	 */
	PUBLISHED("4"),
	/**
	 * 已退回
	 */
	REJECT("5"),
	/**
	 * 待办
	 */
	TO_DO("todo"),
	/**
	 * 已办
	 */
	DONE("done");

	private String name;

	StatusEnum(String name) {
	     this.name = name;
	}
	
	@Override
	public String toString(){
		return this.name;
	}
}


