package com.casi.project.system.mapper;

import java.util.List;
import java.util.Map;

import com.casi.project.portal.domain.thirdParty.UserInfo;
import com.casi.project.system.domain.UserViewManage;
import org.apache.ibatis.annotations.Param;

import com.casi.project.system.domain.SysUser;

/**
 * 用户表 数据层
 *
 * @author lzb
 */
public interface SysUserMapper
{
    /**
     * 根据条件分页查询用户列表
     *
     * @param sysUser 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserList(SysUser sysUser);

    public List<SysUser> noDisabledList(@Param("deptId") String deptId,@Param("applyId")String applyId);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    public SysUser selectUserByUserName(String userName);

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUserById(String userId);

    public List<SysUser> selectUserByIds(String[] userId);

    /**
     * 新增用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int insertUser(SysUser user);

    /**
     * 修改用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUser(SysUser user);

    /**
     * 锁定状态更改
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserIsLock(SysUser user);

    /**
     * 修改用户头像
     *
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public int updateUserAvatar(@Param("userName") String userName, @Param("avatar") String avatar);

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetUserPwd(@Param("userName") String userName, @Param("password") String password);

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    public int deleteUserById(String userId);

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteUserByIds(String[] userIds);

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    public int checkUserNameUnique(String userName);

    /**
     * 校验用户编码是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    public int checkUserCodeUnique(String userCode);

    /**
     * 校验手机号码是否唯一
     *
     * @param phonenumber 手机号码
     * @return 结果
     */
    public SysUser checkPhoneUnique(String phonenumber);

    /**
     * 校验email是否唯一
     *
     * @param email 用户邮箱
     * @return 结果
     */
    public int checkEmailUnique(String email);

    /**
     * 离职状态更改
     */
    int resignation(SysUser user);
    /**
     * 根据用户名查询userName
     */
    public SysUser selectByUserName(String userName);
    /**
     * 数据同步根据uid删除用户
     */
    public int deleteSysUserOne(String uid);
    /**
     * 根据传过来的用户id查询也就是uid查询
     */
    public SysUser selectSysUserByUid(String uid);

    /**
     * 员工视图创建
     */
    int userViewAdd(UserViewManage userViewManage);

    /**
     * 员工视图展示
     */
    List<UserViewManage> userViewList(String userId);

    /**
     * 视图内容展示
     */
    UserViewManage viewContentDisplay(String viewId);

    /**
     * 通过视图找员工
     */
    List<SysUser> findViewUserList(String[] userIds);


    /**
     * 员工视图修改
     */
    int userViewEdit(UserViewManage userViewManage);

    /**
     * 员工视图删除
     */
    int userViewDelete(String userViewId);

    /**
     * 获取组织父id
     */
    String selectOrganPid(String deptId);

    /**
     * 获取组织列表
     */
    String[] selectDeptIds(String deptId);

    /**
     * 获取组织父id
     */
    List<SysUser> selectUserListByDeptIds(String deptIds);
    /**
     * 根据组织id查询用户
     */
    List<Map<String,Object>> selectByDeptId(String deptId);
    /**
     * 根据部门id查看用户
     */
    List<String> selectIdsByDeptId(String deptId);


    List<SysUser> selectDeveloperList();

    int addThirdPartyInfo(@Param("appUserName") String appUserName, @Param("appKey") String appKey, @Param("appSecret")String appSecret, @Param("admin")Boolean admin, @Param("userInfo")UserInfo userInfo);

    List<SysUser> linkedRoleList(String roleId);

    int updateByPrimaryKeySelective(SysUser record);

    int insertSelective(SysUser record);

    SysUser selectByUserId(String  userId);
    /**
     * 查询未授权的人员信息列表
     *
     * 根据应用的id查询未授权此应用的用户信息
     * 应为应用授权了组织的同时，也会将这个应用赋值给这个部门下面的所有人，同时加入到应用与用户的关联表里面
     * 那么问题来了  如果已经授权了这个部门之后，这个部门又来人了那么这个人的信息在应用与用户关联表中是没有的
     */
    public List<Map<String,Object>> selectNotImpowerUsers(@Param("applyId") String applyId,@Param("userIds")String[] userIds);

    /**
     * 该应用没有给部门授权，那么只需要查询应用与用户的关联表即可
     *
     */
    public List<Map<String,Object>> selectNotImpowerUsers1(String applyId);
    /**
     * 根据应用的id查询已经授权的用户的信息，此时是该应用授权了组织机构
     */
    public List<Map<String,Object>> selectImpowerUsers(@Param("applyId")String applyId,@Param("userIds")String[] userIds);
    /**
     * 根据应用的id查询已经授权的用户的信息，此时该应用没有授权组织结构,那么只需要查询应用与用户表就可以了
     */
    public List<Map<String,Object>> selectImpowerUsers1(String applyId);
    /**
     * 根据部门的id查询出用户的id
     */
    public String[] selectUserGetUserIdsByDeptIds(@Param("deptIds") String[] deptIds);
    /**
     * 根据应用的id查询应用与用户关联表   查询出用户解除该应用的用户id
     */
    public String[] selectMobileApplyUsersGetUserIdsbyApplyId(String ApplyId);
    /**
     * 应用授权查询未授权的人  且应用授权了部门  但是部门下面没有人
     */
    public List<Map<String,Object>> selectUnauthorizedUsers();
}
