package com.casi.project.portal.controller;

import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.StringUtils;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.MobileNotice;
import com.casi.project.portal.domain.UnreadStatus;
import com.casi.project.portal.domain.vo.NoticeVO;
import com.casi.project.portal.service.NoticeService;
import com.casi.project.system.domain.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Security;
import java.util.ArrayList;

@Api(tags = { "公告管理" })
@RestController
@RequestMapping("/app/notice")
public class NoticeController extends BaseController {


    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);
    @Autowired
    private NoticeService noticeService;

    /**
     * 查询公告列表  /app/portal/notice/getNoticeList
     *
     * @param
     * @return
     */

    @ApiOperation(value = "查询公告列表")
    @GetMapping("/getNoticeList")
    public AjaxResult getNoticeList() {
        logger.info("===============查询公告列表=============");
        return noticeService.getNoticeList();
    }

    /**
     * 根据ID查询公告具体详情
     *
     * @param
     * @return
     */
    //@RequestMapping(value = "/getNoticeId", method = RequestMethod.GET)
    //@CrossOrigin
    @ApiOperation(value = "根据ID查询公告具体详情")
    @PostMapping("/getNoticeId")
    public AjaxResult getNoticeId(String id) {
        logger.info("===============根据ID查询公告具体详情=============");
        if (null == id) {
            return AjaxResult.error("ID不能为空");
        }
        MobileNotice vo = new MobileNotice();
        try {
            vo = noticeService.getNoticeId(id);
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.error("查询失败");
        }
        logger.info("查询成功=======" + vo);
        return AjaxResult.success("查询成功", vo);
    }

    /**
     * 新增公告已读未读
     * 0未读 默认 1已读
     *
     * @param
     * @return
     */
    @ApiOperation(value = "新增公告已读未读")
    @PostMapping(value = "/addUnreadStatus")
    @CrossOrigin
    public AjaxResult addUnreadStatus(@RequestBody UnreadStatus unreadStatus) {
        logger.info("===============新增公告已读未读=============");
        if (null == unreadStatus.getNoticeId()) {
            return AjaxResult.error("请传公告noticeId");
        }
        AjaxResult ajaxResult = null;
        try {
            SysUser loginUser = SecurityUtils.getLoginUser().getUser();
            if (StringUtils.isNull(loginUser.getUserId())) {
                return AjaxResult.error("没有当前登录用户");
            }
            unreadStatus.setUserId(loginUser.getUserId());
            ajaxResult = noticeService.addUnreadStatus(unreadStatus);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
        }
        return ajaxResult;
    }

//    /**
//     * 查询公告是否已读状态
//     * 0未读 默认
//     * 1已读
//     *
//     * @param noticeId
//     * @param
//     * @return
//     */
//    @GetMapping("/getUnreadStatus")
//    @CrossOrigin
//    public AjaxResult getUnreadStatus(String noticeId) {
//        logger.info("===============查询公告是否已读状态=============");
//        if (null == noticeId) {
//            return AjaxResult.error("请传公告noticeId");
//        }
//        UnreadStatus unreadStatus;
//        try {
//            SysUser loginUser = SecurityUtils.getLoginUser().getUser();
//            if (StringUtils.isNull(loginUser)) {
//                return AjaxResult.error("没有当前登录用户");
//            }
//            unreadStatus = noticeService.getUnreadStatus(noticeId, loginUser.getUserId());
//        } catch (Exception e) {
//            e.printStackTrace();
//            logger.error(e.toString(), e);
//            return AjaxResult.error("查询失败");
//        }
//        logger.info("查询成功========" + unreadStatus);
//        return AjaxResult.success("查询成功", unreadStatus);
//    }
}
