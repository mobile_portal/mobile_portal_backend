package com.casi.project.tenant.service.impl;

import java.util.List;
import com.casi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.tenant.mapper.TenantCompanyMapper;
import com.casi.project.tenant.domain.TenantCompany;
import com.casi.project.tenant.service.ITenantCompanyService;

/**
 * 租户企业Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class TenantCompanyServiceImpl implements ITenantCompanyService 
{
    @Autowired
    private TenantCompanyMapper tenantCompanyMapper;

    /**
     * 查询租户企业
     * 
     * @param uuid 租户企业ID
     * @return 租户企业
     */
    @Override
    public TenantCompany selectTenantCompanyById(String uuid)
    {
        return tenantCompanyMapper.selectTenantCompanyById(uuid);
    }

    /**
     * 查询租户企业列表
     * 
     * @param tenantCompany 租户企业
     * @return 租户企业
     */
    @Override
    public List<TenantCompany> selectTenantCompanyList(TenantCompany tenantCompany)
    {
        return tenantCompanyMapper.selectTenantCompanyList(tenantCompany);
    }

    /**
     * 新增租户企业
     * 
     * @param tenantCompany 租户企业
     * @return 结果
     */
    @Override
    public int insertTenantCompany(TenantCompany tenantCompany)
    {
        tenantCompany.setCreateTime(DateUtils.getNowDate());
        return tenantCompanyMapper.insertTenantCompany(tenantCompany);
    }

    /**
     * 修改租户企业
     * 
     * @param tenantCompany 租户企业
     * @return 结果
     */
    @Override
    public int updateTenantCompany(TenantCompany tenantCompany)
    {
        tenantCompany.setUpdateTime(DateUtils.getNowDate());
        return tenantCompanyMapper.updateTenantCompany(tenantCompany);
    }

    /**
     * 批量删除租户企业
     * 
     * @param uuids 需要删除的租户企业ID
     * @return 结果
     */
    @Override
    public int deleteTenantCompanyByIds(String[] uuids)
    {
        return tenantCompanyMapper.deleteTenantCompanyByIds(uuids);
    }

    /**
     * 删除租户企业信息
     * 
     * @param uuid 租户企业ID
     * @return 结果
     */
    @Override
    public int deleteTenantCompanyById(String uuid)
    {
        return tenantCompanyMapper.deleteTenantCompanyById(uuid);
    }
}
