package com.casi.project.system.service.impl;

import com.casi.project.system.domain.SysWhiteIp;
import com.casi.project.system.mapper.SysWhiteIpMapper;
import com.casi.project.system.service.ISysWhiteIpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 白名单管理Service业务层处理
 *
 * @author casi
 * @date 2020-06-16
 */
@Service
public class SysWhiteIpServiceImpl implements ISysWhiteIpService {
    @Autowired
    private SysWhiteIpMapper sysWhiteIpMapper;

    /**
     * 查询白名单管理
     *
     * @param uuid 白名单管理ID
     * @return 白名单管理
     */
    @Override
    public SysWhiteIp selectSysWhiteIpById(String uuid) {
        return sysWhiteIpMapper.selectSysWhiteIpById(uuid);
    }

    /**
     * 查询白名单管理列表
     *
     * @param sysWhiteIp 白名单管理
     * @return 白名单管理
     */
    @Override
    public List<SysWhiteIp> selectSysWhiteIpList(SysWhiteIp sysWhiteIp) {
        return sysWhiteIpMapper.selectSysWhiteIpList(sysWhiteIp);
    }

    /**
     * 新增白名单管理
     *
     * @param sysWhiteIp 白名单管理
     * @return 结果
     */
    @Override
    public int insertSysWhiteIp(SysWhiteIp sysWhiteIp) {
        return sysWhiteIpMapper.insertSysWhiteIp(sysWhiteIp);
    }

    /**
     * 修改白名单管理
     *
     * @param sysWhiteIp 白名单管理
     * @return 结果
     */
    @Override
    public int updateSysWhiteIp(SysWhiteIp sysWhiteIp) {
        return sysWhiteIpMapper.updateSysWhiteIp(sysWhiteIp);
    }

    /**
     * 批量删除白名单管理
     *
     * @param uuids 需要删除的白名单管理ID
     * @return 结果
     */
    @Override
    public int deleteSysWhiteIpByIds(List<String> uuids) {
        return sysWhiteIpMapper.deleteSysWhiteIpByIds(uuids);
    }

    /**
     * 删除白名单管理信息
     *
     * @param uuid 白名单管理ID
     * @return 结果
     */
    @Override
    public int deleteSysWhiteIpById(String uuid) {
        return sysWhiteIpMapper.deleteSysWhiteIpById(uuid);
    }

    @Override
    public SysWhiteIp getInfoByIp(String whiteIp) {
        return sysWhiteIpMapper.getInfoByIp(whiteIp);
    }
}
