package com.casi.project.portal.service.impl;


import com.casi.common.utils.file.FileUploadUtils;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.LocalFile;
import com.casi.project.portal.mapper.LocalFileMapper;
import com.casi.project.portal.service.UpFileService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Arrays;

/**
 * @Auther shp
 * @data2020/4/2017:45
 * @description
 */


@Service
@Transactional(rollbackFor = Exception.class)
public class UpFileServiceImpl implements UpFileService {


    @Override
    public AjaxResult uploadFile(MultipartFile file) {
        if (file.isEmpty()) {
            return AjaxResult.error("请上传一个文件");
        }
        AjaxResult ajaxResult = new AjaxResult();
        String upload = "";//输出一个相对地址
        try {
            //返回文件上传的地址
            upload = FileUploadUtils.upload(file);
            //截取掉 profile 路径
            upload = upload.replaceAll("/profile/", "");
            //传入数组，判断只能是图片格式
            String[] arrs = {"jpg", "jpeg", "png"};
            if (useList(arrs, upload.substring(upload.indexOf(".") + 1))) {
                long size = file.getSize() / 1024;
                if (size > 1024) {
                    return AjaxResult.error("图片大小不能超过1mb");
                }
            }

            if (upload.endsWith(".apk")) {
                ajaxResult.put("appRoIos", 1);
            }
            if (upload.endsWith(".IPA")) {
                ajaxResult.put("appRoIos", 2);
            }

            //返回文件名和文件路径
            ajaxResult.put("fileName", file.getOriginalFilename());
            ajaxResult.put("filePath", upload);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return ajaxResult;
    }


    @Resource
    private LocalFileMapper mapper;

    @Override
    public AjaxResult newUploadFile(MultipartFile file) {
        if (file.isEmpty()) {
            return AjaxResult.error("请上传一个文件");
        }

        LocalFile localFile = new LocalFile();

        AjaxResult ajaxResult = new AjaxResult();

        String upload = "";//输出一个相对地址

        try {
            upload = FileUploadUtils.upload(file);
            String[] arrs = {"jpg", "jpeg", "png"};
            if (useList(arrs, upload.substring(upload.indexOf(".") + 1))) {
                long size = file.getSize() / 1024;
                if (size > 1024) {
                    return AjaxResult.error("图片大小不能超过1mb");
                }
                localFile.setSize((int) size);
                localFile.setFileName(file.getOriginalFilename());
                localFile.setFileUrl(upload);
                localFile.setName(file.getName());

                Integer id = mapper.addFile(localFile);
                ajaxResult.put("file_id", localFile.getId());//获取当前主键id

            }

            ajaxResult.put("fileName", file.getOriginalFilename());
            ajaxResult.put("filePath", upload);

        } catch (IOException e) {
            e.printStackTrace();
            return AjaxResult.error("请检查代码");
        }

        return ajaxResult;
    }


    /**
     * 传入后缀数组和数据集合
     * 判断数据集合中的数据是否包含数组里的后缀
     * @param arr
     * @param targetValue
     * @return
     */
    public static boolean useList(String[] arr, String targetValue) {
        return Arrays.asList(arr).contains(targetValue);
    }

}

