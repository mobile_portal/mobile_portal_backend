package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreCategory;
import com.casi.project.store.service.IStoreCategoryService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用分类Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用分类"})
@RestController
@RequestMapping("/store/category")
public class StoreCategoryController extends BaseController
{
    @Autowired
    private IStoreCategoryService storeCategoryService;

    /**
     * 查询应用分类列表
     */
    @ApiOperation(value = "查询应用分类列表")
    @PreAuthorize("@ss.hasPermi('store:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreCategory storeCategory)
    {
        startPage();
        List<StoreCategory> list = storeCategoryService.selectStoreCategoryList(storeCategory);
        return getDataTable(list);
    }

    /**
     * 导出应用分类列表
     */
    @PreAuthorize("@ss.hasPermi('store:category:export')")
    @Log(title = "应用分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreCategory storeCategory)
    {
        List<StoreCategory> list = storeCategoryService.selectStoreCategoryList(storeCategory);
        ExcelUtil<StoreCategory> util = new ExcelUtil<StoreCategory>(StoreCategory.class);
        return util.exportExcel(list, "category");
    }

    /**
     * 获取应用分类详细信息
     */
    @ApiOperation(value = "获取应用分类详细信息")
    @PreAuthorize("@ss.hasPermi('store:category:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeCategoryService.selectStoreCategoryById(uuid));
    }

    /**
     * 新增应用分类
     */
    @ApiOperation(value = "新增应用分类")
    @PreAuthorize("@ss.hasPermi('store:category:add')")
    @Log(title = "应用分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreCategory storeCategory)
    {
        return toAjax(storeCategoryService.insertStoreCategory(storeCategory));
    }

    /**
     * 修改应用分类
     */
    @ApiOperation(value = "修改应用分类")
    @PreAuthorize("@ss.hasPermi('store:category:edit')")
    @Log(title = "应用分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreCategory storeCategory)
    {
        return toAjax(storeCategoryService.updateStoreCategory(storeCategory));
    }

    /**
     * 删除应用分类
     */
    @ApiOperation(value = "删除应用分类")
    @PreAuthorize("@ss.hasPermi('store:category:remove')")
    @Log(title = "应用分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeCategoryService.deleteStoreCategoryByIds(uuids));
    }
}
