package com.casi.project.interfaces.domain;

import java.util.Date;

public class ApiInvok {
    private Long id;

    private String apiUuid;

    private String appKey;

    private String appSecret;

    private String invokUrl;

    private String invokName;

    private Date createTime;

    private String applyId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApiUuid() {
        return apiUuid;
    }

    public void setApiUuid(String apiUuid) {
        this.apiUuid = apiUuid == null ? null : apiUuid.trim();
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey == null ? null : appKey.trim();
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret == null ? null : appSecret.trim();
    }

    public String getInvokUrl() {
        return invokUrl;
    }

    public void setInvokUrl(String invokUrl) {
        this.invokUrl = invokUrl == null ? null : invokUrl.trim();
    }

    public String getInvokName() {
        return invokName;
    }

    public void setInvokName(String invokName) {
        this.invokName = invokName == null ? null : invokName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getApplyId() {
        return applyId;
    }

    public void setApplyId(String applyId) {
        this.applyId = applyId == null ? null : applyId.trim();
    }
}