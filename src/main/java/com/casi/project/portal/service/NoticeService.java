package com.casi.project.portal.service;


import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.MobileNotice;
import com.casi.project.portal.domain.UnreadStatus;
import com.casi.project.portal.domain.vo.NoticeVO;

import java.util.ArrayList;

/**
 * 公告接口
 */
public interface NoticeService {

    /**
     * 查询公告列表
     *
     * @return
     */
    public AjaxResult getNoticeList();


    /**
     * 根据ID查询公告具体详情
     *
     * @param
     * @return
     */
    public MobileNotice getNoticeId(String id) throws Exception;

    /**
     * 新增公告已读未读
     * unreadStatus
     *
     * @return
     */
    AjaxResult addUnreadStatus(UnreadStatus unreadStatus) throws Exception;

//    /**
//     * @param noticeId
//     * @param userId
//     * @return
//     * @throws Exception
//     */
//    UnreadStatus getUnreadStatus(String noticeId, String userId) throws Exception;
}
