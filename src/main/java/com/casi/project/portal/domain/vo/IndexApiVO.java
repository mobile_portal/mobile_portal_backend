package com.casi.project.portal.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther shp
 * @data2020/4/1510:28
 * @description
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndexApiVO {

    private Long id ;
    private String menuName;
    private String skipUrl;
    private String menuButtonName;
    private String menuButtonIcon;
    private String menuButtonUrl;

}
