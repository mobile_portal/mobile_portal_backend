package com.casi.project.store.service.impl;

import java.util.List;
import com.casi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreTagMapper;
import com.casi.project.store.domain.StoreTag;
import com.casi.project.store.service.IStoreTagService;

/**
 * 默认标签Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreTagServiceImpl implements IStoreTagService 
{
    @Autowired
    private StoreTagMapper storeTagMapper;

    /**
     * 查询默认标签
     * 
     * @param uuid 默认标签ID
     * @return 默认标签
     */
    @Override
    public StoreTag selectStoreTagById(String uuid)
    {
        return storeTagMapper.selectStoreTagById(uuid);
    }

    /**
     * 查询默认标签列表
     * 
     * @param storeTag 默认标签
     * @return 默认标签
     */
    @Override
    public List<StoreTag> selectStoreTagList(StoreTag storeTag)
    {
        return storeTagMapper.selectStoreTagList(storeTag);
    }

    /**
     * 新增默认标签
     * 
     * @param storeTag 默认标签
     * @return 结果
     */
    @Override
    public int insertStoreTag(StoreTag storeTag)
    {
        storeTag.setCreateTime(DateUtils.getNowDate());
        return storeTagMapper.insertStoreTag(storeTag);
    }

    /**
     * 修改默认标签
     * 
     * @param storeTag 默认标签
     * @return 结果
     */
    @Override
    public int updateStoreTag(StoreTag storeTag)
    {
        storeTag.setUpdateTime(DateUtils.getNowDate());
        return storeTagMapper.updateStoreTag(storeTag);
    }

    /**
     * 批量删除默认标签
     * 
     * @param uuids 需要删除的默认标签ID
     * @return 结果
     */
    @Override
    public int deleteStoreTagByIds(String[] uuids)
    {
        return storeTagMapper.deleteStoreTagByIds(uuids);
    }

    /**
     * 删除默认标签信息
     * 
     * @param uuid 默认标签ID
     * @return 结果
     */
    @Override
    public int deleteStoreTagById(String uuid)
    {
        return storeTagMapper.deleteStoreTagById(uuid);
    }
}
