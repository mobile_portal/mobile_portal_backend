package com.casi.project.portal.mapper;


import com.casi.project.portal.domain.MediaFile;

import java.util.List;

/**
 * @Description:基础素材管理mapper
 */
public interface MediaFileMapper {
    int insert(MediaFile mediaFile);

    int update(MediaFile mediaFile);

    int delete(Long id);

    MediaFile queryById(Long id);

    List<MediaFile> queryList(MediaFile mediaFile);
}
