package com.casi.project.portal.mapper;


import com.casi.project.portal.domain.MediaNews;

import java.util.List;

/**
 * @Description:基础素材管理mapper
 */
public interface MediaNewsMapper {
    int insert(MediaNews mediaNews);

    int update(MediaNews mediaNews);

    int delete(Long id);

    MediaNews queryById(Long id);

    List<MediaNews> queryList(MediaNews mediaNews);
}
