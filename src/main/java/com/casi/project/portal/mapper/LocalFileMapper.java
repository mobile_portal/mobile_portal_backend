package com.casi.project.portal.mapper;


import com.casi.project.portal.domain.LocalFile;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

/**
 * @Auther shp
 * @data2020/4/2414:34
 * @description
 */


@Mapper
public interface LocalFileMapper {

    @Options(useGeneratedKeys = true,keyProperty = "id",keyColumn = "id")
    @Insert("INSERT INTO local_file(file_name,file_url,size,name) VALUE(#{fileName},#{fileUrl},#{size},#{name});")
    Integer addFile(LocalFile localFile);

}
