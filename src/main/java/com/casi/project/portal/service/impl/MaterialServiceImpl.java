package com.casi.project.portal.service.impl;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.casi.project.portal.domain.*;
import com.casi.project.portal.mapper.*;
import com.casi.project.portal.service.MaterialService;
import com.casi.project.portal.util.CommonApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhongzj
 * @Description:素材发布服务类
 * @Create: 2019/11/25
 */
@Service
@Transactional
public class MaterialServiceImpl implements MaterialService {


    @Autowired
    private MediaBaseMapper baseMapper;
    @Autowired
    private MediaFileMapper mediaFileMapper;
    @Autowired
    private MaterialRecordMapper recordMapper;
    @Autowired
    private MediaNewsMapper newsMapper;
    @Autowired
    private MediaArticleMapper articleMapper;
    //@Autowired
    //private TerminalService terminalService;
    //@Autowired
    //private ProcessService processService;
    //@Autowired
    //private ProcessHardwareMapper hardwareMapper;

    //@Override
    //@Transactional(rollbackFor = {Exception.class,RuntimeException.class})
    //public boolean insert(Process process) throws Exception{
    //    boolean result = false;
    //    //Long baseId = processService.queryMaterialIdByPid(process.getPid());
    //    MediaBase mediaBase = baseMapper.queryById(baseId);
    //    //如果有微信公众号信息，进行微信公众号发布
    //    if (process.getTerminalType().contains(TerminalEnum.Wx.toString())){
    //        logger.info("开始进行微信公众号发布");
    //        List<String> wxIdList = Arrays.asList(process.getWxList() .split(",")).stream().map(s -> (s.trim())).collect(Collectors.toList());
    //        for (int i = 0; i < wxIdList.size(); i++) {
    //            AccountWx account = terminalService.queryWxById(Long.valueOf(wxIdList.get(i)));
    //            //logger.info("当前微信公众号为：" + account.getName() +"，appid：" + account.getAppid() + "，appsecert：" + account.getAppsecret());
    //            //根据appid和appsecert获取accessToken
    //            String accessToken = WeChatConfig.getToken(account.getAppid(),account.getAppsecret());
    //            if (null == accessToken || accessToken.isEmpty()){
    //                logger.error("微信公众号accessToken获取失败");
    //                throw new RuntimeException("微信公众号accessToken获取失败");
    //            }
    //            logger.info("微信公众号accessToken获取成功:" + accessToken);
    //            //1.根据本地该条素材信息调用微信api上传素材
    //            //2.上传素材成功后获取到返回的media_id
    //            String mediaId = null;
    //            if (MediaType.Image.toString().equals(mediaBase.getMediaType()) || MediaType.Voice.toString().equals(mediaBase.getMediaType())){
    //                MediaFile mediaFile = mediaFileMapper.queryById(mediaBase.getMediaId());
    //                JSONObject jsonObject = uploadPermanentMaterial(accessToken, mediaBase.getMediaType(),mediaFile.getLocalUrl() );
    //                if (jsonObject != null) {
    //                    if (jsonObject.getString("media_id") != null) {
    //                        mediaId = jsonObject.getString("media_id");
    //                        logger.info("新增素材成功:"+jsonObject.getString("media_id"));
    //                        //5.错误消息处理
    //                    } else {
    //                        int errCode = jsonObject.getInteger("errcode");
    //                        String errMsg = jsonObject.getString("errmsg");
    //                        logger.error("新增素材失败"+" errcode:"+errCode+", errmsg:"+errMsg);
    //                        throw new RuntimeException("上传素材至微信端失败");
    //                    }
    //                }
    //            }
    //            else if (MediaType.Video.toString().equals(mediaBase.getMediaType())){
    //                MediaFile mediaFile = mediaFileMapper.queryById(mediaBase.getMediaId());
    //                JSONObject jsonObject = uploadVedioMaterial(accessToken, mediaFile );
    //                //4.解析结果
    //                if (jsonObject != null) {
    //                    if (jsonObject.getString("media_id") != null) {
    //                        logger.info("新增视频素材成功:"+jsonObject.getString("media_id"));
    //                        mediaId = jsonObject.getString("media_id");
    //                        //5.错误消息处理
    //                    } else {
    //                        int errCode = jsonObject.getInteger("errcode");
    //                        String errMsg = jsonObject.getString("errmsg");
    //                        logger.error("新增视频素材失败"+" errcode:"+errCode+", errmsg:"+errMsg);
    //                        throw new RuntimeException("上传素材至微信端失败");
    //                    }
    //                }
    //            }
    //            else if (MediaType.News.toString().equals(mediaBase.getMediaType())){
    //                MediaNews news = newsMapper.queryById(mediaBase.getMediaId());
    //                JSONObject jsonObject = uploadPerNewsMaterial(accessToken, news);
    //                if (jsonObject != null) {
    //
    //                    //4.1 错误消息处理
    //                    if (jsonObject.getInteger("errcode") != null) {
    //                        int errCode = jsonObject.getInteger("errcode");
    //                        String errMsg = jsonObject.getString("errmsg");
    //                        logger.error("上传图文信息失败 "+"errcode:"+errCode+", errmsg:"+errMsg);
    //                        throw new RuntimeException("上传素材至微信端失败");
    //                        //4.2 新增成功
    //                    } else {
    //                        logger.info("上传图文信息成功 "+jsonObject.getString("media_id"));
    //                        mediaId = jsonObject.getString("media_id");
    //                    }
    //                }
    //            }
    //
    //            //3.根据media_id和token调用群发接口进行群发
    //            String msgId = null;
    //            logger.info("开始进行微信公众号群发");
    //            JSONObject sendObject = sendToTag(accessToken,mediaBase.getMediaType(), mediaId);
    //            //4.解析结果
    //            if (sendObject != null) {
    //                //4.群发成功获取msg_id
    //                if (sendObject.getString("msg_id") != null) {
    //                    logger.info("群发素材成功:"+sendObject.getString("msg_id"));
    //                    msgId = sendObject.getString("msg_id");
    //                    //5.错误消息处理
    //                } else {
    //                    int errCode = sendObject.getInteger("errcode");
    //                    String errMsg = sendObject.getString("errmsg");
    //                    logger.error("群发素材失败"+" errcode:"+errCode+", errmsg:"+errMsg);
    //                    throw new RuntimeException("上传素材至微信端失败");
    //                }
    //            }
    //
    //            //5.将msg_id及素材信息保存进material_record表中
    //            MaterialRecord materialRecord = new MaterialRecord();
    //            materialRecord.setBaseId(baseId);
    //            materialRecord.setAccountId(account.getId());
    //            materialRecord.setTerminalType("wxList");
    //            materialRecord.setMediaType(mediaBase.getMediaType());
    //            materialRecord.setWxMediaId(msgId);
    //            recordMapper.insert(materialRecord);
    //
    //            result = true;
    //        }
    //
    //    }
    //    //如果有移动平台公众号信息，进行移动平台公众号发布
    //    //if (process.getTerminalType().contains(TerminalEnum.Yd.toString())) {
    //    //    logger.info("开始进行移动平台公众号发布");
    //    //
    //    //    //具体发布功能待开发
    //    //
    //    //    result = true;
    //    //}
    //    //如果有其他终端信息，进行其他终端发布
    //    //if (process.getTerminalType().contains(TerminalEnum.Other.toString())) {
    //    //    logger.info("开始进行其他终端发布");
    //    //
    //    //    //具体发布功能待开发
    //    //
    //    //    result = true;
    //    //}
    //    //如果有硬件终端信息，进行硬件终端发布
    //    //if (process.getTerminalType().contains(TerminalEnum.Hardware.toString())) {
    //    //    //logger.info("开始进行硬件终端发布");
    //    //
    //    //    //具体发布功能待开发
    //    //
    //    //    //发布成功，将发布的终端id进行排序储存
    //    //    //List<String> hardwareIdList = Arrays.asList(process.getHardwareList() .split(",")).stream().map(s -> (s.trim())).collect(Collectors.toList());
    //    //    for (int i = 0; i < hardwareIdList.size(); i++) {
    //    //        ProcessHardware hardware = new ProcessHardware();
    //    //        hardware.setTerminalId(Integer.valueOf(hardwareIdList.get(i)));
    //    //        hardware.setMaterialId(mediaBase.getId().intValue());
    //    //        //hardwareMapper.insert(hardware);
    //    //    }
    //    //    result = true;
    //    //
    //    //}
    //
    //
    //    return result;
    //}

    /**
     * @desc ： 4.新增永久素材——新增其他类型永久素材(image、voice、thumb)
     *
     * @param accessToken  调用接口凭证
     * @param type 媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
     * @param fileDir 本地图片路径
     *
     * @return
     * media_id    新增的永久素材的media_id
     * url    新增的图片素材的图片URL（仅新增图片素材时会返回该字段）
     *
     * @throws Exception String
     */
    public JSONObject uploadPermanentMaterial(String accessToken, String type, String fileDir) throws Exception {
        //1.创建本地文件
        File file=new File(fileDir);

        //2.拼接请求url
        String url = CommonApi.getUploadMediaUrl(accessToken,type);

        //3.调用接口，发送请求，上传文件到微信服务器
        //JSONObject jsonObject= HttpUtil.uploadMedia(url, file);
        //logger.info("JsonObject:"+jsonObject.toJSONString());

        return null;
    }


    /**
     * 上传video永久素材
     * 返回格式
     * {
     * "media_id":MEDIA_ID
     * }
     *
     * @param accessToken
     * @param
     * @return
     */
    private JSONObject uploadVedioMaterial(String accessToken, MediaFile mediaFile) throws Exception {

        //1.创建本地文件
        File file=new File(mediaFile.getLocalUrl());

        Map<String,String> params=new HashMap<>();
        JSONObject json=new JSONObject();
        json.put("title", mediaFile.getTitle());
        json.put("introduction", mediaFile.getIntroduction());
        params.put("description", json.toJSONString());

        //2.拼接请求url
        String url = CommonApi.getUploadMediaUrl(accessToken, MediaType.Video.toString());

        //3.调用接口，发送请求，上传文件到微信服务器
        //JSONObject jsonObject=HttpUtil.uploadMediaParam(url, file,params);
        //logger.info("JsonObject:"+jsonObject.toJSONString());


        return null;
    }

    /**
     * @desc ：上传永久图文素材
     *
     * @param accessToken 调用接口凭证
     * @return
     *
     * @throws Exception JSONObject
     */
    public JSONObject uploadPerNewsMaterial(String accessToken, MediaNews news) throws Exception {
        //1.准备好json请求参数
        JSONArray jsonArr = new JSONArray();
        JSONObject jsonObj = new JSONObject();
        // 上传图片素材
        for (MediaArticle article : news.getArticleList()) {
            article.getPicUrl();
            JSONObject img = uploadPermanentMaterial(accessToken, MediaEnum.Image.toString(),article.getPicUrl());
            String imgMediaId = img.getString("media_id");
            jsonObj.put("thumb_media_id", imgMediaId);
            if (article.getAuthor() != null) {
                jsonObj.put("author", article.getAuthor());
            } else {
                jsonObj.put("author", "");
            }
            if (article.getTitle() != null) {
                jsonObj.put("title", article.getTitle());
            } else {
                jsonObj.put("title", "");
            }

            jsonObj.put("content_source_url", "");
            if (article.getDigest() != null) {
                jsonObj.put("digest", article.getDigest());
            } else {
                jsonObj.put("digest", "");
            }
            jsonObj.put("show_cover_pic", article.getShowCoverPic());
            jsonObj.put("content", article.getContent());
            jsonArr.add(jsonObj);
        }

        JSONObject data = new JSONObject();
        data.put("articles", jsonArr);
        //logger.info("上传图文消息所传参数为："+data);

        //2.准备好请求url
        String url=CommonApi.getUploadNewsUrl(accessToken);

        //3.发起HTTP请求，获取返回结果
        //JSONObject jsonObject=HttpUtil.doPost(url, data);
        //logger.info("jsonObject:"+jsonObject.toJSONString());

        return null;
    }

    /**
     * 根据Tag进行群发消息
     *
     * @param accessToken
     * @return
     */
    public JSONObject sendToTag(String accessToken, String type, String mediaId) throws Exception {
        //1.准备好json请求参数
        JSONObject data = new JSONObject();
        JSONObject filter = new JSONObject();
        JSONObject media = new JSONObject();

        media.put("media_id",mediaId);
        filter.put("is_to_all",true);//用于设定是否向全部用户发送，值为true或false，选择true该消息群发给所有用户，选择false可根据tag_id发送给指定群组的用户
        data.put("filter",filter);

        //根据素材类型封装不同json
        //图文素材
        if (MediaEnum.News.toString().equals(type)){
            data.put("mpnews",media);
            data.put("msgtype","mpnews");
            data.put("send_ignore_reprint",1);//图文消息被判定为转载时，是否继续群发。 1为继续群发（转载），0为停止群发。 该参数默认为0。
        }
        //图片素材
        if (MediaEnum.Image.toString().equals(type)){
            data.put("image",media);
            data.put(type,media);
            data.put("msgtype",type);
        }
        //语音素材
        if (MediaEnum.Voice.toString().equals(type)){
            data.put("voice",media);
            data.put(type,media);
            data.put("msgtype",type);
        }
        //视频素材
        if (MediaEnum.Video.toString().equals(type)){
            data.put("video",media);
            data.put("mpvideo",media);
            data.put("msgtype","mpvideo");
        }
        //文字素材
        if (type.equals("text")){
            data.put("text","微信公众号群发功能测试");
            data.put("msgtype","text");
        }

        //2.准备好请求url
        String url=CommonApi.getMassSendUrl(accessToken);

        //logger.info("微信公众号群发参数为：" + data.toString());
        //JSONObject jsonObject = HttpUtil.doPost(url, data);
        return null;
    }

    @Override
    public boolean insert(Process process) throws Exception {
        return false;
    }
}
