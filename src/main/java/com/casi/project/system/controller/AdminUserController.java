package com.casi.project.system.controller;

import com.alibaba.fastjson.JSONObject;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.system.annotation.JwtIgnore;
import com.casi.project.system.common.response.Result;
import com.casi.project.system.domain.Audience;

import com.casi.project.system.util.JwtTokenUtil;
import io.jsonwebtoken.Claims;
import io.swagger.models.Model;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

/**
 * ========================
 * Created with IntelliJ IDEA.
 * User：pyy
 * Date：2019/7/18 10:41
 * Version: v1.0
 * ========================
 */
@Slf4j
@RestController
@RequestMapping("/JWT")
public class AdminUserController extends BaseController {
    @Autowired
    private Audience audience;
    @PostMapping("/loginJWT")
    @JwtIgnore
    public Result adminLogin(HttpServletResponse response, String username, String password) {
        // 这里模拟测试, 默认登录成功，返回用户ID和角色信息
        String userId = UUID.randomUUID().toString();
        String role = "admin";

        // 创建token
        String token = JwtTokenUtil.createJWT(userId, username, role, audience);
        log.info("### 登录成功, token={} ###", token);

        // 将token放在响应头
        response.setHeader(JwtTokenUtil.AUTH_HEADER_KEY, JwtTokenUtil.TOKEN_PREFIX + token);
        // 将token响应给客户端
        JSONObject result = new JSONObject();
        result.put("token", token);
        return Result.SUCCESS(result);
    }

    @GetMapping("/users")
    public Result userList() {
        log.info("### 查询所有用户列表 ###");
        return Result.SUCCESS();
    }
    @RequestMapping("/JWTlogin")
    @CrossOrigin
    public AjaxResult JWTlogin(@RequestParam String id_token, String redirect_url, Model model, HttpServletRequest request){
        //解析token
        Claims claims = JwtTokenUtil.parseJWT(id_token, audience.getBase64Secret());
        if(claims==null){
            return AjaxResult.error(-1,"token错误！");
        }
        System.out.println("claims为:"+claims);
        String username = JwtTokenUtil.getUsername(id_token, audience.getBase64Secret());
        System.out.println("username为："+username);
        return null;
    }
}
