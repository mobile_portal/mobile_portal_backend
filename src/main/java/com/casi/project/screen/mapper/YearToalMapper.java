package com.casi.project.screen.mapper;

import com.casi.project.screen.domain.*;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Auther shp
 * @data2020/5/813:02
 * @description
 */
public interface YearToalMapper {


    @Select("SELECT * FROM (SELECT count(*) number,con_unitcode conUnitcode FROM `edw_contract_detail` GROUP BY con_unitcode) a WHERE a.conUnitcode like #{code};")
    List<Object> modular1(@Param("code") String code);

    @Select("SELECT * FROM (SELECT SUM(con_convalue) number ,con_unitcode conUnitcode FROM `edw_contract_detail` WHERE CON_CREDDEBRT='01'  GROUP BY con_unitcode) a WHERE a.conUnitcode like #{code}")
    List<Object> modular2(@Param("code") String code);

    @Select("SELECT * FROM (SELECT SUM(con_convalue) number,con_unitcode conUnitcode FROM `edw_contract_detail` WHERE CON_CREDDEBRT='02'  GROUP BY con_unitcode) a  WHERE a.conUnitcode like #{code}")
    List<Object> modular3(@Param("code") String code);

    @Select("SELECT * FROM (SELECT count(*) number,con_unitcode conUnitcode FROM `edw_contract_detail` WHERE CON_CREDDEBRT='01' GROUP BY con_unitcode) a  WHERE a.conUnitcode like #{code}")
    List<Object> modular4(@Param("code") String code);

    @Select("SELECT * FROM (SELECT SUM(con_convalue) number,con_unitcode conUnitcode FROM `edw_contract_detail` WHERE CON_CREDDEBRT='01' GROUP BY con_unitcode ) a WHERE a.conUnitcode like #{code}")
    List<Object> modular5(@Param("code") String code);

    @Select("SELECT * FROM(SELECT SUM(con_convalue) number,con_unitcode conUnitcode FROM `edw_contract_detail` WHERE CON_CREDDEBRT='02'  GROUP BY con_unitcode) a WHERE a.conUnitcode like #{code}")
    List<Object> modular6(@Param("code") String code);

    //新签订总额
    @Select("SELECT sum(con_convalue) from edw_contract_detail where CON_CONSTATUS='11' and CON_CREDDEBRT=#{soft} and con_unitcode like #{code} and YEAR(`create_date`)=DATE_FORMAT(NOW(), '%Y')")
    Object YearNoneyTotal( @Param("code") String code,@Param("soft") String soft);
    //回款总额
    @Select("SELECT sum(con_convalue) from edw_contract_detail where con_unitcode like #{code} and  line_no  = '03' and CON_CREDDEBRT=#{soft}  and YEAR(`create_date`)=DATE_FORMAT(NOW(), '%Y')")
    Object moneyBackTotal( @Param("code") String code,@Param("soft") String soft);
    //应收总额
    @Select("SELECT sum(con_convalue) from edw_contract_detail where  line_no  = '03'  and con_unitcode like #{code} and CON_CREDDEBRT=#{soft}  and YEAR(`create_date`)=DATE_FORMAT(NOW(), '%Y')")
    Object answerMoneyTotal(@Param("soft") String soft, @Param("code") String code);
    //进行中合同份数
    @Select("SELECT COUNT(*) from edw_contract_detail where CON_CONSTATUS=12 and CON_CREDDEBRT=#{soft} and con_unitcode like #{code}  and YEAR(`create_date`)=DATE_FORMAT(NOW(), '%Y')")
    Object findAddToContract2(@Param("soft") String soft, @Param("code") String code);
    //外币
    @Select("SELECT SUM(con_convalue) FROM edw_contract_detail WHERE con_concur <> 'CNY' and CON_CREDDEBRT=#{soft} and con_unitcode like #{code} and YEAR(`create_date`)=DATE_FORMAT(NOW(), '%Y')")
    Object findcoreignCurrency(@Param("soft") String soft, @Param("code") String code);


    //进行中合同份数
    @Select("SELECT COUNT(*) from edw_contract_detail where CON_CONSTATUS=12 and con_unitcode like #{code}  and YEAR(`act_date`)=DATE_FORMAT(NOW(), '%Y')")
    Object findAddToContract(@Param("code") String code);

    //合同签订趋势
    @Select("SELECT d.`name` as state, SUM(CASE MONTH(a.`create_date`) WHEN '1' THEN 1 ELSE 0 END) AS january,  SUM(CASE MONTH(a.`create_date`) WHEN '2' THEN 1 ELSE 0 END) AS february,  SUM(CASE MONTH(a.`create_date`)WHEN '3' THEN 1 ELSE 0 END) AS march,  SUM(CASE MONTH(a.`create_date`) WHEN '4' THEN 1 ELSE 0 END) AS april, SUM(CASE MONTH(a.`create_date`) WHEN '5' THEN 1 ELSE 0 END) AS may,  SUM(CASE MONTH(a.`create_date`) WHEN '6' THEN 1 ELSE 0 END) AS june, SUM(CASE MONTH(a.`create_date`) WHEN '7' THEN 1 ELSE 0 END) AS july, SUM(CASE MONTH(a.`create_date`)WHEN '8' THEN 1 ELSE 0 END) AS august, SUM(CASE MONTH(a.`create_date`) WHEN '9' THEN 1 ELSE 0 END) AS september,  SUM(CASE MONTH(a.`create_date`)WHEN '10' THEN 1 ELSE 0 END) AS october, SUM(CASE MONTH(a.`create_date`)WHEN '11' THEN 1 ELSE 0 END) AS november, SUM(CASE MONTH(a.`create_date`) WHEN '12' THEN 1 ELSE 0 END) AS december,SUM(CASE MONTH(a.`create_date`) WHEN '1' THEN 1 ELSE 1 END) AS sum,SUM(CASE MONTH(a.`create_date`) WHEN '1' THEN 1 ELSE 1 END)/12 AS average FROM edw_contract_detail a ,dim_contract_creddebrt d WHERE a.con_unitcode like #{code} AND YEAR(`create_date`)=DATE_FORMAT(NOW(), '%Y')  and d.`name`='收款' or d.`name`='付款'  GROUP BY d.`name`")
    List<YearTotalVo> findSign(@Param("code") String code);

    //合同收付款计划：
    @Select("SELECT d.`name` as state, SUM(CASE MONTH(a.`update_date`) WHEN '1' THEN 1 ELSE 0 END) AS january,  SUM(CASE MONTH(a.`update_date`) WHEN '2' THEN 1 ELSE 0 END) AS february,  SUM(CASE MONTH(a.`update_date`)WHEN '3' THEN 1 ELSE 0 END) AS march,  SUM(CASE MONTH(a.`update_date`) WHEN '4' THEN 1 ELSE 0 END) AS april,  SUM(CASE MONTH(a.`update_date`) WHEN '5' THEN 1 ELSE 0 END) AS may,  SUM(CASE MONTH(a.`update_date`) WHEN '6' THEN 1 ELSE 0 END) AS june,  SUM(CASE MONTH(a.`update_date`) WHEN '7' THEN 1 ELSE 0 END) AS july,  SUM(CASE MONTH(a.`update_date`)WHEN '8' THEN 1 ELSE 0 END) AS august,  SUM(CASE MONTH(a.`update_date`) WHEN '9' THEN 1 ELSE 0 END) AS september,  SUM(CASE MONTH(a.`update_date`)WHEN '10' THEN 1 ELSE 0 END) AS october,  SUM(CASE MONTH(a.`update_date`)WHEN '11' THEN 1 ELSE 0 END) AS november, SUM(CASE MONTH(a.`update_date`) WHEN '12' THEN 1 ELSE 0 END) AS december,SUM(CASE MONTH(a.`update_date`) WHEN '1' THEN 1 ELSE 1 END) AS sum,SUM(CASE MONTH(a.`update_date`) WHEN '1' THEN 1 ELSE 1 END)/12 AS average FROM edw_contract_detail a ,dim_contract_creddebrt d WHERE a.con_unitcode like #{code} AND  YEAR(`update_date`)=DATE_FORMAT(NOW(), '%Y') and a.CON_CREDDEBRT=d.`code` GROUP BY d.`name`")
    List<YearTotalVo> findPayMent(@Param("code") String code);

    //本年新增合同数量
    @Select("SELECT COUNT(*) FROM edw_contract_detail WHERE YEAR(`create_date`)=DATE_FORMAT(NOW(), '%Y') and  con_unitcode like #{code} and CON_CONSTATUS='11'")
    Object findAddContract(@Param("code") String code);

    ///往年结转
    @Select("SELECT COUNT(*) FROM edw_contract_detail  WHERE con_enddate < 20200101 and con_unitcode like #{code}")
    Object findCarryForward(@Param("code") String code);

    //// 逾期总额
    @Select("SELECT SUM(con_convalue) FROM `edw_contract_detail` WHERE line_no='03' AND con_enddate IS NOT NULL AND act_date > con_enddate and con_unitcode like #{code}")
    Object findTotalOverdue(@Param("code") String code);


    //公司编码 ---公司名称
    @Select("SELECT l.con_unitcode conUnitcode,t.orgname orgName FROM edw_contract_detail l,dim_unit t WHERE l.con_unitcode=t.orgcode  GROUP BY l.con_unitcode,t.orgname ")
    List<YearCompanyVo> findYearCompanyVo();


    /**
     * 新签定。。。
     * @return
     */
    @Select("SELECT a.conUnitcode,a.orgName,b.total,b.conConvalue,b.actAmount  FROM (SELECT l.con_unitcode conUnitcode,t.orgname orgName FROM edw_contract_detail l,dim_unit t WHERE l.con_unitcode=t.orgcode GROUP BY l.con_unitcode,t.orgname) a,(SELECT COUNT(*) total,SUM(con_convalue) conConvalue,SUM(act_amount) actAmount,con_unitcode conUnitcode FROM `edw_contract_detail` WHERE line_no ='03'  GROUP BY con_unitcode) b WHERE a.conUnitcode=b.conUnitcode and SELECT a.conUnitcode,a.orgName,b.total,b.conConvalue,b.actAmount  FROM (SELECT l.con_unitcode conUnitcode,t.orgname orgName FROM edw_contract_detail l,dim_unit t WHERE l.con_unitcode=t.orgcode GROUP BY l.con_unitcode,t.orgname ASC) a, (SELECT COUNT(*) total,SUM(con_convalue) conConvalue,SUM(act_amount) actAmount,con_unitcode conUnitcode FROM `edw_contract_detail` GROUP BY con_unitcode) b WHERE a.conUnitcode=b.conUnitcode and a.con_unitcode like #{code}")
    List<YearMoneyTotal> newContractTotal(@Param("code") String code);

    /**
     * 总额
     * @return
     */
    @Select("SELECT a.conUnitcode,a.orgName,b.total,b.conConvalue,b.actAmount  FROM (SELECT l.con_unitcode conUnitcode,t.orgname orgName FROM edw_contract_detail l,dim_unit t WHERE l.con_unitcode=t.orgcode GROUP BY l.con_unitcode,t.orgname ASC) a, (SELECT COUNT(*) total,SUM(con_convalue) conConvalue,SUM(act_amount) actAmount,con_unitcode conUnitcode FROM `edw_contract_detail` GROUP BY con_unitcode) b WHERE a.conUnitcode=b.conUnitcode and a.con_unitcode like #{code}")
    List<YearMoneyTotal> ContractTotal(@Param("code") String code);


    //合同等级分布
    @Select("SELECT con_conlevel name,count(*) value  FROM edw_contract_detail where con_unitcode like #{code}  and con_conlevel is not null AND  YEAR(`create_date`)=DATE_FORMAT(NOW(), '%Y') GROUP BY  con_conlevel")
    List<YearStaus> distribution(@Param("code") String code);

    //合同履行状态
    @Select("SELECT s.name,COUNT(l.con_constatus) value from edw_contract_detail l,dim_contract_status s where l.con_constatus=s.`code` and con_unitcode like #{code} GROUP BY con_constatus,s.`name`")
    List<YearStaus> performStaus(@Param("code") String code);

    //合同收款统计
    @Select("SELECT a.line_no state, SUM(CASE MONTH(a.`create_date`) WHEN '1' THEN 1 ELSE 0 END) AS january,  SUM(CASE MONTH(a.`create_date`) WHEN '2' THEN 1 ELSE 0 END) AS february,  SUM(CASE MONTH(a.`create_date`)WHEN '3' THEN 1 ELSE 0 END) AS march,  SUM(CASE MONTH(a.`create_date`) WHEN '4' THEN 1 ELSE 0 END) AS april,  SUM(CASE MONTH(a.`create_date`) WHEN '5' THEN 1 ELSE 0 END) AS may,  SUM(CASE MONTH(a.`create_date`) WHEN '6' THEN 1 ELSE 0 END) AS june,  SUM(CASE MONTH(a.`create_date`) WHEN '7' THEN 1 ELSE 0 END) AS july,  SUM(CASE MONTH(a.`create_date`)WHEN '8' THEN 1 ELSE 0 END) AS august,  SUM(CASE MONTH(a.`create_date`) WHEN '9' THEN 1 ELSE 0 END) AS september,  SUM(CASE MONTH(a.`create_date`)WHEN '10' THEN 1 ELSE 0 END) AS october,  SUM(CASE MONTH(a.`create_date`)WHEN '11' THEN 1 ELSE 0 END) AS november,  SUM(CASE MONTH(a.`create_date`) WHEN '12' THEN 1 ELSE 0 END) AS december, SUM(CASE MONTH(a.`create_date`) WHEN '1' THEN 1 ELSE 1 END) AS sum,SUM(CASE MONTH(a.`create_date`) WHEN '1' THEN 1 ELSE 1 END)/12 AS average FROM edw_contract_detail a WHERE a.line_no='01' or a.line_no='03' and con_unitcode like #{code} and YEAR(`create_date`)=DATE_FORMAT(NOW(), '%Y') GROUP BY a.line_no")
    List<YearTotalVo>  inputTotal(@Param("code") String code);

    //延期收款
    @Select("SELECT  SUM(CASE MONTH(a.`con_enddate`) WHEN '1' THEN 1 ELSE 0 END) AS january,  SUM(CASE MONTH(a.`con_enddate`) WHEN '2' THEN 1 ELSE 0 END) AS february,  SUM(CASE MONTH(a.`con_enddate`)WHEN '3' THEN 1 ELSE 0 END) AS march,  SUM(CASE MONTH(a.`con_enddate`) WHEN '4' THEN 1 ELSE 0 END) AS april,  SUM(CASE MONTH(a.`con_enddate`) WHEN '5' THEN 1 ELSE 0 END) AS may,  SUM(CASE MONTH(a.`con_enddate`) WHEN '6' THEN 1 ELSE 0 END) AS june,  SUM(CASE MONTH(a.`con_enddate`) WHEN '7' THEN 1 ELSE 0 END) AS july,  SUM(CASE MONTH(a.`con_enddate`)WHEN '8' THEN 1 ELSE 0 END) AS august,  SUM(CASE MONTH(a.`con_enddate`) WHEN '9' THEN 1 ELSE 0 END) AS september,  SUM(CASE MONTH(a.`con_enddate`)WHEN '10' THEN 1 ELSE 0 END) AS october,  SUM(CASE MONTH(a.`con_enddate`)WHEN '11' THEN 1 ELSE 0 END) AS november,  SUM(CASE MONTH(a.`con_enddate`) WHEN '12' THEN 1 ELSE 0 END) AS december, SUM(CASE MONTH(a.`con_enddate`) WHEN '1' THEN 1 ELSE 1 END) AS sum,SUM(CASE MONTH(a.`con_enddate`) WHEN '1' THEN 1 ELSE 1 END)/12 AS average FROM edw_contract_detail a WHERE YEAR(a.`create_date`)=DATE_FORMAT(NOW(), '%Y') and a.CON_CONSTATUS=11 AND act_date > con_enddate  AND con_unitcode like #{code};")
    List<YearTotalVo>  deferredCollection(@Param("code") String code);

    //合同付款统计
    @Select("SELECT a.line_no state, SUM(CASE MONTH(a.create_date) WHEN '1' THEN 1 ELSE 0 END) AS january,  SUM(CASE MONTH(a.create_date) WHEN '2' THEN 1 ELSE 0 END) AS february,  SUM(CASE MONTH(a.create_date)WHEN '3' THEN 1 ELSE 0 END) AS march,  SUM(CASE MONTH(a.create_date) WHEN '4' THEN 1 ELSE 0 END) AS april,  SUM(CASE MONTH(a.create_date) WHEN '5' THEN 1 ELSE 0 END) AS may,  SUM(CASE MONTH(a.create_date) WHEN '6' THEN 1 ELSE 0 END) AS june,  SUM(CASE MONTH(a.create_date) WHEN '7' THEN 1 ELSE 0 END) AS july,  SUM(CASE MONTH(a.create_date)WHEN '8' THEN 1 ELSE 0 END) AS august,  SUM(CASE MONTH(a.create_date) WHEN '9' THEN 1 ELSE 0 END) AS september,  SUM(CASE MONTH(a.create_date)WHEN '10' THEN 1 ELSE 0 END) AS october,  SUM(CASE MONTH(a.create_date)WHEN '11' THEN 1 ELSE 0 END) AS november,  SUM(CASE MONTH(a.create_date) WHEN '12' THEN 1 ELSE 0 END) AS december, SUM(CASE MONTH(a.create_date) WHEN '1' THEN 1 ELSE 1 END) AS sum,SUM(CASE MONTH(a.create_date) WHEN '1' THEN 1 ELSE 1 END)/12 AS average FROM edw_contract_detail a WHERE a.line_no='02' or a.line_no='04'  and con_unitcode=2250 and YEAR(`act_date`)=DATE_FORMAT(NOW(), '%Y') GROUP BY a.line_no")
    List<YearTotalVo>  outTotal(@Param("code") String code);


    // 采集总数据量
    @Select("SELECT count(1) from edw_contract_detail where 1=1")
    Object getTotalQuantity();

    // 今日采集总数据量
    @Select("SELECT count(1) from edw_contract_detail where act_date=CURDATE()")
    Object getTotalQuantityToday();
    // 采集单位总数据量
    @Select("SELECT COUNT(1) FROM (SELECT COUNT(1)FROM edw_contract_detail WHERE  con_unitcode IS NOT NULL GROUP BY  con_unitcode) AS a")
    Integer getTotalQuantityCompany();


    // 合同数据采集监控,各公司合同采集总数
    @Select("SELECT unit_code 'unitCode',unit_name 'unitName',data_collect_total 'dataCollectTotal',data_collect_today 'dataCollectToday',today_date 'todayDate',data_valid_date 'dataValidDate' FROM edw_contract_data_monitor WHERE 1=1  ")
    List<EdwContractDataMonitor> conCollectionMonitor();

    // 提供数据服务的系统
    @Select("SELECT COUNT(1) FROM `edw_data_supply_monitor`")
    Object collectionSys();

    // 对外提供数据的总数据量
    @Select("SELECT SUM(contract_num_total) FROM edw_data_supply_monitor")
    Integer collectTotal();


    //对外提供数据监控
    @Select("SELECT sys_code 'sysCode', sys_name 'sysName', contract_num_total 'contractNumTotal' , contract_amt_total 'contractAmtTotal', contract_pay_total 'contractPayTotal', contract_num_today 'contractNumToday', contract_amt_today 'contractAmtToday', contract_pay_today 'contractPayToday', today_date 'todayDate', data_valid_date 'dataValidDate' FROM edw_data_supply_monitor")
    List<EdwDataSupplyMonitor> sideCollection();

    @Select("SELECT sys_code as 'sysCode',sys_name as 'sysName',collect_total as 'conllectTotal',collect_today as 'conllectToday',collect_amt_today as 'conllectAmtToday',today_date 'todayDate',data_valid_date 'dataValidDate',type FROM edw_data_collect_monitor")
    List<EdwDataCollectMonitor> otherCollection();

    @Select("SELECT orgcode, orgname,immeuporg,uplevlname,filltwoorg,secend_dept_name secendDeptName,create_date createDate from dim_unit where 1=1")
    List<DimUnit> getOrganize();




}
