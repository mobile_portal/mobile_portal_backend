package com.casi.project.tenant.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 租户自定义应用角色对象 tenant_app_role
 * 
 * @author casi
 * @date 2020-06-02
 */
public class TenantAppRole extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 租户编号 */
    @Excel(name = "租户编号")
    private String tenantId;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private String appId;

    /** 角色名称 */
    @Excel(name = "角色名称")
    private String name;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setTenantId(String tenantId) 
    {
        this.tenantId = tenantId;
    }

    public String getTenantId() 
    {
        return tenantId;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("tenantId", getTenantId())
            .append("appId", getAppId())
            .append("name", getName())
            .toString();
    }
}
