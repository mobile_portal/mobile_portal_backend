package com.casi.project.portal.service;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.param.MobileApplyVersionNativeParam;

/**
 * @Auther shp
 * @data2020/4/219:58
 * @description
 */
public interface MobileApplyVersionNativeService {


    /**
     * 添加原生应用版本
     * @param m
     * @return
     */

    AjaxResult addApp(MobileApplyVersionNativeParam m);



}
