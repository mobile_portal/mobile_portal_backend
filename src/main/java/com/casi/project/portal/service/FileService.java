package com.casi.project.portal.service;


import com.casi.project.portal.domain.LocalFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description:文件管理
 */
public interface FileService {

    /**
     * @Description:上传文件
     * @param
     * @return
     */
    public Long uploadFile(MultipartFile multipartFile, String type);

    /**
     * @Description: 查询文件信息
     * @param id
     * @return cn.com.casic.domain.materialmanager.LocalFileMapper
     */
    public LocalFile queryById(Long id);

    /**
     * @Description: 删除文件
     * @param id
     * @return cn.com.casic.domain.materialmanager.LocalFileMapper
     */
    public boolean delete(Long id);

}
