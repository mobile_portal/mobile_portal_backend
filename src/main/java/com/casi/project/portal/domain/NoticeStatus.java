package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticeStatus {
    private String [] id;
    private Integer type;
    private String checker;
    private String pushByUser;
}
