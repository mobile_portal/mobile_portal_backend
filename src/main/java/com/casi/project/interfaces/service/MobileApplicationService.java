package com.casi.project.interfaces.service;

import com.casi.framework.web.domain.AjaxResult;

import java.util.Map;

public interface MobileApplicationService {
    /**
     * @author: liyuan
     * @date: 2020/5/21 15:31
     * @Description:  单点登录（移动商店调用门户平台，登录）
     */
    public AjaxResult login(String userName);
    /**
     * @author: liyuan
     * @date: 2020/5/22 9:56
     * @Description:  推送消息接口（移动商店调用门户平台，推送消息）
     */
    public AjaxResult sendMsg(Map<String,Object> params);
}
