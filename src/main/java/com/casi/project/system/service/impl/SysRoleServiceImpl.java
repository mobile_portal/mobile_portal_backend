package com.casi.project.system.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.casi.common.utils.SecurityUtils;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.framework.web.domain.server.Sys;
import com.casi.project.portal.domain.RoleApplyResource;
import com.casi.project.system.domain.*;
import com.casi.project.system.mapper.*;
import org.aspectj.weaver.loadtime.Aj;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.casi.common.constant.UserConstants;
import com.casi.common.exception.CustomException;
import com.casi.common.utils.StringUtils;
import com.casi.framework.aspectj.lang.annotation.DataScope;
import com.casi.project.system.service.ISysRoleService;

/**
 * 角色 业务层处理
 *
 * @author lzb
 */
@Service
public class SysRoleServiceImpl implements ISysRoleService {
    @Autowired
    private SysRoleMapper roleMapper;

    @Autowired
    private SysRoleMenuMapper roleMenuMapper;

    @Autowired
    private SysUserRoleMapper userRoleMapper;

    @Autowired
    private SysRoleDeptMapper roleDeptMapper;

    @Autowired
    private SysDeptMapper deptMapper;

    @Autowired
    private SysMenuMapper menuMapper;

    @Autowired
    private SysUserMapper userMapper;


    /**
     * 根据条件分页查询角色数据
     *
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    @Override
    @DataScope(deptAlias = "d")
    public List<SysRole> selectRoleList(SysRole role) {

        return roleMapper.selectRoleList(role);
    }


    /**
     * 根据登录用户获取角色列表
     */
    @Override
    //@DataScope(deptAlias = "d")
    public List<SysRole> roleListByLogin() {
        if (SecurityUtils.getLoginUser().getUser().isAdmin()) {
            return roleMapper.selectRoleList(new SysRole());
        } else {
            List<SysRole> sysRoles = new ArrayList<>();
            String userId = SecurityUtils.getLoginUser().getUser().getUserId();
            sysRoles = roleMapper.selectRoleByUserId(userId);
            return sysRoles;
        }

    }


    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectRolePermissionByUserId(String userId) {
        List<SysRole> perms = roleMapper.selectRolePermissionByUserId(userId);
        Set<String> permsSet = new HashSet<>();
        for (SysRole perm : perms) {
            if (StringUtils.isNotNull(perm)) {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 查询所有系统角色
     *
     * @return 角色列表
     */
    public List<SysRole> selectRoleAll() {
        return roleMapper.selectRoleAll();
    }

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param userId 用户ID
     * @return 选中角色ID列表
     */
    public List<String> selectRoleListByUserId(String userId) {
        return roleMapper.selectRoleListByUserId(userId);
    }

    /**
     * 通过角色ID查询角色
     *
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    public SysRole selectRoleById(String roleId) {
        return roleMapper.selectRoleById(roleId);
    }

    public SysRole queryApplyRoleOneByRoleId(String roleId) {
        return roleMapper.queryApplyRoleOneByRoleId(roleId);
    }


    /**
     * 校验角色名称是否唯一
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleNameUnique(SysRole role) {
        String roleId = StringUtils.isNull(role.getRoleId()) ? "-1" : role.getRoleId();
        SysRole info = roleMapper.checkRoleNameUnique(role.getRoleName());
        if (StringUtils.isNotNull(info) && !info.getRoleId().equals(roleId)) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色编码是否唯一
     */
    public String checkRoleCodeUnique(String roleCode) {

        int row = roleMapper.selectRoleByRoleCode(roleCode);
        if (row > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


    /**
     * 校验角色名称是否唯一
     */
    public String checkRoleNameUnique(String roleName) {

        int row = roleMapper.selectRoleByRoleName(roleName);
        if (row > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色权限是否唯一
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleKeyUnique(SysRole role) {
        String roleId = StringUtils.isNull(role.getRoleId()) ? "-1" : role.getRoleId();
        SysRole info = roleMapper.checkRoleKeyUnique(role.getRoleKey());
        if (StringUtils.isNotNull(info) && info.getRoleId().equals(roleId)) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色是否允许操作
     *
     * @param role 角色信息
     */
    public void checkRoleAllowed(SysRole role) {
        if (StringUtils.isNotNull(role.getRoleId()) && role.isAdmin()) {
            throw new CustomException("不允许操作超级管理员角色");
        }
    }

    public AjaxResult checkRoleAllowedAjax(SysRole role) {
        if (StringUtils.isNotNull(role.getRoleId()) && role.isAdmin()) {
            return AjaxResult.error("不允许操作超级管理员角色");
        }else {
            return AjaxResult.success();
        }
    }

    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public int countUserRoleByRoleId(String roleId) {
        return userRoleMapper.countUserRoleByRoleId(roleId);
    }

    /**
     * 新增保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertRole(SysRole role) {
        // 新增角色信息
        roleMapper.insertRole(role);
        role.setRoleId(roleMapper.selectRoleIdByRoleCode(role.getRoleCode()));
        return insertRoleMenu(role);
    }

    /**
     * 新增应用角色
     *
     * @param role
     * @return
     */
    @Override
    @Transactional
    public int addApplyRole(SysRole role) {
        // 新增角色信息
        roleMapper.insertRole(role);
        role.setRoleId(roleMapper.selectRoleIdByRoleCode(role.getRoleCode()));
        return addApplyToRole(role);
    }


    /**
     * 修改保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateRole(SysRole role) {
        // 修改角色信息
        roleMapper.updateRole(role);
        // 删除角色与菜单关联
        String roleId = roleMapper.selectRoleIdByRoleCode(role.getRoleCode());
        roleMenuMapper.deleteRoleMenuByRoleId(roleId);
        return insertRoleMenu(role);
    }


    /**
     * 修改应用角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int editApplyRole(SysRole role) {
        // 修改角色信息
        roleMapper.updateRole(role);
        // 删除角色与应用关联
        String roleId = roleMapper.selectRoleIdByRoleCode(role.getRoleCode());
        roleMenuMapper.deleteApplyRoleByRoleId(roleId);
        return addApplyToRole(role);
    }


    /**
     * 修改角色状态
     *
     * @param role 角色信息
     * @return 结果
     */
    public int updateRoleStatus(SysRole role) {
        return roleMapper.updateRole(role);
    }

    /**
     * 修改数据权限信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int authDataScope(SysRole role) {
        // 修改角色信息
        roleMapper.updateRole(role);
        // 删除角色与部门关联
        roleDeptMapper.deleteRoleDeptByRoleId(role.getRoleId());
        // 新增角色和部门信息（数据权限）
        return insertRoleDept(role);
    }

    @Override
    @Transactional
    public AjaxResult addDeveloper(String[] userIds) {

        List<SysUserRole> list = new ArrayList<>();
        for (String userId : userIds) {
            int count = userRoleMapper.selectRoleByUserIdAndRoleId(userId, "32");
            if (count < 1) {
                //  return AjaxResult.error("该用户是开发者角色，不能重复赋权");
                SysUserRole ur = new SysUserRole();
                // 32 是开发者角色id 写死
                ur.setRoleId("32");
                ur.setUserId(userId);
                list.add(ur);
            }

        }

        if (StringUtils.isNotEmpty(list)) {
            int i = userRoleMapper.batchUserRole(list);
            if (i > 0) {
                return AjaxResult.success("授权开发者角色成功");
            } else {
                return AjaxResult.error("授权开发者角色失败");
            }
        }else {
            return AjaxResult.success("授权开发者角色成功");
        }


    }


    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    public int insertRoleMenu(SysRole role) {
        int rows = 1;
        // 新增用户与角色管理
        List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
        List<SysMenu> menuList = role.getMenuList();

        Set<String> menuIds = new HashSet<>();

        for (String menuId : role.getMenuIds()) {
            menuIds.add(menuId);
            if (StringUtils.isNotEmpty(menuId) && !"0".equals(menuId)) {
                SysMenu menu = menuMapper.selectMenuById(menuId);
                String parentId = menu.getParentId();
                if (StringUtils.isNotEmpty(parentId) && !"0".equals(parentId)) {
                    menuIds.add(parentId);
                    SysMenu menu1 = menuMapper.selectMenuById(parentId);
                    String parentId1 = menu1.getParentId();
                    if (StringUtils.isNotEmpty(parentId1) && !"0".equals(parentId1)) {
                        menuIds.add(parentId1);
                        SysMenu menu2 = menuMapper.selectMenuById(parentId1);
                        String parentId2 = menu2.getParentId();
                        if (StringUtils.isNotEmpty(parentId2) && !"0".equals(parentId2)) {
                            menuIds.add(parentId2);
                            SysMenu menu3 = menuMapper.selectMenuById(parentId2);
                            String parentId3 = menu3.getParentId();
                            if (StringUtils.isNotEmpty(parentId3) && !"0".equals(parentId3)) {
                                menuIds.add(parentId3);
                                SysMenu menu4 = menuMapper.selectMenuById(parentId3);
                                String parentId4 = menu4.getParentId();
                                if (StringUtils.isNotEmpty(parentId4) && !"0".equals(parentId4)) {
                                    menuIds.add(parentId4);
                                }
                            }
                        }
                    }
                }
            }
        }


        for (String menuId : menuIds) {
            SysRoleMenu rm = new SysRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
/*        for (String menuId : role.getMenuIds()) {
            SysRoleMenu rm = new SysRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }*/

        if (list.size() > 0) {
            rows = roleMenuMapper.batchRoleMenu(list);
        }
        return rows;
    }


    /**
     * 新增角色应用信息
     *
     * @param role 角色对象
     */
    public int addApplyToRole(SysRole role) {
        int rows = 1;
        // 新增应用与角色管理
        List<RoleApplyResource> list = role.getRoleApplyResourceList();
        if (list != null) {
            for (RoleApplyResource roleApplyResource : list) {
                if (roleApplyResource != null) {
                    if (roleApplyResource.getApplyRoleId() == null) {
                        roleApplyResource.setApplyRoleId("");
                    }
                    if (roleApplyResource.getAppPermsId() == null) {
                        roleApplyResource.setAppPermsId("");
                    }
                    if (roleApplyResource.getMobileApplyId() == null) {
                        roleApplyResource.setMobileApplyId("");
                    }
                    roleApplyResource.setApplyRoleId(roleMapper.selectRoleIdByRoleCode(role.getRoleCode()));
                }

            }
        }
        Set<RoleApplyResource> set = new HashSet(list);

        if (set != null && set.size() > 0) {
            rows = roleMenuMapper.batchRoleApply(set);
        }
        return rows;
    }

    /**
     * 新增角色部门信息(数据权限)
     *
     * @param role 角色对象
     */
    public int insertRoleDept(SysRole role) {
        int rows = 1;
        // 新增角色与部门（数据权限）管理
        List<SysRoleDept> list = new ArrayList<SysRoleDept>();
        for (String deptId : role.getDeptIds()) {
            SysRoleDept rd = new SysRoleDept();
            rd.setRoleId(role.getRoleId());
            rd.setDeptId(deptId);
            list.add(rd);
        }
        if (list.size() > 0) {
            rows = roleDeptMapper.batchRoleDept(list);
        }
        return rows;
    }

    /**
     * 通过角色ID删除角色
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public int deleteRoleById(String roleId) {
        return roleMapper.deleteRoleById(roleId);
    }

    /**
     * 批量删除角色信息
     *
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    public int deleteRoleByIds(String[] roleIds) {
        for (String roleId : roleIds) {
            System.out.println(roleId);
            checkRoleAllowed(new SysRole(roleId));
            SysRole role = selectRoleById(roleId);
            if (countUserRoleByRoleId(roleId) > 0) {
                throw new CustomException(String.format("%1$s已分配,不能删除", role.getRoleName()));
            }
        }
        System.out.println(roleIds);
        return roleMapper.deleteRoleByIds(roleIds);
    }


    @Override
    public int addRole(SysRole role, String deptId) {

        role.setRoleBelongsDept(deptId);
        //role.setDataScope("4");

        // 新增角色信息
        roleMapper.insertRole(role);
        int i = roleMapper.selectRoleByRoleCode(role.getRoleCode());
        String roleId = roleMapper.selectRoleIdByRoleCode(role.getRoleCode());
        role.setRoleId(roleId);
        return insertRoleMenu(role);
    }

    /**
     * 判断用户是否有权限操作
     */
    public void isHavePerm(SysUser sysUser) {
        if (!SecurityUtils.getLoginUser().getUser().isAdmin()) {
            List<SysRole> list = roleMapper.selectRolesByUserName(SecurityUtils.getUsername());
            Set<String> set = new HashSet();
            for (SysRole role : list) {
                SysDept sysDept = deptMapper.selectDeptById(role.getRoleBelongsDept());
                List<SysDept> sysDepts = deptMapper.selectDeptList(sysDept);
                for (SysDept dept : sysDepts) {
                    set.add(dept.getDeptId());
                }
                set.remove(sysDept.getDeptId());
            }

            boolean contains = set.contains(sysUser.getDeptId());
            if (!contains) {
                throw new CustomException("当前操作无权限");
            }
        }
    }

    public void isHavePerm(String[] userIds) {
        if (!SecurityUtils.getLoginUser().getUser().isAdmin()) {
            List<SysRole> list = roleMapper.selectRolesByUserName(SecurityUtils.getUsername());
            Set<String> set = new HashSet();
            for (SysRole role : list) {
                SysDept sysDept = deptMapper.selectDeptById(role.getRoleBelongsDept());
                List<SysDept> sysDepts = deptMapper.selectDeptList(sysDept);
                for (SysDept dept : sysDepts) {
                    set.add(dept.getDeptId());
                }
                set.remove(sysDept.getDeptId());
            }

            for (String userId : userIds) {
                List<SysRole> sysRoles = roleMapper.selectRoleByUserId(userId);
                for (SysRole sysRole : sysRoles) {
                    String roleBelongsDept = sysRole.getRoleBelongsDept();
                    if (!set.contains(roleBelongsDept)) {
                        throw new CustomException("当前操作无权限");
                    }
                }
            }
        }
    }

    public void isHavePerm(SysRole sysRole) {
        if (!SecurityUtils.getLoginUser().getUser().isAdmin()) {


            List<SysRole> list = roleMapper.selectRolesByUserName(SecurityUtils.getUsername());
            Set<String> set = new HashSet();
            for (SysRole role : list) {
                SysDept sysDept = deptMapper.selectDeptById(role.getRoleBelongsDept());
                List<SysDept> sysDepts = deptMapper.selectDeptList(sysDept);
                for (SysDept dept : sysDepts) {
                    set.add(dept.getDeptId());
                }
                set.remove(sysDept.getDeptId());
            }
            boolean contains = set.contains(sysRole.getRoleBelongsDept());
            if (!contains) {
                throw new CustomException("当前操作无权限");
            }
        }
    }

    public AjaxResult isHavePermByRoleId(String[] roleIds) {
        if (!SecurityUtils.getLoginUser().getUser().isAdmin()) {
            List<SysRole> list = roleMapper.selectRolesByUserName(SecurityUtils.getUsername());
            Set<String> set = new HashSet();
            if (!SecurityUtils.getLoginUser().getUser().isAdmin()) {
                for (SysRole role : list) {
                    SysDept sysDept = deptMapper.selectDeptById(role.getRoleBelongsDept());
                    List<SysDept> sysDepts = deptMapper.selectDeptList(sysDept);
                    for (SysDept dept : sysDepts) {
                        set.add(dept.getDeptId());
                    }
                    set.remove(sysDept.getDeptId());
                }
                for (String roleId : roleIds) {
                    SysRole sysRole = roleMapper.selectRoleById(roleId);
                    boolean contains = set.contains(sysRole.getRoleBelongsDept());
                    if (!contains) {
                        return AjaxResult.error("当前操作无权限");
//                        throw new CustomException("当前操作无权限");
                    }
                }
            }
        }
        return null;
    }


    @Override
    @Transactional
    public AjaxResult usersLinkedRole(String roleId, String[] userIds) {

        for (String userId : userIds) {
            int count = userRoleMapper.selectRoleByUserIdAndRoleId(userId, roleId);
            if (count < 1) {
                int i = userRoleMapper.usersLinkedRole(userId, roleId);
                if (i == 0) {
                    return AjaxResult.error("关联失败");
                }
            }
        }

        return AjaxResult.success();
    }

    @Override
    public SysRole selectApplyRoleById(String roleId) {
        return roleMapper.selectApplyRoleById(roleId);
    }

    @Override
    public String[] checkApplyResourcesById(String roleId) {

        return roleMapper.checkApplyResourcesById(roleId);
    }


    /**
     * 根据角色id 查询应用的id
     *
     * @param roleId
     * @return
     */
    @Override
    public String[] selectApplyId(String roleId) {
        return roleMapper.selectApplyId(roleId);
    }

    /**
     * 根据角色的id查询菜单的id
     */
    public String[] selectByRoleId(String roleId) {
        List<SysMenu> sysMenus = menuMapper.selectByRoleId(roleId);

        // 测试超级管理员用
        if ("1".equals(roleId)) {
            Set<String> selectedIds = new HashSet();
            sysMenus = menuMapper.selectMenuList(new SysMenu());
            for (SysMenu sysMenu : sysMenus) {
                if (sysMenu != null) {
                    selectedIds.add(sysMenu.getMenuId());
                }

            }
            Object[] objects = selectedIds.toArray();
            String[] strings = new String[objects.length];
            for (int i = 0; i < objects.length; i++) {
                strings[i] = (String) objects[i];
            }

            return strings;
        }
        Set<String> setCh = new HashSet();
        if (StringUtils.isNotEmpty(sysMenus)) {
            for (SysMenu sysMenu : sysMenus) {
                if (sysMenu != null) {
                    setCh.add(sysMenu.getMenuId());
                }
            }

            for (SysMenu sysMenu : sysMenus) {
                if (sysMenu != null && setCh.contains(sysMenu.getParentId())) {
                    setCh.remove(sysMenu.getParentId());
                }
            }


        }

        if (StringUtils.isEmpty(setCh)) {
            return new String[0];
        }

        Object[] objects = setCh.toArray();
        String[] strings = new String[objects.length];
        for (int i = 0; i < objects.length; i++) {
            strings[i] = (String) objects[i];
        }

        return strings;
    }


}
