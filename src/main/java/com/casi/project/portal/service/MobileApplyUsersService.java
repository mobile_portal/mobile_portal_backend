package com.casi.project.portal.service;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.DoubleArray;
import com.casi.project.system.domain.SysUser;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public interface MobileApplyUsersService {
    /**
     * 应用授权给用户接口
     */
    public AjaxResult addApplyIdsAndUserIds(String[] applyIds,String[] userIds);

    /**
     * 应用禁用用户
     */
    public AjaxResult addApplyToDisabledUserIds(String mobileApplyId, String userId);

    /**
     * 应用解除禁用用户
     */
    public AjaxResult romoveApplyToDisabledUserIds(String mobileApplyId, String userId);

    /**
     * 回显已经授权的用户
     * @param doubleArray
     * @return
     */
    AjaxResult findApplyIdsAndUserIds(DoubleArray doubleArray);

    List<SysUser> applyDisabledUserList(String applyId);
    /**
     *根据应用的id查询出该应用没有授权的用户信息
     */
    public AjaxResult queryNotImpowerUsers(Map<String,Object> map);
    /**
     * 根据应用的id查询已经授权的用户的信息
     */
    public AjaxResult queryImpowerUsers(Map<String,Object> map);
    /**
     * 根据应用的id和用户的id（用户id可为多个）   解除该应用授权的用户
     */
    public AjaxResult relieveUsersImpower(Map<String,Object> map);
}
