package com.casi.project.system.service.impl;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.file.FileUploadUtils;
import com.casi.framework.config.SysConfig;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.system.domain.*;
import com.casi.project.system.mapper.SysRoleDeptMapper;
import com.casi.project.system.mapper.SysRoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.common.constant.UserConstants;
import com.casi.common.exception.CustomException;
import com.casi.common.utils.StringUtils;
import com.casi.framework.aspectj.lang.annotation.DataScope;
import com.casi.framework.web.domain.TreeSelect;
import com.casi.project.system.mapper.SysDeptMapper;
import com.casi.project.system.service.ISysDeptService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 * 部门管理 服务实现
 *
 * @author lzb
 */
@Service
public class SysDeptServiceImpl implements ISysDeptService {
    @Autowired
    private SysDeptMapper deptMapper;

    @Autowired
    private SysRoleMapper roleMapper;
    @Autowired
    private SysRoleDeptMapper sysRoleDeptMapper;

    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    @Override
    //@DataScope(deptAlias = "d")
    public List<SysDept> selectDeptList(SysDept dept) {
        return deptMapper.selectDeptList(dept);
    }


    /**
     *  根据当前用户权限获取部门列表树
     */
    // @DataScope(deptAlias = "d")
    public List<SysDept> selectDeptListByLoginIn() {
        List<SysDept> sysDepts = new ArrayList<>();
        if (SecurityUtils.getLoginUser().getUser().isAdmin()) {
            sysDepts = deptMapper.selectDeptList(new SysDept());
        } else {
            SysRole sysRole;
            Set<String> roleBelongsDepts = new HashSet<>();
            String userId = SecurityUtils.getLoginUser().getUser().getUserId();
            if (userId != null) {
                Set<String> roleIds = roleMapper.findRoleListByUserId(userId);
                /*if (roleIds != null) {
                    for (String roleId : roleIds) {
                        sysRole = roleMapper.selectRoleById(roleId);
                        String roleBelongsDept = sysRole.getRoleBelongsDept();
                        roleBelongsDepts.add(roleBelongsDept);
                    }
                }*/
                if(roleIds != null){
                    for (String roleId : roleIds) {
                        List<SysRoleDept> sysRoleDepts = sysRoleDeptMapper.selectByRoleId(roleId);
                        if(null != sysRoleDepts && sysRoleDepts.size()>0){
                            for(SysRoleDept sysRoleDept : sysRoleDepts){
                                roleBelongsDepts.add(sysRoleDept.getDeptId());
                            }
                        }
                    }
                }
            }
            SysDept sysDept = new SysDept();
            for (String roleBelongsDept : roleBelongsDepts) {
                if (roleBelongsDept != null) {
                    sysDept = deptMapper.selectDeptById(roleBelongsDept);
                }
                if (sysDept != null) {
                    sysDepts = deptMapper.selectChildrenDeptById(sysDept.getDeptId());
                    sysDepts.add(deptMapper.selectDeptById(sysDept.getDeptId()));
                }
            }
        }

        return sysDepts;
    }


    /**
     * 构建前端所需要树结构
     *
     * @param depts 部门列表
     * @return 树结构列表
     */
    @Override
    public List<SysDept> buildDeptTree(List<SysDept> depts) {
        List<SysDept> returnList = new ArrayList<SysDept>();
        List<String> tempList = new ArrayList<String>();
        for (SysDept dept : depts) {
            tempList.add(dept.getDeptId());
        }
        for (Iterator<SysDept> iterator = depts.iterator(); iterator.hasNext(); ) {
            SysDept dept = (SysDept) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentId())) {
                recursionFn(depts, dept);
                returnList.add(dept);
            }
        }
        if (returnList.isEmpty()) {
            returnList = depts;
        }
        return returnList;
    }

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    @Override
    public List<TreeSelect> buildDeptTreeSelect(List<SysDept> depts) {
        List<SysDept> deptTrees = buildDeptTree(depts);
        return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
    }

    /**
     * 根据角色ID查询部门树信息
     *
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    @Override
    public List<Integer> selectDeptListByRoleId(String roleId) {
        return deptMapper.selectDeptListByRoleId(roleId);
    }

    /**
     * 根据部门ID查询信息
     *
     * @param deptId 部门ID
     * @return 部门信息
     */
    @Override
    public SysDept selectDeptById(String deptId) {
        SysDept sysDept = deptMapper.selectDeptById(deptId);
        if (sysDept != null) {
            String parentId = sysDept.getParentId();
            if (parentId == null || parentId.equals("0")) {
                sysDept.setParentName("无");
            } else {
                SysDept dept = deptMapper.selectDeptById(parentId);
                if(null != dept){
                    sysDept.setParentName(dept.getDeptName());
                }

            }

        }

        return sysDept;
    }

    /**
     * 是否存在子节点
     *
     * @param deptId 部门ID
     * @return 结果
     */
    @Override
    public boolean hasChildByDeptId(String deptId) {
        int result = deptMapper.hasChildByDeptId(deptId);
        return result > 0 ? true : false;
    }

    /**
     * 查询部门是否存在用户
     *
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    @Override
    public boolean checkDeptExistUser(String deptId) {
        int result = deptMapper.checkDeptExistUser(deptId);
        return result > 0 ? true : false;
    }

    /**
     * 校验部门名称是否唯一
     *
     * @param deptName 部门信息
     * @return 结果
     */
    @Override
    public String checkDeptNameUnique(String deptName) {
        int count = deptMapper.checkDeptNameUniqueNew(deptName);
        if (count > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;


       /* String deptId = StringUtils.isNull(dept.getDeptId()) ? "-1" : dept.getDeptId();
        SysDept info = deptMapper.checkDeptNameUnique(dept.getDeptName(), dept.getParentId());
        if (StringUtils.isNotNull(info) && !info.getDeptId().equals(deptId)) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;*/
    }

    /**
     * 校验部门编码是否唯一
     */
    @Override
    public String checkDeptCodeUnique(String deptCode) {
        int count = deptMapper.selectDeptByDeptCode(deptCode);
        if (count > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 新增保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public int insertDept(SysDept dept) {
        SysDept info = deptMapper.selectDeptById(dept.getParentId());
        // 如果父节点不为正常状态,则不允许新增子节点
        if (!UserConstants.DEPT_NORMAL.equals(info.getStatus())) {
            throw new CustomException("部门停用，不允许新增");
        }
        dept.setAncestors(info.getAncestors() + "," + dept.getParentId());
        //dept.setDeptId(UUID.randomUUID().toString().replaceAll("-", ""));
        return deptMapper.insertDept(dept);
    }

    /**
     * 修改保存部门信息
     *
     * @param dept 部门信息
     * @return 结果
     */
    @Override
    public int updateDept(SysDept dept) {
        SysDept newParentDept = deptMapper.selectDeptById(dept.getParentId());
        SysDept oldDept = deptMapper.selectDeptById(dept.getDeptId());
        if (StringUtils.isNotNull(newParentDept) && StringUtils.isNotNull(oldDept)) {
            String newAncestors = newParentDept.getAncestors() + "," + newParentDept.getDeptId();
            String oldAncestors = oldDept.getAncestors();
            dept.setAncestors(newAncestors);
            updateDeptChildren(dept.getDeptId(), newAncestors, oldAncestors);
        }
        int result = deptMapper.updateDept(dept);
/*        if (dept.getStatus() != null && UserConstants.DEPT_NORMAL.equals(dept.getStatus())) {
            // 如果该部门是启用状态，则启用该部门的所有上级部门
            updateParentDeptStatus(dept);
        }*/
        return result;
    }

    /**
     * 组织状态修改
     */
    @Transactional
    @Override
    public int changeStatus(SysDept dept) {

        List<SysDept> sysDepts = deptMapper.selectChildrenDeptById(dept.getDeptId());
        sysDepts.add(dept);
        String[] deptIds = new String[sysDepts.size()];
        for (int i = 0; i < sysDepts.size(); i++) {
            deptIds[i] = sysDepts.get(i).getDeptId();
        }
        int result =deptMapper.changeStatus(deptIds,dept.getStatus());

/*        if (dept.getStatus() != null && UserConstants.DEPT_NORMAL.equals(dept.getStatus())) {
            // 如果该部门是启用状态，则启用该部门的所有上级部门
            updateParentDeptStatus(dept);
        }*/
        return result;
    }

    /**
     * 修改该部门的父级部门状态
     *
     * @param dept 当前部门
     */
    private void updateParentDeptStatus(SysDept dept) {
        if (dept.getUpdateBy() != null){
            String updateBy = dept.getUpdateBy();
            dept.setUpdateBy(updateBy);
        }
        dept = deptMapper.selectDeptById(dept.getDeptId());
        deptMapper.updateDeptStatus(dept);
    }

    /**
     * 修改子元素关系
     *
     * @param deptId       被修改的部门ID
     * @param newAncestors 新的父ID集合
     * @param oldAncestors 旧的父ID集合
     */
    public void updateDeptChildren(String deptId, String newAncestors, String oldAncestors) {
        List<SysDept> children = deptMapper.selectChildrenDeptById(deptId);
        for (SysDept child : children) {
            child.setAncestors(child.getAncestors().replace(oldAncestors, newAncestors));
        }
        if (children.size() > 0) {
            deptMapper.updateDeptChildren(children);
        }
    }

    /**
     * 删除部门管理信息
     *
     * @param deptId 部门ID
     * @return 结果
     */
    @Override
    public int deleteDeptById(String deptId) {
        return deptMapper.deleteDeptById(deptId);
    }


    /**
     * 递归列表
     */
    private void recursionFn(List<SysDept> list, SysDept t) {
        // 得到子节点列表
        List<SysDept> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysDept tChild : childList) {
            if (hasChild(list, tChild)) {
                // 判断是否有子节点
                Iterator<SysDept> it = childList.iterator();
                while (it.hasNext()) {
                    SysDept n = (SysDept) it.next();
                    recursionFn(list, n);
                }
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysDept> getChildList(List<SysDept> list, SysDept t) {
        List<SysDept> tlist = new ArrayList<SysDept>();
        Iterator<SysDept> it = list.iterator();
        while (it.hasNext()) {
            SysDept n = (SysDept) it.next();
            if (StringUtils.isNotNull(n.getParentId()) && n.getParentId().equals(t.getDeptId())) {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysDept> list, SysDept t) {
        return getChildList(list, t).size() > 0 ? true : false;
    }

    /**
     * 获取组织列表
     *
     * @param
     * @return
     */
    @Override
    public List<SysDept> selectOrganList() {
        return deptMapper.selectOrganList();
    }

    /**
     * 删除组织信息
     *
     * @param deptId 组织ID
     * @return 结果
     */
    @Override
    public int deleteOrganById(String deptId) {
        return deptMapper.deleteOrganById(deptId);
    }

    /**
     * 数据同步添加组织机构
     * 0	SP返回错误码0,即视为成功
     * 400	参数异常
     */
    public AjaxResult insertSysDeptOne(Institution institution) {
        SysDept sysDept = new SysDept();
        sysDept.setDeptId(institution.getUuid());
        sysDept.setParentId(institution.getParentExternalId());
        sysDept.setAncestors(institution.getParentDirectory());
        sysDept.setCreateTime(institution.getCreateTime());
        sysDept.setDeptName(institution.getName());
        sysDept.setDeptCode(institution.getId());
        sysDept.setDeptCode(institution.getExternalId());
        if(institution.getRootNode()==true){
            sysDept.setRootNode("1");
        }else if(institution.getRootNode()==false){
            sysDept.setRootNode("2");
        }else{
            return AjaxResult.error("组织机构同步失败");
        }
        sysDept.setRegionId(institution.getRegionId());
        sysDept.setType(institution.getType());
        sysDept.setRemark(institution.getDescription());
        sysDept.setLevelNumber(institution.getLevelNumber());
        String[] managerUDAccountUuids = institution.getManagerUDAccountUuids();
        String str=String.join(",",managerUDAccountUuids);
        sysDept.setManagerUDAccountUuids(str);
        sysDept.setLastModifyTime(institution.getLastModifyTime());
        deptMapper.insertDept(sysDept);
        return AjaxResult.error("成功");
        /*if (institution == null) {
            return AjaxResult.error(400, "参数异常");
        }
        if (institution.getOrganization() == null || institution.getOrganization().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        if (institution.getRootNode() == null || institution.getRootNode().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        if (institution.getParentUuid() == null || institution.getParentUuid().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        if (institution.getOrganizationUuid() == null || institution.getOrganizationUuid().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        if (institution.getType() == null || institution.getType().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        if (institution.getType() != null) {
            if (institution.getType().equals("DEPARTMENT")) {
                if (institution.getRegionId() != null || !institution.getRegionId().trim().isEmpty()) {
                    return AjaxResult.error(400, "参数异常");
                }
            }
        }
        SysDept sysDept = new SysDept();
        List<Manager> managers = institution.getManager();
        if (managers != null && managers.size() != 0) {
            String s = "";
            String s2 = "";
            for (Manager manager : managers) {
                s += manager.getValue() + ",";
                s2 += manager.getDisplayName() + ",";
            }
            String s1 = s.substring(0, s.length() - 1);
            String s3 = s2.substring(0, s2.length() - 1);
            sysDept.setCreateId(s1);
            sysDept.setCreateName(s3);
        }
        System.out.println("这个值是：" + institution.getRootNode());
        sysDept.setDeptName(institution.getOrganization());
        if (institution.getRootNode() == "false") {
            sysDept.setAncestors("0");
        } else if (institution.getRootNode() == "true") {
            sysDept.setAncestors("0," + institution.getParentUuid());
        } else {
            return AjaxResult.error(400, "参数异常");
        }
        sysDept.setOrganizationUuid(institution.getOrganizationUuid());
        if (institution.getType().equals("DEPARTMENT")) {
            sysDept.setType("DEPARTMENT");
        }
        if (institution.getType().equals("SELF_OU")) {
            sysDept.setType("SELF_OU");
            if (institution.getRegionId() != null && !institution.getRegionId().trim().isEmpty()) {
                sysDept.setRegionId(institution.getRegionId());
            }
        }
        int i = deptMapper.insertDept(sysDept);
        if (i != 1) {
            return AjaxResult.error(-1, "数据同步添加组织失败");
        }
        return AjaxResult.success(0, "组织机构数据同步成功");*/
    }

    /**
     * 数据同步删除组织机构
     * 0	SP返回错误码0,即视为成功
     * 400	参数异常
     */
   /* public AjaxResult deleteOne(Institution institution) {
        if (institution == null) {
            return AjaxResult.error(400, "参数异常");
        }
        if (institution.getOrganizationUuid() == null || institution.getOrganizationUuid().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        int i = deptMapper.deleteOne(institution.getOrganizationUuid());
        if (i != 1) {
            return AjaxResult.error(-1, "数据同步删除组织机构失败");
        }
        return AjaxResult.success(0, "数据同步删除组织机构成功");
    }*/

    /**
     * 熟不同步更新组织机构
     * 0	SP返回错误码0,即视为成功
     * 400	参数异常t
     * -1   该对象不存在
     */
    /*public AjaxResult updetaDeptOne(Institution institution) {
        if (institution == null) {
            return AjaxResult.error(400, "参数异常");
        }
        if (institution.getOrganizationUuid() == null || institution.getOrganizationUuid().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        //根据本组织机构的uuid查询
        SysDept sysDept = deptMapper.selectByOrganizationUuid(institution.getOrganizationUuid());
        if (sysDept == null) {
            return AjaxResult.error(-1, "该组织机构不存在");
        }
        if (institution.getOrganization() != null && !institution.getOrganization().trim().isEmpty()) {
            sysDept.setDeptName(institution.getOrganization());
        }
        if (institution.getRootNode() != null && !institution.getRootNode().trim().isEmpty()) {
            if (institution.getRootNode() == "false") {
                sysDept.setAncestors("0");
            } else if (institution.getRootNode() == "true") {
                sysDept.setAncestors("0," + institution.getParentUuid());
            } else {
                return AjaxResult.error(400, "参数异常");
            }
        }
        if (institution.getParentUuid() != null && !institution.getParentUuid().trim().isEmpty()) {
            sysDept.setParentId(institution.getParentUuid());
        }
        if (institution.getOrganizationUuid() != null && !institution.getOrganizationUuid().trim().isEmpty()) {
            sysDept.setOrganizationUuid(institution.getOrganizationUuid());
        }
        if (institution.getType() != null && !institution.getType().trim().isEmpty()) {
            System.out.println("这个值为：" + institution.getType());
            if (institution.getType().equals("SELF_OU")) {
                if (institution.getRegionId() != null && !institution.getRegionId().trim().isEmpty()) {
                    sysDept.setRegionId(institution.getRegionId());
                }
                sysDept.setType(institution.getType());
            } else if (institution.getType().equals("DEPARTMENT")) {
                sysDept.setType(institution.getType());
            } else {
                return AjaxResult.error(400, "参数错误");
            }
        }
        List<Manager> managers = institution.getManager();
        if (managers != null && managers.size() != 0) {
            String s = "";
            String s2 = "";
            for (Manager manager : managers) {
                s += manager.getValue() + ",";
                s2 += manager.getDisplayName() + ",";
            }
            String s1 = s.substring(0, s.length() - 1);
            String s3 = s2.substring(0, s2.length() - 1);
            sysDept.setCreateId(s1);
            sysDept.setCreateName(s3);
        }
        int i = deptMapper.updateDept(sysDept);
        if (i != 1) {
            return AjaxResult.error(-1, "组织机构数据更新同步失败");
        }
        return AjaxResult.success(0, "组织机构数据更新同步成功");
    }*/

}
