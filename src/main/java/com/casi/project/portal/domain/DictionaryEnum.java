package com.casi.project.portal.domain;

/**
 * @author zhongzj
 * @Description:字典类型枚举值
 * @date 2019/11/19
 */

public enum DictionaryEnum {
    /**
     * 状态
     */
    Status("status"),
    /**
     * 审批流程
     */
    Process("process"),
    /**
     * 终端类别
     */
    Teminal("terminal"),
    /**
     * 其他终端栏目
     */
    Section("section"),

    /**
     * 新增
     */
    Add("add"),

    /**
     * 提交
     */
    Submit("submit");

    private String name;

    DictionaryEnum(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}


