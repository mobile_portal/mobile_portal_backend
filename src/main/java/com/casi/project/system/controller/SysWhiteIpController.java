package com.casi.project.system.controller;

import java.util.List;
import java.util.UUID;

import com.casi.common.utils.StringUtils;
import com.casi.project.system.domain.SysWhiteIp;
import com.casi.project.system.service.ISysWhiteIpService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 白名单管理Controller
 *
 * @author casi
 * @date 2020-06-16
 */
@Api(tags = {"白名单管理"})
@RestController
@RequestMapping("/system/white")
public class SysWhiteIpController extends BaseController {
    @Autowired
    private ISysWhiteIpService sysWhiteIpService;

    /**
     * 查询白名单管理列表
     */
    @ApiOperation(value = "查询白名单管理列表")
    @PreAuthorize("@ss.hasPermi('system:white/ip:list')")
    @PostMapping("/list")
    public TableDataInfo list(@RequestBody SysWhiteIp sysWhiteIp) {
        startPage();
        List<SysWhiteIp> list = sysWhiteIpService.selectSysWhiteIpList(sysWhiteIp);
        return getDataTable(list);
    }

    /**
     * 获取白名单管理详细信息
     */
    @ApiOperation(value = "获取白名单管理详细信息")
    @PreAuthorize("@ss.hasPermi('system:white/ip:query')")
    @PostMapping("uuid")
    public AjaxResult getInfo(String uuid) {
        return AjaxResult.success(sysWhiteIpService.selectSysWhiteIpById(uuid));
    }

    @ApiOperation(value = "查询白名单详细信息")
    @PostMapping("getByIp")
    public AjaxResult getInfoByIp(String whiteIp) {
        return AjaxResult.success("查询成功！", sysWhiteIpService.getInfoByIp(whiteIp));
    }

    /**
     * 新增白名单管理
     */
    @ApiOperation(value = "新增白名单IP")
    @PreAuthorize("@ss.hasPermi('system:white/ip:add')")
    @Log(title = "白名单管理", businessType = BusinessType.INSERT)
    @PostMapping(value = "add")
    public AjaxResult add(@RequestBody SysWhiteIp sysWhiteIp) {
        if(StringUtils.isEmpty(sysWhiteIp.getWhiteIp())){
            return  AjaxResult.error("IP不能为空");
        }
        sysWhiteIp.setUuid(UUID.randomUUID().toString().replaceAll("-", ""));
        SysWhiteIp old = sysWhiteIpService.getInfoByIp(sysWhiteIp.getWhiteIp());
        if(null != old){
            return  AjaxResult.error("此IP已存在");
        }
        return toAjax(sysWhiteIpService.insertSysWhiteIp(sysWhiteIp));
    }

    /**
     * 修改白名单管理
     */
    @ApiOperation(value = "修改白名单管理")
    @PreAuthorize("@ss.hasPermi('system:white/ip:edit')")
    @Log(title = "白名单管理", businessType = BusinessType.UPDATE)
    @PostMapping(value = "edit")
    public AjaxResult edit(@RequestBody SysWhiteIp sysWhiteIp) {
        return toAjax(sysWhiteIpService.updateSysWhiteIp(sysWhiteIp));
    }

    /**
     * 删除白名单管理
     */
    @ApiOperation(value = "删除白名单管理")
    @PreAuthorize("@ss.hasPermi('system:white/ip:remove')")
    @Log(title = "白名单管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    public AjaxResult remove(@RequestParam(value = "uuid") List<String> uuid) {
        return toAjax(sysWhiteIpService.deleteSysWhiteIpByIds(uuid));
    }
}
