package com.casi.project.store.mapper;

import java.util.List;
import com.casi.project.store.domain.StoreAppVersion;

/**
 * 应用版本Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface StoreAppVersionMapper 
{
    /**
     * 查询应用版本
     * 
     * @param uuid 应用版本ID
     * @return 应用版本
     */
    public StoreAppVersion selectStoreAppVersionById(String uuid);

    /**
     * 查询应用版本列表
     * 
     * @param storeAppVersion 应用版本
     * @return 应用版本集合
     */
    public List<StoreAppVersion> selectStoreAppVersionList(StoreAppVersion storeAppVersion);

    /**
     * 新增应用版本
     * 
     * @param storeAppVersion 应用版本
     * @return 结果
     */
    public int insertStoreAppVersion(StoreAppVersion storeAppVersion);

    /**
     * 修改应用版本
     * 
     * @param storeAppVersion 应用版本
     * @return 结果
     */
    public int updateStoreAppVersion(StoreAppVersion storeAppVersion);

    /**
     * 删除应用版本
     * 
     * @param uuid 应用版本ID
     * @return 结果
     */
    public int deleteStoreAppVersionById(String uuid);

    /**
     * 批量删除应用版本
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreAppVersionByIds(String[] uuids);
}
