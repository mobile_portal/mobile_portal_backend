package com.casi.project.portal.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/*应用具体信息*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApplyDetails implements Serializable {
    private String id;
    private String menuButtonName;
    //菜单按钮图片
    private String menuButtonIcon;
    //菜单按钮图片不带ip
    private String menuButtonIconNoIp;
    //菜单按钮跳转链接
    private String menuButtonUrl;
    //逻辑id-关联mobile_home_menu(应用类别)的ID
    private String buttonParentId;
    private Long isShow;
    private Long ifMandatory;
    private Long ifUp;
    private Long recommendApply;
    private Long buttonSort;
    private String synopsis;
    //逻辑id-关联mobile_app_type(应用类型)的ID
    private String applyTypeId;
    //应用的appKey 和appSecret
    private String appKey;
    private String appSecret;
    private String lbUrl;
    //组织结构id
    private String[] ids;
    private Integer checkStatus;
    private String createTime;
    private String updateTime;
    private String token;
    private String createBy;
    private Integer isIfream;
}
