package com.casi.project.tenant.service;

import java.util.List;
import com.casi.project.tenant.domain.TenantAppResources;

/**
 * 租户应用资源Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface ITenantAppResourcesService 
{
    /**
     * 查询租户应用资源
     * 
     * @param uuid 租户应用资源ID
     * @return 租户应用资源
     */
    public TenantAppResources selectTenantAppResourcesById(String uuid);

    /**
     * 查询租户应用资源列表
     * 
     * @param tenantAppResources 租户应用资源
     * @return 租户应用资源集合
     */
    public List<TenantAppResources> selectTenantAppResourcesList(TenantAppResources tenantAppResources);

    /**
     * 新增租户应用资源
     * 
     * @param tenantAppResources 租户应用资源
     * @return 结果
     */
    public int insertTenantAppResources(TenantAppResources tenantAppResources);

    /**
     * 修改租户应用资源
     * 
     * @param tenantAppResources 租户应用资源
     * @return 结果
     */
    public int updateTenantAppResources(TenantAppResources tenantAppResources);

    /**
     * 批量删除租户应用资源
     * 
     * @param uuids 需要删除的租户应用资源ID
     * @return 结果
     */
    public int deleteTenantAppResourcesByIds(String[] uuids);

    /**
     * 删除租户应用资源信息
     * 
     * @param uuid 租户应用资源ID
     * @return 结果
     */
    public int deleteTenantAppResourcesById(String uuid);
}
