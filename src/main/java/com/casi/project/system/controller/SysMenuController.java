package com.casi.project.system.controller;


import java.util.List;

import com.casi.common.utils.StringUtils;
import com.casi.framework.web.domain.TreeSelect;
import com.casi.framework.web.page.TableDataInfo;
import com.casi.project.system.mapper.SysUserRoleMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.casi.common.constant.UserConstants;
import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.ServletUtils;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.security.LoginUser;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.system.domain.SysMenu;
import com.casi.project.system.service.ISysMenuService;

/**
 * 菜单信息
 * 
 * @author lzb
 */
@Api(tags = { "菜单管理" })
@RestController
@RequestMapping("/system/menu")
public class SysMenuController extends BaseController
{
    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private TokenService tokenService;
    @Autowired
    private SysUserRoleMapper sysUserRoleMapper;

    /**
     * 根据SysMenu参数获取菜单列表
     */
    @PreAuthorize("@ss.hasPermi('system:menu:list')")
    @GetMapping("/list")
    @CrossOrigin
    public AjaxResult list(SysMenu menu)
    {
        logger.info("===============获取菜单列表=============");
        LoginUser loginUser;
        String userId;
        List<SysMenu> menus;
        try {
            loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            userId = loginUser.getUser().getUserId();
            menus = menuService.selectMenuList(menu, userId);
        } catch (Exception e) {
            logger.error("获取菜单列表失败", e);
            return AjaxResult.error("获取菜单列表失败");
        }

        return AjaxResult.success(menus);
    }

    /**
     * 获取菜单下拉列表
     */
    @ApiOperation(value = "获取菜单下拉列表")
    @PreAuthorize("@ss.hasPermi('system:menu:list')")
    @GetMapping("/menuListByMenuId")
    @CrossOrigin
    public AjaxResult menuListByMenuId(String menuId)
    {
        logger.info("===============获取菜单下拉列表=============");
        List<SysMenu> menus;
        try {
            menus = menuService.menuListByMenuId(menuId);
        } catch (Exception e) {
            logger.error("获取菜单下拉列表失败", e);
            return AjaxResult.error("获取菜单下拉列表失败");
        }

        return AjaxResult.success(menus);
    }


    /**
     * 获取登录用户角色的菜单列表
     */
    @PreAuthorize("@ss.hasPermi('system:menu:list')")
    @GetMapping("/menuList")
    public AjaxResult menuList(SysMenu sysMenu, Integer pageNum, Integer pageSize) {
        logger.info("===============获取菜单列表=============");
        if (pageNum == null || pageSize == null) {
            return AjaxResult.error("请传分页的参数当前页和每页显示的条数");
        }
        PageHelper.startPage(pageNum, pageSize);
        LoginUser loginUser;
        String userId;
        List<SysMenu> menus;
        try {
            loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            userId = loginUser.getUser().getUserId();
            menus = menuService.selectMenuList(sysMenu, userId);
        } catch (Exception e) {
            logger.error("获取菜单列表失败", e);
            TableDataInfo dataInfo = new TableDataInfo();
            dataInfo.setCode(500);
            dataInfo.setMsg("获取菜单列表失败");
            dataInfo.setTotal(0L);
            return AjaxResult.error("查询失败", dataInfo);
        }
        PageInfo<SysMenu> sysMenuPageInfo = new PageInfo<>(menus);
        return AjaxResult.success("查询成功", sysMenuPageInfo);
    }

    /**
     * 根据菜单编号获取详细信息
     */
    @ApiOperation(value = "菜单回显")
    @PreAuthorize("@ss.hasPermi('system:menu:edit')")
    @GetMapping(value = "/getMenuInfo")
    public AjaxResult getInfo(String menuId)
    {
        logger.info("===============根据菜单编号获取详细信息=============");
        SysMenu menu;

        try {
            menu = menuService.selectMenuById(menuId);
        } catch (Exception e) {
            logger.error("根据菜单编号获取详细信息失败", e);
            return AjaxResult.error("根据菜单编号获取详细信息失败");
        }
        return AjaxResult.success(menu);
    }

    /**
     * 菜单下拉树
     */
    @GetMapping("/treeselect")
    public AjaxResult treeselect(SysMenu menu)
    {

        logger.info("===============菜单下拉树=============");
        LoginUser loginUser;
        String userId;
        List<SysMenu> menus;
        List<TreeSelect> treeSelects;
        try {
            loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            userId = loginUser.getUser().getUserId();
            menus = menuService.selectMenuList(menu, userId);
            treeSelects = menuService.buildMenuTreeSelect(menus);
        } catch (Exception e) {
            logger.error("获取菜单下拉树失败", e);
            return AjaxResult.error("获取菜单下拉树失败");
        }
        return AjaxResult.success(treeSelects);
    }
    /**
     *  菜单下拉树
     */
    @ApiOperation(value = "菜单下拉树")
    @GetMapping("/treeselect1")
    public AjaxResult treeselectSysRole()
    {
        logger.info("===============加载菜单下拉树=============");
        LoginUser loginUser;
        List<SysMenu> menus;
        AjaxResult ajax;
        try {
            loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            menus = menuService.selectMenuList(loginUser.getUser().getUserId());
            ajax = AjaxResult.success();
            //根据用户id查询角色
            List<String> longs = sysUserRoleMapper.selectByUserId(loginUser.getUser().getUserId());
          // ajax.put("checkedKeys", menuService.selectMenuListByRoleId1(longs));
            ajax.put("menus", menuService.buildMenuTreeSelect(menus));
        } catch (Exception e) {
            logger.error("加载菜单下拉树失败", e);
            return AjaxResult.error("加载菜单下拉树失败");
        }
        return ajax;
    }
    /**
     * 加载对应角色菜单列表树
     */
    @GetMapping(value = "/roleMenuTreeselect/{roleId}")
    public AjaxResult roleMenuTreeselect(@PathVariable("roleId") String roleId)
    {
        logger.info("===============加载对应角色菜单列表树=============");
        LoginUser loginUser;
        List<SysMenu> menus;
        AjaxResult ajax;
        try {
            loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            menus = menuService.selectMenuList(loginUser.getUser().getUserId());
            ajax = AjaxResult.success();
            ajax.put("checkedKeys", menuService.selectMenuListByRoleId(roleId));
            ajax.put("menus", menuService.buildMenuTreeSelect(menus));
        } catch (Exception e) {
            logger.error("加载对应角色菜单列表树失败", e);
            return AjaxResult.error("加载对应角色菜单列表树失败");
        }
        return ajax;
    }

    /**
     * 新增菜单
     */
    @ApiOperation(value = "新增菜单")
    @PreAuthorize("@ss.hasPermi('system:menu:add')")
    @Log(title = "菜单管理", businessType = BusinessType.INSERT)
    @PostMapping("/addMenu")
    public AjaxResult add(@Validated @RequestBody SysMenu menu)
    {

        logger.info("===============新增菜单=============");
        int i;
        try {
            if (menu == null){
               return AjaxResult.error("功能内容不能为空");
            }
            if (StringUtils.isEmpty(menu.getMenuName())){
                return AjaxResult.error("功能名称不能为空");
            }
            if (StringUtils.isEmpty(menu.getMenuType())){
                return AjaxResult.error("功能类型不能为空");
            }
            if (StringUtils.isEmpty(menu.getMenuCode())){
                return AjaxResult.error("功能编码不能为空");
            }

            if (UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu)))
            {
                return AjaxResult.error("功能名称已存在");
            }

            if (UserConstants.NOT_UNIQUE.equals(menuService.checkMenuCodeUnique(menu.getMenuCode())))
            {
                return AjaxResult.error("功能编码已存在");
            }

            menu.setPath(menu.getUrlPath());
            if(("M".equals(menu.getMenuType()) || "C".equals(menu.getMenuType())) && StringUtils.isEmpty(menu.getPath())){
                return AjaxResult.error("请填写URL");
            }

            menu.setCreateBy(SecurityUtils.getUsername());
            i = menuService.insertMenu(menu);
        } catch (Exception e) {
            logger.error("新增菜单失败", e);
            return AjaxResult.error("新增菜单失败");
        }
        return toAjax(i);

    }

    /**
     * 修改菜单
     */
    @ApiOperation(value = "修改菜单")
    @PreAuthorize("@ss.hasPermi('system:menu:edit')")
    @Log(title = "菜单管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody SysMenu menu)
    {
        logger.info("===============修改菜单=============");
        int i;
        try {
            if (StringUtils.isEmpty(menu.getMenuCode())){
                return AjaxResult.error("功能编码不能为空");
            }
            if (!menu.getMenuCode().equals(menuService.selectMenuById(menu.getMenuId()).getMenuCode()) &&
                    UserConstants.NOT_UNIQUE.equals(menuService.checkMenuCodeUnique(menu.getMenuCode())))
            {
                return AjaxResult.error("功能编码已存在");
            }
            menu.setPath(menu.getUrlPath());
            if(("M".equals(menu.getMenuType()) || "C".equals(menu.getMenuType())) && StringUtils.isEmpty(menu.getPath())){
                return AjaxResult.error("请填写URL");
            }


/*            if (UserConstants.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu)))
            {
                return AjaxResult.error("功能名称已存在");
            }*/
            menu.setUpdateBy(SecurityUtils.getUsername());
            i = menuService.updateMenu(menu);
        } catch (Exception e) {
            logger.error("修改菜单失败", e);
            return AjaxResult.error("修改菜单失败");
        }
        return toAjax(i);

    }

    /**
     * 删除菜单
     */
    @ApiOperation(value = "删除菜单")
    @PreAuthorize("@ss.hasPermi('system:menu:remove')")
    @Log(title = "菜单管理", businessType = BusinessType.DELETE)
    @GetMapping("/deleteMenu")
    public AjaxResult remove(String menuId)
    {
        logger.info("===============删除菜单=============");
        int i;
        try {
            if (menuService.hasChildByMenuId(menuId))
            {
                return AjaxResult.error("存在子菜单,不允许删除");
            }
/*            if (menuService.checkMenuExistRole(menuId))
            {
                return AjaxResult.error("菜单已分配,不允许删除");
            }*/
            i= menuService.deleteMenuById(menuId);
        } catch (Exception e) {
            logger.error("删除菜单失败", e);
            return AjaxResult.error("删除菜单失败");
        }
        return toAjax(i);
    }
}