package com.casi.project.store.mapper;

import java.util.List;
import com.casi.project.store.domain.StoreAppTenant;

/**
 * 应用租户授权Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface StoreAppTenantMapper 
{
    /**
     * 查询应用租户授权
     * 
     * @param uuid 应用租户授权ID
     * @return 应用租户授权
     */
    public StoreAppTenant selectStoreAppTenantById(String uuid);

    /**
     * 查询应用租户授权列表
     * 
     * @param storeAppTenant 应用租户授权
     * @return 应用租户授权集合
     */
    public List<StoreAppTenant> selectStoreAppTenantList(StoreAppTenant storeAppTenant);

    /**
     * 新增应用租户授权
     * 
     * @param storeAppTenant 应用租户授权
     * @return 结果
     */
    public int insertStoreAppTenant(StoreAppTenant storeAppTenant);

    /**
     * 修改应用租户授权
     * 
     * @param storeAppTenant 应用租户授权
     * @return 结果
     */
    public int updateStoreAppTenant(StoreAppTenant storeAppTenant);

    /**
     * 删除应用租户授权
     * 
     * @param uuid 应用租户授权ID
     * @return 结果
     */
    public int deleteStoreAppTenantById(String uuid);

    /**
     * 批量删除应用租户授权
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreAppTenantByIds(String[] uuids);
}
