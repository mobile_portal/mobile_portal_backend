package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.MobileApplyUser;
import com.casi.project.system.domain.SysUserPost;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

public interface MobileApplyUserMapper {
    /**
     * 批量新增用户应用信息
     *list集合
     */
//    public int insertMobileApplyUsers(List<MobileApplyUser> applyUserList);

    /**
     * 批量新增用户应用信息
     * Set集合
     */
    public int insertMobileApplyUsers(@Param("tags") Set<MobileApplyUser> tags);

    /**
     * 根据用户id和应用id查询
     */
    public MobileApplyUser selectByApplyIdAndUserId(@Param("applyId") Long applyId, @Param("userId") String userId);


    /**
     * 批量新增组织机构id和应用名称信息
     * Set集合
     */
    public int insertMobileApplyDept(@Param("applyId") Long applyId, @Param("deptId") String deptId);

}
