package com.casi.project.system.controller;


import java.util.List;

import com.casi.common.utils.StringUtils;
import com.casi.project.system.domain.Institution;
import com.casi.project.system.domain.OrganizationUnitList;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.parameters.P;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.casi.common.constant.UserConstants;
import com.casi.common.utils.SecurityUtils;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.system.domain.Institution;
import com.casi.project.system.domain.SysDept;
import com.casi.project.system.service.ISysDeptService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 部门信息
 *
 * @author lzb
 */
@Api(tags = { "部门管理" })
@RestController
@RequestMapping("/system/dept")
public class SysDeptController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysDeptController.class);

    @Autowired
    private ISysDeptService deptService;

    /**
     * 获取部门列表
     */
    /*@PreAuthorize("@ss.hasPermi('system:dept:list')")
    @GetMapping("/list")
    public AjaxResult list(@RequestBody(required = false) SysDept dept) {
        logger.info("===============获取部门列表=============");
        List<SysDept> depts;
        try {
            depts = deptService.selectDeptList(dept);
        } catch (Exception e) {
            logger.error("获取部门列表失败", e);
            return AjaxResult.error("获取部门列表失败");
        }
        return AjaxResult.success(depts);

    }*/
    /**
     * 获取部门列表（模糊查询根据部门名称）
     */
    @ApiOperation(value = "部门列表")
    @PreAuthorize("@ss.hasPermi('system:dept:list')")
    @PostMapping("/list")
    public AjaxResult list(@RequestBody SysDept dept) {
        logger.info("===============获取部门列表=============");
        List<SysDept> depts;
        try {
            depts = deptService.selectDeptList(dept);
        } catch (Exception e) {
            logger.error("获取部门列表失败", e);
            return AjaxResult.error("获取部门列表失败");
        }
        return AjaxResult.success(depts);

    }

    /**
     * 根据部门编号获取详细信息
     */
    @ApiOperation(value = "根据部门编号获取详细信息")
    //@PreAuthorize("@ss.hasPermi('system:dept:query')")
    @GetMapping("/getDeptInfo")
    public AjaxResult getInfo(String deptId) {
        logger.info("===============根据部门编号获取详细信息=============");
        SysDept sysDept;
        try {
            logger.debug("=============deptId===============", deptId);
            sysDept = deptService.selectDeptById(deptId);
        } catch (Exception e) {
            logger.error("根据部门编号获取详细信息失败", e);
            return AjaxResult.error("根据部门编号获取详细信息失败");
        }
        return AjaxResult.success(sysDept);
    }

    /**
     * 获取部门下拉树列表
     */
    @GetMapping("/treeselect")
    public AjaxResult treeselect(@RequestBody(required = false) SysDept dept) {

        logger.info("===============获取部门下拉树列表=============");
        List<SysDept> depts;
        try {
            depts = deptService.selectDeptList(dept);
        } catch (Exception e) {
            logger.error("获取部门下拉树列表失败", e);
            return AjaxResult.error("获取部门下拉树列表失败");
        }
        return AjaxResult.success(depts);
    }

    /**
     * 加载对应角色部门列表树
     */
    @GetMapping(value = "/roleDeptTreeselect/{roleId}")
    public AjaxResult roleDeptTreeselect(@PathVariable("roleId") String roleId) {
        logger.info("===============加载对应角色部门列表树=============");
        List<SysDept> depts;
        AjaxResult ajax;
        try {
            depts = deptService.selectDeptList(new SysDept());
            ajax = AjaxResult.success();
            ajax.put("checkedKeys", deptService.selectDeptListByRoleId(roleId));
            ajax.put("depts", deptService.buildDeptTreeSelect(depts));
        } catch (Exception e) {
            logger.error("加载对应角色部门列表树失败", e);
            return AjaxResult.error("加载对应角色部门列表树失败");
        }

        return ajax;
    }


    /**
     * 根据当前用户权限获取部门列表树
     */
    @ApiOperation(value = "获取部门列表树")
    @GetMapping(value = "/roleDeptTreeselect1")
    public AjaxResult loginInRoleDeptTreeselect() {
        logger.info("===============根据当前用户权限获取部门列表树=============");
        List<SysDept> depts;
        AjaxResult ajax;
        try {
           // depts = deptService.selectDeptListByLoginIn();
            depts = deptService.selectDeptList(new SysDept());
            ajax = AjaxResult.success();
            if (StringUtils.isEmpty(depts)){
                ajax.put("depts","");
            return ajax;
            }
            if (depts.get(0) != null){
                ajax.put("depts", deptService.buildDeptTreeSelect(depts));
            }else {
                ajax.put("depts","");
            }
            ajax.put("checkedKeys", "");
        } catch (Exception e) {
            logger.error("根据当前用户权限获取部门列表树失败", e);
            return AjaxResult.error("根据当前用户权限获取部门列表树失败");
        }

        return ajax;
    }

    /**
     * 新增部门
     */
    @ApiOperation(value = "新增部门")
    @PreAuthorize("@ss.hasPermi('system:dept:add')")
    @Log(title = "部门管理", businessType = BusinessType.INSERT)
    @PostMapping("/addDept")
    public AjaxResult add(@Validated @RequestBody SysDept dept) {

        logger.info("===============新增部门=============");
        int i;
        try {
            if (dept == null) {
                return AjaxResult.error("部门内容不能为空");
            }
            if (StringUtils.isEmpty(dept.getDeptName())) {
                return AjaxResult.error("部门名称不能为空");
            }
            if (StringUtils.isEmpty(dept.getParentId()) ) {
                return AjaxResult.error("部门上级组织不能为空");
            }
            if (StringUtils.isEmpty(dept.getDeptCode())) {
                return AjaxResult.error("部门编码不能为空");
            }
            if (UserConstants.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept.getDeptName()))) {
                return AjaxResult.error("部门名称已存在");
            }

            if (UserConstants.NOT_UNIQUE.equals(deptService.checkDeptCodeUnique(dept.getDeptCode()))) {
                return AjaxResult.error("部门编码已存在");
            }
            dept.setCreateBy(SecurityUtils.getUsername());
            i = deptService.insertDept(dept);
        } catch (Exception e) {
            logger.error("新增部门失败", e);
            return AjaxResult.error("新增部门失败");
        }
        return toAjax(i);
    }

    /**
     * 修改部门
     */
    @ApiOperation(value = "修改部门")
    @PreAuthorize("@ss.hasPermi('system:dept:edit')")
    @Log(title = "部门管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@RequestBody SysDept dept) {

        logger.info("===============修改部门=============");
        int i;
        try {
            if (dept == null) {
                return AjaxResult.error("部门内容不能为空");
            }
            if (StringUtils.isEmpty(dept.getDeptId())) {
                return AjaxResult.error("请选择需要修改的部门");
            }
/*            if (dept.getParentId() == null || "".equals(dept.getParentId())){
                return AjaxResult.error("部门上级组织不能为空");
            }*/
            if (!dept.getDeptName().equals(deptService.selectDeptById(dept.getDeptId()).getDeptName()) &&
                    UserConstants.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept.getDeptName()))) {
                return AjaxResult.error("部门名称已存在");
            }
            if (StringUtils.isEmpty(dept.getDeptCode())) {
                return AjaxResult.error("部门编码不能为空");
            }

            if (!dept.getDeptCode().equals(deptService.selectDeptById(dept.getDeptId()).getDeptCode()) &&
                    UserConstants.NOT_UNIQUE.equals(deptService.checkDeptCodeUnique(dept.getDeptCode()))) {
                return AjaxResult.error("部门编码已存在");
            }

            dept.setUpdateBy(SecurityUtils.getUsername());
            i = deptService.updateDept(dept);
        } catch (Exception e) {
            logger.error("修改部门失败", e);
            return AjaxResult.error("修改部门失败");
        }
        return toAjax(i);
    }


    /**
     * 组织启用/禁用
     */
    @ApiOperation(value = "组织启用/禁用")
    @PreAuthorize("@ss.hasPermi('system:dept:edit')")
    @Log(title = "部门管理", businessType = BusinessType.UPDATE)
    @PostMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody SysDept dept) {

        logger.info("===============组织状态修改=============");
        int i;
        try {
            dept.setUpdateBy(SecurityUtils.getUsername());
            i = deptService.changeStatus(dept);
        } catch (Exception e) {
            logger.error("组织启用/禁用修改失败", e);
            return AjaxResult.error("组织启用/禁用修改失败");
        }
        return toAjax(i);
    }


    /**
     * 删除部门
     */
    @ApiOperation(value = "删除部门")
    @PreAuthorize("@ss.hasPermi('system:dept:remove')")
    @Log(title = "部门管理", businessType = BusinessType.DELETE)
    @GetMapping("/deleteDept")
    public AjaxResult remove(String deptId) {

        logger.info("===============删除部门=============");
        int i;
        try {
            if (deptService.hasChildByDeptId(deptId)) {
                return AjaxResult.error("存在下级部门,不允许删除");
            }
            if (deptService.checkDeptExistUser(deptId)) {
                return AjaxResult.error("部门存在用户,不允许删除");
            }
            i = deptService.deleteDeptById(deptId);
        } catch (Exception e) {
            logger.error("删除部门失败", e);
            return AjaxResult.error("删除部门失败");
        }
        return toAjax(i);
    }

    /**
     * 数据同步添加组织机构
     *//*
    @GetMapping("/addSysDeptOne")
    public AjaxResult addSysDeptOne(@RequestBody Institution institution) {
        return deptService.insertSysDeptOne(institution);
    }*/
    /**
     * 数据同步添加组织机构
     */
    @RequestMapping("/addSysDeptOne")
    public AjaxResult addSysDeptOne(@RequestBody OrganizationUnitList organizationUnitList) {
        List<Institution> organizationUnitList1 = organizationUnitList.getOrganizationUnitList();
        for(Institution institution : organizationUnitList1){
            deptService.insertSysDeptOne(institution);
        }
        return AjaxResult.success("组织机构数据同步成功");
//        return deptService.insertSysDeptOne(institution);
    }

    /**
     * 数据同步删除组织机构
     */
    /*@GetMapping("/deleteDeptByOrganizationUuid")
    public AjaxResult deleteDeptByOrganizationUuid(@RequestBody Institution institution) {
        return deptService.deleteOne(institution);
    }*/

    /**
     * 数据同步更新组织机构
     */
    /*@GetMapping("/updateDeptByOrganizationUuid")
    public AjaxResult updateOne(@RequestBody Institution institution) {
        return deptService.deleteOne(institution);
    }
*/


}
