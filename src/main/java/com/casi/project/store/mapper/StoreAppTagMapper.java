package com.casi.project.store.mapper;

import java.util.List;
import com.casi.project.store.domain.StoreAppTag;

/**
 * 应用标签关联Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface StoreAppTagMapper 
{
    /**
     * 查询应用标签关联
     * 
     * @param uuid 应用标签关联ID
     * @return 应用标签关联
     */
    public StoreAppTag selectStoreAppTagById(String uuid);

    /**
     * 查询应用标签关联列表
     * 
     * @param storeAppTag 应用标签关联
     * @return 应用标签关联集合
     */
    public List<StoreAppTag> selectStoreAppTagList(StoreAppTag storeAppTag);

    /**
     * 新增应用标签关联
     * 
     * @param storeAppTag 应用标签关联
     * @return 结果
     */
    public int insertStoreAppTag(StoreAppTag storeAppTag);

    /**
     * 修改应用标签关联
     * 
     * @param storeAppTag 应用标签关联
     * @return 结果
     */
    public int updateStoreAppTag(StoreAppTag storeAppTag);

    /**
     * 删除应用标签关联
     * 
     * @param uuid 应用标签关联ID
     * @return 结果
     */
    public int deleteStoreAppTagById(String uuid);

    /**
     * 批量删除应用标签关联
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreAppTagByIds(String[] uuids);
}
