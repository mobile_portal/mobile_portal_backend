package com.casi.project.portal.domain;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther shp
 * @data2020/4/2118:31
 * @description
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class MobileType {

    private Long id;
    private Long typeSort;
    private String typeName;
    private String createTime;
    private String updateTime;

}
