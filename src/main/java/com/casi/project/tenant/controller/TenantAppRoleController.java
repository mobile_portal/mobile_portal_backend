package com.casi.project.tenant.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.tenant.domain.TenantAppRole;
import com.casi.project.tenant.service.ITenantAppRoleService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 租户自定义应用角色Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"租户自定义应用角色"})
@RestController
@RequestMapping("/tenant/appRole")
public class TenantAppRoleController extends BaseController
{
    @Autowired
    private ITenantAppRoleService tenantAppRoleService;

    /**
     * 查询租户自定义应用角色列表
     */
    @ApiOperation(value = "查询租户自定义应用角色列表")
    @PreAuthorize("@ss.hasPermi('tenant:appRole:list')")
    @GetMapping("/list")
    public TableDataInfo list(TenantAppRole tenantAppRole)
    {
        startPage();
        List<TenantAppRole> list = tenantAppRoleService.selectTenantAppRoleList(tenantAppRole);
        return getDataTable(list);
    }

    /**
     * 导出租户自定义应用角色列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:appRole:export')")
    @Log(title = "租户自定义应用角色", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TenantAppRole tenantAppRole)
    {
        List<TenantAppRole> list = tenantAppRoleService.selectTenantAppRoleList(tenantAppRole);
        ExcelUtil<TenantAppRole> util = new ExcelUtil<TenantAppRole>(TenantAppRole.class);
        return util.exportExcel(list, "appRole");
    }

    /**
     * 获取租户自定义应用角色详细信息
     */
    @ApiOperation(value = "获取租户自定义应用角色详细信息")
    @PreAuthorize("@ss.hasPermi('tenant:appRole:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(tenantAppRoleService.selectTenantAppRoleById(uuid));
    }

    /**
     * 新增租户自定义应用角色
     */
    @ApiOperation(value = "新增租户自定义应用角色")
    @PreAuthorize("@ss.hasPermi('tenant:appRole:add')")
    @Log(title = "租户自定义应用角色", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TenantAppRole tenantAppRole)
    {
        return toAjax(tenantAppRoleService.insertTenantAppRole(tenantAppRole));
    }

    /**
     * 修改租户自定义应用角色
     */
    @ApiOperation(value = "修改租户自定义应用角色")
    @PreAuthorize("@ss.hasPermi('tenant:appRole:edit')")
    @Log(title = "租户自定义应用角色", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TenantAppRole tenantAppRole)
    {
        return toAjax(tenantAppRoleService.updateTenantAppRole(tenantAppRole));
    }

    /**
     * 删除租户自定义应用角色
     */
    @ApiOperation(value = "删除租户自定义应用角色")
    @PreAuthorize("@ss.hasPermi('tenant:appRole:remove')")
    @Log(title = "租户自定义应用角色", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(tenantAppRoleService.deleteTenantAppRoleByIds(uuids));
    }
}
