package com.casi.project.tenant.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 租户账号对象 tenant_user
 * 
 * @author casi
 * @date 2020-06-02
 */
public class TenantUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 登录账号 */
    @Excel(name = "登录账号")
    private String username;

    /** 所属公司 */
    @Excel(name = "所属公司")
    private String tenantId;

    /** 密码 */
    private String password;

    /** 盐 */
    private String salt;

    /** 姓名 */
    @Excel(name = "姓名")
    private String name;

    /** 角色编号 */
    @Excel(name = "角色编号")
    private String roleId;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setTenantId(String tenantId) 
    {
        this.tenantId = tenantId;
    }

    public String getTenantId() 
    {
        return tenantId;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }
    public void setSalt(String salt) 
    {
        this.salt = salt;
    }

    public String getSalt() 
    {
        return salt;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setRoleId(String roleId) 
    {
        this.roleId = roleId;
    }

    public String getRoleId() 
    {
        return roleId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("status", getStatus())
            .append("username", getUsername())
            .append("tenantId", getTenantId())
            .append("password", getPassword())
            .append("salt", getSalt())
            .append("name", getName())
            .append("roleId", getRoleId())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
