package com.casi.project.screen.util;

import java.math.BigDecimal;

/**
 * @Auther shp
 * @data2020/5/1211:56
 * @description DefaultLoadUtil.isDefault()
 * DefaultLoadUtil.toMillion()
 */
public class DefaultLoadUtil {
    /**
     * 模糊查询时使用  对SQL的拼接
     * @param str
     * @return
     */
    public static String isDefault(String  str){
        return str==null?"%%":"%"+str+"%";
    }


    /**
     * 以亿级为单位并进行四舍五入
     * @param l
     * @return
     */
    public static double toMillion(Long l){
        double d = l.doubleValue();
        double f=d/100000000;
        //将f的值转换为字符串这样不会造成精度损失
        BigDecimal b = new BigDecimal(f+"");
        double f1 = b.setScale(2,   BigDecimal.ROUND_HALF_UP).doubleValue();
        return f1;
    }

}
