package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.MobileHomeLb;
import com.casi.project.portal.domain.MobileHomeMenu;
import com.casi.project.portal.domain.MobileHomeMenuDetails;
import com.casi.project.portal.domain.MoblieNews;
import com.casi.project.portal.domain.vo.NoticeVO;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * @Auther shp
 * @data2020/4/1510:31
 * @description
 */

@Mapper
public interface IndexApiMapper {


    @Select("SELECT id,news_title newsTitle,news_img  newsImg,news_descript newsDescript,create_time  createTime,update_time  updateTime, news_source newsSource,news_url newsUrl FROM moblie_news ORDER BY id asc")
    List<MoblieNews> findNews();


    @Select("SELECT m.id,m.title,m.push_time pushTime,m.notices FROM mobile_notice m where is_show=0 and is_online=0 ORDER BY m.id desc;")
    List<NoticeVO> findNotice();


    /**
     * 返回轮播信息
     *
     * @return
     */
    @Select("SELECT m.menu_button_url lbAppUrl,b.id,b.lb_name lbName,b.lb_soft lbSoft,b.lb_url lbUrl,b.create_time createTime,b.update_time updateTime,m.`is_ifream` isIfream FROM mobile_home_lb b left join mobile_home_menu_details m on m.`id`=b.`home_menu_details_id` where b.is_show=0 AND m.recommend_apply = 0 ORDER BY b.create_time DESC LIMIT 3")
    List<MobileHomeLb> findMobileHomeLb();


    /**
     * 根据id查询详细的按钮信息
     * @param pid
     * @param userId
     * @return
     */
    //@Select("SELECT * FROM (SELECT s.id,s.menu_button_icon menuButtonIcon,s.menu_button_name menuButtonName,s.button_parent_id buttonParentId,s.menu_button_url menuButtonUrl  FROM mobile_home_menu_details s where button_parent_id='3' and s.is_show=0 ORDER BY s.button_sort ASC) a, mobile_apply_users u WHERE a.id=u.applyId AND u.is_show=0 AND u.userId=#{userId}")
    //List<MobileHomeMenuDetails> findById(@Param("pid") Long pid,@Param("userId")Long userId);


    /**
     * 根据id查询详细的按钮信息
     *
     * @param id
     * @return
     */
    @Select("SELECT * FROM(SELECT s.button_sort sort,s.id,s.menu_button_icon menuButtonIcon,s.menu_button_name menuButtonName,s.button_parent_id buttonParentId,s.menu_button_url menuButtonUrl, s.is_ifream isIfream FROM mobile_home_menu_details s where check_status=1 and  button_parent_id=#{id} and s.is_show=0 AND s.is_del=0 ORDER BY s.button_sort ASC) m,mobile_apply_users u WHERE  u.is_show=0 AND m.id=u.applyId AND u.userId='1'  ORDER BY m.sort ASC")
    List<MobileHomeMenuDetails> findById(String id);

    @Select("SELECT * FROM(SELECT s.button_sort sort,s.id,s.menu_button_icon menuButtonIcon,s.menu_button_name menuButtonName,s.button_parent_id buttonParentId,s.menu_button_url menuButtonUrl, s.is_ifream isIfream FROM mobile_home_menu_details s where check_status=1 and  button_parent_id=#{id} and s.is_show=0 AND s.is_del=0 ORDER BY s.button_sort ASC) m,mobile_apply_users u WHERE  u.is_show=1 AND m.id=u.applyId AND u.userId='1'  ORDER BY m.sort ASC")
    List<MobileHomeMenuDetails> findMyAppMenuList(String id);

    /**
     * 多对多查询按钮信息
     *
     * @return
     */

    @Select("SELECT u.id,u.menu_name menuName,u.parent_id parentId,u.skip_url skipUrl," +
            "u.create_time createTime,update_time updateTime FROM mobile_home_menu u " +
            "where u.is_show=0 and u.menu_type = '0' ORDER BY menu_sort asc;")
    List<MobileHomeMenu> findAll();


    // 在更多中展示
    @Select("SELECT u.id,u.menu_name menuName,u.parent_id parentId,u.skip_url skipUrl,u.create_time createTime,update_time updateTime FROM mobile_home_menu u where u.is_show=0 and u.menu_type = '1' ORDER BY menu_sort asc;")
    List<MobileHomeMenu> getAddMyAppMenu();

    // 在首页中展示
    @Select("SELECT   * FROM  (SELECT   s.button_sort sort,   s.id,  s.menu_button_icon menuButtonIcon, s.menu_type menuType , s.menu_button_name menuButtonName,   s.button_parent_id buttonParentId,  s.menu_button_url menuButtonUrl," +
            "    s.is_ifream isIfream  FROM    mobile_home_menu_details s where check_status = 1   and button_parent_id in   (SELECT     u.id  FROM mobile_home_menu u " +
            "    where u.is_show = 0  and u.menu_type = '1')   and s.is_show = 0    AND s.is_del = 0   ORDER BY s.button_sort ASC) m,  mobile_apply_users u WHERE u.is_show = 0   AND m.id = u.applyId   AND u.userId = '1' ORDER BY m.sort ASC ")
    List<MobileHomeMenuDetails> getAddMyAppMenuInIndex();


    /**
     * 根据应用类别查询应用集合
     *
     * @param typeId
     * @return
     */
    @Select("SELECT s.menu_button_icon menuButtonIcon,s.menu_button_name menuButtonName,s.button_parent_id buttonParentId,s.menu_button_url menuButtonUrl  FROM mobile_home_menu_details s where button_parent_id=#{typeId} and s.is_show=0 ORDER BY s.button_sort ASC;")
    List<MobileHomeMenuDetails> getMoreApply(Long typeId);

    /**
     * 搜索栏模糊查询
     *
     * @param appName
     * @return
     */

    List<MobileHomeMenuDetails> searchApp(Map<String,Object> params);


    /**
     * 隐藏应用按钮
     *
     * @param id
     * @return
     */
    @Update("UPDATE mobile_apply_users set is_show =1 WHERE applyId=#{id}")
    Long hidden(@Param("id") Long id);

    /**
     * 显示应用按钮
     *
     * @param id
     * @return
     */
    @Update("UPDATE mobile_apply_users set is_show =0 WHERE applyId=#{id}")
    int show(@Param("id") String id);


    /**
     * 获取添加页面  根据pid  和 用户id
     *
     * @param pid
     * @return
     */
    @Select("SELECT * FROM(SELECT id,menu_button_name  menuButtonName,menu_button_icon  menuButtonIcon, menu_button_url  menuButtonUrl FROM mobile_home_menu_details WHERE is_del=0 AND is_show=0 AND button_parent_id=#{pid} ) a,mobile_apply_users b WHERE b.is_show=1 and a.id=b.applyId  AND b.userId=#{userId}")
    List<MobileHomeMenuDetails> findAddApp(@Param("pid") Long pid, @Param("userId") String userId);

    // 查询应用详情表的父id
    @Select("SELECT button_parent_id FROM mobile_home_menu_details WHERE id = #{id}")
    String findMenuIdByDetailsId(@Param("id") String id);

    // 查询应用父表的类型(0-首页展示,1-更多添加展示)
    @Select("SELECT menu_type FROM mobile_home_menu WHERE id = #{id}")
    String findMenuBypeByMenuId(@Param("id") String id);

    // 查询首页展示应用个数
    @Select("SELECT COUNT(id) FROM mobile_home_menu_details WHERE button_parent_id IN (SELECT id FROM mobile_home_menu " +
            "WHERE menu_type = '1') AND id IN (SELECT applyId FROM mobile_apply_users WHERE userId=#{userId} AND is_show = 0)")
    int findDetailsCount(@Param("userId") String userId);

    // 查询"集团应用"展示个数
    @Select("SELECT count(id) FROM mobile_home_menu_details WHERE button_parent_id IN (SELECT id FROM mobile_home_menu  WHERE menu_type = '0' ) AND id IN (SELECT applyId FROM mobile_apply_users WHERE userId=#{userId} AND is_show = 0) AND button_parent_id = '1'")
    int findGroupDetailsCount(@Param("userId") String userId);

    @Select("SELECT de.menu_type menuType,de.id,de.menu_button_name menuButtonName,de.menu_button_icon menuButtonIcon,de.menu_button_url menuButtonUrl," +
            "de.is_ifream isIfream,m.menu_name groupName FROM mobile_home_menu_details de JOIN mobile_apply_users us ON us.applyId = de.id " +
            "join mobile_home_menu m on m.id = de.button_parent_id " +
            "WHERE us.is_show = #{show} AND de.is_disable = '0' AND de.is_del='0' AND userId = #{userId} AND button_parent_id = #{pid}")
    List<MobileHomeMenuDetails> getGroupApp(@Param("userId") String userId, @Param("pid") String pid,@Param("show") String show);

    List<MobileHomeMenuDetails> getAppDataListByParams(Map<String, Object> params);

    int updateMenuShow(Map<String, Object> params);

    int getMyShowAppCount(Map<String, Object> params);

    void insertData(Map<String, Object> params);

    int getInterfaceAppShow(Map<String, Object> map1);

    List<MobileHomeMenuDetails> getMyAppDetiles(Map<String, Object> params);
}
