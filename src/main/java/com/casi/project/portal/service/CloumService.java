package com.casi.project.portal.service;

import com.casi.project.portal.domain.Cloum;

import java.util.List;

/**
 * @Auther shp
 * @data2020/4/2416:34
 * @description
 */


public interface CloumService  {


    /**
     * 查询单条数据
     * @param cloum
     * @return
     */
    Cloum findById(Cloum cloum);


    /**
     * 返回栏目数据
     * @param cloum
     * @return
     */
    List<Cloum> findCloum(Cloum cloum);

    /**
     * 添加栏目
     * @param cloum
     * @return
     */
    Integer addCloum(Cloum cloum);

    /**
     * 修改栏目
     * @param cloum
     * @return
     */
    Integer editCloum(Cloum cloum);

    /**
     * 删除栏目
     * @param cloum
     * @return
     */
    Integer delCloum(Cloum cloum);
}
