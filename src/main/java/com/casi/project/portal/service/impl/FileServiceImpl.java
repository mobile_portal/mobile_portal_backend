package com.casi.project.portal.service.impl;


import com.casi.project.portal.domain.LocalFile;
import com.casi.project.portal.domain.MediaEnum;
import com.casi.project.portal.mapper.FileMapper;
import com.casi.project.portal.service.FileService;
import com.casi.project.portal.util.CommonApi;
import com.casi.project.portal.util.MediaTypeUtil;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Random;

/**
 * @Author zhongzj
 * @Description:
 * @Create: 2019/11/28
 */
@Service
@Transactional
public class FileServiceImpl implements FileService {

    @Autowired
    private FileMapper fileMapper;

    @Override
    @Transactional(rollbackFor = {Exception.class,RuntimeException.class})
    public Long uploadFile(MultipartFile multipartFile, String type) {
        //校验文件格式和大小是否满足要求


        Long fileId = null;
        //原文件名称
        String trueName = multipartFile.getOriginalFilename();
        //文件后缀名
        String ext = FilenameUtils.getExtension(trueName);

        if (MediaEnum.Image.toString().equals(type) && !MediaTypeUtil.isImg(ext)){
                throw new RuntimeException("图片格式不正确");
        }
        if (MediaEnum.Video.toString().equals(type) && !MediaTypeUtil.isVideo(ext)){
            throw new RuntimeException("视频格式不正确");
        }
        if (MediaEnum.Voice.toString().equals(type) && !MediaTypeUtil.isAudio(ext)){
            throw new RuntimeException("语音格式不正确");
        }

        //系统生成的文件名
        String fileName = multipartFile.getOriginalFilename();
        fileName = System.currentTimeMillis() + new Random().nextInt(10000) + "." + ext;
        //文件上传路径
//        String filePath = CommonApi.UPLOAD_IMG_URL + fileName;//windows
        String filePath = CommonApi.UPLOAD_IMG_URL_BASE + fileName;//linux


        File saveFile = new File(filePath);

//        if (!saveFile.exists()) {
//            saveFile.mkdirs();
//        }
        try {
            multipartFile.transferTo(saveFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //保存文件信息进数据库
        LocalFile localFile = new LocalFile();
        localFile.setFileName(trueName);
        localFile.setFileUrl(filePath);
        localFile.setSize((int)multipartFile.getSize());
        localFile.setName(fileName);
        if (0 < fileMapper.insert(localFile)){
            fileId = localFile.getId();
        }
        return fileId;
    }

    @Override
    public LocalFile queryById(Long id) {
        return fileMapper.queryById(id);
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,RuntimeException.class})
    public boolean delete(Long id) {
        boolean result = false;
        LocalFile localFile = fileMapper.queryById(id);
        File file = new File(localFile.getFileUrl());
        boolean exis = file.exists();     // 判断目录或文件是否存在
        boolean isex = file.isFile();     // 判断是否为文件
        boolean isdelete = file.delete();  //删除文件（需要判断自行 if）
        int fileResult = fileMapper.delete(id); //删除数据库
        if (0 < fileResult && exis && isex && isdelete){
            result = true;
        }
        return result;
    }


}
