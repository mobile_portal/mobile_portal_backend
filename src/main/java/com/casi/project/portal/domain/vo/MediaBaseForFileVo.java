package com.casi.project.portal.domain.vo;

import java.util.Date;

/**
 * @Author zhongzj
 * @Description:
 * @Param:
 * @Return:
 * @Create: 2019/11/23
 */
public class MediaBaseForFileVo {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 状态（1：待提交，2：审核中，3：已审核，4：已发布，5：已退回）
     */
    private String status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 文件名称
     */
    private String name;
    /**
     * 素材上传路径
     */
    private String uploadUrl;
    /**
     * 文件大小
     */
    private int size;
    /**
     * 视频标题（仅视频素材有）
     */
    private String title;
    /**
     * 视频描述（仅视频素材有）
     */
    private String introduction;
    /**
     * 更新时间
     */
    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUploadUrl() {
        return uploadUrl;
    }

    public void setUploadUrl(String uploadUrl) {
        this.uploadUrl = uploadUrl;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}
