package com.casi.project.portal.controller;

import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.*;
import com.casi.project.portal.domain.vo.NoticeVO;
import com.casi.project.portal.service.IndexApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther shp
 * @data2020/4/1311:25
 * @description
 */

@RequestMapping("/indexApi")
//@RequestMapping("/portal/indexApi")
@Api(tags = "首页相关接口")
@RestController
public class IndexApiController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(IndexApiController.class);

    @Autowired
    private IndexApiService service;


    /** 
    * @Description: 查询首页应用数据
    * @Param:  
    * @return:  
    * @Author: y_xiaopeng 
    * @Date: 2020/5/22 
    */
    @ApiOperation(value = "查询首页应用数据")
    @PostMapping("/getAppDataListByParams")
    public  List<MobileHomeMenuDetails> getAppDataListByParams(@RequestBody Map<String,Object> params){
        return service.getAppDataListByParams(params);

    }

    
    /** 
    * @Description: 修改首页是否显示
    * @Param:  
    * @return:  
    * @Author: y_xiaopeng 
    * @Date: 2020/5/22 
    */
    @ApiOperation(value = "首页是否显示")
    @PostMapping("/updateMenuShow")
    public AjaxResult updateMenuShow(@RequestBody Map<String,Object> params){
        return service.updateMenuShow(params);
    }

    
    /** 
    * @Description: 获取我的应用详情   xin
    * @Param:  
    * @return:  
    * @Author: y_xiaopeng 
    * @Date: 2020/5/23 
    */
    @ApiOperation(value = "获取我的应用详情")
    @PostMapping("/getMyAppDetilesByParams")
    public List<Map<String,Object>> getMyAppDetilesByParams(@RequestBody Map<String,Object> params){

        return service.getMyAppDetiles(params);

    }


    @ApiOperation("返回办公/商店/服务的数据")
    @GetMapping("getAllMenu")
    List<MobileHomeMenu> getAllMenu() {
        return service.getAllMenu();
    }

    
    /** 
    * @Description: 获取已安装应用列表
    * @Param:  
    * @return:  
    * @Author: y_xiaopeng 
    * @Date: 2020/5/21 
    */
    @ApiOperation(value = "获取已安装应用列表")
    @GetMapping("/getMyApp")
    List<MobileHomeMenuDetails> getMyApp(){
        return service.getMyApp("0");
    }

//    @PostMapping("/getMyAppDetails")
//    List<Map<String,Object>> getMyAppDetails(@RequestBody Map<String,Object> params){
//        List<Map<String,Object>> rul = new ArrayList<>();
//        List<MobileHomeMenuDetails> myApp = service.getAppDataListByParams(params);
//
//        Map<String, List<MobileHomeMenuDetails>> map = new HashMap<>();
//        if(myApp != null && myApp.size()>0){
//            for(MobileHomeMenuDetails details : myApp){
//                if ("更多".equals(details.getMenuButtonName()) || "添加".equals(details.getMenuButtonName()))
//                    continue;
//                if(map.containsKey(details.getGroupName())){//map中存在此id，将数据存放当前key的map中
//                    map.get(details.getGroupName()).add(details);
//                }else{//map中不存在，新建key，用来存放数据
//                    List<MobileHomeMenuDetails> tmpList = new ArrayList<>();
//                    tmpList.add(details);
//                    map.put(details.getGroupName(), tmpList);
//                }
//            }
//        }
//
//        for (Map.Entry<String, List<MobileHomeMenuDetails>> entry : map.entrySet()) {
//            Map<String,Object> maps = new HashMap<>();
//            maps.put("name",entry.getKey());
//            maps.put("data",entry.getValue());
//            rul.add(maps);
//        }
//        return rul;
//    }
    
    /** 
    * @Description: 获取集团应用列表
    * @Param:  
    * @return:  
    * @Author: y_xiaopeng 
    * @Date: 2020/5/21 
    */
    @ApiOperation(value = "获取集团应用列表")
    @GetMapping("/getGroupApp")
    List<MobileHomeMenuDetails> getGroupApp(){
        return service.getGroupApp();
    }
    
    /** 
    * @Description: 推荐应用列表
    * @Param:  
    * @return:  
    * @Author: y_xiaopeng 
    * @Date: 2020/5/21 
    */
    @ApiOperation(value = "推荐应用列表")
    @GetMapping("/getrRecommendedApp")
    List<MobileHomeMenuDetails> getrRecommendedApp(){
        return service.getrRecommendedApp();
    }

    
    /** 
    * @Description: 查询应用商店详情
    * @Param:  
    * @return:  
    * @Author: y_xiaopeng 
    * @Date: 2020/5/21 
    */
    @ApiOperation(value = "查询应用商店详情")
    @GetMapping("/getRecommendedAppDetl")
    public List<Map<String,Object>> getRecommendedAppDetl(){
        Map<String,Object> map = new HashMap<>();
        Map<String,Object> params = new HashMap<>();
        //已安装应用
        params.put("pid","3");
        params.put("isInterface","1");
        List<MobileHomeMenuDetails> myApp = service.getAppDataListByParams(params);
        for(MobileHomeMenuDetails data : myApp){
            if(data.getMenuButtonName().equalsIgnoreCase("更多")||data.getMenuButtonName().equals("添加")){
                myApp.remove(data);
                break;
            }

        }
        map.put("title","已安装应用");
        map.put("data",myApp);

        //推荐应用
        params.put("pid","2");
        List<MobileHomeMenuDetails> mobileHomeMenuDetails = service.getAppDataListByParams(params);
        for(MobileHomeMenuDetails data2 : mobileHomeMenuDetails){
            if(data2.getMenuButtonName().equalsIgnoreCase("更多")||data2.getMenuButtonName().equals("添加")){
                mobileHomeMenuDetails.remove(data2);
                break;
            }

        }
        Map<String,Object> map2 = new HashMap<>();
        map2.put("title","推荐应用");
        map2.put("data",mobileHomeMenuDetails);
        List<Map<String,Object>> rul = new ArrayList<>();
        rul.add(map2);
        rul.add(map);


        return rul;
    }


    /**
     * 在"我的应用"添加中出现的菜单
     *
     * @return
     */
    @ApiOperation("返回我的应用添加中出现的菜单的数据")
    @GetMapping("getAddMyAppMenu")
    AjaxResult getAddMyAppMenu() {
        List<MobileHomeMenu> mobileHomeMenus = service.getAddMyAppMenu();
        return AjaxResult.success(mobileHomeMenus);
    }

    /**
     * 添加应用
     *
     * @return
     */
    @ApiOperation("添加应用")
    @GetMapping("addMyAppMenu")
    List<MobileHomeMenu> addMyAppMenu() {
        return service.getAddMyAppMenu();
    }

    @ApiOperation("返回新闻资讯的数据")
    @GetMapping("getNews")
    List<MoblieNews> getNews() {
        return service.getNews();
    }

    /**
     * 移动端新闻资讯的展示
     */
    @ApiOperation("返回新闻资讯的数据")
    @GetMapping("/queryNewsList/{limit}")
    List<MediaArticle> queryNewsList(@PathVariable("limit") String limit) {
        List<MediaArticle> articles = service.queryNewsList(limit);
        return articles;
    }

    /**
     * 移动端新闻资讯详情
     * @return
     */
    @ApiOperation("移动端新闻资讯详情")
    @GetMapping("/queryNewsById")
    MediaArticle queryNewsById(Long id){
        return service.queryNewsById(id);
    }


    @ApiOperation("返回公告的数据")
    @GetMapping("findNotice")
    List<NoticeVO> findNotice() {
        return service.findNotice();
    }


    @ApiOperation("返回轮播图数据")
    @GetMapping("findMobileHomeLb")
    List<MobileHomeLb> findMobileHomeLb() {
        return service.findMobileHomeLb();
    }


    @GetMapping("getMoreApply")
    List<MobileHomeMenuDetails> getMoreApply(Long typeId) {
        return service.getMoreApply(typeId);
    }


    @GetMapping("search")
    @ApiOperation("首页搜索功能")
    AjaxResult search(String appName) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("code", 200);
        ajaxResult.put("msg", "操作成功");
        ajaxResult.put("data", service.searchApp(appName));
        return ajaxResult;
    }

    @GetMapping("hidden")
    @ApiOperation("隐藏首页按钮")
    AjaxResult hidden(Long id) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("code", 200);
        ajaxResult.put("msg", "操作成功");
        ajaxResult.put("data", service.hidden(id));
        return ajaxResult;
    }

    @GetMapping("/show")
    @ApiOperation("显示首页按钮")
    AjaxResult show(String id) {

        return service.show(id);

    }


    //查询可以添加的应用
    @GetMapping("findAddApp")
    @ApiOperation("查询添加应用列表")
    AjaxResult findAddApp(Long pid) {

        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("code", 200);
        ajaxResult.put("msg", "操作成功");
        ajaxResult.put("data", service.findAddApp(pid));
        return ajaxResult;
    }



}
