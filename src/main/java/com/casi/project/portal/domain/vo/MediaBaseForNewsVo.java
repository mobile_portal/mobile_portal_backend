package com.casi.project.portal.domain.vo;


import com.casi.common.constant.Constants;
import lombok.ToString;

@ToString
public class MediaBaseForNewsVo {



    /**
     * 作者
     */

    private String author;


    /**
     * 主键id
     */
    private Long id;
    /**
     * 状态（1：待提交，2：审核中，3：已审核，4：已发布，5：已退回）
     */
    private String status;
    /**
     * 备注
     */
    private String remark;
    /**
     * 图文标题
     */
    private String title;
    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 更新时间
     */
    private String createTime;


    /**
     * 封面图片url
     */
    private String picUrl;

    public MediaBaseForNewsVo() {
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getPicUrl() {
        return  Constants.BASE_IMG_URL+picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
}
