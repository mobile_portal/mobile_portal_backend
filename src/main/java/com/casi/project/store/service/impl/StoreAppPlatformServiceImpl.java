package com.casi.project.store.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppPlatformMapper;
import com.casi.project.store.domain.StoreAppPlatform;
import com.casi.project.store.service.IStoreAppPlatformService;

/**
 * 应用平台表Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppPlatformServiceImpl implements IStoreAppPlatformService 
{
    @Autowired
    private StoreAppPlatformMapper storeAppPlatformMapper;

    /**
     * 查询应用平台表
     * 
     * @param uuid 应用平台表ID
     * @return 应用平台表
     */
    @Override
    public StoreAppPlatform selectStoreAppPlatformById(String uuid)
    {
        return storeAppPlatformMapper.selectStoreAppPlatformById(uuid);
    }

    /**
     * 查询应用平台表列表
     * 
     * @param storeAppPlatform 应用平台表
     * @return 应用平台表
     */
    @Override
    public List<StoreAppPlatform> selectStoreAppPlatformList(StoreAppPlatform storeAppPlatform)
    {
        return storeAppPlatformMapper.selectStoreAppPlatformList(storeAppPlatform);
    }

    /**
     * 新增应用平台表
     * 
     * @param storeAppPlatform 应用平台表
     * @return 结果
     */
    @Override
    public int insertStoreAppPlatform(StoreAppPlatform storeAppPlatform)
    {
        return storeAppPlatformMapper.insertStoreAppPlatform(storeAppPlatform);
    }

    /**
     * 修改应用平台表
     * 
     * @param storeAppPlatform 应用平台表
     * @return 结果
     */
    @Override
    public int updateStoreAppPlatform(StoreAppPlatform storeAppPlatform)
    {
        return storeAppPlatformMapper.updateStoreAppPlatform(storeAppPlatform);
    }

    /**
     * 批量删除应用平台表
     * 
     * @param uuids 需要删除的应用平台表ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppPlatformByIds(String[] uuids)
    {
        return storeAppPlatformMapper.deleteStoreAppPlatformByIds(uuids);
    }

    /**
     * 删除应用平台表信息
     * 
     * @param uuid 应用平台表ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppPlatformById(String uuid)
    {
        return storeAppPlatformMapper.deleteStoreAppPlatformById(uuid);
    }
}
