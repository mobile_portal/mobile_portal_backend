package com.casi.project.portal.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Auther shp
 * @data2020/4/1316:56
 * @description
 */

@ApiModel
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MobileHomeMenu {

    @ApiModelProperty("主键")
    private String id;
    @ApiModelProperty("菜单名称")
    private String menuName;
    @ApiModelProperty("所属home_id")
    private String parentId;
    @ApiModelProperty("0-显示,1-隐藏")
    private Integer isShow;
    @ApiModelProperty("排序字段")
    private Integer menuSort;
    @ApiModelProperty("点击更多跳转页面")
    private String skipUrl;
    @ApiModelProperty("创建时间")
    private Date createTime;
    @ApiModelProperty("更新时间")
    private Date updateTime;

    // 类型(0-首页展示,1-更多添加展示)
    @ApiModelProperty("类型")
    private String menuType;

    //@ApiModelProperty("对应的详细信息")
    List<MobileHomeMenuDetails> arrs=new ArrayList<>();
}
