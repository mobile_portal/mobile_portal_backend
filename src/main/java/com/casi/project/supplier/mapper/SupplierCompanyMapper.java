package com.casi.project.supplier.mapper;

import java.util.List;
import com.casi.project.supplier.domain.SupplierCompany;

/**
 * 供应商企业Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface SupplierCompanyMapper 
{
    /**
     * 查询供应商企业
     * 
     * @param uuid 供应商企业ID
     * @return 供应商企业
     */
    public SupplierCompany selectSupplierCompanyById(String uuid);

    /**
     * 查询供应商企业列表
     * 
     * @param supplierCompany 供应商企业
     * @return 供应商企业集合
     */
    public List<SupplierCompany> selectSupplierCompanyList(SupplierCompany supplierCompany);

    /**
     * 新增供应商企业
     * 
     * @param supplierCompany 供应商企业
     * @return 结果
     */
    public int insertSupplierCompany(SupplierCompany supplierCompany);

    /**
     * 修改供应商企业
     * 
     * @param supplierCompany 供应商企业
     * @return 结果
     */
    public int updateSupplierCompany(SupplierCompany supplierCompany);

    /**
     * 删除供应商企业
     * 
     * @param uuid 供应商企业ID
     * @return 结果
     */
    public int deleteSupplierCompanyById(String uuid);

    /**
     * 批量删除供应商企业
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteSupplierCompanyByIds(String[] uuids);
}
