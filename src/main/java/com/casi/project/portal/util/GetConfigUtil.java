/**
 * 
 */
package com.casi.project.portal.util;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * 读取properties文件的工具类 
 * @author dingjianwei
 *
 */
public class GetConfigUtil {
	private static Properties p = new Properties();  
	  
    /** 
     * 读取properties配置文件信息 
     */  
    static{  
        try { 
        	InputStreamReader isr = new InputStreamReader(GetConfigUtil.class.getClassLoader().getResourceAsStream("config.properties"),"utf-8");
            p.load(isr);
        } catch (IOException e) {  
            e.printStackTrace();   
        }  
    }  
    /** 
     * 根据key得到value的值 
     */  
    public static String getValue(String key)  
    {  
        return p.getProperty(key);  
    } 
}
