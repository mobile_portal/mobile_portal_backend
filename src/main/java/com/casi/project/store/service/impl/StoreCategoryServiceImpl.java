package com.casi.project.store.service.impl;

import java.util.List;
import com.casi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreCategoryMapper;
import com.casi.project.store.domain.StoreCategory;
import com.casi.project.store.service.IStoreCategoryService;

/**
 * 应用分类Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreCategoryServiceImpl implements IStoreCategoryService 
{
    @Autowired
    private StoreCategoryMapper storeCategoryMapper;

    /**
     * 查询应用分类
     * 
     * @param uuid 应用分类ID
     * @return 应用分类
     */
    @Override
    public StoreCategory selectStoreCategoryById(String uuid)
    {
        return storeCategoryMapper.selectStoreCategoryById(uuid);
    }

    /**
     * 查询应用分类列表
     * 
     * @param storeCategory 应用分类
     * @return 应用分类
     */
    @Override
    public List<StoreCategory> selectStoreCategoryList(StoreCategory storeCategory)
    {
        return storeCategoryMapper.selectStoreCategoryList(storeCategory);
    }

    /**
     * 新增应用分类
     * 
     * @param storeCategory 应用分类
     * @return 结果
     */
    @Override
    public int insertStoreCategory(StoreCategory storeCategory)
    {
        storeCategory.setCreateTime(DateUtils.getNowDate());
        return storeCategoryMapper.insertStoreCategory(storeCategory);
    }

    /**
     * 修改应用分类
     * 
     * @param storeCategory 应用分类
     * @return 结果
     */
    @Override
    public int updateStoreCategory(StoreCategory storeCategory)
    {
        storeCategory.setUpdateTime(DateUtils.getNowDate());
        return storeCategoryMapper.updateStoreCategory(storeCategory);
    }

    /**
     * 批量删除应用分类
     * 
     * @param uuids 需要删除的应用分类ID
     * @return 结果
     */
    @Override
    public int deleteStoreCategoryByIds(String[] uuids)
    {
        return storeCategoryMapper.deleteStoreCategoryByIds(uuids);
    }

    /**
     * 删除应用分类信息
     * 
     * @param uuid 应用分类ID
     * @return 结果
     */
    @Override
    public int deleteStoreCategoryById(String uuid)
    {
        return storeCategoryMapper.deleteStoreCategoryById(uuid);
    }
}
