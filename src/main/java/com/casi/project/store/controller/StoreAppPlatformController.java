package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreAppPlatform;
import com.casi.project.store.service.IStoreAppPlatformService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用平台表Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用平台表"})
@RestController
@RequestMapping("/store/platform")
public class StoreAppPlatformController extends BaseController
{
    @Autowired
    private IStoreAppPlatformService storeAppPlatformService;

    /**
     * 查询应用平台表列表
     */
    @ApiOperation(value = "查询应用平台表列表")
    @PreAuthorize("@ss.hasPermi('store:platform:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreAppPlatform storeAppPlatform)
    {
        startPage();
        List<StoreAppPlatform> list = storeAppPlatformService.selectStoreAppPlatformList(storeAppPlatform);
        return getDataTable(list);
    }

    /**
     * 导出应用平台表列表
     */
    @PreAuthorize("@ss.hasPermi('store:platform:export')")
    @Log(title = "应用平台表", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreAppPlatform storeAppPlatform)
    {
        List<StoreAppPlatform> list = storeAppPlatformService.selectStoreAppPlatformList(storeAppPlatform);
        ExcelUtil<StoreAppPlatform> util = new ExcelUtil<StoreAppPlatform>(StoreAppPlatform.class);
        return util.exportExcel(list, "platform");
    }

    /**
     * 获取应用平台表详细信息
     */
    @ApiOperation(value = "获取应用平台表详细信息")
    @PreAuthorize("@ss.hasPermi('store:platform:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppPlatformService.selectStoreAppPlatformById(uuid));
    }

    /**
     * 新增应用平台表
     */
    @ApiOperation(value = "新增应用平台表")
    @PreAuthorize("@ss.hasPermi('store:platform:add')")
    @Log(title = "应用平台表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreAppPlatform storeAppPlatform)
    {
        return toAjax(storeAppPlatformService.insertStoreAppPlatform(storeAppPlatform));
    }

    /**
     * 修改应用平台表
     */
    @ApiOperation(value = "修改应用平台表")
    @PreAuthorize("@ss.hasPermi('store:platform:edit')")
    @Log(title = "应用平台表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreAppPlatform storeAppPlatform)
    {
        return toAjax(storeAppPlatformService.updateStoreAppPlatform(storeAppPlatform));
    }

    /**
     * 删除应用平台表
     */
    @ApiOperation(value = "删除应用平台表")
    @PreAuthorize("@ss.hasPermi('store:platform:remove')")
    @Log(title = "应用平台表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppPlatformService.deleteStoreAppPlatformByIds(uuids));
    }
}
