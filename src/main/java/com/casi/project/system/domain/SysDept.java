package com.casi.project.system.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 部门表 sys_dept
 * 
 * @author lzb
 */
public class SysDept extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 部门主键 */
    private String deptUUID;

    /** 部门ID */
    private String deptId;

    /** 父部门ID */
    private String parentId;

    /** 祖级列表 */
    private String ancestors;

    /** 部门名称 */
    private String deptName;

    /** 显示顺序 */
    private String orderNum;

    /** 负责人 */
    private String leader;

    /** 联系电话 */
    private String phone;

    /** 邮箱 */
    private String email;
    /**创建时间**/
    private Date createTime;
    /** 部门状态:0正常,1停用 */
    private String status;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 父部门名称 */
    private String parentName;
    
    /** 子部门 */
    private List<SysDept> children = new ArrayList<SysDept>();


    // 新增字段

    /** 组织LOGO */
    private String organizationLogo;

    /** 组织编码 */
    private String deptCode;
    /** 编码 */
    private String code;

    /** 部门类型 (0组织 1公司 2部门) */
    private String deptType;
    /** 新加的不知道啥意思  传过来的是集合多个用逗号隔开 */
    private String ouDirectory;
    /** 新加的不知道啥意思  传过来的是集合多个用逗号隔开 */
    private String belongOuUuid;
    /** 1.代表false   2.代表true(新加的不知道啥意思  传过来的是集合多个用逗号隔开) */
    private String rootNode;
    /** 用户ｉｄ（新添加的）*/
    private String uid;
    /** 本组织机构的uuid或外部id*/
    private String organizationUuid;
    /** 管理者账户的外部id*/
    private String createId;
    /** 管理者账户的外部id*/
    private String createName;
    /** 组织机构所属的区域idtype为SELF_OU(自建组织机构)时有可能会有值,可为空,type为DEPARTMENT("自建部门")不会出现值*/
    private String regionId;
    /** SELF_OU(自建组织机构)或DEPARTMENT("自建部门")*/
    private String type;

    /** 备注 */
    private String remark;
    /**后来新建的最后一次新建的不知道什么意思**/
    private Integer levelNumber;
    /**后添加的不知道什么意思但是猜测是部门管理者的id**/
    private String managerUDAccountUuids;
    /**最新时间不知道是用来做什么的**/
    private Date lastModifyTime;

    public String getDeptUUID() {
        return deptUUID;
    }

    public void setDeptUUID(String deptUUID) {
        this.deptUUID = deptUUID;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getDeptId()
    {
        return deptId;
    }

    public void setDeptId(String deptId)
    {
        this.deptId = deptId;
    }

    public String getParentId()
    {
        return parentId;
    }

    public void setParentId(String parentId)
    {
        this.parentId = parentId;
    }

    public String getAncestors()
    {
        return ancestors;
    }

    public void setAncestors(String ancestors)
    {
        this.ancestors = ancestors;
    }

    @NotBlank(message = "部门名称不能为空")
    @Size(min = 0, max = 30, message = "部门名称长度不能超过30个字符")
    public String getDeptName()
    {
        return deptName;
    }

    public void setDeptName(String deptName)
    {
        this.deptName = deptName;
    }

    //@NotBlank(message = "显示顺序不能为空")
    public String getOrderNum()
    {
        return orderNum;
    }

    public void setOrderNum(String orderNum)
    {
        this.orderNum = orderNum;
    }

    public String getLeader()
    {
        return leader;
    }

    public void setLeader(String leader)
    {
        this.leader = leader;
    }

    @Size(min = 0, max = 11, message = "联系电话长度不能超过11个字符")
    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    @Email(message = "邮箱格式不正确")
    @Size(min = 0, max = 50, message = "邮箱长度不能超过50个字符")
    public String getEmail()
    {
        return email;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getStatus()
    {
        return status;
    }

    public void setStatus(String status)
    {
        this.status = status;
    }

    public String getDelFlag()
    {
        return delFlag;
    }

    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getParentName()
    {
        return parentName;
    }

    public void setParentName(String parentName)
    {
        this.parentName = parentName;
    }

    public List<SysDept> getChildren()
    {
        return children;
    }

    public void setChildren(List<SysDept> children)
    {
        this.children = children;
    }


    public String getOrganizationLogo() {
        return organizationLogo;
    }

    public void setOrganizationLogo(String organizationLogo) {
        this.organizationLogo = organizationLogo;
    }

    public String getDeptCode() {
        return deptCode;
    }

    public void setDeptCode(String deptCode) {
        this.deptCode = deptCode;
    }

    public String getDeptType() {
        return deptType;
    }

    public void setDeptType(String deptType) {
        this.deptType = deptType;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRootNode() {
        return rootNode;
    }

    public void setRootNode(String rootNode) {
        this.rootNode = rootNode;
    }

    public String getBelongOuUuid() {
        return belongOuUuid;
    }

    public void setBelongOuUuid(String belongOuUuid) {
        this.belongOuUuid = belongOuUuid;
    }

    public String getOuDirectory() {
        return ouDirectory;
    }

    public void setOuDirectory(String ouDirectory) {
        this.ouDirectory = ouDirectory;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegionId() {

        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getCreateId() {

        return createId;
    }

    public void setCreateId(String createId) {
        this.createId = createId;
    }

    public String getOrganizationUuid() {

        return organizationUuid;
    }

    @Override
    public String getRemark() {
        return remark;
    }

    @Override
    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public Date getCreateTime() {
        return createTime;
    }

    @Override
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public void setOrganizationUuid(String organizationUuid) {
        this.organizationUuid = organizationUuid;
    }

    public Integer getLevelNumber() {
        return levelNumber;
    }

    public void setLevelNumber(Integer levelNumber) {
        this.levelNumber = levelNumber;
    }

    public String getManagerUDAccountUuids() {
        return managerUDAccountUuids;
    }

    public void setManagerUDAccountUuids(String managerUDAccountUuids) {
        this.managerUDAccountUuids = managerUDAccountUuids;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    @Override
    public String toString() {
        return "SysDept{" +
                "deptUUID='" + deptUUID + '\'' +
                ", deptId='" + deptId + '\'' +
                ", parentId='" + parentId + '\'' +
                ", ancestors='" + ancestors + '\'' +
                ", deptName='" + deptName + '\'' +
                ", orderNum='" + orderNum + '\'' +
                ", leader='" + leader + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", createTime=" + createTime +
                ", status='" + status + '\'' +
                ", delFlag='" + delFlag + '\'' +
                ", parentName='" + parentName + '\'' +
                ", children=" + children +
                ", organizationLogo='" + organizationLogo + '\'' +
                ", deptCode='" + deptCode + '\'' +
                ", code='" + code + '\'' +
                ", deptType='" + deptType + '\'' +
                ", ouDirectory='" + ouDirectory + '\'' +
                ", belongOuUuid='" + belongOuUuid + '\'' +
                ", rootNode='" + rootNode + '\'' +
                ", uid='" + uid + '\'' +
                ", organizationUuid='" + organizationUuid + '\'' +
                ", createId='" + createId + '\'' +
                ", createName='" + createName + '\'' +
                ", regionId='" + regionId + '\'' +
                ", type='" + type + '\'' +
                ", remark='" + remark + '\'' +
                ", levelNumber=" + levelNumber +
                ", managerUDAccountUuids='" + managerUDAccountUuids + '\'' +
                ", lastModifyTime=" + lastModifyTime +
                '}';
    }
}
