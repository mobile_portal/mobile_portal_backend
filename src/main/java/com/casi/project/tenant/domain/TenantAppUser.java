package com.casi.project.tenant.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 租户应用用户授权对象 tenant_app_user
 * 
 * @author casi
 * @date 2020-06-02
 */
public class TenantAppUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 租户编号 */
    @Excel(name = "租户编号")
    private String tenantId;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private String appId;

    /** 角色编号 */
    @Excel(name = "角色编号")
    private String roleId;

    /** 角色类型 */
    @Excel(name = "角色类型")
    private String roleType;

    /** 用户编号 */
    @Excel(name = "用户编号")
    private String userId;

    /** 操作人 */
    @Excel(name = "操作人")
    private String authUserId;

    /** 授权时间 */
    @Excel(name = "授权时间")
    private String authTime;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setTenantId(String tenantId) 
    {
        this.tenantId = tenantId;
    }

    public String getTenantId() 
    {
        return tenantId;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setRoleId(String roleId) 
    {
        this.roleId = roleId;
    }

    public String getRoleId() 
    {
        return roleId;
    }
    public void setRoleType(String roleType) 
    {
        this.roleType = roleType;
    }

    public String getRoleType() 
    {
        return roleType;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setAuthUserId(String authUserId) 
    {
        this.authUserId = authUserId;
    }

    public String getAuthUserId() 
    {
        return authUserId;
    }
    public void setAuthTime(String authTime) 
    {
        this.authTime = authTime;
    }

    public String getAuthTime() 
    {
        return authTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("tenantId", getTenantId())
            .append("appId", getAppId())
            .append("roleId", getRoleId())
            .append("roleType", getRoleType())
            .append("userId", getUserId())
            .append("authUserId", getAuthUserId())
            .append("authTime", getAuthTime())
            .toString();
    }
}
