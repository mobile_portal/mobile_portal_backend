package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther shp
 * @data2020/4/2014:06
 * @description 原生应用对应的版本信息
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MobileApplyVersionNative {

    //@ApiModelProperty("主键id")
    private Long id;

    //@ApiModelProperty("图标地址")
    private String icon;

    //@ApiModelProperty("屏幕截图地址")
    private String screenCapture;

    //@ApiModelProperty("版本号")
    private String versionNumber;

    //@ApiModelProperty("版本描述")
    private String versionDescribe;

    //@ApiModelProperty("0-上架，1-不上架")
    private Integer isPutaway;

    //@ApiModelProperty("应用范围:1-移动端 2-PC端 3-管理后台")
    private Integer applyScope;

    //@ApiModelProperty("移动端访问地址")
    private String appAddress;

    //@ApiModelProperty("离线资源包地址")
    private String offAddress;

    //@ApiModelProperty("是否全屏:0-全屏,1-非全屏")
    private Integer isFull;

    //@ApiModelProperty("显示风格:0-竖屏，1-横屏")
    private Integer displayStyle;

    //@ApiModelProperty("标题栏设置:0-默认,1-自定义颜色，2-自定义图片")
    private Integer titleBar;

    //@ApiModelProperty("标题栏颜色")
    private String titleBarColor;

    //@ApiModelProperty("进度栏颜色设置:0-默认,1-自定义颜色")
    private Integer progressBar;

   // @ApiModelProperty("进度栏颜色")
    private String progressBarColor;

    //@ApiModelProperty("PC端访问地址")
    private String pcAddress;

    //@ApiModelProperty("是否用内嵌服务器:0-是,1-否")
    private Integer isEmbeddedServer;

    //@ApiModelProperty("管理后台访问地址")
    private String adminAddress;

    //@ApiModelProperty("消息通知源地址")
    private String messageAddress;

    //@ApiModelProperty("应用参数设置:参数名 参数值")
    private String applyParameter;

    //@ApiModelProperty("和应用详细表主键id关联")
    private Long applyAersion;

    //@ApiModelProperty("创建时间")
    private Data createTime;

    //@ApiModelProperty("修改时间")
    private Data updateTime;


    //@ApiModelProperty("区分安卓或者ios 0为安卓 1为ios")
    private Integer appOrIos;

    //@ApiModelProperty("0为自定义应用 1为系统应用")
    private Integer isCustom;

    //@ApiModelProperty("安装包")
    private String installPackage;

    //@ApiModelProperty("包名")
    private String packageName;

    //@ApiModelProperty("启动跳转地址")
    private String toUrl;

    //@ApiModelProperty("关联应用详细中的apply_version")
    private Long mobileHomeMenuDetailsId;

}
