package com.casi.project.interfaces.mapper;

import com.casi.project.interfaces.domain.DepartmentVo;
import com.casi.project.system.domain.SysDept;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 部门管理 数据层
 * 
 * @author lzb
 */
public interface DeptTempMapper
{



    /**
     * 批量插入从云信拉回来的组织
     *
     */
     int batchSave(@Param("depts") List<DepartmentVo> depts);


    /**
     * 更新组织
     *
     */
    int updateBySelective(DepartmentVo dept);


    /**
     * 单条插入
     * @param record
     * @return
     */
    int insertSelective(DepartmentVo record);


    /**
     * 查询
     * @param id
     * @return
     */
    DepartmentVo selectByPrimaryKey(Integer id);

    /***
     * 查询全部组织
     * @return
     */
    List<DepartmentVo> getAllDept();
}
