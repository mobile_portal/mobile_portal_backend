package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;



@Data
@NoArgsConstructor
@AllArgsConstructor
public class MediaFile {
  /**
   * 主键id
   */
  private Long id;
  /**
   * 系统生成文件名
   */
  private String name;
  /**
   * 文件名称
   */
  private String fileName;
  /**
   * 素材上传路径
   */
  private String uploadUrl;
  /**
   * 本地保存路径
   */
  private String localUrl;
  /**
   * 文件大小
   */
  private int size;
  /**
   * 视频标题（仅视频素材有）
   */
  private String title;
  /**
   * 视频描述（仅视频素材有）
   */
  private String introduction;
  /**
   * 创建时间
   */
  private Date createTime;
  /**
   * 更新时间
   */
  private Date updateTime;



}
