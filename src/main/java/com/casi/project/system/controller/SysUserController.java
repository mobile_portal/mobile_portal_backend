package com.casi.project.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.casi.common.constant.Constants;
import com.casi.common.exception.CustomException;
import com.casi.framework.manager.AsyncManager;
import com.casi.framework.manager.factory.AsyncFactory;
import com.casi.framework.redis.RedisCache;
import com.casi.project.system.domain.*;
import com.casi.project.system.service.impl.SysUserServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.casi.common.constant.UserConstants;
import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.ServletUtils;
import com.casi.common.utils.StringUtils;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.security.LoginUser;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.framework.web.page.TableDataInfo;
import com.casi.project.system.service.ISysPostService;
import com.casi.project.system.service.ISysRoleService;
import com.casi.project.system.service.ISysUserService;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户信息
 *
 * @author lzb
 */
@Api(tags = {"用户管理"})
@RestController
@RequestMapping("/system/user")
public class SysUserController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(SysUserController.class);

    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private ISysPostService postService;

    @Autowired
    private TokenService tokenService;


    /**
     * 获取用户列表
     */

    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysUser user) {
        logger.info("===============获取用户列表=============");
        List<SysUser> list;
        try {
            startPage();
            list = userService.selectUserList(user);
        } catch (Exception e) {
            logger.error("获取用户列表失败", e);
            TableDataInfo dataInfo = new TableDataInfo();
            dataInfo.setCode(500);
            dataInfo.setMsg("获取用户列表失败");
            dataInfo.setTotal(0L);
            return dataInfo;
        }
        return getDataTable(list);
    }

    /**
     * 获取未禁用用户列表
     */
    @ApiOperation(value = "获取未禁用用户列表")
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/noDisabledList")
    public TableDataInfo noDisabledList(SysUser user) {
        logger.info("===============获取未禁用用户列表=============");
        List<SysUser> list;
        TableDataInfo dataInfo  = new TableDataInfo();
        try {
            if (user == null){
                dataInfo.setCode(500);
                dataInfo.setMsg("参数为空");
                dataInfo.setTotal(0L);
                return dataInfo;
            }
            if (StringUtils.isEmpty(user.getApplyId())){
                dataInfo.setCode(500);
                dataInfo.setMsg("请勾选应用");
                dataInfo.setTotal(0L);
                return dataInfo;
            }
            if (StringUtils.isEmpty(user.getDeptId())){
                dataInfo.setCode(500);
                dataInfo.setMsg("请选择用户");
                dataInfo.setTotal(0L);
                return dataInfo;
            }

            startPage();
            list = userService.noDisabledList(user.getDeptId(),user.getApplyId());
            if (StringUtils.isEmpty(list)){
                dataInfo.setCode(200);
               // dataInfo.setMsg("获取用户列表失败");
                dataInfo.setTotal(0L);
                dataInfo.setRows(null);
                return dataInfo;
            }
        } catch (Exception e) {
            logger.error("获取用户列表失败", e);
            dataInfo.setCode(500);
            dataInfo.setMsg("获取用户列表失败");
            dataInfo.setTotal(0L);
            return dataInfo;
        }
        return getDataTable(list);
    }


    /**
     * 获取用户列表
     */
    @ApiOperation(value = "获取用户列表")
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/list1")
    public AjaxResult listNew(SysUser user,Integer pageNum, Integer pageSize) {
        logger.info("===============获取用户列表=============");
        if (null == pageNum || null == pageSize) {
            return AjaxResult.error("请传入页数或每页展示的条数");
        }
        PageHelper.startPage(pageNum, pageSize);
        List<SysUser> list = null;
        try {
            startPage();
            list = userService.selectUserList(user);
        } catch (Exception e) {
            logger.error("获取用户列表失败", e);
            TableDataInfo dataInfo = new TableDataInfo();
            dataInfo.setCode(500);
            dataInfo.setMsg("获取用户列表失败");
            dataInfo.setTotal(0L);
            return AjaxResult.error("获取用户列表失败");
        }
        PageInfo<SysUser> noticeVOPageInfo = new PageInfo<>(list);
        return AjaxResult.success("查询成功", noticeVOPageInfo);
    }

    @Log(title = "用户管理", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:user:export')")
    @GetMapping("/export")
    public AjaxResult export(SysUser user) {
        List<SysUser> list = userService.selectUserList(user);
        ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
        return util.exportExcel(list, "用户数据");
    }

    @Log(title = "用户管理", businessType = BusinessType.IMPORT)
    @PreAuthorize("@ss.hasPermi('system:user:import')")
    @PostMapping("/importData")
    public AjaxResult importData(MultipartFile file, boolean updateSupport) {
        logger.info("===============导入用户数据=============");
        String message = "";
        try {
            ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
            List<SysUser> userList = util.importExcel(file.getInputStream());
            LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
            String operName = loginUser.getUsername();
            message = userService.importUser(userList, updateSupport, operName);
        } catch (Exception e) {
            logger.error("导入用户数据失败", e);
            return AjaxResult.error("导入用户数据失败");
        }
        return AjaxResult.success(message);
    }

    @GetMapping("/importTemplate")
    public AjaxResult importTemplate() {
        logger.info("===============下载模板=============");
        try {
            ExcelUtil<SysUser> util = new ExcelUtil<SysUser>(SysUser.class);
            return util.importTemplateExcel("用户数据");
        } catch (Exception e) {
            logger.error("下载模板失败", e);
            return AjaxResult.error("下载模板失败");
        }

    }

    /**
     * 根据用户编号获取详细信息  修改回显
     */
    @ApiOperation(value = "根据用户编号获取详细信息")
    @GetMapping(value = {"/getUserDetails"})
    public AjaxResult getInfo(String userId) {
        logger.info("===============根据用户编号获取详细信息=============");
        AjaxResult ajax;
        try {
            ajax = AjaxResult.success();
            ajax.put("roles", roleService.selectRoleAll());
            ajax.put("posts", postService.selectPostAll());
            if (StringUtils.isNotNull(userId)) {
                SysUser sysUser = userService.selectUserById(userId);
                List<String> listRole = roleService.selectRoleListByUserId(userId);
                if (listRole != null) {
                    String[] longs = new String[listRole.size()];
                    for (int i = 0; i < listRole.size(); i++) {
                        longs[i] = String.valueOf(listRole.get(i));
                    }
                    sysUser.setRoleIds(longs);
                    ajax.put("data", sysUser);
                }
            }
        } catch (Exception e) {
            logger.error("根据用户编号获取详细信息失败", e);
            return AjaxResult.error();
        }
        return ajax;
    }


    /**
     * 新增用户
     */
    @ApiOperation(value = "新增用户")
    @PreAuthorize("@ss.hasPermi('system:user:add')")
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysUser user) {
        logger.info("===============新增用户=============" + user.toString());
        int i;
        try {
            if (StringUtils.isEmpty(user.getUserName())) {
                return AjaxResult.error("用户账号不能为空");
            }
            if (StringUtils.isEmpty(user.getNickName())) {
                return AjaxResult.error("用户姓名不能为空");
            }
            if (StringUtils.isEmpty(user.getPassword())) {
                return AjaxResult.error("用户密码不能为空");
            }
            if (StringUtils.isEmpty(user.getUserCode())) {
                return AjaxResult.error("用户编码不能为空");
            }
            if (StringUtils.isEmpty(user.getDeptId())) {
                return AjaxResult.error("部门不能为空");
            }
            if (StringUtils.isEmpty(user.getPhonenumber())) {
                return AjaxResult.error("手机号不能为空");
            }
            if (StringUtils.isEmpty(user.getEmail()) ) {
                return AjaxResult.error("邮箱不能为空");
            }
            if (UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user.getUserName()))) {
                return AjaxResult.error("用户登录账号已存在");
            }
            if (UserConstants.NOT_UNIQUE.equals(userService.checkUserCodeUnique(user.getUserCode()))) {
                return AjaxResult.error("用户编码已存在");
            }
            if (UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user))) {
                return AjaxResult.error("手机号码已存在");
            }
            if (UserConstants.NOT_UNIQUE.equals(userService.checkEmailUnique(user.getEmail()))) {
                return AjaxResult.error("邮箱账号已存在");
            }
            user.setCreateBy(SecurityUtils.getUsername());
            user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
            i = userService.insertUser(user);
        } catch (Exception e) {
            logger.error("新增用户失败", e);
            return AjaxResult.error("新增用户失败");
        }
        return toAjax(i);
    }

    /**
     * 修改用户
     */
    @ApiOperation(value = "修改用户")
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    public AjaxResult edit(@Validated @RequestBody SysUser user) {
        logger.info("===============修改用户=============");
        int i;
        try {
            // userService.checkUserAllowed(user);
           // roleService.isHavePerm(user);

            SysUser sysUser = userService.selectUserById(user.getUserId());

            if (StringUtils.isNotNull(sysUser.getUserId()) && sysUser.isAdmin()) {
                return AjaxResult.error("不允许操作超级管理员用户");
            }
            if (StringUtils.isEmpty(user.getUserName())) {
                return AjaxResult.error("用户账号不能为空");
            }
            if (StringUtils.isEmpty(user.getNickName())) {
                return AjaxResult.error("用户姓名不能为空");
            }
            if (StringUtils.isEmpty(user.getUserCode())) {
                return AjaxResult.error("用户编码不能为空");
            }
            if (StringUtils.isEmpty(user.getDeptId())) {
                return AjaxResult.error("部门不能为空");
            }
            if (StringUtils.isEmpty(user.getPhonenumber())) {
                return AjaxResult.error("手机号不能为空");
            }
            if (StringUtils.isEmpty(user.getEmail())) {
                return AjaxResult.error("邮箱不能为空");
            }
            if (!user.getUserName().equals(sysUser.getUserName()) && UserConstants.NOT_UNIQUE.equals(userService.checkUserNameUnique(user.getUserName()))) {
                return AjaxResult.error("用户登录账号已存在");
            }
            if (!user.getUserCode().equals(sysUser.getUserCode()) && UserConstants.NOT_UNIQUE.equals(userService.checkUserCodeUnique(user.getUserCode()))) {
                return AjaxResult.error("用户编码已存在");
            }
            if (!user.getPhonenumber().equals(sysUser.getPhonenumber()) && UserConstants.NOT_UNIQUE.equals(userService.checkPhoneUnique(user))) {
                return AjaxResult.error("手机号码已存在");
            }
            if (!user.getEmail().equals(sysUser.getEmail()) && UserConstants.NOT_UNIQUE.equals(userService.checkEmailUnique(user.getEmail()))) {
                return AjaxResult.error("邮箱账号已存在");
            }
            user.setUpdateBy(SecurityUtils.getUsername());
            i = userService.updateUser(user);
        } catch (Exception e) {
            logger.error("修改用户失败", e);
            return AjaxResult.error("修改用户失败");
        }
        return toAjax(i);


    }

    /**
     * 批量删除用户
     */
    @ApiOperation(value = "批量删除用户")
    @PreAuthorize("@ss.hasPermi('system:user:remove')")
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @PostMapping("/deleteUsers")
    public AjaxResult removeUsers(@RequestBody Map<String,String[]> params ) {

        logger.info("===============删除用户=============");
        int i;
        try {
            String[] userIds = params.get("userIds");
          //  roleService.isHavePerm(userIds);
            i = userService.deleteUserByIds(userIds);
        } catch (Exception e) {
            logger.error("删除用户失败", e);
            return AjaxResult.error("删除用户失败");
        }
        return toAjax(i);
    }


    /**
     * 删除单个用户
     */
    @ApiOperation(value = "删除用户")
    @PreAuthorize("@ss.hasPermi('system:user:remove')")
    @Log(title = "用户管理", businessType = BusinessType.DELETE)
    @GetMapping("/deleteOneUser")
    public AjaxResult remove(String userId) {
        logger.info("===============删除单个用户=============");
        int i;
        try {
            if (StringUtils.isEmpty(userId)) {
                return AjaxResult.error("请选择删除用户");
            }
            String[] userIds = {userId};
            //roleService.isHavePerm(userIds);
            i = userService.deleteUserById(userId);
        } catch (Exception e) {
            logger.error("删除单个用户失败", e);
            return AjaxResult.error("删除单个用户失败");
        }
        return toAjax(i);
    }

    /**
     * 重置密码
     */
    @ApiOperation(value = "重置密码")
    @PreAuthorize("@ss.hasPermi('system:user:resetPwd')")
    @PostMapping("/resetPwd")
    public AjaxResult resetPwd(@RequestBody SysUser user) {
        logger.info("===============重置密码=============");
        int i;
        AjaxResult ajax = new AjaxResult();
        try {
            if (user == null){
                return AjaxResult.error("请选择用户");
            }
           // userService.checkUserAllowed(user);
            if (StringUtils.isNotNull(user.getUserId()) && user.isAdmin()) {
                return AjaxResult.error("不允许操作超级管理员用户");
            }
            user = userService.selectUserById(user.getUserId());
            user.setPassword(SecurityUtils.encryptPassword("123456"));
            user.setUpdateBy(SecurityUtils.getUsername());
            i = userService.resetPwd(user);
            if (i > 0){
                ajax.put("msg","重置密码为: 123456");
                ajax.put("code",200);
            }else {
                ajax.put("msg","重置密码失败");
                ajax.put("code",500);
            }

            // 写入日志
            if ( i > 0){
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(SecurityUtils.getUsername(),Constants.LOGIN_SUCCESS,"重置用户"+user.getUserName()+"密码成功",null));
            }else {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(SecurityUtils.getUsername(),Constants.LOGIN_FAIL,"重置用户"+user.getUserName()+"密码失败",null));
            }

        } catch (Exception e) {
            logger.error("重置密码失败", e);
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(SecurityUtils.getUsername(),Constants.LOGIN_FAIL,"重置用户"+user.getUserName()+"密码失败",null));
            return AjaxResult.error("重置密码失败");
        }
        return ajax;
    }

    /**
     * 修改密码
     */
    @ApiOperation(value = "修改密码")
    @PreAuthorize("@ss.hasPermi('system:user:resetPwd')")
    @PostMapping("/changePwd")
    public AjaxResult changePwd(@RequestBody SysUser user) {
        logger.info("===============修改密码=============");
        int i;
        try {
            if (StringUtils.isNotNull(user.getUserId()) && user.isAdmin()) {
                return AjaxResult.error("不允许操作超级管理员用户");
            }
            user.setPassword(SecurityUtils.encryptPassword(user.getPassword()));
            user.setUpdateBy(SecurityUtils.getUsername());
            i = userService.resetPwd(user);

            // 写入日志
            if ( i > 0){
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(SecurityUtils.getUsername(),Constants.LOGIN_SUCCESS,"修改用户"+user.getUserName()+"密码成功",null));
            }else {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(SecurityUtils.getUsername(),Constants.LOGIN_FAIL,"修改用户"+user.getUserName()+"密码失败",null));
            }
        } catch (Exception e) {
            logger.error("修改密码失败", e);
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(SecurityUtils.getUsername(),Constants.LOGIN_FAIL,"修改用户"+user.getUserName()+"密码失败",null));
            return AjaxResult.error("修改密码失败");
        }
        return toAjax(i);
    }

    /**
     * 状态修改
     */
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PostMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody SysUser user) {
        logger.info("===============状态修改=============");
        int i;
        try {
            if (user == null) {
                return AjaxResult.error("用户不能为空");
            }
            if (user.getStatus() == null) {
                return AjaxResult.error("输入用户状态不合法");
            }
            userService.checkUserAllowed(user);
           // roleService.isHavePerm(user);
            user.setUpdateBy(SecurityUtils.getUsername());
            i = userService.updateUserStatus(user);
        } catch (Exception e) {
            logger.error("状态修改失败", e);
            return AjaxResult.error("状态修改失败");
        }
        return toAjax(i);
    }


    /**
     * 锁定状态更改
     */
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PostMapping("/changeLock")
    public AjaxResult changeLock(@RequestBody SysUser user) {
        logger.info("===============锁定状态更改=============");
        int i;
        try {
            userService.checkUserAllowed(user);
           // roleService.isHavePerm(user);
            user.setUpdateBy(SecurityUtils.getUsername());
            i = userService.updateUserIsLock(user);
        } catch (Exception e) {
            logger.error("锁定状态更改失败", e);
            return AjaxResult.error("锁定状态更改失败");
        }
        return toAjax(i);

    }

    /**
     * 数据同步添加用户
     */
    @PostMapping("/addSysUser")
    public AjaxResult addSysUser(@RequestBody SystemAccountLists systemAccountLists) {
        List<SysUserJson> systemAccountLists1 = systemAccountLists.getSystemAccountLists();
        for (SysUserJson sysUserJson : systemAccountLists1) {
            userService.insertSysUser(sysUserJson);
        }
        return AjaxResult.success("同步用户数据成功");
    }
    /**
     * 用户数据同步（删除）
     */
    /*@GetMapping("/deleteSysUser")
    public AjaxResult deleteSysUser(@RequestBody SysUserJson sysUserJson) {
        return userService.deleteSysUserById(sysUserJson);
    }*/

    /**
     * 用户数据同步（更新）
     */
    /*@GetMapping("/updateSysUser")
    public AjaxResult updateSysUser(@RequestBody SysUserJson sysUserJson) {
        return userService.updateSysUser(sysUserJson);
    }*/

    /**
     * 离职状态更改
     */
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PostMapping("/resignation")
    public AjaxResult resignation(@RequestBody SysUser user) {
        logger.info("===============离职状态更改=============");
        int i;
        try {
            userService.checkUserAllowed(user);
           // roleService.isHavePerm(user);
            user.setUpdateBy(SecurityUtils.getUsername());
            i = userService.resignation(user);
        } catch (Exception e) {
            logger.error("离职状态更改失败", e);
            return AjaxResult.error("离职状态更改失败");
        }
        return toAjax(i);
    }


    /**
     * 员工视图创建
     */
   /* @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "员工视图创建", businessType = BusinessType.INSERT)
    @PostMapping("/userViewAdd")
    public AjaxResult userViewAdd(@RequestBody UserViewManage userViewManage) {

        logger.info("===============员工视图创建=============");
        int i;
        try {
            userViewManage.setCreatorId(SecurityUtils.getLoginUser().getUser().getUserId());
            i = userService.userViewAdd(userViewManage);
        } catch (Exception e) {
            logger.error("员工视图创建失败", e);
            return AjaxResult.error("员工视图创建失败");
        }
        return toAjax(i);

    }*/

    /**
     * 员工视图展示
     */
  /*  @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @GetMapping("/userViewList")
    public AjaxResult userViewList() {
        logger.info("===============员工视图展示=============");
        List<UserViewManage> viewManages;
        try {
            viewManages = userService.userViewList();
        } catch (Exception e) {
            logger.error("员工视图展示失败", e);
            return AjaxResult.error("员工视图展示失败");
        }
        return AjaxResult.success(viewManages);

    }
*/

    /**
     * 视图内容展示
     */
   /* @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/viewContentDisplay/{viewId}")
    public AjaxResult viewContentDisplay(@PathVariable String viewId) {
        logger.info("===============视图内容展示=============");
        UserViewManage viewManage;
        try {
            if (viewId == null) {
                return AjaxResult.error("视图不存在");
            }
            viewManage = userService.viewContentDisplay(viewId);
        } catch (Exception e) {
            logger.error("视图内容展示失败", e);
            return AjaxResult.error("视图内容展示失败", e);
        }
        return AjaxResult.success(viewManage);
    }
*/
    /**
     * 员工视图修改
     */
   /* @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "员工视图管理", businessType = BusinessType.UPDATE)
    @PostMapping("/userViewEdit")
    public AjaxResult userViewEdit(@RequestBody UserViewManage userViewManage) {
        logger.info("===============员工视图修改=============");
        int i;
        try {
            i = userService.userViewEdit(userViewManage);
        } catch (Exception e) {
            logger.error("员工视图失败", e);
            return AjaxResult.error("员工视图失败");
        }
        return toAjax(i);
    }
*/
    /**
     * 员工视图删除
     */
    /*@PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "员工视图删除", businessType = BusinessType.DELETE)
    @DeleteMapping("/userViewDelete/{userViewId}")
    public AjaxResult userViewDelete(@PathVariable String userViewId) {
        logger.info("===============员工视图删除=============");
        int i;
        try {
            i = userService.userViewDelete(userViewId);
        } catch (Exception e) {
            logger.error("员工视图删除失败", e);
            return AjaxResult.error("员工视图删除失败");
        }
        return toAjax(i);
    }
*/

    /**
     * 获取登录用户的组织内人员列表
     */
    @ApiOperation(value = "获取登录用户的组织内人员列表")
    // @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/listByLoginIn")
    public AjaxResult listByLoginIn() {
        logger.info("===============获取登录用户的组织内人员列表=============");
        List<SysUser> sysUsers = new ArrayList<>();
        try {
            SysUser user = SecurityUtils.getLoginUser().getUser();
            if (user != null){
                sysUsers = userService.listByLoginIn(user.getDeptId());
            }
        } catch (Exception e) {
            logger.error("获取登录用户的组织内人员列表失败", e);
            return AjaxResult.error("获取登录用户的组织内人员列表失败");
        }
        return AjaxResult.success(sysUsers);

    }


    /**
     * 添加组织管理员
     */
/*    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @Log(title = "用户管理", businessType = BusinessType.INSERT)
    @PostMapping("/addOrganAdmin/{userIds}")
    public AjaxResult addOrganAdmin(@PathVariable String[] userIds) {
        logger.info("===============添加组织管理员=============");
        AjaxResult ajaxResult;
        try {
            ajaxResult = toAjax(userService.addOrganAdmin(userIds));
        } catch (Exception e) {
            logger.error("添加组织管理员失败", e);
            return AjaxResult.error("添加组织管理员失败");
        }
        return ajaxResult;
    }*/
    /**
     * 单点登录认证
     */
    @PostMapping("/jwt/sso/login")
    @CrossOrigin
    public String ssoUrl(@RequestParam String id_token, String redirect_url, Model model, HttpServletRequest request) {
        return userService.ssoUrl(id_token, redirect_url, model, request);
    }

    /**单点登录认证
     * 获取用户列表
     */
    @PreAuthorize("@ss.hasPermi('system:user:list')")
    @GetMapping("/listByUserIds")
    public TableDataInfo list(String[] userIds) {
        logger.info("===============获取用户列表=============");
        List<SysUser> list;
        try {
            startPage();
            list = userService.selectUserList(userIds);
        } catch (Exception e) {
            logger.error("获取用户列表失败", e);
            TableDataInfo dataInfo = new TableDataInfo();
            dataInfo.setCode(500);
            dataInfo.setMsg("获取用户列表失败");
            dataInfo.setTotal(0L);
            return dataInfo;
        }
        return getDataTable(list);
    }

    /**
     * 用户的启用禁用
     */
    @ApiOperation(value = "用户的启用禁用")
    @Log(title = "用户的启用禁用", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('system:user:edit')")
    @PostMapping("/disableStartup")
    public AjaxResult disableStartup(@RequestBody SysUser sysUser){
        if (StringUtils.isNotNull(sysUser.getUserId()) && sysUser.isAdmin()) {
            return AjaxResult.error("不允许操作超级管理员用户");
        }
        return userService.disableStartup(sysUser);
    }
    /**
     * 给用户设置角色接口
     */
    @ApiOperation(value = "设置角色")
    @Log(title = "设置角色", businessType = BusinessType.UPDATE)
    //@PreAuthorize("@ss.hasPermi('system:user:edit')")
    @PostMapping("/setTheRole")
    public AjaxResult setTheRole(@RequestBody SysUser sysUser){
        System.out.println(sysUser.getRoleIds());
        logger.info("===============给用户设置角色=============");
        AjaxResult ajaxResult;
        try {
            ajaxResult =userService.setTheRole(sysUser);

        } catch (Exception e) {
            logger.error("给用户设置角色失败", e);
            return AjaxResult.error("给用户设置角色失败");
        }
        return ajaxResult;


    }
    /**
     * 给用户设置角色数据回显接口
     */
    @PostMapping("/setRoleDataEcho")
    public AjaxResult setRoleDataEcho(@RequestBody SysUser sysUser){
        return userService.setRoleDataEcho(sysUser);
    }


}