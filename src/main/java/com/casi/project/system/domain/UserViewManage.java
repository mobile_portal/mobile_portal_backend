package com.casi.project.system.domain;

import lombok.Data;

import java.util.List;

/**
 * 自定义用户视图 view_manage
 */
@Data
public class UserViewManage {

    /** 视图ID */
    private String viewId;

    /** 视图名称 */
    private String viewName;

    /** 视图范围 */
    private String viewRange;

    /** 视图访问者 */
    private String viewVisitors;

    /** 视图创建者id */
    private String creatorId;

    /** 视图范围 用户ids  */
    private String[] viewRangeIds;

    /** 视图访问者 用户ids */
    private String[] viewVisitorsIds;

    /** 视图范围 */
    private List<SysUser> viewRangeList;

    /** 视图访问者 */
    private List<SysUser> viewVisitorsList;


    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;
}
