package com.casi.project.store.service;

import java.util.List;
import com.casi.project.store.domain.StoreAppImage;

/**
 * 应用图片Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface IStoreAppImageService 
{
    /**
     * 查询应用图片
     * 
     * @param uuid 应用图片ID
     * @return 应用图片
     */
    public StoreAppImage selectStoreAppImageById(String uuid);

    /**
     * 查询应用图片列表
     * 
     * @param storeAppImage 应用图片
     * @return 应用图片集合
     */
    public List<StoreAppImage> selectStoreAppImageList(StoreAppImage storeAppImage);

    /**
     * 新增应用图片
     * 
     * @param storeAppImage 应用图片
     * @return 结果
     */
    public int insertStoreAppImage(StoreAppImage storeAppImage);

    /**
     * 修改应用图片
     * 
     * @param storeAppImage 应用图片
     * @return 结果
     */
    public int updateStoreAppImage(StoreAppImage storeAppImage);

    /**
     * 批量删除应用图片
     * 
     * @param uuids 需要删除的应用图片ID
     * @return 结果
     */
    public int deleteStoreAppImageByIds(String[] uuids);

    /**
     * 删除应用图片信息
     * 
     * @param uuid 应用图片ID
     * @return 结果
     */
    public int deleteStoreAppImageById(String uuid);
}
