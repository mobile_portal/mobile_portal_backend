package com.casi.project.supplier.service.impl;

import java.util.List;
import com.casi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.supplier.mapper.SupplierCompanyMapper;
import com.casi.project.supplier.domain.SupplierCompany;
import com.casi.project.supplier.service.ISupplierCompanyService;

/**
 * 供应商企业Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class SupplierCompanyServiceImpl implements ISupplierCompanyService 
{
    @Autowired
    private SupplierCompanyMapper supplierCompanyMapper;

    /**
     * 查询供应商企业
     * 
     * @param uuid 供应商企业ID
     * @return 供应商企业
     */
    @Override
    public SupplierCompany selectSupplierCompanyById(String uuid)
    {
        return supplierCompanyMapper.selectSupplierCompanyById(uuid);
    }

    /**
     * 查询供应商企业列表
     * 
     * @param supplierCompany 供应商企业
     * @return 供应商企业
     */
    @Override
    public List<SupplierCompany> selectSupplierCompanyList(SupplierCompany supplierCompany)
    {
        return supplierCompanyMapper.selectSupplierCompanyList(supplierCompany);
    }

    /**
     * 新增供应商企业
     * 
     * @param supplierCompany 供应商企业
     * @return 结果
     */
    @Override
    public int insertSupplierCompany(SupplierCompany supplierCompany)
    {
        supplierCompany.setCreateTime(DateUtils.getNowDate());
        return supplierCompanyMapper.insertSupplierCompany(supplierCompany);
    }

    /**
     * 修改供应商企业
     * 
     * @param supplierCompany 供应商企业
     * @return 结果
     */
    @Override
    public int updateSupplierCompany(SupplierCompany supplierCompany)
    {
        supplierCompany.setUpdateTime(DateUtils.getNowDate());
        return supplierCompanyMapper.updateSupplierCompany(supplierCompany);
    }

    /**
     * 批量删除供应商企业
     * 
     * @param uuids 需要删除的供应商企业ID
     * @return 结果
     */
    @Override
    public int deleteSupplierCompanyByIds(String[] uuids)
    {
        return supplierCompanyMapper.deleteSupplierCompanyByIds(uuids);
    }

    /**
     * 删除供应商企业信息
     * 
     * @param uuid 供应商企业ID
     * @return 结果
     */
    @Override
    public int deleteSupplierCompanyById(String uuid)
    {
        return supplierCompanyMapper.deleteSupplierCompanyById(uuid);
    }
}
