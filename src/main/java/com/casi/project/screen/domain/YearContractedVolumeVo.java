package com.casi.project.screen.domain;

import lombok.Data;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther shp
 * @data2020/5/1023:23
 * @description
 */

@Data
public class YearContractedVolumeVo {


    /**
     * 公司名称
     */
    List<String>  companyList;


    /**
     * 公司对应的六大数据
     */
    Map<String,List<Object>> companyDetailed=new LinkedHashMap();

}
