package com.casi.project.portal.domain;

/**
 * @Description:多媒体文件类型
 * @author zhongzj
 * @date 2019/11/19
 */

public enum MediaEnum {
	/**
	 * 图文
	 */
	News("news"),
	/**
	 * 图片
	 */
	Image("image"),
	/**
	 * 语音
	 */
	Voice("voice"),
	/**
	 * 视频
	 */
	Video("video"),
	/**
	 * 网页
	 */
	Web("web"),
	/**
	 * 文件
	 */
	File("file");
	
	private String name;
	
	MediaEnum(String name) {
	     this.name = name;
	}
	
	@Override
	public String toString(){
		return this.name;
	}
}


