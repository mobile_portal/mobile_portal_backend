package com.casi.project.portal.service;


import com.casi.project.portal.domain.MediaBase;
import com.casi.project.portal.domain.vo.MediaBaseForWebVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Description:网页素材管理服务类
 */
public interface MediaWebService {

    /**
     * @Description: 网页列表获取
     * @param base
     * @return boolean
     */
    public List<MediaBaseForWebVo> queryList(MediaBase base);

    /**
     * @Description: 网页详细信息获取
     * @param id
     * @return boolean
     */
    public MediaBase queryDetail(Long id);

    /**
     * @Description: 新增网页素材
     * @param base
     * @return boolean
     */
    public boolean add(MediaBase base, HttpServletRequest request, String type) throws Exception;

    /**
     * @Description: 更新网页素材
     * @param base
     * @return boolean
     */
    public boolean update(MediaBase base) throws Exception;

    /**
     * @Description: 删除网页素材
     * @param base
     * @return boolean
     */
    public boolean delete(MediaBase base) throws Exception;
}
