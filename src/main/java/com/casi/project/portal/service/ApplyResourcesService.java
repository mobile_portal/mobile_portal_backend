package com.casi.project.portal.service;

import com.casi.project.portal.domain.MobileApplyResource;

import java.util.List;

/**
 * @author css
 * @date 2020/5/8 17:08
 */
public interface ApplyResourcesService {

    //List<MobileApplyResource> selectMenuList(String userId,String applyId);
    List<MobileApplyResource> selectMenuList(String applyId);

    /**
     * 新增应用资源
     */
    int insertResource(MobileApplyResource mobileApplyResource);

    /**
     * 修改应用资源
     */
    int updateResource(MobileApplyResource mobileApplyResource);

    /**
     * 删除应用资源
     */
    int deleteMenuById(String mobileApplyId);


    String[] selectRoleList(String userId);
}
