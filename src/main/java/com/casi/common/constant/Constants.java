package com.casi.common.constant;

import io.jsonwebtoken.Claims;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 通用常量信息
 *
 * @author lzb
 */
@Component
public class Constants
{
    /**
     * UTF-8 字符集
     */
    public static final String UTF8 = "UTF-8";

    /**
     * 通用成功标识
     */
    public static final String SUCCESS = "0";

    /**
     * 通用失败标识
     */
    public static final String FAIL = "1";

    /**
     * 登录成功
     */
    public static final String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    public static final String LOGOUT = "Logout";

    /**
     * 登录失败
     */
    public static final String LOGIN_FAIL = "Error";

    /**
     * 验证码 redis key
     */
    public static final String CAPTCHA_CODE_KEY = "captcha_codes:";

    /**
     * 登录用户 redis key
     */
    public static final String LOGIN_TOKEN_KEY = "login_tokens:";

    /**
     * 验证码有效期（分钟）
     */
    public static final Integer CAPTCHA_EXPIRATION = 2;

    /**
     * 令牌
     */
    public static final String TOKEN = "token";

    /**
     * 令牌前缀
     */
    public static final String TOKEN_PREFIX = "Bearer ";

    /**
     * 令牌前缀
     */
    public static final String LOGIN_USER_KEY = "login_user_key";

    /**
     * 用户ID
     */
    public static final String JWT_USERID = "userid";

    /**
     * 用户名称
     */
    public static final String JWT_USERNAME = Claims.SUBJECT;

    /**
     * 用户头像
     */
    public static final String JWT_AVATAR = "avatar";

    /**
     * 创建时间
     */
    public static final String JWT_CREATED = "created";

    /**
     * 用户权限
     */
    public static final String JWT_AUTHORITIES = "authorities";

    /**
     * 资源映射路径 前缀
     */
    public static final String RESOURCE_PREFIX = "/profile";
    /**
     * WX_ACCESS_TOKEN
     * 微信入口token
     */
    public static final String WX_ACCESS_TOKEN = "WX_ACCESS_TOKEN";
    /**
     * 本地域名
     */
 //   public static final String BASE_IMG_URL = "http://47.105.204.120:8089";


    public static final String BASE_IMG_URL = "http://47.92.220.25:4579";
//
 //    public static final String BASE_IMG_URL = "http://192.168.13.102:8081";

//    public static final String BASE_IMG_URL = "http://192.168.1.114:8080";


    public static final String CASIC_APP_ID_TOKEN = "1231213";//调用移动商店token


    public static String url = "";

    public static String HAS_BEEN_USED_URL = "";//已使用的app接口地址

    public static String RECOMMENDED_URL = "";//推荐的app接口地址

    public static String WX_DEPARTMANT_LIST_URL = "";//请求云信部门列表

    public static String WX_USER_LIST_URL = "";//请求云信用户列表

    public static String WX_SEND_MSG_URL = "https://htyxuat.casicloud.com/cgi-bin/message/send";//请求云信消息推送接口
    @Value("appstore.url.recommendedUrl")
    public static String WX_USER_URL = "";//请求云信用户详情

    public static String WX_OAUTHCODE_URL = "";//网页授权

    public static String CORPID = "";//商户ID

    public static String CORPSECRET = "";//商户密钥

    public static String WX_USERINFO_CODE = "";//根据code拿用户

    public static String  WX_PORTAL_URL = "";//移动门户登录地址

    @Value("${appstore.url.recommendedUrl}")
    public void setRecommendedUrl(String recommendedUrl) {
        RECOMMENDED_URL = recommendedUrl;
    }

    @Value("${appstore.url.hasBeenUsedUrl}")
    public void setHasBeenUsedUrl(String hasBeenUsedUrl) {
        HAS_BEEN_USED_URL = hasBeenUsedUrl;
    }

    @Value("${wx.url.accessToken}")
    public void setUrl(String accessToken) {
        Constants.url = accessToken;
    }
    @Value("${wx.url.departmentList}")
    public void setDepartmentList(String departmentList) {
        WX_DEPARTMANT_LIST_URL = departmentList;
    }
    @Value("${wx.url.userList}")
    public void setUserList(String userList) {
        WX_USER_LIST_URL = userList;
    }

    @Value("${wx.url.getByUser}")
    public void setUser(String getByUser) {
        WX_USER_URL = getByUser;
    }

    @Value("${wx.url.oauthCode}")
    public void setOauthCode(String oauthCode) {
        WX_OAUTHCODE_URL = oauthCode;
    }
    @Value("${wx.url.corpid}")
    public void setCorpid(String corpid) {
        CORPID = corpid;
    }
    @Value("${wx.url.corpsecret}")
    public void setCorpsecret(String corpsecret) {
        CORPSECRET = corpsecret;
    }

    @Value("${wx.url.getuserinfo}")
    public void setUserInfo(String getuserinfo) {
        WX_USERINFO_CODE = getuserinfo;
    }

    @Value("${wx.url.portalUrl}")
    public void setPortalUrl(String portalUrl) {
        WX_PORTAL_URL = portalUrl;
    }
}
