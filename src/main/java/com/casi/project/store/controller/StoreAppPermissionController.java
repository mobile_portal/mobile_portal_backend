package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreAppPermission;
import com.casi.project.store.service.IStoreAppPermissionService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用权限Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用权限"})
@RestController
@RequestMapping("/store/permission")
public class StoreAppPermissionController extends BaseController
{
    @Autowired
    private IStoreAppPermissionService storeAppPermissionService;

    /**
     * 查询应用权限列表
     */
    @ApiOperation(value = "查询应用权限列表")
    @PreAuthorize("@ss.hasPermi('store:permission:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreAppPermission storeAppPermission)
    {
        startPage();
        List<StoreAppPermission> list = storeAppPermissionService.selectStoreAppPermissionList(storeAppPermission);
        return getDataTable(list);
    }

    /**
     * 导出应用权限列表
     */
    @PreAuthorize("@ss.hasPermi('store:permission:export')")
    @Log(title = "应用权限", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreAppPermission storeAppPermission)
    {
        List<StoreAppPermission> list = storeAppPermissionService.selectStoreAppPermissionList(storeAppPermission);
        ExcelUtil<StoreAppPermission> util = new ExcelUtil<StoreAppPermission>(StoreAppPermission.class);
        return util.exportExcel(list, "permission");
    }

    /**
     * 获取应用权限详细信息
     */
    @ApiOperation(value = "获取应用权限详细信息")
    @PreAuthorize("@ss.hasPermi('store:permission:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppPermissionService.selectStoreAppPermissionById(uuid));
    }

    /**
     * 新增应用权限
     */
    @ApiOperation(value = "新增应用权限")
    @PreAuthorize("@ss.hasPermi('store:permission:add')")
    @Log(title = "应用权限", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreAppPermission storeAppPermission)
    {
        return toAjax(storeAppPermissionService.insertStoreAppPermission(storeAppPermission));
    }

    /**
     * 修改应用权限
     */
    @ApiOperation(value = "修改应用权限")
    @PreAuthorize("@ss.hasPermi('store:permission:edit')")
    @Log(title = "应用权限", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreAppPermission storeAppPermission)
    {
        return toAjax(storeAppPermissionService.updateStoreAppPermission(storeAppPermission));
    }

    /**
     * 删除应用权限
     */
    @ApiOperation(value = "删除应用权限")
    @PreAuthorize("@ss.hasPermi('store:permission:remove')")
    @Log(title = "应用权限", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppPermissionService.deleteStoreAppPermissionByIds(uuids));
    }
}
