package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UnreadStatus implements Serializable {
   private String noticeId;
   private String userId;
   private Long isRead;
   private Date lastTime;
}
