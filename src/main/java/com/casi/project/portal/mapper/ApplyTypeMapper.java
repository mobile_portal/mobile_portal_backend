package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.ApplyIType;
import com.casi.project.portal.domain.vo.ApplyType;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @author 18732
 */
public interface ApplyTypeMapper {

    int checkTypeUnique(@Param("typeName") String typeName);

    /**
     * 新增应用类型
     *
     * @param applyType
     * @return
     */
    int addApplyType(ApplyIType applyType);

    /**
     * 根据id回显应用类型具体详情
     *
     * @param id
     * @return
     */
    ApplyIType getApplyTypeId(@Param("id") String id);

    /**
     * 修改应用类型
     *
     * @param applyType
     * @return
     */
    int updateApplyType(ApplyIType applyType);

    /**
     * 禁用应用类型
     *
     * @param id
     * @param type
     * @return
     */
    int disableEnabledApplyType(@Param("id") String id, @Param("type") Integer type);

    /**
     * 查询应用类型下拉列表
     *
     * @return
     */
    public ArrayList<ApplyType> getApplyTypeList();

    /**
     * 查询应用类型列表
     *
     * @return
     */
    ArrayList<ApplyType> getApplyTypeListPage();

    /**
     * 查询首页menu名称下拉列表
     *
     * @return
     */
    List<HashMap<String, Object>> getHomePageList();
}
