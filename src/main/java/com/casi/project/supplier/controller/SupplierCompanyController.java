package com.casi.project.supplier.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.supplier.domain.SupplierCompany;
import com.casi.project.supplier.service.ISupplierCompanyService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 供应商企业Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"供应商企业"})
@RestController
@RequestMapping("/supplier/company")
public class SupplierCompanyController extends BaseController
{
    @Autowired
    private ISupplierCompanyService supplierCompanyService;

    /**
     * 查询供应商企业列表
     */
    @ApiOperation(value = "查询供应商企业列表")
    @PreAuthorize("@ss.hasPermi('supplier:company:list')")
    @GetMapping("/list")
    public TableDataInfo list(SupplierCompany supplierCompany)
    {
        startPage();
        List<SupplierCompany> list = supplierCompanyService.selectSupplierCompanyList(supplierCompany);
        return getDataTable(list);
    }

    /**
     * 导出供应商企业列表
     */
    @PreAuthorize("@ss.hasPermi('supplier:company:export')")
    @Log(title = "供应商企业", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SupplierCompany supplierCompany)
    {
        List<SupplierCompany> list = supplierCompanyService.selectSupplierCompanyList(supplierCompany);
        ExcelUtil<SupplierCompany> util = new ExcelUtil<SupplierCompany>(SupplierCompany.class);
        return util.exportExcel(list, "company");
    }

    /**
     * 获取供应商企业详细信息
     */
    @ApiOperation(value = "获取供应商企业详细信息")
    @PreAuthorize("@ss.hasPermi('supplier:company:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(supplierCompanyService.selectSupplierCompanyById(uuid));
    }

    /**
     * 新增供应商企业
     */
    @ApiOperation(value = "新增供应商企业")
    @PreAuthorize("@ss.hasPermi('supplier:company:add')")
    @Log(title = "供应商企业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SupplierCompany supplierCompany)
    {
        return toAjax(supplierCompanyService.insertSupplierCompany(supplierCompany));
    }

    /**
     * 修改供应商企业
     */
    @ApiOperation(value = "修改供应商企业")
    @PreAuthorize("@ss.hasPermi('supplier:company:edit')")
    @Log(title = "供应商企业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SupplierCompany supplierCompany)
    {
        return toAjax(supplierCompanyService.updateSupplierCompany(supplierCompany));
    }

    /**
     * 删除供应商企业
     */
    @ApiOperation(value = "删除供应商企业")
    @PreAuthorize("@ss.hasPermi('supplier:company:remove')")
    @Log(title = "供应商企业", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(supplierCompanyService.deleteSupplierCompanyByIds(uuids));
    }
}
