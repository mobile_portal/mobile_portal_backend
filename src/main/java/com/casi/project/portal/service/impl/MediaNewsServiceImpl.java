package com.casi.project.portal.service.impl;


import com.casi.common.constant.Constants;
import com.casi.common.utils.DateUtils;
import com.casi.common.utils.StringUtils;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.*;
import com.casi.project.portal.domain.vo.MediaArticleAddVo;
import com.casi.project.portal.domain.vo.MediaBaseForNewsVo;
import com.casi.project.portal.domain.vo.MediaNewsAddVo;
import com.casi.project.portal.mapper.MediaArticleMapper;
import com.casi.project.portal.mapper.MediaBaseMapper;
import com.casi.project.portal.mapper.MediaNewsMapper;
import com.casi.project.portal.service.FileService;
import com.casi.project.portal.service.MediaNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author zhongzj
 * @Description:
 * @Param:
 * @Return:
 * @Create: 2019/11/18
 */
@Service
@Transactional
public class MediaNewsServiceImpl implements MediaNewsService {


    @Autowired
    private MediaNewsMapper mediaNewsMapper;

    @Autowired
    private MediaBaseMapper mediaBaseMapper;

    @Autowired
    private MediaArticleMapper mediaArticleMapper;

    @Autowired
    private FileService fileService;

    //@Autowired
    //private ProcessService processService; // 流程业务相关服务
    //
    //@Autowired
    //private RedisService redisService; // 緩存相关服务

    @Override
    public List<MediaBaseForNewsVo> queryList(MediaBase mediaBase) {
        //查询对应图文列表视图类
        List<MediaBaseForNewsVo> newsVoList = mediaBaseMapper.queryListForNews(mediaBase);
        return newsVoList;
    }

    @Override
    public MediaBase queryDetail(Long id) {

        MediaBase mediaBase;
        try {
            //根据baseId获取base表素材信息
             mediaBase = mediaBaseMapper.queryById(id);
            //根据信息中的mediaId去mediaFile表获取具体信息
            MediaNews news = mediaNewsMapper.queryById(mediaBase.getMediaId());
            //根据newsid查询mediaartice表
            List<MediaArticle> byNewsId = mediaArticleMapper.getByNewsId(news.getId());
            String newsUrl = byNewsId.get(0).getNewsUrl();
            //（开始）从这开始拼接图片全路径但是没有返回给前段   所以没有
            //根据news id 查询详情
            List<MediaArticle> articleList = mediaArticleMapper.getByNewsId(news.getId());

            //拼接图片地址
            for (MediaArticle mediaArticle : articleList) {
                mediaArticle.setPicUrl( Constants.BASE_IMG_URL+mediaArticle.getPicUrl());
            }
            //结束
            //查询出图文信息集合将其放入ArticleList属性
            news.setArticleList(mediaArticleMapper.getByNewsId(news.getId()));
            mediaBase.setNewsUrl(newsUrl);
            mediaBase.setMediaNews(news);

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

        return mediaBase;
}

    @Override
    @Transactional(rollbackFor = {Exception.class,RuntimeException.class})
    public boolean add(MediaNewsAddVo newsAddVo, String type) throws Exception{


        newsAddVo.setSubmitTime(new Date());//提交时间，默认当前时间
        boolean result = false;

        //base赋值
        MediaBase mediaBase = new MediaBase();
        mediaBase.setStatus(newsAddVo.getStatus());
        mediaBase.setCreateTime(new Date(System.currentTimeMillis()));
        mediaBase.setColumnId(newsAddVo.getColumnId());
        mediaBase.setMediaType(MediaEnum.News.toString());

        //news赋值
        MediaNews news = new MediaNews();

        List<MediaArticle> articleList = new ArrayList<>();

        List<MediaArticleAddVo> addVoList = newsAddVo.getArticleList();

        List<Long> fileIdList = new ArrayList<>();


        for (int i = 0; i < addVoList.size(); i++) {
            MediaArticle article = new MediaArticle();
            article.setAuthor(addVoList.get(i).getAuthor());
            article.setDigest(addVoList.get(i).getDigest());
            article.setTitle(addVoList.get(i).getTitle());
            article.setContent(addVoList.get(i).getContent());

            article.setNewsIndex((long)i);
            article.setShowCoverPic(addVoList.get(i).getShowCoverPic());
            article.setAuthor(addVoList.get(i).getAuthor());
            article.setFileId(addVoList.get(i).getFileId());
            article.setNewsUrl(addVoList.get(i).getNewsUrl());

            //查询新闻的文件信息
            LocalFile file = fileService.queryById(addVoList.get(i).getFileId());

            if(file==null){
                throw new RuntimeException("没有找到对应的文件信息。");
            }


                //截取掉  profile 后缀
                String str1 = file.getFileUrl().replace("profile//",  "");
                article.setPicUrl(str1);
                article.setPicDir("");
                article.setPicName(file.getName());
                article.setCreateTime(DateUtils.dateTime(new Date(System.currentTimeMillis())));
                articleList.add(article);
                //保存文件id
                fileIdList.add(addVoList.get(i).getFileId());
                //添加图文总信息,将图文信息中第一个图文的标题和封面图片路径保存
                news.setFirstPicUrl(articleList.get(0).getPicUrl());
                news.setFirstTitle(articleList.get(0).getTitle());
                news.setAuthor(articleList.get(0).getAuthor());
                news.setCreateTime(new Date(System.currentTimeMillis()));
                news.setArticleList(articleList);
                mediaBase.setMediaNews(news);
        }

        //将文件idlist转为字符串用，隔开
        mediaBase.setFileIdList(StringUtils.join(fileIdList.toArray(), ","));
        //保存总图文信息
        int newsResult = mediaNewsMapper.insert(news);

        //给单图文表添加newsId
        for (MediaArticle article : articleList) {
            article.setNewsId(news.getId());
        }
        //添加图文信息 //循环添加
        int articleResult = mediaArticleMapper.insertByBatch(news.getArticleList());

        // 新增后获取主键id做mediaId
        mediaBase.setMediaId(news.getId());
        //将保存的mediaId存入base信息中再存入base表中
        int baseResult = mediaBaseMapper.insert(mediaBase);

        //启动流程
        //保存标题名字存入流程数据
        mediaBase.setName(news.getFirstTitle());

        if (0 < newsResult && 0 < articleResult && 0 < baseResult ){
            return  true;
        }else{
            throw new RuntimeException("新增图文失败");
        }
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,RuntimeException.class})
    public boolean update(MediaNewsAddVo newsAddVo, HttpServletRequest request) throws Exception{

        boolean result = false;

        //初始化数据
        MediaBase mediaBase = queryDetail(newsAddVo.getId());

        //base赋值
        mediaBase.setStatus(newsAddVo.getStatus());
        mediaBase.setApproveProcess(newsAddVo.getApproveProcess());
        mediaBase.setTerminalType(newsAddVo.getTerminalType());
        mediaBase.setUpdateTime(new Date(System.currentTimeMillis()));
        mediaBase.setColumnId(newsAddVo.getColumnId());
        mediaBase.setMediaType(MediaEnum.News.toString());

        //article更新,删除之前的article新增现在的article
        MediaNews news = mediaBase.getMediaNews();

        //删除之前的article
        int deleteResult = mediaArticleMapper.deleteByBatch(news.getId());

        List<MediaArticle> articleList = new ArrayList<>();
        List<MediaArticleAddVo> addVoList = newsAddVo.getArticleList();
        List<Long> fileIdList = new ArrayList<>();
        //article赋值
        for (int i = 0; i < addVoList.size(); i++) {
            MediaArticle article = new MediaArticle();
            article.setNewsId(news.getId());
            article.setAuthor(addVoList.get(i).getAuthor());
            article.setDigest(addVoList.get(i).getDigest());
            article.setTitle(addVoList.get(i).getTitle());
            article.setContent(addVoList.get(i).getContent());
            article.setFileId(addVoList.get(i).getFileId());
            article.setNewsIndex((long)i+1);
            article.setShowCoverPic(addVoList.get(i).getShowCoverPic());
            //跳转路径
            article.setNewsUrl(addVoList.get(i).getNewsUrl());


            LocalFile file = fileService.queryById(addVoList.get(i).getFileId());

            if(file==null){
                System.out.println("没有对应文件信息");
               return false;
            }

            String str1 = file.getFileUrl().replace("profile//",  "");
            article.setPicUrl(str1);
            article.setPicDir("");
            article.setPicName(file.getName());
            articleList.add(article);
            //保存文件id
            fileIdList.add(addVoList.get(i).getFileId());

        }

        //news赋值
        //添加图文总信息,将图文信息中第一个图文的标题和封面图片路径保存
        news.setFirstPicUrl(articleList.get(0).getPicUrl());
        news.setFirstTitle(articleList.get(0).getTitle());
        news.setUpdateTime(new Date(System.currentTimeMillis()));
        news.setAuthor(articleList.get(0).getAuthor());
        news.setArticleList(articleList);
        mediaBase.setMediaNews(news);


        mediaBase.setFileIdList(StringUtils.join(fileIdList.toArray(), ","));

        //更新article表
        int articleResult = mediaArticleMapper.insertByBatch(articleList);
        //更新news表
        int newsResult = mediaNewsMapper.update(news);
        //更新base表
        int baseResult = mediaBaseMapper.update(mediaBase);

        if (0 < newsResult && 0 < articleResult && 0 < baseResult){
            result = true;
        }

        return  result;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,RuntimeException.class})
    public boolean examine(MediaNewsAddVo newsAddVo, HttpServletRequest request) throws Exception{

        if(null == newsAddVo.getStatus()){
            System.out.println("未知状态，不可以进行操作");
            return false;
        }

        if(newsAddVo.getId()==null || newsAddVo.getId()==0){
            System.out.println("修改失败，没有对应的新闻信息");
            return false;
        }

        //当前状态
        String status = mediaBaseMapper.queryById(newsAddVo.getId()).getStatus();
        String newStatus = newsAddVo.getStatus();

        //只对发布审核退回进行判断
        if(status.equals(StatusEnum.PASS_REVIEW.toString())||status.equals(StatusEnum.PUBLISHED.toString())||status.equals(StatusEnum.REJECT.toString())){
            if(status.equals(newStatus)){
                System.out.println("已经完成过当前操作，不能重复执行同一操作");
                return false;
            }
        }

        //校验状态,已退回和已发布的不可以进行操作 PUBLISHED
        if (StatusEnum.REJECT.toString().equals(status)&&StatusEnum.PUBLISHED.toString().equals(status)){
            System.out.println("已退回和已发布的不可以进行操作");
            return false;
        }

        //执行发布或者退回时  要先审核通过
        if(newStatus.equals(StatusEnum.PUBLISHED.toString())||newStatus.equals(StatusEnum.REJECT.toString())){
            if(!status.equals(StatusEnum.PASS_REVIEW.toString())){
                System.out.println("未审核通过");
                return false;
            }
        }


        boolean result = false;

        //初始化数据
        MediaBase mediaBase = queryDetail(newsAddVo.getId());

        //base赋值
        mediaBase.setStatus(newsAddVo.getStatus());
        mediaBase.setApproveProcess(newsAddVo.getApproveProcess());
        mediaBase.setTerminalType(newsAddVo.getTerminalType());
        mediaBase.setUpdateTime(new Date(System.currentTimeMillis()));
        mediaBase.setColumnId(newsAddVo.getColumnId());
        mediaBase.setMediaType(MediaEnum.News.toString());

        //article更新,删除之前的article新增现在的article
        MediaNews news = mediaBase.getMediaNews();

        //删除之前的article
        int deleteResult = mediaArticleMapper.deleteByBatch(news.getId());

        List<MediaArticle> articleList = new ArrayList<>();
        List<MediaArticleAddVo> addVoList = newsAddVo.getArticleList();
        List<Long> fileIdList = new ArrayList<>();
        //article赋值
        for (int i = 0; i < addVoList.size(); i++) {
            MediaArticle article = new MediaArticle();
            article.setNewsId(news.getId());
            article.setAuthor(addVoList.get(i).getAuthor());
            article.setDigest(addVoList.get(i).getDigest());
            article.setTitle(addVoList.get(i).getTitle());
            article.setContent(addVoList.get(i).getContent());
            article.setFileId(addVoList.get(i).getFileId());
            article.setNewsIndex((long)i+1);
            article.setShowCoverPic(addVoList.get(i).getShowCoverPic());


            LocalFile file = fileService.queryById(addVoList.get(i).getFileId());

            if(file==null){
                System.out.println("没有对应文件信息");
                return false;
            }

            String str1 = file.getFileUrl().replace("profile//",  "");
            article.setPicUrl(str1);
            article.setPicDir("");
            article.setPicName(file.getName());
            articleList.add(article);
            //保存文件id
            fileIdList.add(addVoList.get(i).getFileId());

        }

        //news赋值
        //添加图文总信息,将图文信息中第一个图文的标题和封面图片路径保存
        news.setFirstPicUrl(articleList.get(0).getPicUrl());
        news.setFirstTitle(articleList.get(0).getTitle());
        news.setUpdateTime(new Date(System.currentTimeMillis()));
        news.setAuthor(articleList.get(0).getAuthor());
        news.setArticleList(articleList);
        mediaBase.setMediaNews(news);


        mediaBase.setFileIdList(StringUtils.join(fileIdList.toArray(), ","));

        //更新article表
        int articleResult = mediaArticleMapper.insertByBatch(articleList);
        //更新news表
        int newsResult = mediaNewsMapper.update(news);
        //更新base表
        int baseResult = mediaBaseMapper.update(mediaBase);

        if (0 < newsResult && 0 < articleResult && 0 < baseResult){
            result = true;
        }

        return  result;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,RuntimeException.class})
    public boolean delete(MediaBase mediaBase) throws Exception{
        boolean result = false;

        if(mediaBase!=null){
            if(mediaBase.getId()==null || mediaBase.getId()==0){
                return false;
            }
        }

        mediaBase = queryDetail(mediaBase.getId());

        //校验状态是否为待提交或已退回
        if (!StatusEnum.TO_SUBMIT.toString().equals(mediaBase.getStatus()) && !StatusEnum.REJECT.toString().equals(mediaBase.getStatus())){
            System.out.println("当前状态不可删除");
            return false;
        }


        try {
            //删除article表
            int articleResult = mediaArticleMapper.deleteByBatch(mediaBase.getMediaId());
            //删除news表
            int newsResult = mediaNewsMapper.delete(mediaBase.getMediaId());
            //删除文件

            String fileId = queryDetail(mediaBase.getId()).getFileIdList();
            List<Long> fileIdList = Arrays.asList(fileId.split(",")).stream().map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
            for (int i = 0; i < fileIdList.size(); i++) {
                fileService.delete(fileIdList.get(i));
            }
            //删除base表
            int baseResult = mediaBaseMapper.delete(mediaBase.getId());

            if (0 < articleResult && 0 < newsResult && 0 < baseResult){
                result = true;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }

        return result;
    }
    /**
     * 资讯管理审核接口
     */
    @Override
    public AjaxResult auditNews(MediaBase mediaBase) {
        if(StringUtils.isNull(mediaBase)){
            return AjaxResult.error("缺失参数");
        }
        if(mediaBase.getId() == null){
            return AjaxResult.error("id不能为空");
        }
        MediaBase mediaBase1 = mediaBaseMapper.queryById(mediaBase.getId());
        if(StringUtils.isNull(mediaBase1)){
            return AjaxResult.error("该资讯不存在");
        }
        if(mediaBase1.getStatus().equals("2")){
            mediaBase1.setStatus("3");
        }else if(mediaBase1.getStatus().equals("3")){
            return AjaxResult.error("该资讯已审核，不能重复审核");
        }else if(mediaBase1.getStatus().equals("4")){
            return AjaxResult.error("该资讯已审核且已发布，不能在进行审核");
        }else{
            return AjaxResult.error("该资讯审核有问题，请检查代码");
        }
        int i = mediaBaseMapper.update(mediaBase1);
        if(i < 1){
            return AjaxResult.error("审核失败，请检查代码");
        }
        return AjaxResult.success("审核成功");
    }
    /**
     * 资讯管理发布接口
     */
    @Override
    public AjaxResult publishNews(MediaBase mediaBase) {
        if(StringUtils.isNull(mediaBase)){
            return AjaxResult.error("缺失参数");
        }
        if(mediaBase.getId() == null){
            return AjaxResult.error("id不能为空");
        }
        MediaBase mediaBase1 = mediaBaseMapper.queryById(mediaBase.getId());
        if(StringUtils.isNull(mediaBase1)){
            return AjaxResult.error("该资讯不存在");
        }
        if(mediaBase1.getStatus().equals("2")){
            return AjaxResult.error("该资讯还未通过审核，不允许发布");
        }else if(mediaBase1.getStatus().equals("3")){
            mediaBase1.setStatus("4");
        }else if(mediaBase1.getStatus().equals("4")){
            return AjaxResult.error("该资讯已发布不允许重复发布");
        }else{
            return AjaxResult.error("该资讯发布有问题，请检查代码");
        }
        int i = mediaBaseMapper.update(mediaBase1);
        if(i < 1){
            return AjaxResult.error("发布资讯失败");
        }
        return AjaxResult.success("发布资讯成功");
    }
    /**
     * 资讯管理退回接口
     */
    @Override
    public AjaxResult sendBackNews(String id) {
        if(StringUtils.isEmpty(id)){
            return AjaxResult.error("id不能为空");
        }
        MediaBase mediaBase1 = mediaBaseMapper.queryById(Long.valueOf(id));
        if(StringUtils.isNull(mediaBase1)){
            return AjaxResult.error("该资讯不存在");
        }
        if(mediaBase1.getStatus().equals("2")){
            return AjaxResult.success("该资讯退回成功");
        }else if(mediaBase1.getStatus().equals("3")){
            return AjaxResult.error("该资讯已审核不允许退回");
        }else if(mediaBase1.getStatus().equals("4")){
            return AjaxResult.error("该公告已发布，不允许退回");
        }else{
            return AjaxResult.error("该资讯退回有问题，请检查代码");
        }
    }
    /**
     * 删除素材
     */
    @Override
    @Transactional
    public AjaxResult deleteById(MediaBase mediaBase) {
        if(StringUtils.isNull(mediaBase)){
            return AjaxResult.error("缺失参数");
        }
        if(null == mediaBase.getId()){
            return AjaxResult.error("Id不能为空");
        }
        //根据主键id查询
        MediaBase mediaBase1 = mediaBaseMapper.queryById(mediaBase.getId());
        if(StringUtils.isNull(mediaBase1)){
            return AjaxResult.error("该资讯不存在");
        }
        int i = mediaBaseMapper.delete(mediaBase.getId());
        if(i < 1){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
            return AjaxResult.error("删除素材失败");
        }
        int i1 = mediaNewsMapper.delete(mediaBase1.getMediaId());
        if(i1 < 1){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
            return AjaxResult.error("删除素材失败");
        }
        List<MediaArticle> byNewsId = mediaArticleMapper.getByNewsId(mediaBase1.getMediaId());
        if(byNewsId.size() < 1){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
            return AjaxResult.error("删除素材失败");
        }
        for(MediaArticle  mediaArticle: byNewsId){
            int i3 = mediaArticleMapper.delete(mediaArticle.getId());
            if(i3 < 1){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
                return AjaxResult.error("删除素材失败");
            }
        }
        return AjaxResult.success("删除素材成功");
    }
}
