package com.casi.project.system.service;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.system.domain.SysWhiteIp;

import java.util.List;

/**
 * 白名单管理Service接口
 * 
 * @author casi
 * @date 2020-06-16
 */
public interface ISysWhiteIpService 
{
    /**
     * 查询白名单管理
     * 
     * @param uuid 白名单管理ID
     * @return 白名单管理
     */
    public SysWhiteIp selectSysWhiteIpById(String uuid);

    /**
     * 查询白名单管理列表
     * 
     * @param sysWhiteIp 白名单管理
     * @return 白名单管理集合
     */
    public List<SysWhiteIp> selectSysWhiteIpList(SysWhiteIp sysWhiteIp);

    /**
     * 新增白名单管理
     * 
     * @param sysWhiteIp 白名单管理
     * @return 结果
     */
    public int insertSysWhiteIp(SysWhiteIp sysWhiteIp);

    /**
     * 修改白名单管理
     * 
     * @param sysWhiteIp 白名单管理
     * @return 结果
     */
    public int updateSysWhiteIp(SysWhiteIp sysWhiteIp);

    /**
     * 批量删除白名单管理
     * 
     * @param uuids 需要删除的白名单管理ID
     * @return 结果
     */
    public int deleteSysWhiteIpByIds(List<String> uuids);

    /**
     * 删除白名单管理信息
     * 
     * @param uuid 白名单管理ID
     * @return 结果
     */
    public int deleteSysWhiteIpById(String uuid);

    SysWhiteIp getInfoByIp(String whiteIp);
}
