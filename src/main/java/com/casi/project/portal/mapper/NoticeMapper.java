package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.MobileNotice;
import com.casi.project.portal.domain.UnreadStatus;
import com.casi.project.portal.domain.vo.NoticeVO;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;

public interface NoticeMapper {

    /**
     * 查询公告列表
     *
     * @return
     */
    public ArrayList<MobileNotice> getNoticeList();


    /**
     * 根据ID查询公告具体详情
     *
     * @param id
     * @return
     */
    public MobileNotice getNoticeId(@Param("id") String id);


    /**
     * 新增公告已读未读
     * unreadStatus
     *
     * @return
     */
    int addUnreadStatus(UnreadStatus unreadStatus);

    /**
     * 查询已读未读
     *
     * @param noticeId
     * @param userId
     * @return
     */
    public int getUnreadStatus(@Param("noticeId") String noticeId, @Param("userId") String userId);

    public int updateUnreadStatus(UnreadStatus unreadStatus);

    int getNoticeIsRead(String noticeId, String userId);
}
