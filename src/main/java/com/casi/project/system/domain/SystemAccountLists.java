package com.casi.project.system.domain;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class SystemAccountLists {
    private List<SysUserJson> systemAccountLists;

    public List<SysUserJson> getSystemAccountLists() {
        return systemAccountLists;
    }

    public void setSystemAccountLists(List<SysUserJson> systemAccountLists) {
        this.systemAccountLists = systemAccountLists;
    }

    @Override
    public String toString() {
        return "SystemAccountLists{" +
                "systemAccountLists=" + systemAccountLists +
                '}';
    }
}
