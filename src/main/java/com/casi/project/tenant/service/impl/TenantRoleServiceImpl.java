package com.casi.project.tenant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.tenant.mapper.TenantRoleMapper;
import com.casi.project.tenant.domain.TenantRole;
import com.casi.project.tenant.service.ITenantRoleService;

/**
 * 租户角色Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class TenantRoleServiceImpl implements ITenantRoleService 
{
    @Autowired
    private TenantRoleMapper tenantRoleMapper;

    /**
     * 查询租户角色
     * 
     * @param uuid 租户角色ID
     * @return 租户角色
     */
    @Override
    public TenantRole selectTenantRoleById(String uuid)
    {
        return tenantRoleMapper.selectTenantRoleById(uuid);
    }

    /**
     * 查询租户角色列表
     * 
     * @param tenantRole 租户角色
     * @return 租户角色
     */
    @Override
    public List<TenantRole> selectTenantRoleList(TenantRole tenantRole)
    {
        return tenantRoleMapper.selectTenantRoleList(tenantRole);
    }

    /**
     * 新增租户角色
     * 
     * @param tenantRole 租户角色
     * @return 结果
     */
    @Override
    public int insertTenantRole(TenantRole tenantRole)
    {
        return tenantRoleMapper.insertTenantRole(tenantRole);
    }

    /**
     * 修改租户角色
     * 
     * @param tenantRole 租户角色
     * @return 结果
     */
    @Override
    public int updateTenantRole(TenantRole tenantRole)
    {
        return tenantRoleMapper.updateTenantRole(tenantRole);
    }

    /**
     * 批量删除租户角色
     * 
     * @param uuids 需要删除的租户角色ID
     * @return 结果
     */
    @Override
    public int deleteTenantRoleByIds(String[] uuids)
    {
        return tenantRoleMapper.deleteTenantRoleByIds(uuids);
    }

    /**
     * 删除租户角色信息
     * 
     * @param uuid 租户角色ID
     * @return 结果
     */
    @Override
    public int deleteTenantRoleById(String uuid)
    {
        return tenantRoleMapper.deleteTenantRoleById(uuid);
    }
}
