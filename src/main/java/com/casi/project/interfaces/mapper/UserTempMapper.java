package com.casi.project.interfaces.mapper;


import com.casi.project.interfaces.domain.UserTemp;

public interface UserTempMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(UserTemp record);

    int insertSelective(UserTemp record);

    UserTemp selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(UserTemp record);

    int updateByPrimaryKey(UserTemp record);

    UserTemp selectByUserId(String  userId);
}