package com.casi.project.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class SysUserJson {
    /*云IDaaS平台主账户唯一*/
    private String uuid;
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm")
    private Date createTime;
    private Boolean archived;
    private String username;
    private String phoneNumber;
    private String email;
    private Boolean locked;
    private String externalId;
    private String ouId;
    private String ouDirectory;
    private String displayName;
    private String[] belongOus;
    private String deptId;
   /* *//*密码*//*
    private String password;
    *//*用户的显示名称,唯一*//*
    private String displayName;
    *//*用户ID,与外部ID值一样*//*
    private String id;
    *//*外部ID,唯一,不为空*//*
    private String externalId;
    *//*邮箱*//*
    private List<Emails> emails;
    *//*手机号*//*
    private List<PhoneNumbers> phoneNumbers;
    *//*为账户指定组织单位*//*
    private List<Belongs> belongs;
*/

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String[] getBelongOus() {
        return belongOus;
    }

    public void setBelongOus(String[] belongOus) {
        this.belongOus = belongOus;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getOuDirectory() {
        return ouDirectory;
    }

    public void setOuDirectory(String ouDirectory) {
        this.ouDirectory = ouDirectory;
    }

    public String getOuId() {
        return ouId;
    }

    public void setOuId(String ouId) {
        this.ouId = ouId;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Boolean getArchived() {
        return archived;
    }

    public void setArchived(Boolean archived) {
        this.archived = archived;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "SysUserJson{" +
                "uuid='" + uuid + '\'' +
                ", createTime=" + createTime +
                ", archived=" + archived +
                ", username='" + username + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", locked=" + locked +
                ", externalId='" + externalId + '\'' +
                ", ouId='" + ouId + '\'' +
                ", ouDirectory='" + ouDirectory + '\'' +
                ", displayName='" + displayName + '\'' +
                ", belongOus=" + Arrays.toString(belongOus) +
                ", deptId='" + deptId + '\'' +
                '}';
    }
}
