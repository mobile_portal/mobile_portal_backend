package com.casi.project.portal.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * @name:CommonUtils.class
 * @description:公共方法工具类
 * @author:dingjianwei
 * @date:2016年11月8日 下午3:05:34
 * @location:cn.com.casic.util.CommonUtils.class
 * Copyright 2015 by Jusfoun.com. All Right Reserved
 */
public class CommonUtils {
	private static Log logger = LogFactory.getLog(CommonUtils.class);
	
	/**
	 * 
	 * @title: getUUID  
	 * @description: 获取UUID主键值
	 * @author: dingjianwei
	 * @date:2016年11月8日 下午3:08:19  
	 * @return
	 */
	public static String getUUID(){
		return UUID.randomUUID().toString();
	}
	
	/**
	 * 
	 * @title: getBusinessCode  
	 * @description: 获取业务行政编码
	 * @author: dingjianwei
	 * @date:2016年11月8日 下午3:21:38  
	 * @param flag 业务字段
	 * @return
	 */
	public static String getBusinessCode(String flag){
		Date day=new Date();
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
		return flag+df.format(day)+getFixLenthString(3);
	}
	
    /**
     * 
     * @title: getFixLenthString  
     * @description: 获取固定长度的随机数
     * @author: dingjianwei
     * @date:2016年11月8日 下午3:27:37  
     * @param strLength
     * @return
     */
    private static String getFixLenthString(int strLength) {
        Random rm = new Random();
        // 获得随机数
        double pross = (1 + rm.nextDouble()) * Math.pow(10, strLength);
        // 将获得的获得随机数转化为字符串
        String fixLenthString = String.valueOf(pross);
        // 返回固定的长度的随机数
        return fixLenthString.substring(1, strLength + 1);
    }
    
}
