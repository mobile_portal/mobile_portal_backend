package com.casi.project.tenant.mapper;

import java.util.List;
import com.casi.project.tenant.domain.TenantAppResources;

/**
 * 租户应用资源Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface TenantAppResourcesMapper 
{
    /**
     * 查询租户应用资源
     * 
     * @param uuid 租户应用资源ID
     * @return 租户应用资源
     */
    public TenantAppResources selectTenantAppResourcesById(String uuid);

    /**
     * 查询租户应用资源列表
     * 
     * @param tenantAppResources 租户应用资源
     * @return 租户应用资源集合
     */
    public List<TenantAppResources> selectTenantAppResourcesList(TenantAppResources tenantAppResources);

    /**
     * 新增租户应用资源
     * 
     * @param tenantAppResources 租户应用资源
     * @return 结果
     */
    public int insertTenantAppResources(TenantAppResources tenantAppResources);

    /**
     * 修改租户应用资源
     * 
     * @param tenantAppResources 租户应用资源
     * @return 结果
     */
    public int updateTenantAppResources(TenantAppResources tenantAppResources);

    /**
     * 删除租户应用资源
     * 
     * @param uuid 租户应用资源ID
     * @return 结果
     */
    public int deleteTenantAppResourcesById(String uuid);

    /**
     * 批量删除租户应用资源
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTenantAppResourcesByIds(String[] uuids);
}
