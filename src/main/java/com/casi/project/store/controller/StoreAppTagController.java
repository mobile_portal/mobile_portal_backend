package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreAppTag;
import com.casi.project.store.service.IStoreAppTagService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用标签关联Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用标签关联"})
@RestController
@RequestMapping("/store/appTag")
public class StoreAppTagController extends BaseController
{
    @Autowired
    private IStoreAppTagService storeAppTagService;

    /**
     * 查询应用标签关联列表
     */
    @ApiOperation(value = "查询应用标签关联列表")
    @PreAuthorize("@ss.hasPermi('store:appTag:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreAppTag storeAppTag)
    {
        startPage();
        List<StoreAppTag> list = storeAppTagService.selectStoreAppTagList(storeAppTag);
        return getDataTable(list);
    }

    /**
     * 导出应用标签关联列表
     */
    @PreAuthorize("@ss.hasPermi('store:appTag:export')")
    @Log(title = "应用标签关联", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreAppTag storeAppTag)
    {
        List<StoreAppTag> list = storeAppTagService.selectStoreAppTagList(storeAppTag);
        ExcelUtil<StoreAppTag> util = new ExcelUtil<StoreAppTag>(StoreAppTag.class);
        return util.exportExcel(list, "appTag");
    }

    /**
     * 获取应用标签关联详细信息
     */
    @ApiOperation(value = "获取应用标签关联详细信息")
    @PreAuthorize("@ss.hasPermi('store:appTag:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppTagService.selectStoreAppTagById(uuid));
    }

    /**
     * 新增应用标签关联
     */
    @ApiOperation(value = "新增应用标签关联")
    @PreAuthorize("@ss.hasPermi('store:appTag:add')")
    @Log(title = "应用标签关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreAppTag storeAppTag)
    {
        return toAjax(storeAppTagService.insertStoreAppTag(storeAppTag));
    }

    /**
     * 修改应用标签关联
     */
    @ApiOperation(value = "修改应用标签关联")
    @PreAuthorize("@ss.hasPermi('store:appTag:edit')")
    @Log(title = "应用标签关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreAppTag storeAppTag)
    {
        return toAjax(storeAppTagService.updateStoreAppTag(storeAppTag));
    }

    /**
     * 删除应用标签关联
     */
    @ApiOperation(value = "删除应用标签关联")
    @PreAuthorize("@ss.hasPermi('store:appTag:remove')")
    @Log(title = "应用标签关联", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppTagService.deleteStoreAppTagByIds(uuids));
    }
}
