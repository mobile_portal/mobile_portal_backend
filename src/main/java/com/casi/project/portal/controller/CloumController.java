package com.casi.project.portal.controller;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.Cloum;
import com.casi.project.portal.service.CloumService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Auther shp
 * @data2020/4/2416:37
 * @description
 */

@RestController
@Api(tags = "栏目相关接口")
@RequestMapping("/cloum")
public class CloumController {

    private static Logger logger = LoggerFactory.getLogger(CloumController.class);


    @Autowired
    private CloumService service;


    @GetMapping("findById")
    @ApiOperation("返回栏目数据")
    public AjaxResult findById(Cloum cloum) {

        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("code", 200);
        ajaxResult.put("data", service.findById(cloum));
        ajaxResult.put("msg", "列表获取成功");

        return ajaxResult;
    }


    @GetMapping("findCloum")
    @ApiOperation("返回栏目数据")
    public AjaxResult findCloum(Cloum cloum) {

        AjaxResult ajaxResult = new AjaxResult();
        if (cloum == null) {
            ajaxResult.put("code", 500);
            ajaxResult.put("msg", "没有获取到值");
        }

        PageHelper.startPage(cloum.getPageNum(), cloum.getPageSize());

        List<Cloum> clomuList = service.findCloum(cloum);

        PageInfo<Cloum> noticeVOPageInfo = new PageInfo<Cloum>(clomuList);


        ajaxResult.put("code", 200);
        ajaxResult.put("data", noticeVOPageInfo);
        ajaxResult.put("msg", "列表获取成功");
        return ajaxResult;
    }

    @PostMapping("addCloum")
    @ApiOperation("添加栏目")
    public AjaxResult addCloum(@RequestBody Cloum cloum) {
        if (null == cloum.getColumnName() || cloum.getColumnName().equals("")) {
            return AjaxResult.error("不能有空值");
        }
        return AjaxResult.success(service.addCloum(cloum) > 0 ? "添加成功" : "添加失败");
    }

    @PostMapping("editCloum")
    @ApiOperation("修改栏目")
    public AjaxResult editCloum(@RequestBody Cloum cloum) {
        if (null == cloum.getColumnName() || null == cloum.getColumnId() || cloum.getColumnName().equals("") || cloum.getColumnId().equals("") || cloum.getUuid() == null || cloum.getUuid().equals("")) {
            return AjaxResult.error("不能有空值");
        }
        return AjaxResult.success(service.editCloum(cloum) > 0 ? "修改成功" : "修改失败");
    }

    @GetMapping("delCloum")
    @ApiOperation("删除栏目")
    public AjaxResult delCloum(String uuid) {

        Cloum cloum = new Cloum();
        cloum.setUuid(uuid);

        if (null == cloum.getUuid() || cloum.getUuid().equalsIgnoreCase("")) {
            return AjaxResult.error("不能有空值");
        }

        AjaxResult ajaxResult = new AjaxResult();


        Integer integer = service.delCloum(cloum);
        if (integer < 1) {
            return AjaxResult.error("删除失败");
        }

        ajaxResult.put("code", 200);
        ajaxResult.put("msg", "删除成功");


        return ajaxResult;
    }


}
