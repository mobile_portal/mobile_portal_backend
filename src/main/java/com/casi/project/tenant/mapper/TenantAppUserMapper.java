package com.casi.project.tenant.mapper;

import java.util.List;
import com.casi.project.tenant.domain.TenantAppUser;

/**
 * 租户应用用户授权Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface TenantAppUserMapper 
{
    /**
     * 查询租户应用用户授权
     * 
     * @param uuid 租户应用用户授权ID
     * @return 租户应用用户授权
     */
    public TenantAppUser selectTenantAppUserById(String uuid);

    /**
     * 查询租户应用用户授权列表
     * 
     * @param tenantAppUser 租户应用用户授权
     * @return 租户应用用户授权集合
     */
    public List<TenantAppUser> selectTenantAppUserList(TenantAppUser tenantAppUser);

    /**
     * 新增租户应用用户授权
     * 
     * @param tenantAppUser 租户应用用户授权
     * @return 结果
     */
    public int insertTenantAppUser(TenantAppUser tenantAppUser);

    /**
     * 修改租户应用用户授权
     * 
     * @param tenantAppUser 租户应用用户授权
     * @return 结果
     */
    public int updateTenantAppUser(TenantAppUser tenantAppUser);

    /**
     * 删除租户应用用户授权
     * 
     * @param uuid 租户应用用户授权ID
     * @return 结果
     */
    public int deleteTenantAppUserById(String uuid);

    /**
     * 批量删除租户应用用户授权
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTenantAppUserByIds(String[] uuids);
}
