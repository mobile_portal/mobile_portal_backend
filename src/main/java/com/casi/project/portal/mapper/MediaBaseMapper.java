package com.casi.project.portal.mapper;


import com.casi.project.portal.domain.MediaBase;
import com.casi.project.portal.domain.vo.MediaBaseForFileVo;
import com.casi.project.portal.domain.vo.MediaBaseForNewsVo;
import com.casi.project.portal.domain.vo.MediaBaseForWebVo;

import java.util.List;

/**
 * @Description:素材信息mapper
 */
public interface MediaBaseMapper {
    int insert(MediaBase mediaBase);

    int update(MediaBase mediaBase);

    int delete(Long id);

    MediaBase queryById(Long id);

    List<MediaBase> queryList(MediaBase mediaBase);

    List<MediaBaseForNewsVo> queryListForNews(MediaBase mediaBase);

    List<MediaBaseForWebVo> queryListForWeb(MediaBase mediaBase);

    List<MediaBaseForFileVo> queryListForFile(MediaBase mediaBase);
}
