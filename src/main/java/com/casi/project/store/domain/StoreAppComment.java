package com.casi.project.store.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 应用评论对象 store_app_comment
 * 
 * @author casi
 * @date 2020-06-02
 */
public class StoreAppComment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private String appId;

    /** 评分 */
    @Excel(name = "评分")
    private Integer rate;

    /** 评论 */
    private String content;

    /** 评论人编号 */
    @Excel(name = "评论人编号")
    private String tenantUserId;

    /** 状态 */
    private String status;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setRate(Integer rate) 
    {
        this.rate = rate;
    }

    public Integer getRate() 
    {
        return rate;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setTenantUserId(String tenantUserId) 
    {
        this.tenantUserId = tenantUserId;
    }

    public String getTenantUserId() 
    {
        return tenantUserId;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("appId", getAppId())
            .append("rate", getRate())
            .append("content", getContent())
            .append("tenantUserId", getTenantUserId())
            .append("createTime", getCreateTime())
            .append("status", getStatus())
            .toString();
    }
}
