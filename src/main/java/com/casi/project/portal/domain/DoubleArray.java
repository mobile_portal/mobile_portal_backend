package com.casi.project.portal.domain;

import lombok.Data;

/**
 * @author css
 * @date 2020/5/24
 */
@Data
public class DoubleArray {
    private String[] applyIds;
    private String[] userIds;
    private String[] deptIds;
}
