package com.casi.project.system.domain;

import java.util.List;

public class RoleIds {

    private List<String> roleIds;

    public List<String> getRoleIds() {
        return roleIds;
    }

    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }

    @Override
    public String toString() {
        return "RoleIds{" +
                "roleIds=" + roleIds +
                '}';
    }
}
