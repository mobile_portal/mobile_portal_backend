package com.casi.project.portal.service;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.DoubleArray;

/**
 * 应用与部门关联表
 */
public interface MobileApplyDeptsService {
    /**
     * 添加应用与部门的关联关系
     */
    public AjaxResult addApplyIdsAndDeptIds(String[] applyIds, String[] deptIds);

    /**
     * 回显应用已授权的组织机构用于回显
     *
     * @param doubleArray
     * @return
     */
    AjaxResult findApplyIdsAndDeptIds(DoubleArray doubleArray);
}
