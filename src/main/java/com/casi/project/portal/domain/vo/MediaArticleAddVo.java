package com.casi.project.portal.domain.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MediaArticleAddVo {


    /**
     * 作者
     */

    private String author;

    /**
     * 图文id
     */
    private Long id;
    /**
     * 图文标题
     */
    private String title;
    /**
     * 文件id
     */
    private Long fileId;




    /**
     * 摘要
     */
    private String digest;
    /**
     * 图文具体内容
     */
    private String content;
    /**
     * 是否显示封面（1：显示  0：不显示）
     */
    private String showCoverPic;
    /**
     * 新闻跳转链接
     */
    private String newsUrl;





}
