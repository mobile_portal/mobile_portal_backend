package com.casi.project.portal.mapper;


import com.casi.project.portal.domain.MaterialRecord;

import java.util.List;

/**
 * @Description:素材信息mapper
 */
public interface MaterialRecordMapper {
    int insert(MaterialRecord materialRecord);

    MaterialRecord queryById(Long id);

    List<MaterialRecord> queryList(MaterialRecord materialRecord);

}
