package com.casi.project.portal.service.impl;

import com.casi.common.constant.Constants;
import com.casi.common.utils.JsonUtils;
import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.ServletUtils;
import com.casi.common.utils.StringUtils;
import com.casi.common.utils.http.HttpUtils;
import com.casi.framework.redis.RedisCache;
import com.casi.framework.security.LoginUser;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.controller.IndexApiController;
import com.casi.project.portal.domain.*;
import com.casi.project.portal.domain.vo.NoticeVO;
import com.casi.project.portal.mapper.IndexApiMapper;
import com.casi.project.portal.mapper.MediaArticleMapper;
import com.casi.project.portal.service.IndexApiService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Auther shp
 * @data2020/4/1510:34
 * @description
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class IndexApiServiceImpl implements IndexApiService {
    private static Logger logger = LoggerFactory.getLogger(IndexApiServiceImpl.class);


    @Autowired
    private IndexApiMapper mapper;

    @Autowired
    private TokenService tokenService;
    @Autowired
    private MediaArticleMapper mediaArticleMapper;

    @Autowired
    private RedisCache redisCache;


    @Override
    public List<MobileHomeMenu> getAllMenu() {
        List<MobileHomeMenu> list = new ArrayList<>();
        List<MobileHomeMenuDetails> detailslist = new ArrayList<>();
        List<MobileHomeMenu> mobileHomeMenus = getMenuList();
        for (MobileHomeMenu mobileHomeMenu : mobileHomeMenus) {
            List<MobileHomeMenuDetails> myAppMenuDetails = mapper.getAddMyAppMenuInIndex();
            List<MobileHomeMenuDetails> menuDetails = findById("3");
            for (MobileHomeMenuDetails myAppMenuDetail : myAppMenuDetails) {
                detailslist.add(myAppMenuDetail);
            }
            for (MobileHomeMenuDetails myAppMenuDetail : menuDetails) {
                detailslist.add(myAppMenuDetail);
            }
            if ("我的应用".equals(mobileHomeMenu.getMenuName())) {

                if (StringUtils.isNotEmpty(detailslist)) {
                    for (MobileHomeMenuDetails arr : detailslist) {
                        if (arr != null) {
                            if (!"更多".equals(arr.getMenuButtonName()) && !"添加".equals(arr.getMenuButtonName()))
                                arr.setMenuButtonUrl(lastPath(arr.getMenuButtonUrl()));
                        }
                        mobileHomeMenu.getArrs().add(arr);
                    }
                }

            } else {
                List<MobileHomeMenuDetails> mobileHomeMenuDetails = findById(mobileHomeMenu.getId());
                if (StringUtils.isNotEmpty(mobileHomeMenuDetails)) {
                    for (MobileHomeMenuDetails arr : mobileHomeMenuDetails) {
                        if (arr != null) {
                            //截取掉图标地址的拼接的地址
                            if (!"更多".equals(arr.getMenuButtonName()) && !"添加".equals(arr.getMenuButtonName()))
                                arr.setMenuButtonUrl(lastPath(arr.getMenuButtonUrl()));
                            //拼接应用跳转的后缀
                            //  mobileHomeMenuDetails.setMenuButtonIcon(arr.getMenuButtonIcon().replace(Constants.BASE_IMG_URL,""));
                            //     mobileHomeMenuDetails.add(arr);
                        }
                    }
                }
                mobileHomeMenu.setArrs(mobileHomeMenuDetails);
            }
            list.add(mobileHomeMenu);

        }
        return list;
    }


    // 获取所有应用菜单

    public List<MobileHomeMenu> getMenuList() {
        return mapper.findAll();
    }

    //  根据菜单id应用详情
    public List<MobileHomeMenuDetails> findById(String id) {
        return mapper.findById(id);
    }


    public List<MobileHomeMenuDetails> findMyAppMenuList(String id) {
        return mapper.findMyAppMenuList(id);
    }

    @Override
    public List<MobileHomeMenu> getAddMyAppMenu() {
        List<MobileHomeMenu> list = new ArrayList<>();
        List<MobileHomeMenu> mobileHomeMenus = mapper.getAddMyAppMenu();
        for (MobileHomeMenu mobileHomeMenu : mobileHomeMenus) {
            List<MobileHomeMenuDetails> mobileHomeMenuDetails = findMyAppMenuList(mobileHomeMenu.getId());
            if (StringUtils.isNotEmpty(mobileHomeMenuDetails)) {
                for (MobileHomeMenuDetails arr : mobileHomeMenuDetails) {
                    if (arr != null) {
                        //截取掉图标地址的拼接的地址
                        arr.setMenuButtonUrl(lastPath(arr.getMenuButtonUrl()));
                    }
                }
            }
            mobileHomeMenu.setArrs(mobileHomeMenuDetails);
            list.add(mobileHomeMenu);
        }
        return list;
    }


    @Override
    public List<NoticeVO> findNotice() {
        return mapper.findNotice();
    }

    @Override
    public List<MobileHomeLb> findMobileHomeLb() {

        List<MobileHomeLb> mobileHomeLb = mapper.findMobileHomeLb();
        for (MobileHomeLb data : mobileHomeLb){

            data.setLbAppUrl(lastPath(data.getLbAppUrl()));
            data.setLbUrl(Constants.BASE_IMG_URL+data.getLbUrl());
        }
        return mobileHomeLb;
    }


    @Override
    public List<MoblieNews> getNews() {
        return mapper.findNews();
    }

    /**
     * 移动端新闻资讯的展示
     */
    @Override
    public List<MediaArticle> queryNewsList(String limit) {
        List<MediaArticle> mediaArticles = mediaArticleMapper.selectAll(Integer.valueOf(limit));
        ArrayList<MediaArticle> list = new ArrayList<>();
        for (MediaArticle mediaArticle : mediaArticles) {
            mediaArticle.setPicUrl(Constants.BASE_IMG_URL + mediaArticle.getPicUrl());
            list.add(mediaArticle);
        }
        return list;
    }

    @Override
    public List<MobileHomeMenuDetails> getMoreApply(Long typeId) {
        return mapper.getMoreApply(typeId);
    }

    @Override
    public List<MobileHomeMenuDetails> searchApp(String appName) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        //拼接%% 模糊查询
        Map<String,Object> params = new HashMap<>();
        params.put("menuName",appName);
        params.put("userId",loginUser.getUser().getUserId());
        List<MobileHomeMenuDetails> list = mapper.searchApp(params);
        if(null != list && list.size() > 0){
            for (MobileHomeMenuDetails arr : list) {
                if (!"更多".equals(arr.getMenuButtonName()) && !"添加".equals(arr.getMenuButtonName())&&!"开发者论坛".equals(arr.getMenuButtonName()))
                    arr.setMenuButtonUrl(lastPath(arr.getMenuButtonUrl()));
                if("开发者论坛".equals(arr.getMenuButtonName())){
                    arr.setMenuButtonUrl(arr.getMenuButtonUrl()+"?mod=logging&action=login&loginsubmit=yes&infloat=yes&lssubmit=yes&fastloginfield=username&username="+loginUser.getUsername()+"&password=1qaz@WSX&quickforward=yes&handlekey=ls");
                }
            }

        }
        return list;
    }


    @Override
    public Long hidden(Long id) {
        return mapper.hidden(id);
    }

    @Override
    public List<MobileHomeMenuDetails> findAddApp(Long pid) {

        return mapper.findAddApp(pid, tokenService.getLoginUser(ServletUtils.getRequest()).getUser().getUserId());
    }


    @Override
    public AjaxResult show(String id) {

        String menuIdByDetailsId = mapper.findMenuIdByDetailsId(id);
        String menuBypeByMenuId = mapper.findMenuBypeByMenuId(menuIdByDetailsId);
        if("1".equals(menuBypeByMenuId)){
            int detailsCount = mapper.findDetailsCount(SecurityUtils.getLoginUser().getUser().getUserId());
            if (detailsCount >= 11){
                return AjaxResult.error("没有空间添加应用,请先移除");
            }

        }
        if("0".equals(menuBypeByMenuId)){
            int groupDetailsCount = mapper.findGroupDetailsCount(SecurityUtils.getLoginUser().getUser().getUserId());
            if (groupDetailsCount >= 12){
                return AjaxResult.error("没有空间添加应用,请先移除");
            }
        }

        int show = mapper.show(id);
        if (show > 0 ){
            return AjaxResult.success();
        }else {
            return AjaxResult.error();
        }

    }


    public String lastPath(String url) {

        if (null == url || url.equals("")) {
            return "";
        }

        String path = "?";

        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());

        if (loginUser.getUser() != null) {

            if (url.indexOf("?") != -1) {
                path = "&";
            }

            path = url + path;
            path = path + "userName=" + loginUser.getUser().getUserName() + "&";
            path = path + "userNo=" + loginUser.getUser().getUserCode() + "&";
            path = path + "deptN0=" + loginUser.getUser().getDeptId() + "&";
            path = path + "casic_app_id_token="+loginUser.getToken();
        } else {
            return "";
        }
        return path;
    }
    /**
     * 移动端新闻资讯详情接口
     */
    @Override
    public MediaArticle queryNewsById(Long id) {
        System.out.println("移动端新闻资讯详情接口"+id);
        return mediaArticleMapper.selectMediaArticById(id);
    }


    @Override
    public List<MobileHomeMenuDetails> getMyApp(String show) {
        List<MobileHomeMenuDetails> rul = new ArrayList<>();
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());



        String username = loginUser.getUsername();
        String token = String.valueOf(redisCache.getCacheObject(username + "_token"));
        String param = "appKey=88E99021AB024AFEB4BC9DFAD6F27790"+"&appSecert=93DC42F5E6EB4FCA8659210E76476849"+"&token="+token ;
        try {
            String s = HttpUtils.sendPost(Constants.HAS_BEEN_USED_URL, param);
            Map<Object, Object> map = JsonUtils.stringToCollect(s);
            if("200".equalsIgnoreCase(String.valueOf(map.get("code")))){
                List<Map<String,Object>> appList = (List<Map<String, Object>>) map.get("data");
                if(appList != null &&appList.size()>0){
                    for (Map<String,Object> map1 : appList){
                        MobileHomeMenuDetails details = new MobileHomeMenuDetails();
                        details.setId(String.valueOf(map1.get("appCode")));
                        details.setMenuButtonName(String.valueOf(map1.get("appName")));
                        details.setMenuButtonIcon(String.valueOf(map1.get("appMaxIcon")));
                        details.setMenuButtonUrl(lastPath(String.valueOf(map1.get("appUrl"))));
                        details.setButtonParentId(Long.valueOf(String.valueOf(map1.get("appSortCode"))));
                        details.setGroupName(String.valueOf(map1.get("appSortName")));
                        details.setIsIfream(0);
                        rul.add(details);
                    }
                }
            }
        /*
        String param = "username="+loginUser.getUsername()+"&casic_app_id_token="+Constants.CASIC_APP_ID_TOKEN;
        try {
            String s = HttpUtils.sendGet(Constants.HAS_BEEN_USED_URL, param);
            Map<Object, Object> map = JsonUtils.stringToCollect(s);
            if("200".equalsIgnoreCase(String.valueOf(map.get("code")))){
                List<Map<String,Object>> appList = (List<Map<String, Object>>) map.get("data");
                if(appList != null &&appList.size()>0){
                    for (Map<String,Object> map1 : appList){
                        MobileHomeMenuDetails details = new MobileHomeMenuDetails();
                        details.setId(String.valueOf(map1.get("appCode")));
                        details.setMenuButtonName(String.valueOf(map1.get("appName")));
                        details.setMenuButtonIcon(String.valueOf(map1.get("appMaxIcon")));
                        details.setMenuButtonUrl(lastPath(String.valueOf(map1.get("appUrl"))));
                        details.setButtonParentId(Long.valueOf(String.valueOf(map1.get("appSortCode"))));
                        details.setGroupName(String.valueOf(map1.get("appSortName")));
                        details.setIsIfream(0);
                        rul.add(details);
                    }
                }
            }*/
        }catch (Exception e){
            logger.error("调用移动商店失败！！！");
        }

        List<MobileHomeMenuDetails> myAppMenuDetails = mapper.getGroupApp(loginUser.getUser().getUserId(),"3",show);
        for (MobileHomeMenuDetails data :myAppMenuDetails){
            if (!"0".equalsIgnoreCase(data.getMenuType())){
                data.setMenuButtonUrl(lastPath(data.getMenuButtonUrl()));
            }

        }
        rul.addAll(myAppMenuDetails);
        return rul;
    }


    @Override
    public List<MobileHomeMenuDetails> getGroupApp() {
        String userId = tokenService.getLoginUser(ServletUtils.getRequest()).getUser().getUserId();

        List<MobileHomeMenuDetails> dataList = mapper.getGroupApp(userId,"1","0");
        for (MobileHomeMenuDetails data :dataList){
            if (!"0".equalsIgnoreCase(data.getMenuType())){
                data.setMenuButtonUrl(lastPath(data.getMenuButtonUrl()));
            }

        }

        return dataList;
    }

    @Override
    public List<MobileHomeMenuDetails> getrRecommendedApp() {

        List<MobileHomeMenuDetails> rul = new ArrayList<>();
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String param = "username="+loginUser.getUsername()+"&casic_app_id_token="+Constants.CASIC_APP_ID_TOKEN;
        try {
            String s = HttpUtils.sendGet(Constants.RECOMMENDED_URL, param);
            Map<Object, Object> map = JsonUtils.stringToCollect(s);
            if("200".equalsIgnoreCase(String.valueOf(map.get("code")))){
                List<Map<String,Object>> appList = (List<Map<String, Object>>) map.get("data");
                if(appList != null &&appList.size()>0){
                    for (Map<String,Object> map1 : appList){
                        MobileHomeMenuDetails details = new MobileHomeMenuDetails();
                        details.setId(String.valueOf(map1.get("appCode")));
                        details.setMenuButtonName(String.valueOf(map1.get("appName")));
                        details.setMenuButtonIcon(String.valueOf(map1.get("appMaxIcon")));
                        details.setMenuButtonUrl(lastPath(String.valueOf(map1.get("appUrl"))));
                        details.setButtonParentId(Long.valueOf(String.valueOf(map1.get("appSortCode"))));
                        details.setIsIfream(0);
                        rul.add(details);
                    }
                }
            }
        }catch (Exception e){
            logger.error("调用移动商店失败！！！");
        }

        List<MobileHomeMenuDetails> dataList = mapper.getGroupApp(loginUser.getUser().getUserId(),"2","0");
        if (dataList != null && dataList.size()>0){
            for (MobileHomeMenuDetails data :dataList){
                if (!"0".equalsIgnoreCase(data.getMenuType())){
                    data.setMenuButtonUrl(lastPath(data.getMenuButtonUrl()));
                }
            }
            rul.addAll(dataList);
        }


        return rul;
    }


    @Override
    public List<MobileHomeMenuDetails> getAppDataListByParams(Map<String, Object> params) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        String menuPid = String.valueOf(params.get("pid"));//分组id
        String isInterface = String.valueOf(params.get("isInterface"));//是否调用接口  1 是 0 否
        String isShow = String.valueOf(params.get("isShow"));//是否调用接口  1 是 0 否
        params.put("userId",loginUser.getUser().getUserId());
        List<MobileHomeMenuDetails> rul = new ArrayList<>();
        if("1".equals(isInterface)){
            String param = "username="+loginUser.getUsername()+"&casic_app_id_token="+Constants.CASIC_APP_ID_TOKEN;
            String url = "";
            if ("3".equals(menuPid))//我的应用
                url = Constants.HAS_BEEN_USED_URL;
            else if ("2".equals(menuPid))//移动商店
                url = Constants.HAS_BEEN_USED_URL;
            try {
                String s = HttpUtils.sendGet(url, param);
                Map<Object, Object> map = JsonUtils.stringToCollect(s);
                if("200".equalsIgnoreCase(String.valueOf(map.get("code")))){
                    List<Map<String,Object>> appList = (List<Map<String, Object>>) map.get("data");
                    if(appList != null &&appList.size()>0){
                        for (Map<String,Object> map1 : appList){
                            map1.put("userId",loginUser.getUser().getUserId());
                            map1.put("isShow",isShow);
                            int no = mapper.getInterfaceAppShow(map1);
                            if (no<1){
                                continue;
                            }

                            MobileHomeMenuDetails details = new MobileHomeMenuDetails();
                            details.setId(String.valueOf(map1.get("appCode")));
                            details.setMenuButtonName(String.valueOf(map1.get("appName")));
                            details.setMenuButtonIcon(String.valueOf(map1.get("appMaxIcon")));
                            details.setMenuButtonUrl(lastPath(String.valueOf(map1.get("appUrl"))));
                            details.setButtonParentId(Long.valueOf(String.valueOf(map1.get("appSortCode"))));
                            details.setGroupName(String.valueOf(map1.get("appSortName")));
                            details.setIsIfream(0);
                            rul.add(details);
                        }
                    }
                }
            }catch (Exception e){
                logger.error("调用移动商店失败！！！");
            }

        }
        List<MobileHomeMenuDetails> dataList = mapper.getAppDataListByParams(params);

        if (dataList != null && dataList.size()>0){
            for (MobileHomeMenuDetails data :dataList){
                if (!"0".equalsIgnoreCase(data.getMenuType())){
                    if(!"更多".equals(data.getMenuButtonName()) && !"添加".equals(data.getMenuButtonName())&&!"开发者论坛".equals(data.getMenuButtonName())){
                        data.setMenuButtonUrl(lastPath(data.getMenuButtonUrl()));
                    }
                    if("开发者论坛".equals(data.getMenuButtonName())){
                        data.setMenuButtonUrl(data.getMenuButtonUrl()+"?mod=logging&action=login&loginsubmit=yes&infloat=yes&lssubmit=yes&fastloginfield=username&username="+loginUser.getUsername()+"&password=1qaz@WSX&quickforward=yes&handlekey=ls");
                    }
                }
            }
            rul.addAll(dataList);
        }

        return rul;
    }


    @Override
    @Transactional
    public AjaxResult updateMenuShow(Map<String, Object> params) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        params.put("userId",loginUser.getUser().getUserId());

        if("0".equals(String.valueOf(params.get("type")))){
            //删除首页应用
            mapper.updateMenuShow(params);
        }else{
            //增加应用
            //判断是否已经满额
            int count = mapper.getMyShowAppCount(params);
            if (count>11)
                return AjaxResult.error("应用已满，请先删除！");

            int i = mapper.updateMenuShow(params);
            if (i <1 )
                mapper.insertData(params);

        }

        return AjaxResult.success();
    }


    @Override
    public List<Map<String, Object>> getMyAppDetiles(Map<String, Object> params) {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        params.put("userId",loginUser.getUser().getUserId());
        List<Map<String,Object>> rul = new ArrayList<>();
        List<MobileHomeMenuDetails> dataList = mapper.getMyAppDetiles(params);
        Map<String, List<MobileHomeMenuDetails>> map = new HashMap<>();
        if(dataList != null && dataList.size()>0){
            for(MobileHomeMenuDetails details : dataList){
                if ("更多".equals(details.getMenuButtonName()) || "添加".equals(details.getMenuButtonName()))

                    continue;
                if(map.containsKey(details.getGroupName())){//map中存在此id，将数据存放当前key的map中
                    map.get(details.getGroupName()).add(details);
                }else{//map中不存在，新建key，用来存放数据
                    List<MobileHomeMenuDetails> tmpList = new ArrayList<>();
                    tmpList.add(details);
                    map.put(details.getGroupName(), tmpList);
                }
            }
        }

        for (Map.Entry<String, List<MobileHomeMenuDetails>> entry : map.entrySet()) {
            Map<String,Object> maps = new HashMap<>();
            maps.put("name",entry.getKey());
            maps.put("data",entry.getValue());
            rul.add(maps);
        }
        List<Map<String,Object>> yRulData = new ArrayList<>();



       try {
            String param = "username="+loginUser.getUsername()+"&casic_app_id_token="+Constants.CASIC_APP_ID_TOKEN;
            String s = HttpUtils.sendGet(Constants.HAS_BEEN_USED_URL, param);
            Map<Object, Object> maps = JsonUtils.stringToCollect(s);
            if("200".equalsIgnoreCase(String.valueOf(map.get("code")))){
                List<Map<String,Object>> appList = (List<Map<String, Object>>) maps.get("data");
                if(appList != null &&appList.size()>0){
                    for (Map<String,Object> map1 : appList){
                        map1.put("userId",loginUser.getUser().getUserId());
                        map1.put("isShow","0");
                        int no = mapper.getInterfaceAppShow(map1);
                        if (no<1){
                            continue;
                        }

                        MobileHomeMenuDetails details = new MobileHomeMenuDetails();
                        details.setId(String.valueOf(map1.get("appCode")));
                        details.setMenuButtonName(String.valueOf(map1.get("appName")));
                        details.setMenuButtonIcon(String.valueOf(map1.get("appMaxIcon")));
                        details.setMenuButtonUrl(lastPath(String.valueOf(map1.get("appUrl"))));
                        details.setButtonParentId(Long.valueOf(String.valueOf(map1.get("appSortCode"))));
                        details.setGroupName(String.valueOf(map1.get("appSortName")));
                        details.setIsIfream(0);
                        yRulData.add((Map<String, Object>) details);
                    }
                }
            }
        }catch (Exception e){
            logger.error("调用移动商店失败！！！");

        }




        Map<String,Object> yMap = new HashMap<>();
        yMap.put("name","移动商店");
        yMap.put("data",yRulData);
        rul.add(yMap);
        return rul;
    }
}
