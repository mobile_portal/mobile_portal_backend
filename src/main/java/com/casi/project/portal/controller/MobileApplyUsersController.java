package com.casi.project.portal.controller;

import com.casi.common.utils.StringUtils;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.framework.web.page.TableDataInfo;
import com.casi.project.portal.domain.DoubleArray;
import com.casi.project.portal.domain.StringAndString;
import com.casi.project.portal.service.MobileApplyUsersService;
import com.casi.project.system.domain.SysUser;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Api(tags = { "应用授权管理" })
@RestController
@RequestMapping("/mobileApplyUsers")
public class MobileApplyUsersController extends BaseController {

    @Autowired
    private MobileApplyUsersService mobileApplyUsersService;

    /**
     * 应用指定开发账户（应用授权给用户）
     */
    @ApiOperation(value = "应用授权用户")
    @Log(title = "应用授权用户", businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermi('apply:authorization:list')")
    @PostMapping("/addApplyIdsAndUserIds")
    public AjaxResult addApplyIdsAndUserIds(@RequestBody DoubleArray doubleArray) {
        return mobileApplyUsersService.addApplyIdsAndUserIds(doubleArray.getApplyIds(), doubleArray.getUserIds());
    }

    /**
     * 回显已经授权的用户
     *
     * @param doubleArray
     * @return
     */
    @ApiOperation(value = "回显已经授权的用户")
    @PostMapping("/findApplyIdsAndUserIds")
    public AjaxResult findApplyIdsAndUserIds(@RequestBody DoubleArray doubleArray) {
        return mobileApplyUsersService.findApplyIdsAndUserIds(doubleArray);
    }

    /**
     * 应用禁用用户
     */
    @ApiOperation(value = "应用禁用用户")
    @Log(title = "应用禁用", businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermi('apply:disable:list')")
    @PostMapping("/addApplyToDisabledUserIds")
    public AjaxResult addApplyToDisabledUserIds(@RequestBody StringAndString stringAndString) {
        AjaxResult ajaxResult;
        try {
            ajaxResult = mobileApplyUsersService.addApplyToDisabledUserIds(
                    stringAndString.getMobileApplyId(), stringAndString.getUserId());
        } catch (Exception e) {
            logger.error("应用禁用用户失败", e);
            return AjaxResult.error();

        }
        return ajaxResult;
    }

    /**
     * 应用解除禁用用户
     */
    @ApiOperation(value = "应用解除禁用用户")
    @Log(title = "应用禁用", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('apply:disable:list')")
    @PostMapping("/romoveApplyToDisabledUserIds")
    public AjaxResult romoveApplyToDisabledUserIds(@RequestBody StringAndString stringAndString) {
        return mobileApplyUsersService.romoveApplyToDisabledUserIds(stringAndString.getMobileApplyId(), stringAndString.getUserId());
    }

    /**
     * 应用禁用用户
     */
    @ApiOperation(value = "应用禁用用户列表")
    @PreAuthorize("@ss.hasPermi('apply:disable:list')")
    @GetMapping("/applyDisabledUserList")
    public TableDataInfo applyDisabledUserList(String applyId) {
        startPage();
        List<SysUser> sysUsers = mobileApplyUsersService.applyDisabledUserList(applyId);
        return getDataTable(sysUsers);
    }
    /**
     * 根据应用的id查询出该应用没有授权的用户信息
     */
    @ApiOperation(value = "应用未授权用户")
    @PostMapping("/queryNotImpowerUsers")
    public AjaxResult queryNotImpowerUsers(@RequestBody Map<String,Object> map){
        return mobileApplyUsersService.queryNotImpowerUsers(map);
    }
    /**
     * 根据应用的id查询已经授权的用户的信息
     */
    @ApiOperation(value = "应用已授权用户信息")
    @PostMapping("/queryImpowerUsers")
    public AjaxResult queryImpowerUsers(@RequestBody Map<String,Object> map){
        return mobileApplyUsersService.queryImpowerUsers(map);
    }
    /**
     * 根据应用的id和用户的id（用户id可为多个）   解除该应用授权的用户
     */
    @ApiOperation(value = "应用解除授权用户")
    @PostMapping("/relieveUsersImpower")
    public AjaxResult relieveUsersImpower(@RequestBody Map<String,Object> map){
        return mobileApplyUsersService.relieveUsersImpower(map);
    }
}
