package com.casi.project.portal.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther shp
 * @data2020/4/1514:58
 * @description
 */

@Data
@AllArgsConstructor
@NoArgsConstructor

public class IndexMobliVO {

    private Long pid;
    private String menuName;
    private List<IndexApiVO> arrs=new ArrayList<>();

}
