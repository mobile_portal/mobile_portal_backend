package com.casi.project.portal.service.impl;


import com.casi.common.utils.DateUtils;
import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.StringUtils;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.controller.NoticeController;
import com.casi.project.portal.domain.MobileNotice;
import com.casi.project.portal.domain.UnreadStatus;
import com.casi.project.portal.domain.vo.NoticeVO;
import com.casi.project.portal.mapper.NoticeMapper;
import com.casi.project.portal.service.NoticeService;
import com.casi.project.system.domain.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * @author 18732
 */
@Service
public class NoticeServiceImpl implements NoticeService {
    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);
    @Resource
    private NoticeMapper noticeMapper;

    @Override
    public AjaxResult getNoticeList() {
        ArrayList<MobileNotice> noticeList1;
        ArrayList<NoticeVO> noticeListVo = new ArrayList<>();
        try {
            noticeList1 = noticeMapper.getNoticeList();
            //获取登录的用户
            SysUser loginUser = SecurityUtils.getLoginUser().getUser();
            if (StringUtils.isNull(loginUser.getUserId())) {
                return AjaxResult.error("没有当前登录用户");
            }
            //获取用户的id
            String userId = loginUser.getUserId();
            //遍历集合拿出公告id
            if (StringUtils.isNotNull(noticeList1)) {
                for (int i = 0; i < noticeList1.size(); i++) {
                    MobileNotice noticeIo = noticeList1.get(i);
                    NoticeVO noticeVO = new NoticeVO();
                    String id = noticeIo.getId();
                    int rows = noticeMapper.getNoticeIsRead(id, userId);
                    if (rows > 0) {
                        noticeVO.setRead("1");
                        noticeVO.setId(noticeIo.getId());
                        noticeVO.setPushTime(DateUtils.dateTimeHMS(noticeIo.getPushTime()));
                        noticeVO.setTitle(noticeIo.getTitle());
                    } else {
                        noticeVO.setRead("0");
                        noticeVO.setId(noticeIo.getId());
                        noticeVO.setPushTime(DateUtils.dateTimeHMS(noticeIo.getPushTime()));
                        noticeVO.setTitle(noticeIo.getTitle());
                    }
                    noticeListVo.add(noticeVO);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");
        }
        return AjaxResult.success("查询成功", noticeListVo);
    }


    @Override
    public MobileNotice getNoticeId(String id) throws Exception {
        MobileNotice noticeVo = noticeMapper.getNoticeId(id);
        return noticeVo;
    }

    @Transactional
    @Override
    public AjaxResult addUnreadStatus(UnreadStatus unreadStatus) throws Exception {
        String noticeId = unreadStatus.getNoticeId();
        String userId = unreadStatus.getUserId();
        int rows = noticeMapper.getUnreadStatus(noticeId, userId);
        if (rows > 0) {
            //更新最后已读的时间
            unreadStatus.setLastTime(new Date());
            int updateUnreadStatus = noticeMapper.updateUnreadStatus(unreadStatus);
        } else {
            int row = noticeMapper.addUnreadStatus(unreadStatus);
        }
        return AjaxResult.success("添加成功");
    }

//    @Override
//    public UnreadStatus getUnreadStatus(String noticeId, String userId) throws Exception {
//        return noticeMapper.getUnreadStatus(noticeId, userId);
//    }
}
