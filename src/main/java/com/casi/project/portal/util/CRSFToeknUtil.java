package com.casi.project.portal.util;

import java.util.UUID;

public class CRSFToeknUtil {

    //生成token验证
    public static String genToken()
    {
        String uuid = UUID.randomUUID().toString();
        String tokenId = uuid.replace("-","");
        return  tokenId;
    }

}
