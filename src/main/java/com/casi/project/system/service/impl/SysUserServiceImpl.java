package com.casi.project.system.service.impl;

import com.casi.common.constant.UserConstants;
import com.casi.common.core.lang.UUID;
import com.casi.common.exception.CustomException;
import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.StringUtils;
import com.casi.framework.aspectj.lang.annotation.DataScope;
import com.casi.project.portal.domain.thirdParty.ThirdPartyLoginBody;
import com.casi.project.portal.domain.thirdParty.UserInfo;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.MobileApplyUser;
import com.casi.project.portal.mapper.MobileApplyUserMapper;
import com.casi.project.system.domain.*;
import com.casi.project.system.mapper.*;
import com.casi.project.system.service.ISysConfigService;
import com.casi.project.system.service.ISysUserService;
import com.idsmanager.dingdang.jwt.DingdangUserRetriever;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

import java.util.*;

/**
 * 用户 业务层处理
 *
 * @author lzb
 */
@Service
public class SysUserServiceImpl implements ISysUserService {
    private static final Logger log = LoggerFactory.getLogger(SysUserServiceImpl.class);

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private SysRoleMapper roleMapper;

    @Autowired
    private SysPostMapper postMapper;

    @Autowired
    private SysUserRoleMapper userRoleMapper;

    @Autowired
    private SysUserPostMapper userPostMapper;

    @Autowired
    private ISysConfigService configService;
    @Autowired
    private SysDeptMapper deptMapper;
    @Autowired
    private MobileApplyUserMapper mobileApplyUserMapper;
    @Autowired
    private SysRoleDeptMapper roleDeptMapper;

    /**
     * 根据条件分页查询用户列表
     *
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<SysUser> selectUserList(SysUser user) {
        return userMapper.selectUserList(user);
        /*List<SysUser> sysUsers = userMapper.selectUserList(user);
        SysDept sysDept = new SysDept();
        for (SysUser sysUser : sysUsers) {
            Long deptId = sysUser.getDeptId();
            sysDept = deptMapper.selectDeptById(deptId);
            if (sysDept != null) {
                String ancestors = sysDept.getAncestors();
                StringBuffer buf = new StringBuffer();
                if (ancestors != null && !"".equals(ancestors.trim()) && !ancestors.equals("0")) {
                    String[] ancestorsIds = ancestors.split(",");
                    for (int i = 1; i < ancestorsIds.length; i++) {
                        if (i < ancestorsIds.length - 1) {
                            buf.append(deptMapper.selectDeptById(Long.valueOf(ancestorsIds[i])).getDeptName());
                            buf.append("/");
                        } else {
                            buf.append(deptMapper.selectDeptById(Long.valueOf(ancestorsIds[i])).getDeptName());
                        }
                    }
                    buf.append(sysDept.getDeptName());
                    String deptName = buf.toString();
                    sysDept.setDeptName(deptName);
                }
                sysUser.setDept(sysDept);
            }

        }

        return sysUsers;*/
    }



    @Override
    public List<SysUser> noDisabledList(String deptId,String applyId) {
        List<SysUser> sysUsers = userMapper.noDisabledList(deptId,applyId);
        List<SysUser> users = new ArrayList<>();
        if(StringUtils.isNotEmpty(sysUsers)){
            for (SysUser sysUser : sysUsers) {
                if (sysUser != null) {
                    SysDept sysDept = deptMapper.selectDeptById(sysUser.getDeptId());
                    if (sysDept != null) {
                        sysUser.setDeptName(sysDept.getDeptName());
                    } else {
                        sysUser.setDeptName("");
                    }
                }
                users.add(sysUser);
            }
        }

        return users;
    }

    @Override
    @DataScope(deptAlias = "d", userAlias = "u")
    public List<SysUser> selectUserList(String[] userIds) {
        return  userMapper.selectUserByIds(userIds);

        /*List<SysUser> sysUsers = userMapper.selectUserByIds(userIds);
        SysDept sysDept = new SysDept();
        for (SysUser sysUser : sysUsers) {
            Long deptId = sysUser.getDeptId();
            sysDept = deptMapper.selectDeptById(deptId);
            if (sysDept != null) {
                String ancestors = sysDept.getAncestors();
                StringBuffer buf = new StringBuffer();
                if (ancestors != null && !"".equals(ancestors.trim()) && !ancestors.equals("0")) {
                    String[] ancestorsIds = ancestors.split(",");
                    for (int i = 1; i < ancestorsIds.length; i++) {
                        if (i < ancestorsIds.length - 1) {
                            buf.append(deptMapper.selectDeptById(Long.valueOf(ancestorsIds[i])).getDeptName());
                            buf.append("/");
                        } else {
                            buf.append(deptMapper.selectDeptById(Long.valueOf(ancestorsIds[i])).getDeptName());
                        }
                    }
                    buf.append(sysDept.getDeptName());
                    String deptName = buf.toString();
                    sysDept.setDeptName(deptName);
                }
                sysUser.setDept(sysDept);
            }

        }

        return sysUsers;*/
    }

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserByUserName(String userName) {
        return userMapper.selectUserByUserName(userName);
    }

    /**
     * 通过用户ID查询用户
     *
     * @param userId 用户ID
     * @return 用户对象信息
     */
    @Override
    public SysUser selectUserById(String userId) {
        return userMapper.selectUserById(userId);
    }

    /**
     * 查询用户所属角色组
     *
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public String selectUserRoleGroup(String userName) {
        List<SysRole> list = roleMapper.selectRolesByUserName(userName);
        StringBuffer idsStr = new StringBuffer();
        for (SysRole role : list) {
            idsStr.append(role.getRoleName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString())) {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 查询用户所属岗位组
     *
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public String selectUserPostGroup(String userName) {
        List<SysPost> list = postMapper.selectPostsByUserName(userName);
        StringBuffer idsStr = new StringBuffer();
        for (SysPost post : list) {
            idsStr.append(post.getPostName()).append(",");
        }
        if (StringUtils.isNotEmpty(idsStr.toString())) {
            return idsStr.substring(0, idsStr.length() - 1);
        }
        return idsStr.toString();
    }

    /**
     * 校验用户名称是否唯一
     *
     * @param userName 用户名称
     * @return 结果
     */
    @Override
    public String checkUserNameUnique(String userName) {
        int count = userMapper.checkUserNameUnique(userName);
        if (count > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


    /**
     * 校验用户名称编码
     *
     * @param userName 用户编码
     * @return 结果
     */
    @Override
    public String checkUserCodeUnique(String userCode) {
        int count = userMapper.checkUserCodeUnique(userCode);
        if (count > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public String checkPhoneUnique(SysUser user) {
        String userId = StringUtils.isNull(user.getUserId()) ? "-1" : user.getUserId();
        SysUser info = userMapper.checkPhoneUnique(user.getPhonenumber());
        if (StringUtils.isNotNull(info) && !info.getUserId().equals(userId)) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return
     */
    @Override
    public String checkEmailUnique(String email) {

        int count = userMapper.checkEmailUnique(email);
        if (count > 0) {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;

    }

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    public void checkUserAllowed(SysUser user) {
        if (StringUtils.isNotNull(user.getUserId()) && user.isAdmin()) {
            throw new CustomException("不允许操作超级管理员用户");
        }

    }

    public void checkUserAllowed(String userId) {
        SysUser user = userMapper.selectUserById(userId);
        if (StringUtils.isNotNull(user.getUserId()) && user.isAdmin()) {
            throw new CustomException("不允许操作超级管理员用户");
        }

    }

    /**
     * 新增保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertUser(SysUser user) {
        user.setUserId(UUID.randomUUID().toString().replaceAll("-", ""));
        // 新增用户信息
        int rows = userMapper.insertUser(user);
        // 新增用户岗位关联
          // insertUserPost(user);
        // 新增用户与角色管理
          insertUserRole(user);
        return rows;
    }

    /**
     * 修改保存用户信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateUser(SysUser user) {
        String userId = user.getUserId();
        // 删除用户与角色关联
        userRoleMapper.deleteUserRoleByUserId(userId);
        // 新增用户与角色管理
        insertUserRole(user);
        // 删除用户与岗位关联
        //  userPostMapper.deleteUserPostByUserId(userId);
        // 新增用户与岗位管理
        //  insertUserPost(user);
        return userMapper.updateUser(user);
    }

    /**
     * 修改用户状态
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserStatus(SysUser user) {
        return userMapper.updateUser(user);
    }

    /**
     * 锁定状态更改
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserIsLock(SysUser user) {
        return userMapper.updateUserIsLock(user);
    }

    /**
     * 修改用户基本信息
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int updateUserProfile(SysUser user) {
        return userMapper.updateUser(user);
    }

    /**
     * 修改用户头像
     * <p>
     * 用户IDs
     *
     * @param avatar 头像地址
     * @return 结果
     */
    public boolean updateUserAvatar(String userName, String avatar) {
        return userMapper.updateUserAvatar(userName, avatar) > 0;
    }

    /**
     * 重置用户密码
     *
     * @param user 用户信息
     * @return 结果
     */
    @Override
    public int resetPwd(SysUser user) {
        return userMapper.updateUser(user);
    }

    /**
     * 重置用户密码
     *
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    @Override
    public int resetUserPwd(String userName, String password) {
        return userMapper.resetUserPwd(userName, password);
    }

    /**
     * 新增用户角色信息
     *
     * @param user 用户对象
     */
    public void insertUserRole(SysUser user) {
        String[] roles = user.getRoleIds();
        if (StringUtils.isNotNull(roles)) {
            // 新增用户与角色管理
            List<SysUserRole> list = new ArrayList<SysUserRole>();
            for (String roleId : roles) {
                SysUserRole ur = new SysUserRole();
                ur.setUserId(user.getUserId());
                ur.setRoleId(roleId);
                list.add(ur);
            }
            if (list.size() > 0) {
                userRoleMapper.batchUserRole(list);
            }
        }
    }

    /**
     * 新增用户岗位信息
     *
     * @param user 用户对象
     */
    public void insertUserPost(SysUser user) {
        String[] posts = user.getPostIds();
        if (StringUtils.isNotNull(posts)) {
            // 新增用户与岗位管理
            List<SysUserPost> list = new ArrayList<SysUserPost>();
            for (String postId : posts) {
                SysUserPost up = new SysUserPost();
                up.setUserId(user.getUserId());
                up.setPostId(postId);
                list.add(up);
            }
            if (list.size() > 0) {
                userPostMapper.batchUserPost(list);
            }
        }
    }

    /**
     * 通过用户ID删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    @Override
    public int deleteUserById(String userId) {
        // 删除用户与角色关联
        userRoleMapper.deleteUserRoleByUserId(userId);
        // 删除用户与岗位表
       // userPostMapper.deleteUserPostByUserId(userId);
        return userMapper.deleteUserById(userId);
    }

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteUserByIds(String[] userIds) {
        for (String userId : userIds) {
            checkUserAllowed(new SysUser(userId));
        }
        return userMapper.deleteUserByIds(userIds);
    }

    /**
     * 导入用户数据
     *
     * @param userList        用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    @Override
    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(userList) || userList.size() == 0) {
            throw new CustomException("导入用户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        String password = configService.selectConfigByKey("sys.user.initPassword");
        tar:
        for (SysUser user : userList) {
            try {
                if (user.getNickName() == null || user.getNickName().trim().equals("") || user.getUserName() == null || user.getUserName().trim().equals("")) {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、用户账号和姓名均不能为空");
                    continue tar;
                }
                // 验证是否存在这个用户
                SysUser u = userMapper.selectUserByUserName(user.getUserName());
                if (StringUtils.isNull(u)) {
                    user.setPassword(SecurityUtils.encryptPassword(password));
                    user.setCreateBy(operName);
                    this.insertUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 导入成功");
                } else if (isUpdateSupport) {
                    user.setUpdateBy(operName);
                    this.updateUser(user);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + user.getUserName() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + user.getUserName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + user.getUserName() + " 导入失败：";
                //failureMsg.append(msg + e.getMessage());
                failureMsg.append(msg);
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new CustomException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

    /**
     * 离职状态更改
     */
    @Override
    public int resignation(SysUser user) {
        return userMapper.resignation(user);
    }

    /**
     * 添加用户信息（数据同步）
     * 430	用户已经存在
     * 400	参数异常
     * 0    成功
     */
    @Transactional
    @Override
    public AjaxResult insertSysUser(SysUserJson sysUserJson) {
        SysUser sysUser = new SysUser();
        sysUser.setUserId(sysUserJson.getUuid());
        sysUser.setCreateTime(sysUserJson.getCreateTime());
        if(sysUserJson.getArchived() == true ){
            sysUser.setDelFlag("2");
        }else if(sysUserJson.getArchived() == false){
            sysUser.setDelFlag("0");
        }else{
            return AjaxResult.error("错误了");
        }
        sysUser.setUserName(sysUserJson.getUsername());
        sysUser.setPhonenumber(sysUserJson.getPhoneNumber());
        if(sysUserJson.getLocked()==true){
            sysUser.setIsLock("1");
        }else if(sysUserJson.getLocked()==false){
            sysUser.setIsLock("0");
        }else{
            return AjaxResult.error("错误了");
        }
        sysUser.setEmail(sysUserJson.getEmail());
        sysUser.setNickName(sysUserJson.getDisplayName());

        int i = userMapper.insertUser(sysUser);
        if(i<0){
            return AjaxResult.error("插入失败");
        }
        return AjaxResult.success("插入成功");
        /*if (sysUserJson == null) {
            return AjaxResult.error(400, "参数异常");
        }
        if (sysUserJson.getUserName() == null || sysUserJson.getUserName().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        if (sysUserJson.getPassword() == null || sysUserJson.getPassword().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        if (sysUserJson.getDisplayName() == null || sysUserJson.getDisplayName().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        if (sysUserJson.getId() == null || sysUserJson.getId().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        if (sysUserJson.getExternalId() == null || sysUserJson.getExternalId().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        //根据用户名查询userName
        SysUser sysUser = userMapper.selectByUserName(sysUserJson.getUserName());
        if (sysUser != null) {
            return AjaxResult.error("430", "用户已存在");
        }
        List<Belongs> belongs = sysUserJson.getBelongs();
        if (belongs == null) {
            return AjaxResult.error(400, "参数异常");
        }
        String b = "";
        String b1 = "";
        String b2 = "";
        for (Belongs belong : belongs) {

            if (belong.getOuDirectory() != null || !belong.getOuDirectory().trim().isEmpty()) {
                b += belong.getOuDirectory() + ",";
            }
            if (belong.getBelongOuUuid() != null || !belong.getBelongOuUuid().trim().isEmpty()) {
                b1 += belong.getBelongOuUuid() + ",";
            }
            if (belong.getRootNode() != null || !belong.getRootNode().trim().isEmpty()) {
                if (belong.getRootNode() == "false") {
                    b2 += 1 + ",";
                } else {
                    b2 += 2 + ",";
                }
            }
        }
        String ouDirectory = b.substring(0, b.length() - 1);
        String belongOuUuid = b1.substring(0, b1.length() - 1);
        String rootNode = b2.substring(0, b2.length() - 1);
        SysDept sysDept = new SysDept();
        sysDept.setOuDirectory(ouDirectory);
        sysDept.setBelongOuUuid(belongOuUuid);
        sysDept.setRootNode(rootNode);
        sysDept.setUid(sysUserJson.getId());
        int i1 = deptMapper.insertDept(sysDept);
        if (i1 != 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return AjaxResult.error(-1, "用户数据同步失败");
        }
        List<Emails> emails = sysUserJson.getEmails();
        if (emails == null || emails.size() < 1) {
            return AjaxResult.error(400, "参数异常");
        }
        String s = "";
        for (Emails email : emails) {
            s += email.getValue() + ",";
        }
        String s1 = s.substring(0, s.length() - 1);
        List<PhoneNumbers> phoneNumbers = sysUserJson.getPhoneNumbers();
        if (phoneNumbers == null || phoneNumbers.size() < 1) {
            return AjaxResult.error(400, "参数异常");
        }
        String s2 = "";
        for (PhoneNumbers phoneNumber : phoneNumbers) {
            if (phoneNumber.getValue().length() != 11) {
                return AjaxResult.error(400, "参数异常");
            }
            s2 += phoneNumber.getValue();
        }
        String s3 = s2.substring(0, s2.length() - 1);

        SysUser sysUser1 = new SysUser();
        sysUser1.setPassword(sysUserJson.getPassword());
        sysUser1.setUserName(sysUserJson.getUserName());
        sysUser1.setNickName(sysUserJson.getDisplayName());
        sysUser1.setuId(sysUserJson.getId());
        sysUser1.setExternalId(sysUserJson.getExternalId());
        sysUser1.setEmails(s1);
        sysUser1.setPhonenumber(s3);
        int i = userMapper.insertUser(sysUser1);
        if (i != 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return AjaxResult.error(-1, "用户信息同步失败！");
        }
        return AjaxResult.success(0, "用户数据同步成功");*/
    }

    /**
     * 用户数据同步（删除）
     * <p>
     * 0	代表成功
     * 400	参数异常
     */
    /*@Transactional
    @Override
    public AjaxResult deleteSysUserById(SysUserJson sysUserJson) {
        if (sysUserJson == null) {
            return AjaxResult.error(400, "参数异常");
        }
        if (sysUserJson.getId() == null || sysUserJson.getId().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        int i = userMapper.deleteSysUserOne(sysUserJson.getId());
        if (i != 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return AjaxResult.error(-1, "用户数据同步，删除失败");
        }
        int i1 = deptMapper.deleteSysDetpByUid(sysUserJson.getId());
        if (i1 != 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return AjaxResult.error(-1, "用户数据同步，删除失败");
        }
        return AjaxResult.success(0, "删除成功");
    }*/

    /**
     * 用户数据同步(更新)
     * 0	代表成功
     * 400	参数异常
     * 430	用户已经存在
     * -1   失败
     */
    /*@Override
    @Transactional
    public AjaxResult updateSysUser(SysUserJson sysUserJson) {
        if (sysUserJson == null) {
            return AjaxResult.error(400, "参数异常");
        }
        if (sysUserJson.getId() == null || sysUserJson.getId().trim().isEmpty()) {
            return AjaxResult.error(400, "参数异常");
        }
        SysUser sysUser = userMapper.selectSysUserByUid(sysUserJson.getId());
        if (sysUser == null) {
            return AjaxResult.error(430, "该用户不存在");
        }
        if (sysUserJson.getUserName() != null) {
            sysUser.setUserName(sysUserJson.getUserName());
        }
        if (sysUserJson.getPassword() != null) {
            sysUser.setPassword(sysUserJson.getPassword());
        }
        if (sysUserJson.getDisplayName() != null) {
            sysUser.setNickName(sysUserJson.getDisplayName());
        }
        if (sysUserJson.getExternalId() != null) {
            sysUser.setExternalId(sysUserJson.getExternalId());
        }
        List<Emails> emails = sysUserJson.getEmails();
        if (emails != null && emails.size() >= 1) {
            String s = "";
            for (Emails email : emails) {
                s += email.getValue() + ",";
            }
            String s1 = s.substring(0, s.length() - 1);
            sysUser.setEmails(s1);
        }
        List<PhoneNumbers> phoneNumbers = sysUserJson.getPhoneNumbers();
        if (phoneNumbers != null && phoneNumbers.size() >= 1) {
            String s2 = "";
            for (PhoneNumbers phoneNumber : phoneNumbers) {
                if (phoneNumber.getValue().length() != 11) {
                    return AjaxResult.error(400, "参数异常");
                }
                s2 += phoneNumber.getValue();
            }
            String s3 = s2.substring(0, s2.length() - 1);
            sysUser.setPhonenumbers(s3);
        }
        int i = userMapper.updateUser(sysUser);
        if (i != 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return AjaxResult.error(-1, "用户信息同步更新失败");
        }
        SysDept sysDept = deptMapper.selectBySysDeptByUid(sysUserJson.getId());
        if (sysDept == null) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return AjaxResult.error(430, "该用户不存在");
        }
        List<Belongs> belongs = sysUserJson.getBelongs();
        if (belongs != null && belongs.size() >= 1) {
            String b = "";
            String b1 = "";
            String b2 = "";
            for (Belongs belong : belongs) {
                if (belong.getOuDirectory() != null || !belong.getOuDirectory().trim().isEmpty()) {
                    b += belong.getOuDirectory() + ",";
                }
                if (belong.getBelongOuUuid() != null || !belong.getBelongOuUuid().trim().isEmpty()) {
                    b1 += belong.getBelongOuUuid() + ",";
                }
                if (belong.getRootNode() != null || !belong.getRootNode().trim().isEmpty()) {
                    if (belong.getRootNode() == "false") {
                        b2 += 1 + ",";
                    } else {
                        b2 += 2 + ",";
                    }
                }
            }
            String ouDirectory = b.substring(0, b.length() - 1);
            String belongOuUuid = b1.substring(0, b1.length() - 1);
            String rootNode = b2.substring(0, b2.length() - 1);
            if (!ouDirectory.equals("")) {
                sysDept.setOuDirectory(ouDirectory);
            }
            if (!belongOuUuid.equals("")) {
                sysDept.setBelongOuUuid(belongOuUuid);
            }
            if (!rootNode.equals("")) {
                sysDept.setRootNode(rootNode);
            }
            int i1 = deptMapper.updateDept(sysDept);
            if (i1 != 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return AjaxResult.error(-1, "用户信息同步更新失败");
            }
        }
        return AjaxResult.success(0, "用户信息同步更新成功");
    }*/


    /**
     * 员工视图创建
     */
    public int userViewAdd(UserViewManage userViewManage) {

        String[] viewRangeIds = userViewManage.getViewRangeIds();
        String viewRangeIdsStr = ArrayUtils.toString(viewRangeIds);
        viewRangeIdsStr = viewRangeIdsStr.substring(1, viewRangeIdsStr.length() - 1);
        userViewManage.setViewRange(viewRangeIdsStr);

        String[] viewVisitorsIds = userViewManage.getViewVisitorsIds();
        String viewVisitorsIdsStr = ArrayUtils.toString(viewVisitorsIds);
        viewVisitorsIdsStr = viewVisitorsIdsStr.substring(1, viewVisitorsIdsStr.length() - 1);
        userViewManage.setViewVisitors(viewVisitorsIdsStr);
        int rows = userMapper.userViewAdd(userViewManage);
        return rows;
    }


    /**
     * 员工视图展示
     */
    @Override
    public List<UserViewManage> userViewList() {
        String userId = SecurityUtils.getLoginUser().getUser().getUserId();
        return userMapper.userViewList(userId);
    }

    /**
     * 视图内容展示
     */
    @Override
    public UserViewManage viewContentDisplay(String viewId) {

        UserViewManage userViewManage = userMapper.viewContentDisplay(viewId);

        String viewRangeStr = userViewManage.getViewRange();
        if (viewRangeStr != null) {
            String[] viewRanges = viewRangeStr.split(",");
            String[] longs = new String[viewRanges.length];
            for (int i = 0; i < viewRanges.length; i++) {
                longs[i] = String.valueOf(viewRanges[i]);
            }
            List<SysUser> viewRangeList = userMapper.findViewUserList(longs);
            userViewManage.setViewRangeList(viewRangeList);
        }

        String viewVisitorsStr = userViewManage.getViewVisitors();
        if (viewVisitorsStr != null) {
            String[] viewVisitors = viewVisitorsStr.split(",");

            String[] longs = new String[viewVisitors.length];
            for (int i = 0; i < viewVisitors.length; i++) {
                longs[i] = String.valueOf(viewVisitors[i]);
            }
            List<SysUser> viewVisitorList = userMapper.findViewUserList(longs);
            userViewManage.setViewVisitorsList(viewVisitorList);
        }


        return userViewManage;
    }

    /**
     * 员工视图修改
     */
    @Override
    public int userViewEdit(UserViewManage userViewManage) {

        String[] viewRangeIds = userViewManage.getViewRangeIds();
        String viewRangeIdsStr = ArrayUtils.toString(viewRangeIds);
        viewRangeIdsStr = viewRangeIdsStr.substring(1, viewRangeIdsStr.length() - 1);
        userViewManage.setViewRange(viewRangeIdsStr);

        String[] viewVisitorsIds = userViewManage.getViewVisitorsIds();
        String viewVisitorsIdsStr = ArrayUtils.toString(viewVisitorsIds);
        viewVisitorsIdsStr = viewVisitorsIdsStr.substring(1, viewVisitorsIdsStr.length() - 1);
        userViewManage.setViewVisitors(viewVisitorsIdsStr);

        return userMapper.userViewEdit(userViewManage);
    }

    /**
     * 员工视图删除
     */
    @Override
    public int userViewDelete(String userViewId) {
        return userMapper.userViewDelete(userViewId);
    }


    /**
     * 添加组织管理员
     */
    @Override
    public int addOrganAdmin(String[] userIds) {
        List<SysUserRole> list = new ArrayList<SysUserRole>();
        SysUserRole ur = new SysUserRole();
        for (String userId : userIds) {
            ur.setUserId(userId);
            ur.setRoleId("3");
            list.add(ur);
        }
        return userRoleMapper.batchUserRole(list);
    }


    /**
     * 获取登录用户的组织内人员列表
     */
    @Override
    public List<SysUser> listByLoginIn(String deptId) {

        String deptPId = userMapper.selectOrganPid(deptId);
        String[] deptIds = userMapper.selectDeptIds(deptPId);


        String deptIdsStr = ArrayUtils.toString(deptIds);
        deptIdsStr = deptIdsStr.substring(1, deptIdsStr.length() - 1);


        List<SysUser> sysUsers = userMapper.selectUserListByDeptIds(deptIdsStr);

        return sysUsers;
    }

    /**
     * 单点登录
     *
     * @param id_token
     * @param redirect_url
     * @param model
     * @param request
     * @return
     */
    public String ssoUrl(String id_token, String redirect_url, Model model, HttpServletRequest request) {
        //1.接收方法为GET方式,参数名为id_token
        //2.<解析令牌>为解析id_token并验证代码
        //1.使用公钥，解析id_token
        // 使用publicKey解密上一步获取的id_token令牌
        String publicKey = "";
        DingdangUserRetriever retriever = new DingdangUserRetriever(id_token, publicKey);
        DingdangUserRetriever.User user = null;
        try {
            //2.获取用户信息
            user = retriever.retrieve();
        } catch (Exception e) {
            log.warn("Retrieve SSO user failed", e);
            return "error";
        }
        //3.判断用户名是否在自己系统存在isExistedUsername()方法为业务系统自行判断数据库中是否存在
        //根据用户名查询
        SysUser sysUser = userMapper.selectByUserName(user.getUsername());
        if (sysUser != null) {
            //4.如果存在,登录成功，返回登录成功后的页面
        /*User spUser = userService.updateLoginTimes(user.getUsername());
        request.getSession().setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, saveSecurity(spUser));*/
            //5.如果注册时添加redirect_url，那么返回此自定义url页面
            if (StringUtils.isNotEmpty(redirect_url)) {
                return "redirect:" + redirect_url;
            }
            //6.否则返回系统默认操作页面
            return "redirect:../../index";
        } else {
            //7.如果不存在,返回登录失败页面,提示用户不存在
            model.addAttribute("error", "username { " + user.getUsername() + " } not exist");
            return "error";
        }
    }

    /**
     * 新增用户应用信息
     */
    @Override
    @Transactional
    public AjaxResult insertApplyUser(Long applyId, String[] deptIds) {
        if (null == applyId) {
            return AjaxResult.error("缺失参数");
        }
        if (StringUtils.isEmpty(deptIds)) {
            return AjaxResult.error("缺失参数");
        }
        // 新增用户与应用管理
        HashSet<MobileApplyUser> tags = new HashSet<>();
        for (String deptId : deptIds) {
            //根据部门id 查询部门下有的部门然后在查询的用户
            List<String> longs = userMapper.selectIdsByDeptId(deptId);
            if (StringUtils.isNotEmpty(longs)) {
                for (String userId : longs) {
                    //根据应用id和用户id查询
                    MobileApplyUser mobileApplyUser1 = mobileApplyUserMapper.selectByApplyIdAndUserId(applyId, userId);
                    if (StringUtils.isNull(mobileApplyUser1)) {
                        MobileApplyUser mobileApplyUser = new MobileApplyUser();
                        mobileApplyUser.setApplyId(applyId);
                        mobileApplyUser.setDeptId(deptId);
                        mobileApplyUser.setUserId(userId);
                        tags.add(mobileApplyUser);
                    }
                    continue;
                }
            }
        }
        //如何要是大于0插入用户id
        if (tags.size() > 0) {
            int i = mobileApplyUserMapper.insertMobileApplyUsers(tags);
            if (i < 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return AjaxResult.error("添加应用失败");
            }
            //否则小于0的话插入部门id和应用id
        } else {
            int row = 0;
            for (int i = 0; i < deptIds.length; i++) {
                row = mobileApplyUserMapper.insertMobileApplyDept(applyId, deptIds[i]);
            }
            if (row < 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return AjaxResult.error("添加应用失败");
            }
        }
        return AjaxResult.success("添加应用成功");
    }
    /**
     * 用户的禁用启动
     */
    public AjaxResult disableStartup(SysUser sysUser){
        if(StringUtils.isNull(sysUser)){
            return AjaxResult.error("参数缺失");
        }
        if(StringUtils.isEmpty(sysUser.getUserId())){
            return AjaxResult.error("参数缺失");
        }
        if(StringUtils.isEmpty(sysUser.getStatus())){
            return AjaxResult.error("参数缺失");
        }
        if(!"0".equals(sysUser.getStatus())  && !"1".equals(sysUser.getStatus())){
            return AjaxResult.error("参数有误");
        }
        //根据用户id查询
        SysUser sysUser1 = userMapper.selectUserById(sysUser.getUserId());
        if(StringUtils.isNull(sysUser1)){
            return AjaxResult.error("该用户不存在");
        }
        sysUser1.setStatus(sysUser.getStatus());
        int i = userMapper.updateUser(sysUser1);
        if(i!=1){
            return AjaxResult.error("修改失败");
        }
        return AjaxResult.success("修改成功");
    }
    /**
     * 给用户设置角色接口
     */
    @Override
    @Transactional
    public AjaxResult setTheRole(SysUser sysUser){
        if(StringUtils.isNull(sysUser)){
            return AjaxResult.error("参数缺失");
        }
        if(null == sysUser.getUserId()){
            return AjaxResult.error("参数缺失");
        }
/*        if(!sysUser.getRoleType().equals("0") && !sysUser.getRoleType().equals("1")){
            return AjaxResult.error("参数缺失");
        }*/
        HashMap<String, Object> where = new HashMap<>();
        where.put("userId",sysUser.getUserId());
        where.put("roleType",sysUser.getRoleType());
        //查询是系统的角色或者查询应用的角色的id
        String[] longs = roleMapper.selectRoleIdsByUserId(where);
        // 删除用户与角色关联
//        userRoleMapper.deleteUserRoleByUserId(sysUser.getUserId());
        if(longs.length>0){
            int i = userRoleMapper.deleteUserRoleByUserIdAndRoleIds(sysUser.getUserId(), longs);
            if(i<1){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
                return AjaxResult.error("给用户设置角色失败");
            }
        }
        // 新增用户与角色管理
        try{
            String[] roleIds = sysUser.getRoleIds();
             userRoleMapper.deleteUserRoleByUserId(sysUser.getUserId());

            insertUserRole(sysUser);
        }catch (Exception e){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
            return AjaxResult.error("给用户设置角色失败");
        }
        //根据用户id查询这个用户的部门
      /*  SysUser sysUser1 = userMapper.selectUserById(sysUser.getUserId());
        if(null == sysUser1){
            return AjaxResult.error("该用户不存在");
        }
        if(null == sysUser1.getDeptId()){
            return AjaxResult.error("请先将用户与部门进行绑定");
        }*/
        //根据部门id和角色id删除部门与角色的关联关系
        /*int i = roleDeptMapper.deleteRoleDeptByRoleIdsAndDeptIds(sysUser1.getDeptId(), longs);
        if(i<1){
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
            return AjaxResult.error("给用户设置角色失败");
        }*/
        //取出用户设置的角色id
       /* String[] roles = sysUser.getRoleIds();
        if (StringUtils.isNotNull(roles)) {
            // 新增用户与角色管理
            List<SysRoleDept> list = new ArrayList<SysRoleDept>();
            for (String roleId : roles) {
                SysRoleDept srd = new SysRoleDept();
                srd.setDeptId(sysUser1.getDeptId());
                srd.setRoleId(roleId);
                list.add(srd);
            }
            if (list.size() > 0) {
                roleDeptMapper.batchRoleDept(list);
            }else{
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
                return AjaxResult.error("给用户设置角色失败");
            }
        }*/
        return AjaxResult.success("给用户设置角色成功");
    }
    /**
     * 给用户设置角色数据回显接口
     */
    @Override
    public AjaxResult setRoleDataEcho(SysUser sysUser) {
        if(null == sysUser){
            return AjaxResult.error("缺失参数");
        }
        if(null == sysUser.getUserId()){
            return AjaxResult.error("参数异常");
        }
        List<Map<String, Object>> maps = roleMapper.selectRolesByUserId(sysUser.getUserId());
        return AjaxResult.success("查询成功",maps);
    }

    @Override
    public List<SysUser> selectDeveloperList() {

       return userMapper.selectDeveloperList();
    }

    @Override
    public AjaxResult addThirdPartyInfo(String appUserName, String appKey, String appSecret, ThirdPartyLoginBody body) {
        Boolean admin = body.getAdmin();
        UserInfo userInfo = body.getUserInfo();
        int i = userMapper.addThirdPartyInfo(appUserName,appKey,appSecret,admin,userInfo);
        if (i < 1){
            return AjaxResult.error("保存用户信息失败");
        }
        return AjaxResult.success();
    }

    @Override
    public List<SysUser> linkedRoleList(Map parms) {

        String roleId = String.valueOf(parms.get("roleId"));
        List<SysUser> sysUsers = userMapper.linkedRoleList(roleId);
        for (SysUser sysUser : sysUsers) {

            if (sysUser != null){
                SysDept sysDept = deptMapper.selectDeptById(sysUser.getDeptId());
                if(sysDept != null){
                    sysUser.setDeptName(sysDept.getDeptName());
                }
            }
        }
        return sysUsers;
    }
}
