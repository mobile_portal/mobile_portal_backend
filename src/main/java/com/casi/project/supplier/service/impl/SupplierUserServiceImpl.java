package com.casi.project.supplier.service.impl;

import java.util.List;
import com.casi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.supplier.mapper.SupplierUserMapper;
import com.casi.project.supplier.domain.SupplierUser;
import com.casi.project.supplier.service.ISupplierUserService;

/**
 * 供应商账号表Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class SupplierUserServiceImpl implements ISupplierUserService 
{
    @Autowired
    private SupplierUserMapper supplierUserMapper;

    /**
     * 查询供应商账号表
     * 
     * @param uuid 供应商账号表ID
     * @return 供应商账号表
     */
    @Override
    public SupplierUser selectSupplierUserById(String uuid)
    {
        return supplierUserMapper.selectSupplierUserById(uuid);
    }

    /**
     * 查询供应商账号表列表
     * 
     * @param supplierUser 供应商账号表
     * @return 供应商账号表
     */
    @Override
    public List<SupplierUser> selectSupplierUserList(SupplierUser supplierUser)
    {
        return supplierUserMapper.selectSupplierUserList(supplierUser);
    }

    /**
     * 新增供应商账号表
     * 
     * @param supplierUser 供应商账号表
     * @return 结果
     */
    @Override
    public int insertSupplierUser(SupplierUser supplierUser)
    {
        supplierUser.setCreateTime(DateUtils.getNowDate());
        return supplierUserMapper.insertSupplierUser(supplierUser);
    }

    /**
     * 修改供应商账号表
     * 
     * @param supplierUser 供应商账号表
     * @return 结果
     */
    @Override
    public int updateSupplierUser(SupplierUser supplierUser)
    {
        supplierUser.setUpdateTime(DateUtils.getNowDate());
        return supplierUserMapper.updateSupplierUser(supplierUser);
    }

    /**
     * 批量删除供应商账号表
     * 
     * @param uuids 需要删除的供应商账号表ID
     * @return 结果
     */
    @Override
    public int deleteSupplierUserByIds(String[] uuids)
    {
        return supplierUserMapper.deleteSupplierUserByIds(uuids);
    }

    /**
     * 删除供应商账号表信息
     * 
     * @param uuid 供应商账号表ID
     * @return 结果
     */
    @Override
    public int deleteSupplierUserById(String uuid)
    {
        return supplierUserMapper.deleteSupplierUserById(uuid);
    }
}
