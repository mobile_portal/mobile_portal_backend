package com.casi.project.interfaces.domain;

import java.io.Serializable;

/**
 * @Auther: liyuan
 * @Date: 2020/05/23/14:38
 * @Description:
 */
public class Text implements Serializable {
    //文本消息的内容
    private String content;
    //图片消息 图片媒体文件id，可以调用上传临时素材接口获取
    private String mediaId;
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }
}
