package com.casi.project.tenant.service.impl;

import java.util.List;
import com.casi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.tenant.mapper.TenantUserMapper;
import com.casi.project.tenant.domain.TenantUser;
import com.casi.project.tenant.service.ITenantUserService;

/**
 * 租户账号Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class TenantUserServiceImpl implements ITenantUserService 
{
    @Autowired
    private TenantUserMapper tenantUserMapper;

    /**
     * 查询租户账号
     * 
     * @param uuid 租户账号ID
     * @return 租户账号
     */
    @Override
    public TenantUser selectTenantUserById(String uuid)
    {
        return tenantUserMapper.selectTenantUserById(uuid);
    }

    /**
     * 查询租户账号列表
     * 
     * @param tenantUser 租户账号
     * @return 租户账号
     */
    @Override
    public List<TenantUser> selectTenantUserList(TenantUser tenantUser)
    {
        return tenantUserMapper.selectTenantUserList(tenantUser);
    }

    /**
     * 新增租户账号
     * 
     * @param tenantUser 租户账号
     * @return 结果
     */
    @Override
    public int insertTenantUser(TenantUser tenantUser)
    {
        tenantUser.setCreateTime(DateUtils.getNowDate());
        return tenantUserMapper.insertTenantUser(tenantUser);
    }

    /**
     * 修改租户账号
     * 
     * @param tenantUser 租户账号
     * @return 结果
     */
    @Override
    public int updateTenantUser(TenantUser tenantUser)
    {
        tenantUser.setUpdateTime(DateUtils.getNowDate());
        return tenantUserMapper.updateTenantUser(tenantUser);
    }

    /**
     * 批量删除租户账号
     * 
     * @param uuids 需要删除的租户账号ID
     * @return 结果
     */
    @Override
    public int deleteTenantUserByIds(String[] uuids)
    {
        return tenantUserMapper.deleteTenantUserByIds(uuids);
    }

    /**
     * 删除租户账号信息
     * 
     * @param uuid 租户账号ID
     * @return 结果
     */
    @Override
    public int deleteTenantUserById(String uuid)
    {
        return tenantUserMapper.deleteTenantUserById(uuid);
    }
}
