package com.casi.project.store.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppRoleMapper;
import com.casi.project.store.domain.StoreAppRole;
import com.casi.project.store.service.IStoreAppRoleService;

/**
 * 应用角色Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppRoleServiceImpl implements IStoreAppRoleService 
{
    @Autowired
    private StoreAppRoleMapper storeAppRoleMapper;

    /**
     * 查询应用角色
     * 
     * @param uuid 应用角色ID
     * @return 应用角色
     */
    @Override
    public StoreAppRole selectStoreAppRoleById(String uuid)
    {
        return storeAppRoleMapper.selectStoreAppRoleById(uuid);
    }

    /**
     * 查询应用角色列表
     * 
     * @param storeAppRole 应用角色
     * @return 应用角色
     */
    @Override
    public List<StoreAppRole> selectStoreAppRoleList(StoreAppRole storeAppRole)
    {
        return storeAppRoleMapper.selectStoreAppRoleList(storeAppRole);
    }

    /**
     * 新增应用角色
     * 
     * @param storeAppRole 应用角色
     * @return 结果
     */
    @Override
    public int insertStoreAppRole(StoreAppRole storeAppRole)
    {
        return storeAppRoleMapper.insertStoreAppRole(storeAppRole);
    }

    /**
     * 修改应用角色
     * 
     * @param storeAppRole 应用角色
     * @return 结果
     */
    @Override
    public int updateStoreAppRole(StoreAppRole storeAppRole)
    {
        return storeAppRoleMapper.updateStoreAppRole(storeAppRole);
    }

    /**
     * 批量删除应用角色
     * 
     * @param uuids 需要删除的应用角色ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppRoleByIds(String[] uuids)
    {
        return storeAppRoleMapper.deleteStoreAppRoleByIds(uuids);
    }

    /**
     * 删除应用角色信息
     * 
     * @param uuid 应用角色ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppRoleById(String uuid)
    {
        return storeAppRoleMapper.deleteStoreAppRoleById(uuid);
    }
}
