package com.casi.project.portal.service;

/**
 * @Description:素材发布服务类
 */
public interface MaterialService {
    /**
     * @Description: 发布素材
     * @param process
     * @return boolean
     */
    public boolean insert(Process process) throws Exception;

}
