package com.casi.project.system.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 角色表 sys_white_ip
 *
 * @author lzb
 */
@Data
public class SysWhiteIp implements Serializable {
    private static final long serialVersionUID = 1564878121133L;


    private String uuid;
    private String whiteIp;
    private String status;

    private Long pageNum;
    private Long pageSize;

}
