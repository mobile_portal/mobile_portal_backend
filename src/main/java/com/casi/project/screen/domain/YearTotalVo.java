package com.casi.project.screen.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther shp
 * @data2020/5/811:39
 * @description
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
public class YearTotalVo {


    private String state;
    // 1月到12月
    private Integer january;
    private Integer february;
    private Integer march;
    private Integer april;
    private Integer may;
    private Integer june;
    private Integer july;
    private Integer august;
    private Integer september;
    private Integer october;
    private Integer november;
    private Integer december;
    //和值
    private Integer sum;
    //平均值
    private double average;

}
