package com.casi.framework.security.service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import com.casi.common.utils.DateUtils;
import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.ip.IpUtils;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.domain.SysWhiteIp;
import com.casi.project.system.mapper.SysUserMapper;
import com.casi.project.system.service.ISysWhiteIpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import com.casi.common.constant.Constants;
import com.casi.common.exception.CustomException;
import com.casi.common.exception.user.CaptchaException;
import com.casi.common.exception.user.CaptchaExpireException;
import com.casi.common.exception.user.UserPasswordNotMatchException;
import com.casi.common.utils.MessageUtils;
import com.casi.framework.manager.AsyncManager;
import com.casi.framework.manager.factory.AsyncFactory;
import com.casi.framework.redis.RedisCache;
import com.casi.framework.security.LoginUser;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * 登录校验方法
 *
 * @author lzb
 */
@Component
public class SysLoginService {
    @Autowired
    private TokenService tokenService;

    @Resource
    private AuthenticationManager authenticationManager;

    @Autowired
    private RedisCache redisCache;
    @Autowired
    private SysUserMapper userMapper;
    @Autowired
    private ISysWhiteIpService ipService;


    Logger logger = LoggerFactory.getLogger(SysLoginService.class);

    /**
     * 登录验证
     *
     * @param username 用户名
     * @param password 密码
     * @param code     验证码
     * @param uuid     唯一标识
     * @return 结果
     */
    public AjaxResult login(String username, String password, String code, String uuid, HttpServletRequest request) {
        AjaxResult  ajax = AjaxResult.success();
        String ip = IpUtils.getIpAddr(request);
        logger.info("=============loginIp======== "+ip);
        SysWhiteIp sysWhiteIp = ipService.getInfoByIp(ip);
        if(null == sysWhiteIp){
            return  AjaxResult.error("此IP未授权，请联系管理员授权");
        }
        Integer limit =5;
        String key ="error_count_"+username;
        Integer count = redisCache.getCacheObject(key);
        if(null == count){
            count =0;
        }else{
            if(count >= limit){
                return AjaxResult.error("您今天登录失败的次数已经超过限制，请明天再试。");
            }
        }

        // 管理员账号规定时长重置密码  7天
        SysUser sysUser = userMapper.selectUserByUserName(username);
        if (sysUser != null) {
            if ("1".equals(sysUser.getUserId())) {
                Date passwordUpdateTime = sysUser.getUpdateTime();
                if (passwordUpdateTime == null){
                    return AjaxResult.error("密码过期,请重置密码!");
                }
                Date nowDate = DateUtils.getNowDate();
                long time = nowDate.getTime() - passwordUpdateTime.getTime();
                long day = time / (1000 * 60 * 60 * 24);
                if (day > 7) {
                    return AjaxResult.error("请重置密码!");
                }
            }
        }
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;
        String captcha = redisCache.getCacheObject(verifyKey);
        //  String captcha = "1234";
        redisCache.deleteObject(verifyKey);
        if (captcha == null) {
            logger.info("captcha==========================================" + captcha);
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.expire")));
            throw new CaptchaExpireException();
        }
        if (!code.equalsIgnoreCase(captcha)) {
            logger.info("captcha==========================================" + captcha + "code=============================" + code);
            AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.jcaptcha.error")));
            throw new CaptchaException();
        }
        // 用户验证
        Authentication authentication = null;


        try {
            // 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
            authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (Exception e) {
            //记录当天登录失败次数
            IncrFailLoginCount(key,count);
            if (e instanceof BadCredentialsException) {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
                throw new UserPasswordNotMatchException();
            } else {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
                throw new CustomException(e.getMessage());
            }
        }


        AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        LoginUser loginUser = (LoginUser) authentication.getPrincipal();
        // 生成token
        logger.info("loginUser=============================" + loginUser.toString());
        String token = tokenService.createToken(loginUser);
        System.out.println("token为==========================="+token);
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    // APP免登陆
    public String appLogin(String userId) {
        LoginUser loginUser = new LoginUser();
        SysUser sysUser = userMapper.selectUserById(userId);
        loginUser.setUser(sysUser);
        // 生成token
        logger.info("loginUser=============================" + loginUser.toString());
        return tokenService.createToken(loginUser);
    }


    /**
     * 登录验证
     *
     * @param loginUser username 用户名
     * @param loginUser password 密码
     * @param loginUser code验证码
     * @param loginUser uuid 唯一标识
     * @return 结果
     */
    public String appLogin(LoginUser loginUser) {
        // 用户验证
        Authentication authentication = null;

        authentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginUser.getUser().getUserName(), loginUser.getUser().getPassWhite()));
        AsyncManager.me().execute(AsyncFactory.recordLogininfor(loginUser.getUser().getUserName(), Constants.LOGIN_SUCCESS, MessageUtils.message("user.login.success")));
        LoginUser user = (LoginUser) authentication.getPrincipal();

        return tokenService.createToken(user);
    }

    /**
     * 一天中登录失败的次数统计
     * @param key redis中存储的键
     * @param count 已经登录失败的次数
     * @return count 登录失败次数
     */
    private Integer IncrFailLoginCount(String key,Integer count) {
        count++;
        //设置过期时间为今晚23点59分59秒
        long timeInMillis = DateUtils.getMillsecBeforeMoment(23, 59, 59, 999);
        if (timeInMillis < 100){
            // 避免在最后一秒的时候登录导致过期时间过小甚至为负数
            timeInMillis = 1000*60;
        }
        // 设置过期时间
        redisCache.setCacheObjectByex(key,count,timeInMillis, TimeUnit.MILLISECONDS);
        return count;
    }

    public AjaxResult adminResetPwd(Map params) {
        AjaxResult ajax;
        String username = String.valueOf(params.get("username"));
        String oldPassword = String.valueOf(params.get("oldPassword"));
        String newPassword = String.valueOf(params.get("newPasswordOne"));
        String newPasswordCompare = String.valueOf(params.get("newPasswordTwo"));
        if (!newPassword.equals(newPasswordCompare)){
            return AjaxResult.error("两次输入密码不一致");
        }
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, oldPassword));
            LoginUser user = (LoginUser) authentication.getPrincipal();
            SysUser sysUser = new SysUser();
            sysUser.setUserId("1");
            sysUser.setPassword(SecurityUtils.encryptPassword(newPassword));
            sysUser.setUpdateTime(new Date());
            int i = userMapper.updateUser(sysUser);
            if (i > 0) {
                return AjaxResult.success("密码重置成功");
            }else {
                return AjaxResult.error("密码重置失败");
            }
        } catch (Exception e) {
            if (e instanceof BadCredentialsException) {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, MessageUtils.message("user.password.not.match")));
                throw new UserPasswordNotMatchException();
            } else {
                AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, Constants.LOGIN_FAIL, e.getMessage()));
                throw new CustomException(e.getMessage());
            }
        }
    }
}
