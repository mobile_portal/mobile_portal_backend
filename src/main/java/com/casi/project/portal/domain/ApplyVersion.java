package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/*应用版本*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApplyVersion {
    private String id;
    private String icon;
    //屏目截图地址
    private String screenCapture;
    private String versionNumber;
    private String versionDescribe;
    private Integer isPutAway;
    private Integer applyScope;
    private String appAddress;
    private String offAddress;
    private Integer isFull;
    private Integer displayStyle;
    private Integer titleBar;
    private String titleBarColor;
    private Integer progressBar;
    private String progressBarColor;
    private String pcAddress;
    private Integer isEmbeddedServer;
    private String adminAddress;
    private String messageAddress;
    //应用参数设置
    private String applyParameter;
    //和上级应用明细表ID关联
    private String applyVersion;
    private Date createTime;
    private Date updateTime;


}
