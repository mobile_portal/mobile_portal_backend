package com.casi.project.screen.domain;

import lombok.Data;

/**
 * @Auther: lizhibin
 * @Date: 2020/5/9 11:42
 * @Description:
 */
@Data
public class EdwDataCollectMonitor {

 /**
  * 系统编码
  */
   private  String  sysCode;
    /**
     * 系统名称
     */
   private String sysName;
    /**
     * 数据总量
     */
    private Long conllectTotal;

    /**
     * 今日数据
     */
    private Long conllectToday;


   /**
     * 今日采集金额
     */
    private  Double conllectAmtToday;


    /**
     * 日期
     */
    private  String  todayDate;
   /**
    * 数据有效日期
    */
    private  String dataValidDate;
    /**
     * 数据类型
     */
    private String type;

}
