package com.casi.project.system.domain.vo;

import lombok.Data;

/**
 * @author css
 * @date 2020/5/15 14:50
 */
@Data
public class StringArray {

    private  String[] userIds;

}
