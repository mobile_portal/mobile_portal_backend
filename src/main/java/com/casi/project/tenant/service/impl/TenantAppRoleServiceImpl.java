package com.casi.project.tenant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.tenant.mapper.TenantAppRoleMapper;
import com.casi.project.tenant.domain.TenantAppRole;
import com.casi.project.tenant.service.ITenantAppRoleService;

/**
 * 租户自定义应用角色Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class TenantAppRoleServiceImpl implements ITenantAppRoleService 
{
    @Autowired
    private TenantAppRoleMapper tenantAppRoleMapper;

    /**
     * 查询租户自定义应用角色
     * 
     * @param uuid 租户自定义应用角色ID
     * @return 租户自定义应用角色
     */
    @Override
    public TenantAppRole selectTenantAppRoleById(String uuid)
    {
        return tenantAppRoleMapper.selectTenantAppRoleById(uuid);
    }

    /**
     * 查询租户自定义应用角色列表
     * 
     * @param tenantAppRole 租户自定义应用角色
     * @return 租户自定义应用角色
     */
    @Override
    public List<TenantAppRole> selectTenantAppRoleList(TenantAppRole tenantAppRole)
    {
        return tenantAppRoleMapper.selectTenantAppRoleList(tenantAppRole);
    }

    /**
     * 新增租户自定义应用角色
     * 
     * @param tenantAppRole 租户自定义应用角色
     * @return 结果
     */
    @Override
    public int insertTenantAppRole(TenantAppRole tenantAppRole)
    {
        return tenantAppRoleMapper.insertTenantAppRole(tenantAppRole);
    }

    /**
     * 修改租户自定义应用角色
     * 
     * @param tenantAppRole 租户自定义应用角色
     * @return 结果
     */
    @Override
    public int updateTenantAppRole(TenantAppRole tenantAppRole)
    {
        return tenantAppRoleMapper.updateTenantAppRole(tenantAppRole);
    }

    /**
     * 批量删除租户自定义应用角色
     * 
     * @param uuids 需要删除的租户自定义应用角色ID
     * @return 结果
     */
    @Override
    public int deleteTenantAppRoleByIds(String[] uuids)
    {
        return tenantAppRoleMapper.deleteTenantAppRoleByIds(uuids);
    }

    /**
     * 删除租户自定义应用角色信息
     * 
     * @param uuid 租户自定义应用角色ID
     * @return 结果
     */
    @Override
    public int deleteTenantAppRoleById(String uuid)
    {
        return tenantAppRoleMapper.deleteTenantAppRoleById(uuid);
    }
}
