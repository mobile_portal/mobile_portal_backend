package com.casi.project.system.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.casi.project.portal.domain.RoleApplyResource;
import com.casi.project.system.domain.SysRoleMenu;
import org.apache.ibatis.annotations.Param;

/**
 * 角色与菜单关联表 数据层
 *
 * @author lzb
 */
public interface SysRoleMenuMapper {
    /**
     * 查询菜单使用数量
     *
     * @param menuId 菜单ID
     * @return 结果
     */
    public int checkMenuExistRole(String menuId);

    /**
     * 通过角色ID删除角色和菜单关联
     *
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleMenuByRoleId(String roleId);

    /**
     * 通过角色ID删除应用角色和菜单关联
     */
    public int deleteApplyRoleByRoleId(String roleId);

    /**
     * 批量新增角色菜单信息
     *
     * @param roleMenuList 角色菜单列表
     * @return 结果
     */
    public int batchRoleMenu(List<SysRoleMenu> roleMenuList);


    /**
     * 批量新增角色应用信息
     */
    public int batchRoleApply(@Param("collection") Set<RoleApplyResource> roleApplyResource);




}
