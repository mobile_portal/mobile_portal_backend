package com.casi.project.screen.domain;

import lombok.Data;

/**
 * @Auther shp
 * @data2020/5/1023:18
 * @description
 */

@Data
public class YearCompanyVo {

    /**
     * 部门编码
     */
    private String conUnitcode;
    /**
     * 部门名称
     */
    private String orgName;

}
