package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreTag;
import com.casi.project.store.service.IStoreTagService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 默认标签Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"默认标签"})
@RestController
@RequestMapping("/store/tag")
public class StoreTagController extends BaseController
{
    @Autowired
    private IStoreTagService storeTagService;

    /**
     * 查询默认标签列表
     */
    @ApiOperation(value = "查询默认标签列表")
    @PreAuthorize("@ss.hasPermi('store:tag:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreTag storeTag)
    {
        startPage();
        List<StoreTag> list = storeTagService.selectStoreTagList(storeTag);
        return getDataTable(list);
    }

    /**
     * 导出默认标签列表
     */
    @PreAuthorize("@ss.hasPermi('store:tag:export')")
    @Log(title = "默认标签", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreTag storeTag)
    {
        List<StoreTag> list = storeTagService.selectStoreTagList(storeTag);
        ExcelUtil<StoreTag> util = new ExcelUtil<StoreTag>(StoreTag.class);
        return util.exportExcel(list, "tag");
    }

    /**
     * 获取默认标签详细信息
     */
    @ApiOperation(value = "获取默认标签详细信息")
    @PreAuthorize("@ss.hasPermi('store:tag:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeTagService.selectStoreTagById(uuid));
    }

    /**
     * 新增默认标签
     */
    @ApiOperation(value = "新增默认标签")
    @PreAuthorize("@ss.hasPermi('store:tag:add')")
    @Log(title = "默认标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreTag storeTag)
    {
        return toAjax(storeTagService.insertStoreTag(storeTag));
    }

    /**
     * 修改默认标签
     */
    @ApiOperation(value = "修改默认标签")
    @PreAuthorize("@ss.hasPermi('store:tag:edit')")
    @Log(title = "默认标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreTag storeTag)
    {
        return toAjax(storeTagService.updateStoreTag(storeTag));
    }

    /**
     * 删除默认标签
     */
    @ApiOperation(value = "删除默认标签")
    @PreAuthorize("@ss.hasPermi('store:tag:remove')")
    @Log(title = "默认标签", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeTagService.deleteStoreTagByIds(uuids));
    }
}
