package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreAppCheck;
import com.casi.project.store.service.IStoreAppCheckService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用审核Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用审核"})
@RestController
@RequestMapping("/store/check")
public class StoreAppCheckController extends BaseController
{
    @Autowired
    private IStoreAppCheckService storeAppCheckService;

    /**
     * 查询应用审核列表
     */
    @ApiOperation(value = "查询应用审核列表")
    @PreAuthorize("@ss.hasPermi('store:check:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreAppCheck storeAppCheck)
    {
        startPage();
        List<StoreAppCheck> list = storeAppCheckService.selectStoreAppCheckList(storeAppCheck);
        return getDataTable(list);
    }

    /**
     * 导出应用审核列表
     */
    @PreAuthorize("@ss.hasPermi('store:check:export')")
    @Log(title = "应用审核", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreAppCheck storeAppCheck)
    {
        List<StoreAppCheck> list = storeAppCheckService.selectStoreAppCheckList(storeAppCheck);
        ExcelUtil<StoreAppCheck> util = new ExcelUtil<StoreAppCheck>(StoreAppCheck.class);
        return util.exportExcel(list, "check");
    }

    /**
     * 获取应用审核详细信息
     */
    @ApiOperation(value = "获取应用审核详细信息")
    @PreAuthorize("@ss.hasPermi('store:check:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppCheckService.selectStoreAppCheckById(uuid));
    }

    /**
     * 新增应用审核
     */
    @ApiOperation(value = "新增应用审核")
    @PreAuthorize("@ss.hasPermi('store:check:add')")
    @Log(title = "应用审核", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreAppCheck storeAppCheck)
    {
        return toAjax(storeAppCheckService.insertStoreAppCheck(storeAppCheck));
    }

    /**
     * 修改应用审核
     */
    @ApiOperation(value = "修改应用审核")
    @PreAuthorize("@ss.hasPermi('store:check:edit')")
    @Log(title = "应用审核", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreAppCheck storeAppCheck)
    {
        return toAjax(storeAppCheckService.updateStoreAppCheck(storeAppCheck));
    }

    /**
     * 删除应用审核
     */
    @ApiOperation(value = "删除应用审核")
    @PreAuthorize("@ss.hasPermi('store:check:remove')")
    @Log(title = "应用审核", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppCheckService.deleteStoreAppCheckByIds(uuids));
    }
}
