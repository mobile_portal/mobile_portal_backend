package com.casi.project.interfaces.controller;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.interfaces.domain.DepartmentVo;
import com.casi.project.interfaces.service.WxUserService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequestMapping("/wx")
@Api(tags = "微信接口")
@RestController
public class WxJobController {

    @Autowired
    private WxUserService wxUserService;

    private static Logger logger = LoggerFactory.getLogger(WxJobController.class);

    /**
     *
     * @param idToken
     * @param parentId //上级组织id
     * @return
     */
    @GetMapping("org")
    public AjaxResult getOrgList(String idToken,Integer parentId){

        AjaxResult ajax=  wxUserService.getOrgList(idToken,parentId);

        return ajax;
    }

    @GetMapping("userlist")
    public AjaxResult getuserList(String idToken,Integer orgId){
        AjaxResult ajax = AjaxResult.success();
        if(null == orgId || orgId == 0){
            List<DepartmentVo> list = wxUserService.deptList();
            if(null == list || list.size() <=0){
               return  ajax.error("请先同步组织");
            }
            for (DepartmentVo vo : list ) {
                ajax=  wxUserService.getUserList(null,vo.getId());
                logger.info("ajax=============="+ajax);
            }
        }else{
            ajax=  wxUserService.getUserList(idToken,orgId);
        }


        return ajax;
    }



}
