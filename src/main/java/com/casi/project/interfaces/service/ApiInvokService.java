package com.casi.project.interfaces.service;

import com.casi.project.interfaces.domain.ApiInvok;

public interface ApiInvokService {

    ApiInvok getModelByKey(String appKey, String appSecret);

}
