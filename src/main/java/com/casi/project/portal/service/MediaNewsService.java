package com.casi.project.portal.service;


import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.MediaBase;
import com.casi.project.portal.domain.vo.MediaBaseForNewsVo;
import com.casi.project.portal.domain.vo.MediaNewsAddVo;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Description:图文素材管理服务类
 */
public interface MediaNewsService {

    /**
     * @Description: 图文列表获取
     * @param base
     * @return boolean
     */
    public List<MediaBaseForNewsVo> queryList(MediaBase base);

    /**
     * @Description: 图文详细信息获取
     * @param id
     * @return boolean
     */
    public MediaBase queryDetail(Long id);

    /**
     * @Description: 新增图文素材
     * @return boolean
     */
    public boolean add(MediaNewsAddVo newsAddVo, String type) throws Exception;

    /**
     * @Description: 更新图文素材
     * @return boolean
     */
    public boolean update(MediaNewsAddVo newsAddVo, HttpServletRequest request) throws Exception;

    /**
     * @Description: 删除图文素材
     * @param base
     * @return boolean
     */
    public boolean delete(MediaBase base) throws Exception;


    /**
     * @Description: 审核  发布 退回
     * @return boolean
     */
    public boolean examine(MediaNewsAddVo newsAddVo, HttpServletRequest request) throws Exception;
    /**
     * 资讯管理审核接口
     */
    public AjaxResult auditNews(MediaBase mediaBase);
    /**
     * 资讯管理发布接口
     */
    public AjaxResult publishNews(MediaBase mediaBase);
    /**
     * 资讯管理退回接口
     */
    public AjaxResult sendBackNews(String id);
    /**
     * 删除素材
     */
    public AjaxResult deleteById (MediaBase mediaBase);
}
