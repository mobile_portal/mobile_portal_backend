package com.casi.project.interfaces.utils;

import com.casi.common.constant.Constants;
import com.casi.common.utils.JsonUtils;
import com.casi.common.utils.StringUtils;
import com.casi.common.utils.http.HttpUtils;
import com.casi.framework.redis.RedisCache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class AccessTokenUtils {
    private static Logger logger = LoggerFactory.getLogger(AccessTokenUtils.class);

    @Autowired
    private RedisCache redisCache;



    public String getAccessToken(){
        String acc = redisCache.getCacheObject(Constants.WX_ACCESS_TOKEN);

        if(StringUtils.isNotEmpty(acc)){

            return acc;
        }
        logger.info("Constants.url=========================="+Constants.url);
        String accessToken = HttpUtils.sendGet(Constants.url,"corpid="+Constants.CORPID+"&corpsecret="+Constants.CORPSECRET);
        logger.info("accessToken=========+++++++++================="+accessToken);
        if(StringUtils.isNotEmpty(accessToken)){
            Map<Object, Object> map = JsonUtils.stringToCollect(accessToken);
            Integer errcode = (Integer) map.get("errcode");
            if(errcode == 0){
                String access = (String)map.get("access_token");
                redisCache.setCacheObject(Constants.WX_ACCESS_TOKEN,access,100, TimeUnit.MINUTES);
                return access;
            }
        }else{
            logger.info("+++++++++微信返回失败");
            return null;
        }
        return null;
    }
}
