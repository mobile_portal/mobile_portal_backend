package com.casi.project.screen.domain;

import lombok.Data;

/**
 * @Auther: lizhibin
 * @Date: 2020/5/9 11:42
 * @Description:
 */
@Data
public class EdwDataSupplyMonitor {

 /**
  * 系统编码
  */
   private  String  sysCode;
    /**
     * 系统名称
     */
   private String sysName;
    /**
     * 合同数量总量
     */
    private String contractNumTotal;

    /**
     * 合同金额总量
     */
    private Double contractAmtTotal;

    /**
     * 合同支付金额总量
     */
    private Double contractPayTotal;

    /**
     * 今日合同数量
     */
    private  Integer contractNumToday;
    /**
     * 今日合同金额
     */
    private  Double contractAmtToday;

    /**
     * 今日支付合同金额
     */
    private  Double contractPayToday;

    /**
     * 日期
     */
    private  String todayDate;
   /**
    * 数据有效日期
    */
    private  String dataValidDate;
}
