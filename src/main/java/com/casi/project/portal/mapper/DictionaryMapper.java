package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.Dictionary;

import java.util.List;

/**
 * @Author zhongzj
 * @Description:
 * @Create: 2019/11/22
 */
public interface DictionaryMapper {
    int insert(Dictionary dictionary);

    int update(Dictionary dictionary);

    int delete(Long id);

    Dictionary queryById(Long id);

    List<Dictionary> queryList(Dictionary dictionary);

    String queryNameByCode(Dictionary dictionary);
}
