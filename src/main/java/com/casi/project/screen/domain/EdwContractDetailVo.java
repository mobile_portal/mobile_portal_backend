package com.casi.project.screen.domain;

import lombok.Data;

/**
 * @Auther: lizhibin
 * @Date: 2020/5/8 17:42
 * @Description:
 */
@Data
public class EdwContractDetailVo {

   private  Long id;
    /**
     * 合同编号
     */
   private String conConcode;
    /**
     * 合同所属单位编码
     */
    private String conUnitcode;

    /**
     * 合同所属单位名称
     */
    private String conUnitname;

    /**
     * 创建时间
     */
    private String createDate;

    /**
     * 采集总数据量
     */
    private  Integer TotalQuantity;
    /**
     * 今日采集总数据量
     */
    private  Integer TotalQuantityToday;

    /**
     * 累计提供合同金额
     */
    private  Long TotalConAmount;

    /**
     * 累计付款金额
     */
    private  Long TotalPaymenAmount;
}
