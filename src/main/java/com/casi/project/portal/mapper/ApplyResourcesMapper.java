package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.MobileApplyResource;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author css
 * @date 2020/5/8 17:08
 */
public interface ApplyResourcesMapper {

    /**
     * 查询用户应用的资源列表
     */
   // List<MobileApplyResource> selectApplyList(@Param("applyRoleId") String applyRoleId,@Param("applyId") String applyId);
    List<MobileApplyResource> selectApplyList(@Param("applyId") String applyId);

    /**
     * 新增应用资源
     */
    int insertResource(MobileApplyResource mobileApplyResource);

    /**
     * 修改应用资源
     */
    int updateResource(MobileApplyResource mobileApplyResource);

    /**
     * 删除应用资源
     */
    int deleteMenuById(@Param("appPermsId") String appPermsId);

    String[] selectRoleList(@Param("userId") String userId);
}
