package com.casi.project.portal.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Auther shp
 * @data2020/4/1317:00
 * @description
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class MobileHomePage {

    @ApiModelProperty("主键")
    private Integer id;
    @ApiModelProperty("首页menu名称")
    private String homeName;
    @ApiModelProperty("图标")
    private String homeIcon;
    @ApiModelProperty("创建时间")
    private Date createTime;

}
