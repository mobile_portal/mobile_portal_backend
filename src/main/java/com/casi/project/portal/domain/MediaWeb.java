package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class MediaWeb {
  /**
   * 主键id
   */
  private Long id;
  /**
   * 网页名称
   */
  private String name;
  /**
   * 网页地址
   */
  private String url;
  /**
   * 创建时间
   */
  private Date createTime;
  /**
   * 更新时间
   */
  private Date updateTime;



}
