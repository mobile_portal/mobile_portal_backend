package com.casi.project.system.service;

import java.util.List;
import java.util.Map;

import com.casi.project.portal.domain.thirdParty.ThirdPartyLoginBody;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.domain.SysUserJson;
import com.casi.project.system.domain.UserViewManage;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;

/**
 * 用户 业务层
 * 
 * @author lzb
 */
public interface ISysUserService
{
    /**
     * 根据条件分页查询用户列表
     * 
     * @param user 用户信息
     * @return 用户信息集合信息
     */
    public List<SysUser> selectUserList(SysUser user);

    public List<SysUser> noDisabledList(String deptId,String applyId);

    public List<SysUser> selectUserList(String[] userIds);

    /**
     * 通过用户名查询用户
     * 
     * @param userName 用户名
     * @return 用户对象信息
     */
    public SysUser selectUserByUserName(String userName);

    /**
     * 通过用户ID查询用户
     * 
     * @param userId 用户ID
     * @return 用户对象信息
     */
    public SysUser selectUserById(String userId);

    /**
     * 根据用户ID查询用户所属角色组
     * 
     * @param userName 用户名
     * @return 结果
     */
    public String selectUserRoleGroup(String userName);

    /**
     * 根据用户ID查询用户所属岗位组
     * 
     * @param userName 用户名
     * @return 结果
     */
    public String selectUserPostGroup(String userName);

    /**
     * 校验用户名称是否唯一
     * 
     * @param userName 用户名称
     * @return 结果
     */
    public String checkUserNameUnique(String userName);

    /**
     * 校验用户编码是否唯一
     *
     * @param userCode 用户编码
     * @return 结果
     */
    public String checkUserCodeUnique(String userCode);

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkPhoneUnique(SysUser user);

    /**
     * 校验email是否唯一
     *
     * @param user 用户信息
     * @return 结果
     */
    public String checkEmailUnique(String email);

    /**
     * 校验用户是否允许操作
     * 
     * @param user 用户信息
     */
    public void checkUserAllowed(SysUser user);

    public void checkUserAllowed(String userId);

    /**
     * 新增用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    public int insertUser(SysUser user);

    /**
     * 修改用户信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    public int updateUser(SysUser user);

    /**
     * 修改用户状态
     * 
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserStatus(SysUser user);

    /**
     * 锁定状态更改
     *
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserIsLock(SysUser user);

    /**
     * 修改用户基本信息
     * 
     * @param user 用户信息
     * @return 结果
     */
    public int updateUserProfile(SysUser user);

    /**
     * 修改用户头像
     * 
     * @param userName 用户名
     * @param avatar 头像地址
     * @return 结果
     */
    public boolean updateUserAvatar(String userName, String avatar);

    /**
     * 重置用户密码
     * 
     * @param user 用户信息
     * @return 结果
     */
    public int resetPwd(SysUser user);

    /**
     * 重置用户密码
     * 
     * @param userName 用户名
     * @param password 密码
     * @return 结果
     */
    public int resetUserPwd(String userName, String password);

    /**
     * 通过用户ID删除用户
     * 
     * @param userId 用户ID
     * @return 结果
     */
    public int deleteUserById(String userId);

    /**
     * 批量删除用户信息
     * 
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    public int deleteUserByIds(String[] userIds);

    /**
     * 导入用户数据
     * 
     * @param userList 用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName 操作用户
     * @return 结果
     */
    public String importUser(List<SysUser> userList, Boolean isUpdateSupport, String operName);


    /**
     * 离职状态更改
     */
    int resignation(SysUser user);
    /**
     * 添加用户信息（数据同步）
     */
    public AjaxResult insertSysUser(SysUserJson sysUserJson);
    /**
     * 用户数据同步（删除）
     */
//    public AjaxResult deleteSysUserById(SysUserJson sysUserJson);
    /**
     * 用户数据同步(更新)
     */
//    public AjaxResult updateSysUser(SysUserJson sysUserJson);

    /**
     * 员工视图创建
     */
    int userViewAdd(UserViewManage userViewManage);

    /**
     * 员工视图展示
     */
    List<UserViewManage> userViewList();

    /**
     * 视图内容展示
     */
    UserViewManage viewContentDisplay(String viewId);

    /**
     * 员工视图修改
     */
    int userViewEdit(UserViewManage userViewManage);


    /**
     * 删除用户视图
     */
    int userViewDelete(String userViewId);


    /**
     * 添加组织管理员
     */
    int addOrganAdmin(String[] userIds);

    /**
     * 获取登录用户的组织内人员列表
     */
    List<SysUser>  listByLoginIn(String deptId);

    /**
     * 单点登录
     */
    public String ssoUrl(String id_token, String redirect_url, Model model, HttpServletRequest request);
    /**
     *新增用户应用信息
     */
    public AjaxResult insertApplyUser(Long applyId,String[] deptIds);
    /**
     * 用户的禁用启动
     */
    public AjaxResult disableStartup(SysUser sysUser);
    /**
     * 给用户设置角色接口
     */
    public AjaxResult setTheRole(SysUser sysUser);
    /**
     * 给用户设置角色数据回显接口
     */
    public AjaxResult setRoleDataEcho(SysUser sysUser);

    List<SysUser> selectDeveloperList();

    AjaxResult addThirdPartyInfo(String appUserName, String appKey, String appSecret, ThirdPartyLoginBody body);

    List<SysUser> linkedRoleList(Map parms);

}
