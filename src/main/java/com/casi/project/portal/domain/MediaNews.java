package com.casi.project.portal.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @Description:多图文素材实体类
 * @author zhongzj
 * @date 2019/11/16
 */
@JsonIgnoreProperties(value = "handler")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MediaNews {


  /**
   * 作者
   */

  private String author;


  /**
   * 图文信息集合
   */

  /**
   * 主键id
   */
  private Long id;


  /**
   * 栏目id
   */
  private Long columnId;


  /**
   * 创建时间
   */
  private Date createTime;
  /**
   * 更新时间
   */
  private Date updateTime;
  /**
   * 首个图文标题
   */
  private String firstTitle;
  /**
   * 首个封面图片url
   */
  private String firstPicUrl;
  /**
   * 图文信息集合
   */
  private List<MediaArticle> articleList;



}
