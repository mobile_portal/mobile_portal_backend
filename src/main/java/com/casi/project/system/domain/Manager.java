package com.casi.project.system.domain;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class Manager {
    private String value;
    private String displayName;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getValue() {

        return value;
    }

    public void setValue(String value) {
        this.value = value;

    }

    @Override
    public String toString() {
        return "Manager{" +
                "value='" + value + '\'' +
                ", displayName='" + displayName + '\'' +
                '}';
    }
}
