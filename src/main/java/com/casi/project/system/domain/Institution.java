package com.casi.project.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class Institution {
    private String id;
    private String name;
    private String description;
    private String externalId;
    private String parentDirectory;
    private String parentExternalId;
    private String type;
    private String regionId;
    private Boolean rootNode;
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm")
    private Date lastModifyTime;
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm")
    private Date createTime;
    private String[] managerUDAccountUuids;
    private String uuid;
    private Integer levelNumber;

    @Override
    public String toString() {
        return "Institution{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", externalId='" + externalId + '\'' +
                ", parentDirectory='" + parentDirectory + '\'' +
                ", parentExternalId='" + parentExternalId + '\'' +
                ", type='" + type + '\'' +
                ", regionId='" + regionId + '\'' +
                ", rootNode=" + rootNode +
                ", lastModifyTime=" + lastModifyTime +
                ", createTime=" + createTime +
                ", managerUDAccountUuids=" + Arrays.toString(managerUDAccountUuids) +
                ", uuid='" + uuid + '\'' +
                ", levelNumber=" + levelNumber +
                '}';
    }

    public Boolean getRootNode() {
        return rootNode;
    }

    public void setRootNode(Boolean rootNode) {
        this.rootNode = rootNode;
    }

    public Date getLastModifyTime() {
        return lastModifyTime;
    }

    public void setLastModifyTime(Date lastModifyTime) {
        this.lastModifyTime = lastModifyTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String[] getManagerUDAccountUuids() {
        return managerUDAccountUuids;
    }

    public void setManagerUDAccountUuids(String[] managerUDAccountUuids) {
        this.managerUDAccountUuids = managerUDAccountUuids;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Integer getLevelNumber() {
        return levelNumber;
    }

    public void setLevelNumber(Integer levelNumber) {
        this.levelNumber = levelNumber;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getParentExternalId() {
        return parentExternalId;
    }

    public void setParentExternalId(String parentExternalId) {
        this.parentExternalId = parentExternalId;
    }

    public String getParentDirectory() {
        return parentDirectory;
    }

    public void setParentDirectory(String parentDirectory) {
        this.parentDirectory = parentDirectory;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    /*组织机构的名称*//*
    private String organization;
    *//*是否是根节点*//*
    private String rootNode;
    *//*所属父级组织机构的uuid或外部ID*//*
    private String parentUuid;
    *//*本组织机构的uuid或外部ID*//*
    private String organizationUuid;
    *//*组织机构所属的区域id,type为SELF_OU(自建组织机构)时有可能会有值,可为空,type为DEPARTMENT("自建部门")不会出现值*//*
    private String regionId;
    *//*SELF_OU(自建组织机构)或DEPARTMENT("自建部门")*//*
    private String type;
    private List<Manager> manager;

    public List<Manager> getManager() {
        return manager;
    }

    public void setManager(List<Manager> manager) {
        this.manager = manager;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegionId() {

        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getOrganizationUuid() {

        return organizationUuid;
    }

    public void setOrganizationUuid(String organizationUuid) {
        this.organizationUuid = organizationUuid;
    }

    public String getParentUuid() {

        return parentUuid;
    }

    public void setParentUuid(String parentUuid) {
        this.parentUuid = parentUuid;
    }

    public String getRootNode() {

        return rootNode;
    }

    public void setRootNode(String rootNode) {
        this.rootNode = rootNode;
    }

    public String getOrganization() {

        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    @Override
    public String toString() {
        return "Institution{" +
                "organization='" + organization + '\'' +
                ", rootNode='" + rootNode + '\'' +
                ", parentUuid='" + parentUuid + '\'' +
                ", organizationUuid='" + organizationUuid + '\'' +
                ", regionId='" + regionId + '\'' +
                ", type='" + type + '\'' +
                ", manager=" + manager +
                '}';
    }*/
}
