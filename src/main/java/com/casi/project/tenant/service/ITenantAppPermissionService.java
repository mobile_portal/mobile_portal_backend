package com.casi.project.tenant.service;

import java.util.List;
import com.casi.project.tenant.domain.TenantAppPermission;

/**
 * 租户自定义应用角色权限Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface ITenantAppPermissionService 
{
    /**
     * 查询租户自定义应用角色权限
     * 
     * @param uuid 租户自定义应用角色权限ID
     * @return 租户自定义应用角色权限
     */
    public TenantAppPermission selectTenantAppPermissionById(String uuid);

    /**
     * 查询租户自定义应用角色权限列表
     * 
     * @param tenantAppPermission 租户自定义应用角色权限
     * @return 租户自定义应用角色权限集合
     */
    public List<TenantAppPermission> selectTenantAppPermissionList(TenantAppPermission tenantAppPermission);

    /**
     * 新增租户自定义应用角色权限
     * 
     * @param tenantAppPermission 租户自定义应用角色权限
     * @return 结果
     */
    public int insertTenantAppPermission(TenantAppPermission tenantAppPermission);

    /**
     * 修改租户自定义应用角色权限
     * 
     * @param tenantAppPermission 租户自定义应用角色权限
     * @return 结果
     */
    public int updateTenantAppPermission(TenantAppPermission tenantAppPermission);

    /**
     * 批量删除租户自定义应用角色权限
     * 
     * @param uuids 需要删除的租户自定义应用角色权限ID
     * @return 结果
     */
    public int deleteTenantAppPermissionByIds(String[] uuids);

    /**
     * 删除租户自定义应用角色权限信息
     * 
     * @param uuid 租户自定义应用角色权限ID
     * @return 结果
     */
    public int deleteTenantAppPermissionById(String uuid);
}
