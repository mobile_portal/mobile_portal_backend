package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreAppComment;
import com.casi.project.store.service.IStoreAppCommentService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用评论Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用评论"})
@RestController
@RequestMapping("/store/comment")
public class StoreAppCommentController extends BaseController
{
    @Autowired
    private IStoreAppCommentService storeAppCommentService;

    /**
     * 查询应用评论列表
     */
    @ApiOperation(value = "查询应用评论列表")
    @PreAuthorize("@ss.hasPermi('store:comment:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreAppComment storeAppComment)
    {
        startPage();
        List<StoreAppComment> list = storeAppCommentService.selectStoreAppCommentList(storeAppComment);
        return getDataTable(list);
    }

    /**
     * 导出应用评论列表
     */
    @PreAuthorize("@ss.hasPermi('store:comment:export')")
    @Log(title = "应用评论", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreAppComment storeAppComment)
    {
        List<StoreAppComment> list = storeAppCommentService.selectStoreAppCommentList(storeAppComment);
        ExcelUtil<StoreAppComment> util = new ExcelUtil<StoreAppComment>(StoreAppComment.class);
        return util.exportExcel(list, "comment");
    }

    /**
     * 获取应用评论详细信息
     */
    @ApiOperation(value = "获取应用评论详细信息")
    @PreAuthorize("@ss.hasPermi('store:comment:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppCommentService.selectStoreAppCommentById(uuid));
    }

    /**
     * 新增应用评论
     */
    @ApiOperation(value = "新增应用评论")
    @PreAuthorize("@ss.hasPermi('store:comment:add')")
    @Log(title = "应用评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreAppComment storeAppComment)
    {
        return toAjax(storeAppCommentService.insertStoreAppComment(storeAppComment));
    }

    /**
     * 修改应用评论
     */
    @ApiOperation(value = "修改应用评论")
    @PreAuthorize("@ss.hasPermi('store:comment:edit')")
    @Log(title = "应用评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreAppComment storeAppComment)
    {
        return toAjax(storeAppCommentService.updateStoreAppComment(storeAppComment));
    }

    /**
     * 删除应用评论
     */
    @ApiOperation(value = "删除应用评论")
    @PreAuthorize("@ss.hasPermi('store:comment:remove')")
    @Log(title = "应用评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppCommentService.deleteStoreAppCommentByIds(uuids));
    }
}
