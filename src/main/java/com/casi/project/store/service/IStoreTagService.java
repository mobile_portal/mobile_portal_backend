package com.casi.project.store.service;

import java.util.List;
import com.casi.project.store.domain.StoreTag;

/**
 * 默认标签Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface IStoreTagService 
{
    /**
     * 查询默认标签
     * 
     * @param uuid 默认标签ID
     * @return 默认标签
     */
    public StoreTag selectStoreTagById(String uuid);

    /**
     * 查询默认标签列表
     * 
     * @param storeTag 默认标签
     * @return 默认标签集合
     */
    public List<StoreTag> selectStoreTagList(StoreTag storeTag);

    /**
     * 新增默认标签
     * 
     * @param storeTag 默认标签
     * @return 结果
     */
    public int insertStoreTag(StoreTag storeTag);

    /**
     * 修改默认标签
     * 
     * @param storeTag 默认标签
     * @return 结果
     */
    public int updateStoreTag(StoreTag storeTag);

    /**
     * 批量删除默认标签
     * 
     * @param uuids 需要删除的默认标签ID
     * @return 结果
     */
    public int deleteStoreTagByIds(String[] uuids);

    /**
     * 删除默认标签信息
     * 
     * @param uuid 默认标签ID
     * @return 结果
     */
    public int deleteStoreTagById(String uuid);
}
