package com.casi.project.portal.domain.thirdParty;

import lombok.Data;

/**
 *  第三方用户登录对象
 *
 */
@Data
public class UserInfo
{

    private String username;
    private String userUuid;
    private String fullName;
    private String phone;
    private String departmentName;
    private String displayName;



}
