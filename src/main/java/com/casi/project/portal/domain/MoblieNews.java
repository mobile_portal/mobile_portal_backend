package com.casi.project.portal.domain;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther shp
 * @data2020/4/2110:43
 * @description
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class MoblieNews {

    private Integer id;
    private String newsTitle;
    private String   newsImg;
    private String  newsDescript;
    private String   createTime;
    private String   updateTime;
    private String  newsSource;
    private String  newsUrl;



}
