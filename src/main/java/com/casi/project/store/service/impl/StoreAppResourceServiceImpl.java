package com.casi.project.store.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppResourceMapper;
import com.casi.project.store.domain.StoreAppResource;
import com.casi.project.store.service.IStoreAppResourceService;

/**
 * 应用资源Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppResourceServiceImpl implements IStoreAppResourceService 
{
    @Autowired
    private StoreAppResourceMapper storeAppResourceMapper;

    /**
     * 查询应用资源
     * 
     * @param uuid 应用资源ID
     * @return 应用资源
     */
    @Override
    public StoreAppResource selectStoreAppResourceById(String uuid)
    {
        return storeAppResourceMapper.selectStoreAppResourceById(uuid);
    }

    /**
     * 查询应用资源列表
     * 
     * @param storeAppResource 应用资源
     * @return 应用资源
     */
    @Override
    public List<StoreAppResource> selectStoreAppResourceList(StoreAppResource storeAppResource)
    {
        return storeAppResourceMapper.selectStoreAppResourceList(storeAppResource);
    }

    /**
     * 新增应用资源
     * 
     * @param storeAppResource 应用资源
     * @return 结果
     */
    @Override
    public int insertStoreAppResource(StoreAppResource storeAppResource)
    {
        return storeAppResourceMapper.insertStoreAppResource(storeAppResource);
    }

    /**
     * 修改应用资源
     * 
     * @param storeAppResource 应用资源
     * @return 结果
     */
    @Override
    public int updateStoreAppResource(StoreAppResource storeAppResource)
    {
        return storeAppResourceMapper.updateStoreAppResource(storeAppResource);
    }

    /**
     * 批量删除应用资源
     * 
     * @param uuids 需要删除的应用资源ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppResourceByIds(String[] uuids)
    {
        return storeAppResourceMapper.deleteStoreAppResourceByIds(uuids);
    }

    /**
     * 删除应用资源信息
     * 
     * @param uuid 应用资源ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppResourceById(String uuid)
    {
        return storeAppResourceMapper.deleteStoreAppResourceById(uuid);
    }
}
