package com.casi.project.portal.util;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class JavaScriptUtils {

	/**
	 * 获取解析结果
	 * @return
	 */
	public static ScriptEngine getJavaScriptEngine(){
		ScriptEngineManager sem = new ScriptEngineManager();
		ScriptEngine engine=sem.getEngineByName("javascript");
		return engine;
	}
	
	
	/*
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
*/
}
