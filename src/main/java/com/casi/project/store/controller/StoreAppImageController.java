package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreAppImage;
import com.casi.project.store.service.IStoreAppImageService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用图片Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用图片"})
@RestController
@RequestMapping("/store/image")
public class StoreAppImageController extends BaseController
{
    @Autowired
    private IStoreAppImageService storeAppImageService;

    /**
     * 查询应用图片列表
     */
    @ApiOperation(value = "查询应用图片列表")
    @PreAuthorize("@ss.hasPermi('store:image:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreAppImage storeAppImage)
    {
        startPage();
        List<StoreAppImage> list = storeAppImageService.selectStoreAppImageList(storeAppImage);
        return getDataTable(list);
    }

    /**
     * 导出应用图片列表
     */
    @PreAuthorize("@ss.hasPermi('store:image:export')")
    @Log(title = "应用图片", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreAppImage storeAppImage)
    {
        List<StoreAppImage> list = storeAppImageService.selectStoreAppImageList(storeAppImage);
        ExcelUtil<StoreAppImage> util = new ExcelUtil<StoreAppImage>(StoreAppImage.class);
        return util.exportExcel(list, "image");
    }

    /**
     * 获取应用图片详细信息
     */
    @ApiOperation(value = "获取应用图片详细信息")
    @PreAuthorize("@ss.hasPermi('store:image:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppImageService.selectStoreAppImageById(uuid));
    }

    /**
     * 新增应用图片
     */
    @ApiOperation(value = "新增应用图片")
    @PreAuthorize("@ss.hasPermi('store:image:add')")
    @Log(title = "应用图片", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreAppImage storeAppImage)
    {
        return toAjax(storeAppImageService.insertStoreAppImage(storeAppImage));
    }

    /**
     * 修改应用图片
     */
    @ApiOperation(value = "修改应用图片")
    @PreAuthorize("@ss.hasPermi('store:image:edit')")
    @Log(title = "应用图片", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreAppImage storeAppImage)
    {
        return toAjax(storeAppImageService.updateStoreAppImage(storeAppImage));
    }

    /**
     * 删除应用图片
     */
    @ApiOperation(value = "删除应用图片")
    @PreAuthorize("@ss.hasPermi('store:image:remove')")
    @Log(title = "应用图片", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppImageService.deleteStoreAppImageByIds(uuids));
    }
}
