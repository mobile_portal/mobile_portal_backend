package com.casi.project.portal.util;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * @name:BeanMapperUtil.class
 * @description:bean map 转换类
 * @author:dingjianwei
 * @date:2016年11月10日 下午4:52:30
 * @location:cn.com.casic.util.BeanMapperUtil.class
 * Copyright 2015 by Jusfoun.com. All Right Reserved
 */
public class BeanMapperUtil {
	
	
	/**
	 * 
	 * @title: transBean2Map  
	 * @description: 利用Introspector和PropertyDescriptor 将Bean --> Map  
	 * @author: dingjianwei
	 * @date:2016年11月10日 下午4:55:01  
	 * @param obj
	 * @return
	 */
    public static Map<String, Object> transBean2Map(Object obj) {  
        if(obj == null){  
            return null;  
        }          
        Map<String, Object> map = new HashMap<String, Object>();  
        try {  
            BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());  
            PropertyDescriptor[] propertyDescriptors = beanInfo.getPropertyDescriptors();  
            for (PropertyDescriptor property : propertyDescriptors) {  
                String key = property.getName();  
  
                // 过滤class属性  
                if (!key.equals("class")) {  
                    // 得到property对应的getter方法  
                    Method getter = property.getReadMethod();  
                    Object value = getter.invoke(obj);  
                    map.put(key, value);  
                }  
            }  
        } catch (Exception e) {  
            System.out.println("transBean2Map Error " + e);  
        }  
        return map;  
    }  
}
