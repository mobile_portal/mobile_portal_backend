package com.casi.project.portal.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther shp
 * @data2020/4/1317:04
 * @description
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class MobileNotice implements Serializable {


    @ApiModelProperty("主键")
    private String id;
    @ApiModelProperty("公告标题")
    private String title;
    @ApiModelProperty("发布时间")
    @JsonFormat(locale = "zh",timezone = "GMT+8",pattern = "yyyy-MM-dd")
    private Date pushTime;
    @ApiModelProperty("发布部门ID，外键sys_dept")
    private Long pushDept;
    @ApiModelProperty("公告内容")
    private String notices;
    @ApiModelProperty("0-未发布，1-已发布")
    private Integer isPublish;
    @ApiModelProperty("前端是否显示0-显示,1-隐藏")
    private Integer isShow;
    @ApiModelProperty("创建时间")
    private Date createTime;
    @ApiModelProperty("更新时间")
    private Date updateTime;
    @ApiModelProperty("发布人")
    private String pushByUser;
    @ApiModelProperty("部门名称")
    private String deptName;


}
