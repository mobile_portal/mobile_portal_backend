package com.casi.project.tenant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.tenant.mapper.TenantAppPermissionMapper;
import com.casi.project.tenant.domain.TenantAppPermission;
import com.casi.project.tenant.service.ITenantAppPermissionService;

/**
 * 租户自定义应用角色权限Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class TenantAppPermissionServiceImpl implements ITenantAppPermissionService 
{
    @Autowired
    private TenantAppPermissionMapper tenantAppPermissionMapper;

    /**
     * 查询租户自定义应用角色权限
     * 
     * @param uuid 租户自定义应用角色权限ID
     * @return 租户自定义应用角色权限
     */
    @Override
    public TenantAppPermission selectTenantAppPermissionById(String uuid)
    {
        return tenantAppPermissionMapper.selectTenantAppPermissionById(uuid);
    }

    /**
     * 查询租户自定义应用角色权限列表
     * 
     * @param tenantAppPermission 租户自定义应用角色权限
     * @return 租户自定义应用角色权限
     */
    @Override
    public List<TenantAppPermission> selectTenantAppPermissionList(TenantAppPermission tenantAppPermission)
    {
        return tenantAppPermissionMapper.selectTenantAppPermissionList(tenantAppPermission);
    }

    /**
     * 新增租户自定义应用角色权限
     * 
     * @param tenantAppPermission 租户自定义应用角色权限
     * @return 结果
     */
    @Override
    public int insertTenantAppPermission(TenantAppPermission tenantAppPermission)
    {
        return tenantAppPermissionMapper.insertTenantAppPermission(tenantAppPermission);
    }

    /**
     * 修改租户自定义应用角色权限
     * 
     * @param tenantAppPermission 租户自定义应用角色权限
     * @return 结果
     */
    @Override
    public int updateTenantAppPermission(TenantAppPermission tenantAppPermission)
    {
        return tenantAppPermissionMapper.updateTenantAppPermission(tenantAppPermission);
    }

    /**
     * 批量删除租户自定义应用角色权限
     * 
     * @param uuids 需要删除的租户自定义应用角色权限ID
     * @return 结果
     */
    @Override
    public int deleteTenantAppPermissionByIds(String[] uuids)
    {
        return tenantAppPermissionMapper.deleteTenantAppPermissionByIds(uuids);
    }

    /**
     * 删除租户自定义应用角色权限信息
     * 
     * @param uuid 租户自定义应用角色权限ID
     * @return 结果
     */
    @Override
    public int deleteTenantAppPermissionById(String uuid)
    {
        return tenantAppPermissionMapper.deleteTenantAppPermissionById(uuid);
    }
}
