package com.casi.project.store.service;

import java.util.List;
import com.casi.project.store.domain.StoreAppResource;

/**
 * 应用资源Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface IStoreAppResourceService 
{
    /**
     * 查询应用资源
     * 
     * @param uuid 应用资源ID
     * @return 应用资源
     */
    public StoreAppResource selectStoreAppResourceById(String uuid);

    /**
     * 查询应用资源列表
     * 
     * @param storeAppResource 应用资源
     * @return 应用资源集合
     */
    public List<StoreAppResource> selectStoreAppResourceList(StoreAppResource storeAppResource);

    /**
     * 新增应用资源
     * 
     * @param storeAppResource 应用资源
     * @return 结果
     */
    public int insertStoreAppResource(StoreAppResource storeAppResource);

    /**
     * 修改应用资源
     * 
     * @param storeAppResource 应用资源
     * @return 结果
     */
    public int updateStoreAppResource(StoreAppResource storeAppResource);

    /**
     * 批量删除应用资源
     * 
     * @param uuids 需要删除的应用资源ID
     * @return 结果
     */
    public int deleteStoreAppResourceByIds(String[] uuids);

    /**
     * 删除应用资源信息
     * 
     * @param uuid 应用资源ID
     * @return 结果
     */
    public int deleteStoreAppResourceById(String uuid);
}
