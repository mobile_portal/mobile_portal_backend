package com.casi.project.screen.controller;

import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.screen.domain.*;
import com.casi.project.screen.service.IYearService;
import com.casi.project.screen.util.DefaultLoadUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Auther shp
 * @data2020/5/813:05
 * @description
 */

@RestController
@RequestMapping("/portal/screen")
@Api(tags = "大屏展示相关接口")
public class YearTotalController extends BaseController {


    @Autowired
    private IYearService iYearService;


    /**
     * 获取当前时间
     *
     * @return
     */

    @GetMapping("ParseException")
    @ApiOperation("获取当前时间")
    public String ParseException(){

        return new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString();
    }

    @GetMapping("yearInput")
    @ApiOperation("年度收入类")
    public  Map<String,Object> yearInput(String code)
    {
        code=DefaultLoadUtil.isDefault(code);


        /**
         * 01收款
         * 02付款
         * @param CON_CREDDEBRT
         * @return
         */

        Map<String,Object> objectObjectHashMap = new LinkedHashMap<>();

        String CON_CREDDEBRT="01";

        objectObjectHashMap.put("sign",DefaultLoadUtil.toMillion(iYearService.YearNoneyTotal(CON_CREDDEBRT,code)!=null?Long.valueOf(String.valueOf(iYearService.YearNoneyTotal(CON_CREDDEBRT,code))).longValue():0L));
        objectObjectHashMap.put("moneyBack",DefaultLoadUtil.toMillion(iYearService.moneyBackTotal(CON_CREDDEBRT,code)!=null?Long.valueOf(String.valueOf(iYearService.moneyBackTotal(CON_CREDDEBRT,code))).longValue():0));
        objectObjectHashMap.put("receivable",DefaultLoadUtil.toMillion(iYearService.answerMoneyTotal(CON_CREDDEBRT,code)!=null?Long.valueOf(String.valueOf(iYearService.answerMoneyTotal(CON_CREDDEBRT,code))).longValue():0));
        objectObjectHashMap.put("conduct",iYearService.findAddToContract2(CON_CREDDEBRT,code)!=null?Long.valueOf(String.valueOf(iYearService.findAddToContract2(CON_CREDDEBRT,code))).longValue():0);
        objectObjectHashMap.put("currency",DefaultLoadUtil.toMillion(iYearService.findcoreignCurrency(CON_CREDDEBRT,code)!=null?Long.valueOf(String.valueOf(iYearService.findcoreignCurrency(CON_CREDDEBRT,code))).longValue():0));


        return objectObjectHashMap;

    }

    @GetMapping("yearOut")
    @ApiOperation("年度支出类")
    public Map<String,Object>  yearOut(String code)
    {

        code=DefaultLoadUtil.isDefault(code);

        /**
         * 01收款
         * 02付款
         * @param CON_CREDDEBRT
         * @return
         */


        Map<String,Object> objectObjectHashMap = new LinkedHashMap<>();

        String CON_CREDDEBRT="02";

        objectObjectHashMap.put("sign",DefaultLoadUtil.toMillion(iYearService.YearNoneyTotal(CON_CREDDEBRT,code)!=null?Long.valueOf(String.valueOf(iYearService.YearNoneyTotal(CON_CREDDEBRT,code))).longValue():0L));
        objectObjectHashMap.put("moneyBack",DefaultLoadUtil.toMillion(iYearService.moneyBackTotal(CON_CREDDEBRT,code)!=null?Long.valueOf(String.valueOf(iYearService.moneyBackTotal(CON_CREDDEBRT,code))).longValue():0));
        objectObjectHashMap.put("receivable",DefaultLoadUtil.toMillion(iYearService.answerMoneyTotal(CON_CREDDEBRT,code)!=null?Long.valueOf(String.valueOf(iYearService.answerMoneyTotal(CON_CREDDEBRT,code))).longValue():0));
        objectObjectHashMap.put("conduct",iYearService.findAddToContract2(CON_CREDDEBRT,code)!=null?Long.valueOf(String.valueOf(iYearService.findAddToContract2(CON_CREDDEBRT,code))).longValue():0);
        objectObjectHashMap.put("currency",DefaultLoadUtil.toMillion(iYearService.findcoreignCurrency(CON_CREDDEBRT,code)!=null?Long.valueOf(String.valueOf(iYearService.findcoreignCurrency(CON_CREDDEBRT,code))).longValue():0));

        return objectObjectHashMap;

    }

    @GetMapping("yearTotal2")
    @ApiOperation("合同签约量及金额统计")
    public YearContractedVolumeVo yearTotal2(String code)
    {

        code=DefaultLoadUtil.isDefault(code);

        return iYearService.yearTotal2(code);

    }



    @GetMapping("performLevel")
    @ApiOperation("合同级别分布")
    public List<YearStaus> performLevel(String code)
    {

        code=DefaultLoadUtil.isDefault(code);

        List<YearStaus> list=new ArrayList();

        List<YearStaus> distribution = iYearService.distribution(code);

        if(distribution.size()==0){
            distribution.add(new YearStaus("一级合同",0L));
            distribution.add(new YearStaus("二级合同",0L));
            distribution.add(new YearStaus("三级合同",0L));
        }

        for (YearStaus yearStaus : distribution) {
            if(yearStaus.getName().equals("一级合同")){
                list.add(new YearStaus("一级合同",yearStaus.getValue()));
            }
        }
        for (YearStaus yearStaus : distribution) {
            if(yearStaus.getName().equals("二级合同")){
                list.add(new YearStaus("二级合同",yearStaus.getValue()));
            }
        }
        for (YearStaus yearStaus : distribution) {
            if(yearStaus.getName().equals("三级合同")){
                list.add(new YearStaus("三级合同",yearStaus.getValue()));
            }
        }

        return list;
    }

    @GetMapping("makeTotal")
    @ApiOperation("合同收款统计")
    public List<List<Integer>> makeTotal(String code)
    {
        code=DefaultLoadUtil.isDefault(code);


        List<List<Integer>> lists = new ArrayList();

        List<YearTotalVo> deferred =iYearService.deferredCollection(code);
        if(deferred.get(0)==null){
            return null;
        }
        List<YearTotalVo> yearTotalVoList = iYearService.inputTotal(code);
        if(yearTotalVoList.get(0)==null){
            return null;
        }

        yearTotalVoList.addAll(deferred);

        for (YearTotalVo year : yearTotalVoList) {
            List<Integer> integers=new ArrayList<>();
            integers.add(year.getJanuary());
            integers.add(year.getFebruary());
            integers.add(year.getMarch());
            integers.add(year.getApril());
            integers.add(year.getMay());
            integers.add(year.getJune());
            integers.add(year.getJuly());
            integers.add(year.getAugust());
            integers.add(year.getSeptember());
            integers.add(year.getOctober());
            integers.add(year.getNovember());
            integers.add(year.getDecember());
            lists.add(integers);
        }

        return lists;
    }

    @GetMapping("payMentTotal")
    @ApiOperation("合同付款统计")
    public List<List<Integer>> payMentTotal(String code)
    {

        code=DefaultLoadUtil.isDefault(code);
        List<List<Integer>> lists = new ArrayList();

        List<YearTotalVo> yearTotalVoList = iYearService.outTotal(code);

        if(yearTotalVoList.size()<2){
            yearTotalVoList.add(new YearTotalVo());
        }

        for (YearTotalVo year : yearTotalVoList) {
            List<Integer> integers=new ArrayList<>();
            integers.add(year.getJanuary()==null?0:year.getJanuary());
            integers.add(year.getFebruary()==null?0:year.getFebruary());
            integers.add(year.getMarch()==null?0:year.getMarch());
            integers.add(year.getApril()==null?0:year.getApril());
            integers.add(year.getMay()==null?0:year.getMay());
            integers.add(year.getJune()==null?0:year.getJune());
            integers.add(year.getJuly()==null?0:year.getJuly());
            integers.add(year.getAugust()==null?0:year.getAugust());
            integers.add(year.getSeptember()==null?0:year.getSeptember());
            integers.add(year.getOctober()==null?0:year.getOctober());
            integers.add(year.getNovember()==null?0:year.getNovember());
            integers.add(year.getDecember()==null?0:year.getDecember());
            lists.add(integers);
        }

        return lists;
    }

    @GetMapping("monitor")
    @ApiOperation("合同数据动态监控")
    public Map<String,Object> monitor(String code)
    {
        code=DefaultLoadUtil.isDefault(code);

        Map<String,Object> objectObjectHashMap = new LinkedHashMap<>();

        objectObjectHashMap.put("sign",iYearService.findAddContract(code)!=null?Long.valueOf(String.valueOf(iYearService.findAddContract(code))).longValue():0L);
        objectObjectHashMap.put("moneyBack",iYearService.findCarryForward(code)!=null?Long.valueOf(String.valueOf(iYearService.findCarryForward(code))).longValue():0L);
        objectObjectHashMap.put("receivable",iYearService.findAddToContract(code)!=null?Long.valueOf(String.valueOf(iYearService.findAddToContract(code))).longValue():0L);
        objectObjectHashMap.put("conduct",DefaultLoadUtil.toMillion(iYearService.findTotalOverdue(code)!=null?Long.valueOf(String.valueOf(iYearService.findTotalOverdue(code))).longValue():0L));

        return objectObjectHashMap;
    }


    @GetMapping("performState")
    @ApiOperation("合同履行状态")
    public List<YearStaus> performState(String  code)
    {
        code=DefaultLoadUtil.isDefault(code);

        return iYearService.performStaus(code);
    }


    @GetMapping("findSign")
    @ApiOperation("合同签订趋势")
    public List<List<Integer>> findSign(String code)
    {

        code=DefaultLoadUtil.isDefault(code);
        List<List<Integer>> lists = new ArrayList<List<Integer>>();

        List<YearTotalVo> yearTotalVoList = iYearService.findSign(code);
        for (YearTotalVo year : yearTotalVoList) {
            List<Integer> integers=new ArrayList<>();
            integers.add(year.getJanuary());
            integers.add(year.getFebruary());
            integers.add(year.getMarch());
            integers.add(year.getApril());
            integers.add(year.getMay());
            integers.add(year.getJune());
            integers.add(year.getJuly());
            integers.add(year.getAugust());
            integers.add(year.getSeptember());
            integers.add(year.getOctober());
            integers.add(year.getNovember());
            integers.add(year.getDecember());
            lists.add(integers);
        }

        return lists;
    }

    @GetMapping("PayMent")
    @ApiOperation("合同收付款计划")
    public List<List<Integer>> findPayMent(String code)
    {
        code=DefaultLoadUtil.isDefault(code);
        List<List<Integer>> lists = new ArrayList();

        List<YearTotalVo> yearTotalVoList = iYearService.findPayMent(code);
        if(yearTotalVoList==null){
            return null;
        }

        for (YearTotalVo year : yearTotalVoList) {
            List<Integer> integers=new ArrayList<>();
            integers.add(year.getJanuary());
            integers.add(year.getFebruary());
            integers.add(year.getMarch());
            integers.add(year.getApril());
            integers.add(year.getMay());
            integers.add(year.getJune());
            integers.add(year.getJuly());
            integers.add(year.getAugust());
            integers.add(year.getSeptember());
            integers.add(year.getOctober());
            integers.add(year.getNovember());
            integers.add(year.getDecember());
            lists.add(integers);
        }
        return lists;
    }

    @GetMapping("conCollectionMonitor")
    @ApiOperation("数据采集监控统计")
    public AjaxResult conCollectionMonitor(){
        Map<String, Object> map = iYearService.conCollectionMonitor();
        return  AjaxResult.success("ok",map);
    }
    @GetMapping("conCollectionMonitorDe")
    @ApiOperation("数据采集监控分公司统计")
    public AjaxResult conCollectionMonitorDe(){
        List<EdwContractDataMonitor> model =  iYearService.conCollectionMonitorDe();
        return  AjaxResult.success("ok",model);
    }

    @GetMapping("sideCollection")
    @ApiOperation("对外提供数据监控")
    public AjaxResult sideCollection(){
        List<EdwDataSupplyMonitor> model =  iYearService.sideCollection();
        return  AjaxResult.success("ok",model);
    }

    @GetMapping("otherCollection")
    @ApiOperation("其他数据采集监控")
    public AjaxResult otherCollection(){
        List<EdwDataCollectMonitor> model =  iYearService.otherCollection();
        return  AjaxResult.success("ok",model);
    }


    @GetMapping("getOrganize")
    @ApiOperation("获取组织列表")
    public AjaxResult getOrganize(){
        System.out.println("zuzhiliebiao ======");
        List<DimUnit> model =  iYearService.getOrganize();
        return  AjaxResult.success("ok",model);
    }
}


