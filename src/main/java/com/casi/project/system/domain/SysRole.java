package com.casi.project.system.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.casi.project.portal.domain.RoleApplyResource;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.aspectj.lang.annotation.Excel.ColumnType;
import com.casi.framework.web.domain.BaseEntity;

import java.util.Arrays;
import java.util.List;

/**
 * 角色表 sys_role
 *
 * @author lzb
 */
public class SysRole extends BaseEntity {
    private static final long serialVersionUID = 1L;


    /**
     * 角色ID
     */
    @Excel(name = "角色序号", cellType = ColumnType.NUMERIC)
    private String roleId;

    /**
     * 角色名称
     */
    @Excel(name = "角色名称")
    private String roleName;

    /**
     * 角色权限
     */
    @Excel(name = "角色权限")
    private String roleKey;

    /**
     * 角色排序
     */
    @Excel(name = "角色排序")
    private String roleSort;

    /**
     * 数据范围（1：所有数据权限；2：自定义数据权限；3：本部门数据权限；4：本部门及以下数据权限）
     */
    @Excel(name = "数据范围", readConverterExp = "1=所有数据权限,2=自定义数据权限,3=本部门数据权限,4=本部门及以下数据权限")
    private String dataScope;

    /**
     * 角色状态（0正常 1停用）
     */
    @Excel(name = "角色状态", readConverterExp = "0=正常,1=停用")
    private String status;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    private String delFlag;

    /**
     * 用户是否存在此角色标识 默认不存在
     */
    private boolean flag = false;

    /**
     * 菜单组
     */
    private String[] menuIds;

    private List<SysMenu> menuList;

    private String[] applyId;

    /**
     * 部门组（数据权限）
     */
    private String[] deptIds;

    /**
     * 应用资源勾选框
     */
    private String[] roleApplyResources;

    /**
     * 应用资源
     */
    private List<RoleApplyResource> roleApplyResourceList;

    // 新增
    /**
     * 祖级列表
     */
    private String roleAncestors;

    // 新增
    /**
     * 角色直属部门
     */
    private String roleBelongsDept;
    // 新增
    /**
     * 角色编码
     */
    private String roleCode;
    // 新增
    /**
     * 角色描述
     */
    private String roleDescribe;
    /**
     * 角色类型(0平台角色 1应用角色)
     */
    private String roleType;


    public SysRole() {

    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getRoleDescribe() {
        return roleDescribe;
    }

    public void setRoleDescribe(String roleDescribe) {
        this.roleDescribe = roleDescribe;
    }

    public SysRole(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public boolean isAdmin() {
        return isAdmin(this.roleId);
    }

    public static boolean isAdmin(String roleId) {
        return roleId != null && "1".equals(roleId);
    }

    @NotBlank(message = "角色名称不能为空")
    @Size(min = 0, max = 30, message = "角色名称长度不能超过30个字符")
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    /*@NotBlank(message = "权限字符不能为空")
    @Size(min = 0, max = 100, message = "权限字符长度不能超过100个字符")*/
    public String getRoleKey() {
        return roleKey;
    }

    public void setRoleKey(String roleKey) {
        this.roleKey = roleKey;
    }

    /* @NotBlank(message = "显示顺序不能为空")*/
    public String getRoleSort() {
        return roleSort;
    }

    public void setRoleSort(String roleSort) {
        this.roleSort = roleSort;
    }

    public String getDataScope() {
        return dataScope;
    }

    public void setDataScope(String dataScope) {
        this.dataScope = dataScope;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public String[] getMenuIds() {
        return menuIds;
    }

    public void setMenuIds(String[] menuIds) {
        this.menuIds = menuIds;
    }

    public String[] getDeptIds() {
        return deptIds;
    }

    public void setDeptIds(String[] deptIds) {
        this.deptIds = deptIds;
    }

    public String getRoleAncestors() {
        return roleAncestors;
    }

    public void setRoleAncestors(String roleAncestors) {
        this.roleAncestors = roleAncestors;
    }

    public String getRoleBelongsDept() {
        return roleBelongsDept;
    }

    public void setRoleBelongsDept(String roleBelongsDept) {
        this.roleBelongsDept = roleBelongsDept;
    }

    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
        this.roleCode = roleCode;
    }

    public String[] getRoleApplyResources() {
        return roleApplyResources;
    }

    public void setRoleApplyResources(String[] roleApplyResources) {
        this.roleApplyResources = roleApplyResources;
    }

    public List<RoleApplyResource> getRoleApplyResourceList() {
        return roleApplyResourceList;
    }

    public void setRoleApplyResourceList(List<RoleApplyResource> roleApplyResourceList) {
        this.roleApplyResourceList = roleApplyResourceList;
    }

    public String[] getApplyId() {
        return applyId;
    }

    public void setApplyId(String[] applyId) {
        this.applyId = applyId;
    }

    public List<SysMenu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<SysMenu> menuList) {
        this.menuList = menuList;
    }

    @Override
    public String toString() {
        return "SysRole{" +
                ", roleId='" + roleId + '\'' +
                ", roleName='" + roleName + '\'' +
                ", roleKey='" + roleKey + '\'' +
                ", roleSort='" + roleSort + '\'' +
                ", dataScope='" + dataScope + '\'' +
                ", status='" + status + '\'' +
                ", delFlag='" + delFlag + '\'' +
                ", flag=" + flag +
                ", menuIds=" + Arrays.toString(menuIds) +
                ", applyId=" + Arrays.toString(applyId) +
                ", deptIds=" + Arrays.toString(deptIds) +
                ", roleApplyResources=" + Arrays.toString(roleApplyResources) +
                ", roleApplyResourceList=" + roleApplyResourceList +
                ", roleAncestors='" + roleAncestors + '\'' +
                ", roleBelongsDept='" + roleBelongsDept + '\'' +
                ", roleCode='" + roleCode + '\'' +
                ", roleDescribe='" + roleDescribe + '\'' +
                ", roleType='" + roleType + '\'' +
                '}';
    }
}
