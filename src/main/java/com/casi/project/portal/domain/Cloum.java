package com.casi.project.portal.domain;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther shp
 * @data2020/4/2416:18
 * @description
 */


@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class Cloum {

    private String uuid;
    private Long columnId;
    private String columnName;
    private Integer isDel;
    private String createTime;
    private String updateTime;

    private Integer pageNum;
    private Integer pageSize;

}
