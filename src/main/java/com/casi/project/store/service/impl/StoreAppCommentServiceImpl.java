package com.casi.project.store.service.impl;

import java.util.List;
import com.casi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppCommentMapper;
import com.casi.project.store.domain.StoreAppComment;
import com.casi.project.store.service.IStoreAppCommentService;

/**
 * 应用评论Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppCommentServiceImpl implements IStoreAppCommentService 
{
    @Autowired
    private StoreAppCommentMapper storeAppCommentMapper;

    /**
     * 查询应用评论
     * 
     * @param uuid 应用评论ID
     * @return 应用评论
     */
    @Override
    public StoreAppComment selectStoreAppCommentById(String uuid)
    {
        return storeAppCommentMapper.selectStoreAppCommentById(uuid);
    }

    /**
     * 查询应用评论列表
     * 
     * @param storeAppComment 应用评论
     * @return 应用评论
     */
    @Override
    public List<StoreAppComment> selectStoreAppCommentList(StoreAppComment storeAppComment)
    {
        return storeAppCommentMapper.selectStoreAppCommentList(storeAppComment);
    }

    /**
     * 新增应用评论
     * 
     * @param storeAppComment 应用评论
     * @return 结果
     */
    @Override
    public int insertStoreAppComment(StoreAppComment storeAppComment)
    {
        storeAppComment.setCreateTime(DateUtils.getNowDate());
        return storeAppCommentMapper.insertStoreAppComment(storeAppComment);
    }

    /**
     * 修改应用评论
     * 
     * @param storeAppComment 应用评论
     * @return 结果
     */
    @Override
    public int updateStoreAppComment(StoreAppComment storeAppComment)
    {
        return storeAppCommentMapper.updateStoreAppComment(storeAppComment);
    }

    /**
     * 批量删除应用评论
     * 
     * @param uuids 需要删除的应用评论ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppCommentByIds(String[] uuids)
    {
        return storeAppCommentMapper.deleteStoreAppCommentByIds(uuids);
    }

    /**
     * 删除应用评论信息
     * 
     * @param uuid 应用评论ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppCommentById(String uuid)
    {
        return storeAppCommentMapper.deleteStoreAppCommentById(uuid);
    }
}
