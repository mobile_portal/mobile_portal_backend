package com.casi.project.portal.service.impl;


import com.casi.project.portal.domain.LocalFile;
import com.casi.project.portal.domain.MediaBase;
import com.casi.project.portal.domain.MediaFile;
import com.casi.project.portal.domain.StatusEnum;
import com.casi.project.portal.domain.vo.MediaBaseForFileVo;
import com.casi.project.portal.mapper.MediaBaseMapper;
import com.casi.project.portal.mapper.MediaFileMapper;
import com.casi.project.portal.service.FileService;
import com.casi.project.portal.service.MediaFileService;
import com.casi.project.portal.util.CommonApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

/**
 * @Author zhongzj
 * @Description:
 * @Create: 2019/11/18
 */
@Service
@Transactional
public class MediaFileServiceImpl implements MediaFileService {

    @Autowired
    private MediaFileMapper mediaFileMapper;

    @Autowired
    private MediaBaseMapper mediaBaseMapper;

    //文件管理服务类
    @Autowired
    private FileService fileService;

    @Override
    public List<MediaBaseForFileVo> queryList(MediaBase mediaBase) {
        List<MediaBaseForFileVo> fileVoList = mediaBaseMapper.queryListForFile(mediaBase);
        return fileVoList;
    }

    @Override
    public MediaBase queryDetail(Long id) {
        //根据baseId获取base表素材信息
        MediaBase mediaBase = mediaBaseMapper.queryById(id);
        //根据信息中的mediaId去mediaFile表获取具体信息
        mediaBase.setMediaFile(mediaFileMapper.queryById(mediaBase.getMediaId()));
        return mediaBase;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,RuntimeException.class})
    public boolean add (MediaBase mediaBase, HttpServletRequest request, String type) throws Exception{
        boolean result = false;

        // 获取用户信息
        String userId = request.getHeader("userId");
        // 获取redis中键为userId的账户信息
        //AccountInfo info = (AccountInfo) redisService.get(userId);
        //String displayName = info.getDisplayName();

                //根据文件id获取文件信息
        LocalFile localFile = fileService.queryById(mediaBase.getFileId());
        mediaBase.setFileIdList(mediaBase.getFileId().toString());

        //保存文件相关信息
        MediaFile mediaFile = new MediaFile();
        mediaFile.setName(localFile.getName());
        mediaFile.setFileName(localFile.getFileName());
        mediaFile.setSize(localFile.getSize());
        mediaFile.setLocalUrl(localFile.getFileUrl());
        //设置文件的映射路径
//        mediaFile.setUploadUrl(CommonApi.LOCAL_API + localFile.getName());//windows
        mediaFile.setUploadUrl(CommonApi.MM_API + localFile.getName());//linux
        mediaFile.setCreateTime(new Date(System.currentTimeMillis()));
        mediaFile.setUpdateTime(new Date(System.currentTimeMillis()));

        if (null != mediaBase.getTitle()){
            mediaFile.setTitle(mediaBase.getTitle());
        }
        if (null != mediaBase.getIntroduction()){
            mediaFile.setIntroduction(mediaBase.getIntroduction());
        }

        mediaBase.setMediaFile(mediaFile);

        //根据文件信息先添加mediaFile表
        int fileResult = mediaFileMapper.insert(mediaFile);
        // 新增后获取主键id做mediaId
        mediaBase.setMediaId(mediaFile.getId());
        //将保存的mediaId存入base信息中再存入base表中
        int baseResult = mediaBaseMapper.insert(mediaBase);

        //启动流程
        //保存文件名称存入流程数据
        mediaBase.setName(mediaFile.getFileName());
        //boolean processResult = processService.startProcess(mediaBase,userId,displayName);

        return result;
    }

    @Override
    @Transactional(rollbackFor = {Exception.class,RuntimeException.class})
    public boolean delete(MediaBase mediaBase) throws Exception{
        boolean result = false;
        mediaBase = mediaBaseMapper.queryById(mediaBase.getId());

        //校验状态是否为待提交或已退回
        if (!StatusEnum.TO_SUBMIT.toString().equals(mediaBase.getStatus()) && !StatusEnum.REJECT.toString().equals(mediaBase.getStatus())){
            throw new RuntimeException("当前状态不可删除");
        }
        //如果状态为待提交需删除流程
        if (StatusEnum.TO_SUBMIT.toString().equals(mediaBase.getStatus())){
            //processService.deleteProcess(mediaBase.getId());
        }

        //删除文件
        boolean fileResult = fileService.delete(Long.valueOf(mediaBase.getFileIdList()));
        //根据mediaId先删除meidaFile表中信息
        int mediaResult = mediaFileMapper.delete(mediaBase.getMediaId());
        //删除mediaBase表中信息
        int baseResult = mediaBaseMapper.delete(mediaBase.getId());

        if (0 < mediaResult && 0 < baseResult && fileResult){
            result = true;
        }
        return result;
    }

    @Override
    public boolean remark(MediaBase mediaBase) {
        //修改备注信息
        return 0 < mediaBaseMapper.update(mediaBase);
    }
}
