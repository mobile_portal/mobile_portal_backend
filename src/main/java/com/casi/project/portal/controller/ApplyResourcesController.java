package com.casi.project.portal.controller;

import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.ServletUtils;
import com.casi.common.utils.StringUtils;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.security.LoginUser;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.framework.web.page.TableDataInfo;
import com.casi.project.portal.domain.MobileApplyResource;
import com.casi.project.portal.service.ApplyResourcesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 应用资源管理
 * @author css
 * @date 2020/5/8 17:07
 */
@Api(tags = { "应用资源管理" })
@RestController
@RequestMapping("/apply/applyResources")
public class ApplyResourcesController extends BaseController {

    @Autowired
    private ApplyResourcesService applyResourcesService;

    @Autowired
    private TokenService tokenService;

    /**
     * 获取应用资源列表
     */
    @ApiOperation(value = "应用资源列表")
    @PreAuthorize("@ss.hasPermi('apply:resources:list')")
    @GetMapping("/list")
    public TableDataInfo list(String applyId)
    {
        logger.info("===============获取应用资源列表=============");
        LoginUser loginUser;
        String userId;
        List<MobileApplyResource> mobileApplyPermsList;
        TableDataInfo tableDataInfo = new TableDataInfo();
        try {
            if (StringUtils.isEmpty(applyId)){
                tableDataInfo.setMsg("请选择查询的应用");
                return tableDataInfo;
            }
            startPage();
         //   loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
         //   userId = loginUser.getUser().getUserId();
         //   mobileApplyPermsList = applyResourcesService.selectMenuList(userId,applyId);
            mobileApplyPermsList = applyResourcesService.selectMenuList(applyId);
        } catch (Exception e) {
            logger.error("获取应用资源列表失败", e);
            TableDataInfo dataInfo = new TableDataInfo();
            dataInfo.setCode(500);
            dataInfo.setMsg("获取应用资源列表失败");
            dataInfo.setTotal(0L);
            return dataInfo;
        }

        return getDataTable(mobileApplyPermsList);
    }



    /**
     * 新增应用资源
     */
    @ApiOperation(value = "新增应用资源")
    @Log(title = "新增应用资源", businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermi('apply:resources:add')")
    @PostMapping("/addResource")
    public AjaxResult add(@RequestBody MobileApplyResource mobileApplyResource)
    {

        logger.info("===============新增应用资源=============");
        int i;
        try {
            mobileApplyResource.setCreateBy(SecurityUtils.getUsername());
            i = applyResourcesService.insertResource(mobileApplyResource);
        } catch (Exception e) {
            logger.error("新增应用资源失败", e);
            return AjaxResult.error("新增应用资源失败");
        }
        return toAjax(i);

    }


    /**
     * 修改应用资源
     */
    @ApiOperation(value = "修改应用资源")
    @Log(title = "修改应用资源", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('apply:resources:edit')")
    @PostMapping("/editResource")
    public AjaxResult edit(@RequestBody(required = false) MobileApplyResource mobileApplyResource)
    {
        logger.info("===============修改应用资源=============");
        int i;
        try {
            if (mobileApplyResource == null){
                return AjaxResult.error("修改参数不能为空");
            }
            if (StringUtils.isEmpty(mobileApplyResource.getMobileApplyId())){
                return AjaxResult.error("请选择要修改的应用");
            }
            mobileApplyResource.setUpdateBy(SecurityUtils.getUsername());
            i = applyResourcesService.updateResource(mobileApplyResource);
        } catch (Exception e) {
            logger.error("修改应用资源失败", e);
            return AjaxResult.error("修改应用资源失败");
        }
        return toAjax(i);

    }

    /**
     * 删除应用资源
     */
    @ApiOperation(value = "删除应用资源")
    @Log(title = "删除应用资源", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermi('apply:resources:remove')")
    @GetMapping("/deleteResource")
    public AjaxResult remove(String appPermsId)
    {
        logger.info("===============删除应用资源=============");
        int i;
        try {
/*            if (applyResourcesService.hasChildByMenuId(mobileApplyId))
            {
                return AjaxResult.error("存在子菜单,不允许删除");
            }
            if (applyResourcesService.checkMenuExistRole(mobileApplyId))
            {
                return AjaxResult.error("菜单已分配,不允许删除");
            }*/

            i= applyResourcesService.deleteMenuById(appPermsId);
        } catch (Exception e) {
            logger.error("删除应用资源失败", e);
            return AjaxResult.error("删除应用资源失败");
        }
        return toAjax(i);
    }
}
