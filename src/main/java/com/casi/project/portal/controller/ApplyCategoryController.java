package com.casi.project.portal.controller;

import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.vo.ApplyCategory;
import com.casi.project.portal.service.impl.ApplyCategoryServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 应用类别接口等
 *
 * @author 18732
 */
@Api(tags = { "应用类别管理" })
@RestController
@RequestMapping("/apply/applyCategory")
public class ApplyCategoryController extends BaseController {
    @Resource
    private ApplyCategoryServiceImpl applyCategoryService;

    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);

    /**
     * 新增应用类别
     *
     * @param hashMap
     * @param
     * @return
     */
    @ApiOperation(value = "新增应用类别")
    @Log(title = "应用类别管理", businessType = BusinessType.INSERT)
    @PreAuthorize("@ss.hasPermi('apply:category:add')")
    @PostMapping(value = "/addApplyCategory")
    public AjaxResult addApplyCategory(@RequestBody HashMap<String, Object> hashMap) {
        logger.info("===============新增应用类别=============");
        return applyCategoryService.addApplyCategory(hashMap);
    }

    /**
     * 回显应用类别
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "回显应用类别")
    @PreAuthorize("@ss.hasPermi('apply:category:echo')")
    @GetMapping("/findApplyCategoryId")
    public AjaxResult findApplyCategoryId(String id) {
        logger.info("===============根据id回显应用类别具体详情=============");
        if (id == null) {
            AjaxResult.error("请传要回显应用类别ID");
        }

        Map<String, Object> map = null;
        try {
            map = applyCategoryService.findApplyCategoryId(id);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");
        }
        logger.info("查询成功=======" + map);
        return AjaxResult.success("查询成功", map);
    }


    /**
     * 修改应用类别
     *
     * @param
     * @param
     * @return
     */
    @ApiOperation(value = "修改应用类别")
    @Log(title = "应用类别管理", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('apply:category:edit')")
    @PostMapping(value = "/updateApplyCategory")
    public AjaxResult updateApplyCategory(@RequestBody HashMap<String, Object> hashMap) {
        logger.info("===============修改应用类别=============");
        return applyCategoryService.updateApplyCategory(hashMap);
    }

    /**
     * 禁用启用应用类别
     *
     * @return
     */
    @ApiOperation(value = "禁用启用应用类别")
    @Log(title = "应用类别管理", businessType = BusinessType.UPDATE)
    @PreAuthorize("@ss.hasPermi('apply:category:disable')")
    @GetMapping("/disableEnabledCategory")
    public AjaxResult disableEnabledCategory(String[] id, Integer type) {
        logger.info("===============禁用应用类别=============");
        return applyCategoryService.disableEnabledCategory(id, type);
    }

    /**
     * 根据上级ID查询应用类别下拉列表
     *
     * @param
     * @return
     */
    @ApiOperation(value = "根据上级ID查询应用类别下拉列表")
    @PreAuthorize("@ss.hasPermi('apply:category:list')")
    @GetMapping(value = "/getApplyCategoryId")
    //public AjaxResult getApplyCategoryId(@RequestParam(value = "id", required = false) String id) {
    public AjaxResult getApplyCategoryId() {
        logger.info("===============根据ID查询应用类别下拉列表=============");
/*        if (null == id) {
            return AjaxResult.error("ID不能为空");
        }*/
        ArrayList<ApplyCategory> vo = null;
        try {
           // vo = applyCategoryService.getApplyCategoryId(id);
            vo = applyCategoryService.getApplyCategoryId();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");
        }
        logger.info("查询成功=======" + vo);
        return AjaxResult.success("查询成功", vo);
    }

    /**
     * 查询应用类别列表
     *
     * @return
     */
    @ApiOperation(value = "查询应用类别列表")
    @PreAuthorize("@ss.hasPermi('apply:category:list')")
    @GetMapping("/getApplyCategoryListPage")
    public AjaxResult getApplyCategoryListPage(Integer pageNum, Integer pageSize) {
        logger.info("===============查询应用类型列表=============");
        PageHelper.startPage(pageNum, pageSize);
        ArrayList<ApplyCategory> applyTypeList1 = null;
        try {
            applyTypeList1 = applyCategoryService.getApplyCategoryListPage();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");
        }
        logger.info("查询成功=======" + applyTypeList1);
        PageInfo<ApplyCategory> applyTypePageInfo = new PageInfo<>(applyTypeList1);
        return AjaxResult.success("查询成功", applyTypePageInfo);
    }
}
