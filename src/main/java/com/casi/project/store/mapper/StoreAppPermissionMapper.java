package com.casi.project.store.mapper;

import java.util.List;
import com.casi.project.store.domain.StoreAppPermission;

/**
 * 应用权限Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface StoreAppPermissionMapper 
{
    /**
     * 查询应用权限
     * 
     * @param uuid 应用权限ID
     * @return 应用权限
     */
    public StoreAppPermission selectStoreAppPermissionById(String uuid);

    /**
     * 查询应用权限列表
     * 
     * @param storeAppPermission 应用权限
     * @return 应用权限集合
     */
    public List<StoreAppPermission> selectStoreAppPermissionList(StoreAppPermission storeAppPermission);

    /**
     * 新增应用权限
     * 
     * @param storeAppPermission 应用权限
     * @return 结果
     */
    public int insertStoreAppPermission(StoreAppPermission storeAppPermission);

    /**
     * 修改应用权限
     * 
     * @param storeAppPermission 应用权限
     * @return 结果
     */
    public int updateStoreAppPermission(StoreAppPermission storeAppPermission);

    /**
     * 删除应用权限
     * 
     * @param uuid 应用权限ID
     * @return 结果
     */
    public int deleteStoreAppPermissionById(String uuid);

    /**
     * 批量删除应用权限
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteStoreAppPermissionByIds(String[] uuids);
}
