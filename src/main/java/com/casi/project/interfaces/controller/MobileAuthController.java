package com.casi.project.interfaces.controller;

import com.alibaba.fastjson.JSONObject;
import com.casi.common.utils.StringUtils;
import com.casi.framework.redis.RedisCache;
import com.casi.framework.security.LoginUser;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.interfaces.service.MobileApplicationService;
import com.casi.project.interfaces.service.WxUserService;
import com.casi.project.system.domain.SysUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: MobileApplicationController
 * @description: 应用商店接口
 * @author: y_xiaopeng
 * @create: 2020/5/21
 **/
@RestController
@RequestMapping("/api/service")
public class MobileAuthController extends BaseController {


    private static Logger logger = LoggerFactory.getLogger(MobileAuthController.class);

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private TokenService tokenService;
    @Autowired
    private WxUserService wxUserService;


    @Autowired
    private MobileApplicationService mobileApplicationService;


    /**
     * @Description: 验证登录token
     * @Param:
     * @return:
     * @Author: y_xiaopeng
     * @Date: 2020/5/21
     */
    @PostMapping("/auth/authToken")
    public AjaxResult authToken(@RequestBody Map<String, Object> params) {
        AjaxResult ajax;
        try {
            String idToken = String.valueOf(params.get("idToken"));
            if ("null".equals(idToken) || StringUtils.isEmpty(idToken))
                return AjaxResult.error(9999, "idToken不能为空！！！");
            ajax = wxUserService.checkParams(params);
            Integer ajaxCode = (Integer) ajax.get("code");
            if (ajaxCode == 200) {
                LoginUser loginUser = tokenService.parsingToken(idToken);
                Map<String, Object> map = new HashMap<>();
                map.put("name", loginUser.getUser().getUserName());
                map.put("userid", loginUser.getUser().getUserId());
                map.put("department", loginUser.getUser().getDeptId());
                return AjaxResult.success("调用成功", map);
            }
            return ajax;

        } catch (Exception e) {
            logger.error("验证登录token失败！！！",e);
            return AjaxResult.error(9999, "认证失败！！！");
        }

    }


    /**
     * @Description: 根据idToken获取详细用户信息
     * @Param:
     * @return:
     * @Author: y_xiaopeng
     * @Date: 2020/5/21
     */
    @GetMapping("/user/getUserByAuthKey/{idToken}")
    public AjaxResult getUserByAuthKey(@PathVariable String idToken) {
        AjaxResult ajax ;
        try {
            if (StringUtils.isEmpty(idToken)) {
                return AjaxResult.error(9999, "idToken不能为空！！！");
            }

            LoginUser loginUser = tokenService.parsingToken(idToken);
            if(null != loginUser){
                SysUser user = loginUser.getUser();
                if (user != null) {
                    ajax = wxUserService.getUserByUserId(idToken, user.getUserId());
                    String code = ajax.get("code").toString();
                    if (StringUtils.isNotEmpty(code) && code.equals("200")) {
                        JSONObject json = new JSONObject();
                        json = json.parseObject(ajax.get("msg").toString());
                        String userid =  json.getString("userid");
                        String name =  json.getString("name");
                        String department =  json.getString("department");
                        String position =  json.getString("position");
                        String gender =  json.getString("gender");
                        String email =  json.getString("email");
                        String english_name =  json.getString("english_name");
                        String is_leader_in_dept =  json.getString("is_leader_in_dept");
                        String status =  json.getString("status");
                        String positions =  json.getString("positions");
                        String telephone =  json.getString("telephone");
                        Map<String ,Object> map = new HashMap<>();
                        map.put("userid",userid);
                        map.put("name",name);
                        map.put("department",department);
                        map.put("position",position);
                        map.put("gender",gender);
                        map.put("email",email);
                        map.put("english_name",english_name);
                        map.put("is_leader_in_dept",is_leader_in_dept);
                        map.put("status",status);
                        map.put("positions",positions);
                        map.put("telephone",telephone);

                        return AjaxResult.success("成功",map);

                    }else{
                        return AjaxResult.error("云信状态不正常" + idToken);
                    }
                }else {
                    return AjaxResult.error("用户状态不正常" + idToken);
                }
            }else{
                return AjaxResult.error("idToken无效" + idToken);
            }


        } catch (Exception e) {
            logger.error("根据idToken获取详细用户信息失败！！！",e);
            return AjaxResult.error(9999, "认证失败！！！");
        }

    }

    /**
     * @Description: 推送消息接口（移动商店调用门户平台，推送消息）
     * @Param:
     * @return:
     * @Author: y_xiaopeng
     * @Date: 2020/5/21
     */
    @PostMapping("/msg/sendMsg")
    public AjaxResult sendMsg(@RequestBody Map<String, Object> params) {
        return mobileApplicationService.sendMsg(params);
    }


    /**
     * @Description: 获取用户列表
     * @Param:
     * @return:
     * @Author: y_xiaopeng
     * @Date: 2020/5/21
     */
    @PostMapping("/user/getUserList")
    public AjaxResult getUserList(@RequestBody Map<String, Object> params) {

        Integer org = null;
        String orgId = String.valueOf(params.get("orgId"));
        if (!"null".equals(orgId) && !StringUtils.isEmpty(orgId))
            org = Integer.valueOf(orgId);
        AjaxResult userList = wxUserService.getUserListForThirdParty(org, params);

        return userList;
    }


    /**
     * @Description: 获取组织列表
     * @Param:
     * @return:
     * @Author: y_xiaopeng
     * @Date: 2020/5/21
     */
    @PostMapping("/user/getOrgList")
    public AjaxResult getOrgList(@RequestBody Map<String, Object> params) {

        Integer org = null;
        String orgId = String.valueOf(params.get("orgId"));
        if (!"null".equals(orgId) && !StringUtils.isEmpty(orgId))
            org = Integer.valueOf(orgId);
        AjaxResult orgList = wxUserService.getOrgListForThirdParty(org, params);
        return orgList;
    }

    /**
     * @author: liyuan
     * @date: 2020/5/21 15:27
     * @Description: 单点登录（移动商店调用门户平台，登录）
     */
    @PostMapping("/user/login/{userName}")
    public AjaxResult login(@PathVariable String userName) {
        return mobileApplicationService.login(userName);
    }


}
