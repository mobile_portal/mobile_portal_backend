package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * 应用与用户关系表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MobileApplyUsers implements Serializable {
    private String applyId;
    private String userId;
    private String isShow;
    private Integer status;
    private String deptId;
}
