package com.casi.project.portal.service.impl;

import com.casi.common.utils.ServletUtils;
import com.casi.common.utils.StringUtils;
import com.casi.framework.security.LoginUser;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.controller.NoticeController;
import com.casi.project.portal.domain.MobileNotice;
import com.casi.project.portal.domain.vo.NoticeVO;
import com.casi.project.portal.mapper.NoticePcMapper;
import com.casi.project.portal.service.NoticePcService;
import com.casi.project.portal.util.Tools;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.mapper.SysUserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class NoticePcServiceImpl implements NoticePcService {

    @Resource
    private NoticePcMapper noticePcMapper;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private SysUserMapper userMapper;

    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);

    @Override
    public AjaxResult getNoticeList(HashMap<String, Object> map) {
        if (StringUtils.isEmpty(map)) {
            return AjaxResult.error("参数错误");
        }
        if (null == map.get("pageNum") || null == map.get("pageSize")) {
            return AjaxResult.error("请传入页数或每页展示的条数");
        }
        PageHelper.startPage(Integer.parseInt(map.get("pageNum").toString()), Integer.parseInt(map.get("pageSize").toString()));
        NoticeVO noticeVO = new NoticeVO();

        if (null != map.get("title")) {
            noticeVO.setTitle(String.valueOf(map.get("title")));
        }

        if (null != map.get("onTime")) {
            noticeVO.setOnTime((Date) map.get("onTime"));
        }

        if (null != map.get("offTime")) {
            noticeVO.setOffTime((Date) map.get("offTime"));
        }

        ArrayList<NoticeVO> noticeList = noticePcMapper.getNoticeList(noticeVO);
        PageInfo<NoticeVO> noticeVOPageInfo = new PageInfo<>(noticeList);
        return AjaxResult.success("查询成功", noticeVOPageInfo);
    }

    @Override
    public AjaxResult addNotice(MobileNotice mobileNotice) {
        if (StringUtils.isNull(mobileNotice)) {
            return AjaxResult.error("参数异常");
        }
        if (null == mobileNotice.getPushDept() || mobileNotice.getPushDept() == 0) {
            return AjaxResult.error("请选择要发布部门");
        }
        if (Tools.isEmpty(mobileNotice.getTitle())) {
            return AjaxResult.error("请输入公告标题");
        }
        if (Tools.isEmpty(mobileNotice.getNotices())) {
            return AjaxResult.error("请输入公告内容");
        }
        if (null == mobileNotice.getIsShow()) {
            return AjaxResult.error("请选择公告显示或隐藏");
        }
        if (0 != mobileNotice.getIsShow() && 1 != mobileNotice.getIsShow()) {
            return AjaxResult.error("请选择公告显示或隐藏");
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (StringUtils.isNull(loginUser)) {
            return AjaxResult.error("没有当前登录用户");
        }
        String userId = loginUser.getUser().getUserId();
        //根据用户id查询当前登录的用户名
        SysUser sysUser = userMapper.selectUserById(userId);
        if (StringUtils.isNull(sysUser)) {
            return AjaxResult.error("没有当前登录用户");
        }
        mobileNotice.setPushByUser(sysUser.getUserName());
        int rows = 0;
        try {
            rows = noticePcMapper.addNotice(mobileNotice);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("添加成功") : AjaxResult.error("添加失败");
    }

    @Override
    public AjaxResult getNoticeId(NoticeVO noticeVO) {
        if (null == noticeVO) {
            return AjaxResult.error("参数异常");
        }
        String id = noticeVO.getId();
        if (null == id) {
            return AjaxResult.error("参数异常");
        }
        NoticeVO noticeId = noticePcMapper.getNoticeId(id);
        return AjaxResult.success("查询成功", noticeId);
    }

    /**
     * 根据ID查询公告具体详情包含部门
     *
     * @return
     */
    @Override
    public AjaxResult getNoticeById(NoticeVO noticeVO) {
        if (null == noticeVO) {
            return AjaxResult.error("参数缺失");
        }
        if (null == noticeVO.getId()) {
            return AjaxResult.error("参数缺失");
        }
        Map<String, Object> noticeById = noticePcMapper.getNoticeById(noticeVO.getId());
        return AjaxResult.success("操作成功", noticeById);
    }

    /**
     * 根据ID查询公告具体详情
     * 这个是修改公告的时候可以修改部门上面的没有修改公告部门
     *
     * @return
     */
    @Override
    public Map<String, Object> getNoticeId1(MobileNotice notice) {
        if (null == notice) {
            return AjaxResult.error("缺失参数");
        }
        if (null == notice.getId()) {
            return AjaxResult.error("ID不能为空");
        }
        return noticePcMapper.getNoticeId1(notice.getId());
    }

    @Override
    public AjaxResult updateNotice(MobileNotice mobileNotice) {
        if (null == mobileNotice) {
            return AjaxResult.error("参数异常");
        }
        if (null == mobileNotice.getId()) {
            return AjaxResult.error("请传要修改的id");
        }
        if (Tools.isEmpty(mobileNotice.getTitle())) {
            return AjaxResult.error("请输入公告标题");
        }
        if (Tools.isEmpty(mobileNotice.getNotices())) {
            return AjaxResult.error("请输入公告内容");
        }
        if (null == mobileNotice.getIsShow()) {
            return AjaxResult.error("请传前端是否显示隐藏字段");
        }
        if (mobileNotice.getIsShow() != 0 && mobileNotice.getIsShow() != 1) {
            return AjaxResult.error("请传公告0-显示,1-隐藏这个0或者1数字");
        }
        int rows = 0;
        try {
            rows = noticePcMapper.updateNotice(mobileNotice);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("修改成功") : AjaxResult.error("修改失败");
    }


    /**
     * 删除公告
     *
     * @param id
     * @return
     */
    @Override
    public AjaxResult deleteNotice(String id) {
        if (null == id) {
            return AjaxResult.error("缺失删除的必要参数");
        }
        Map<String, Object> map = noticePcMapper.selectMobileNoticeById(id);
        if (StringUtils.isEmpty(map)) {
            return AjaxResult.error("没有此公告，或该公告已被删除");
        }
        int rows = 0;
        try {
            rows = noticePcMapper.deleteNotice(id);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("删除成功") : AjaxResult.error("删除失败");
    }

    //状态:0-审核中,1-已审核,2-已发布,3-已退回Long id, Integer type
    @Override
    public AjaxResult checkNoticeStatus(HashMap<String, Object> map) {
        if (StringUtils.isEmpty(map)) {
            return AjaxResult.error("参数异常");
        }
        if (null == map.get("id") || null == map.get("type")) {
            return AjaxResult.error("参数错误");
        }
        // Long id = Long.valueOf(String.valueOf(map.get("id")));
        String id = String.valueOf(map.get("id"));
        Integer type = Integer.parseInt(map.get("type").toString());
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (StringUtils.isNull(loginUser)) {
            return AjaxResult.error("没有当前登录用户");
        }
        String userId = loginUser.getUser().getUserId();
        //根据用户id查询当前登录的用户名
        SysUser sysUser = userMapper.selectUserById(userId);
        if (StringUtils.isNull(sysUser)) {
            return AjaxResult.error("没有当前登录用户");
        }
        if (null == id) {
            return AjaxResult.error("请带来要审核公告的ID");
        }
        if (type != 1 && type != 2 && type != 3) {
            return AjaxResult.error("审核状态错误");
        }
        //根据审核id查询  :0-带审核,1-已审核,2-已发布,3-已拒绝（表示审核被拒绝）
        Map<String, Object> map1 = noticePcMapper.selectMobileNoticeById(id);
        if (StringUtils.isEmpty(map)) {
            return AjaxResult.error("没有此公告");
        }
        int i = Integer.parseInt(String.valueOf(map1.get("audit_status")));
        if (i == 0) {
            if (type == 2) {
                return AjaxResult.error("该公告还未审核通过，不允许发布");
            }
        }
        if (i == 1) {
            if (type == 1) {
                return AjaxResult.error("该公告已审核通过，请不要重复审核");
            }
        }
        if (i == 2) {
            if (type == 1) {
                return AjaxResult.error("该公告已发布，不允许审核");
            }
            if (type == 2) {
                return AjaxResult.error("该公告已发布，不允许重复发布");
            }
            if (type == 3) {
                return AjaxResult.error("该公告已发布，不允许审核");
            }
        }
        if (i == 3) {
            if (type == 2) {
                return AjaxResult.error("该公告已经被拒绝不允许发布");
            }
            if (type == 3) {
                return AjaxResult.error("该公告已被拒绝，不允许重复拒绝");
            }
        }
        int rows = 0;
        try {
            if (type == 2) {
                Date date = new Date();
                rows = noticePcMapper.checkNoticeStatus(id, type, sysUser.getUserName(), date);
            } else {
                rows = noticePcMapper.checkNoticeStatus(id, type, sysUser.getUserName(), null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("审核成功") : AjaxResult.error("审核失败");
    }

    //状态:0-审核中,1-已审核,2-已发布,3-已退回
    @Override
    public AjaxResult publishNoticeStatus(String[] id, Integer type) {
        if (null == id || id.length == 0) {
            return AjaxResult.error("请带来要发布公告的ID");
        }
        if (type != 2) {
            return AjaxResult.error("请type字段为2为发布值");
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (StringUtils.isNull(loginUser)) {
            return AjaxResult.error("没有当前登录用户");
        }
        String userId = loginUser.getUser().getUserId();
        //根据用户id查询当前登录的用户名
        SysUser sysUser = userMapper.selectUserById(userId);
        for (int i = 0; i < id.length; i++) {
            List<HashMap<String, Integer>> maps = noticePcMapper.selectIsAudit(id[i]);
            for (HashMap<String, Integer> map : maps) {
                if (map.get("auditStatus") == 0) {
                    return AjaxResult.error("请先审核" + map.get("title") + "公告");
                }
            }
        }
        int rows = 0;
        try {
            for (int i = 0; i < id.length; i++) {
                rows = noticePcMapper.publishNoticeStatus(id[i], type, sysUser.getUserName());
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("发布成功") : AjaxResult.error("发布失败");
    }
}
