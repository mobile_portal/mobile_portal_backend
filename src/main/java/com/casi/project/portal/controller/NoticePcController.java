package com.casi.project.portal.controller;

import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.MobileNotice;
import com.casi.project.portal.domain.vo.NoticeVO;
import com.casi.project.portal.service.NoticePcService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Api(tags = { "PC端公告管理" })
@RestController
@RequestMapping("/noticePc")
public class NoticePcController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);
    @Autowired
    private NoticePcService noticePcService;

    @ApiOperation(value = "查询公告列表")
    @PostMapping("/getNoticeList")
    public AjaxResult getNoticeList(@RequestBody HashMap<String, Object> map) {
        logger.info("===============查询公告列表=============");
        return noticePcService.getNoticeList(map);
    }

    /**
     * 增加公告
     */
    @ApiOperation(value = "增加公告")
    @Log(title = "公告管理", businessType = BusinessType.INSERT)
    @PostMapping("/addNotice")
    public AjaxResult addNotice(@RequestBody MobileNotice mobileNotice) {
        logger.info("===============增加公告=============");
        return noticePcService.addNotice(mobileNotice);
    }

    /**
     * 修改公告数据回显
     */
    @ApiOperation(value = "公告数据回显")
    @GetMapping(value = "/getNoticeId")
    public AjaxResult getNoticeId(@RequestBody NoticeVO noticeVO) {
        logger.info("===============根据ID查询公告具体详情=============");
        return noticePcService.getNoticeId(noticeVO);
    }

    /**
     * 审核公告数据回显接口
     */
    @ApiOperation(value = "审核公告数据回显")
    @PostMapping(value = "/getNoticeById")
    public AjaxResult getNoticeById(@RequestBody NoticeVO noticeVO) {
        logger.info("===============审核公告数据回显接口=============");
        return noticePcService.getNoticeById(noticeVO);
    }

    /**
     * 修改公告数据回显（这个是可以修改公告部门的接口）
     */
    @ApiOperation(value = "修改公告部门数据回显")
    @RequestMapping("/getNoticeId1")
    public AjaxResult getNoticeId1(@RequestBody MobileNotice notice) {
        logger.info("===============根据ID查询公告具体详情=============" + notice.getId());
        Map<String, Object> map = noticePcService.getNoticeId1(notice);
        return AjaxResult.success("查询成功", map);
    }

    /**
     * 修改公告
     */
    @ApiOperation(value = "修改公告")
    @Log(title = "公告管理", businessType = BusinessType.UPDATE)
    @PostMapping("/updateNotice")
    public AjaxResult updateNotice(@RequestBody MobileNotice mobileNotice) {
        logger.info("===============修改公告=============");
        return noticePcService.updateNotice(mobileNotice);
    }

    /**
     * 删除公告
     */
    @ApiOperation(value = "删除公告")
    @Log(title = "公告管理", businessType = BusinessType.DELETE)
    @GetMapping("/deleteNotice")
    public AjaxResult deleteNoticePC(String id) {
        logger.info("===============删除公告=============");
        return noticePcService.deleteNotice(id);
    }

    /**
     * 审核公告（连带着发布写在了一个里面）
     */
    @ApiOperation(value = "审核公告")
    @Log(title = "公告管理", businessType = BusinessType.UPDATE)
    @PostMapping("/checkNoticeStatus")
    public AjaxResult checkNoticeStatus(@RequestBody HashMap<String, Object> map) {
        logger.info("==========审核公告==========");
        return noticePcService.checkNoticeStatus(map);
    }


    /**
     * 发布公告（暂时没有使用这个接口）
     */
    @PutMapping("/publishNoticeStatus")
    public AjaxResult publishNoticeStatus(String[] id, Integer type) {
        logger.info("==========发布公告==========");
        return noticePcService.publishNoticeStatus(id, type);
    }
}
