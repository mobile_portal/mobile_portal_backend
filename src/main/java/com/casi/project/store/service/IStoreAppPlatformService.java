package com.casi.project.store.service;

import java.util.List;
import com.casi.project.store.domain.StoreAppPlatform;

/**
 * 应用平台表Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface IStoreAppPlatformService 
{
    /**
     * 查询应用平台表
     * 
     * @param uuid 应用平台表ID
     * @return 应用平台表
     */
    public StoreAppPlatform selectStoreAppPlatformById(String uuid);

    /**
     * 查询应用平台表列表
     * 
     * @param storeAppPlatform 应用平台表
     * @return 应用平台表集合
     */
    public List<StoreAppPlatform> selectStoreAppPlatformList(StoreAppPlatform storeAppPlatform);

    /**
     * 新增应用平台表
     * 
     * @param storeAppPlatform 应用平台表
     * @return 结果
     */
    public int insertStoreAppPlatform(StoreAppPlatform storeAppPlatform);

    /**
     * 修改应用平台表
     * 
     * @param storeAppPlatform 应用平台表
     * @return 结果
     */
    public int updateStoreAppPlatform(StoreAppPlatform storeAppPlatform);

    /**
     * 批量删除应用平台表
     * 
     * @param uuids 需要删除的应用平台表ID
     * @return 结果
     */
    public int deleteStoreAppPlatformByIds(String[] uuids);

    /**
     * 删除应用平台表信息
     * 
     * @param uuid 应用平台表ID
     * @return 结果
     */
    public int deleteStoreAppPlatformById(String uuid);
}
