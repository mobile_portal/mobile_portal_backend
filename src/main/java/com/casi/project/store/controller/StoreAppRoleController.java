package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreAppRole;
import com.casi.project.store.service.IStoreAppRoleService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用角色Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用角色"})
@RestController
@RequestMapping("/store/role")
public class StoreAppRoleController extends BaseController
{
    @Autowired
    private IStoreAppRoleService storeAppRoleService;

    /**
     * 查询应用角色列表
     */
    @ApiOperation(value = "查询应用角色列表")
    @PreAuthorize("@ss.hasPermi('store:role:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreAppRole storeAppRole)
    {
        startPage();
        List<StoreAppRole> list = storeAppRoleService.selectStoreAppRoleList(storeAppRole);
        return getDataTable(list);
    }

    /**
     * 导出应用角色列表
     */
    @PreAuthorize("@ss.hasPermi('store:role:export')")
    @Log(title = "应用角色", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreAppRole storeAppRole)
    {
        List<StoreAppRole> list = storeAppRoleService.selectStoreAppRoleList(storeAppRole);
        ExcelUtil<StoreAppRole> util = new ExcelUtil<StoreAppRole>(StoreAppRole.class);
        return util.exportExcel(list, "role");
    }

    /**
     * 获取应用角色详细信息
     */
    @ApiOperation(value = "获取应用角色详细信息")
    @PreAuthorize("@ss.hasPermi('store:role:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppRoleService.selectStoreAppRoleById(uuid));
    }

    /**
     * 新增应用角色
     */
    @ApiOperation(value = "新增应用角色")
    @PreAuthorize("@ss.hasPermi('store:role:add')")
    @Log(title = "应用角色", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreAppRole storeAppRole)
    {
        return toAjax(storeAppRoleService.insertStoreAppRole(storeAppRole));
    }

    /**
     * 修改应用角色
     */
    @ApiOperation(value = "修改应用角色")
    @PreAuthorize("@ss.hasPermi('store:role:edit')")
    @Log(title = "应用角色", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreAppRole storeAppRole)
    {
        return toAjax(storeAppRoleService.updateStoreAppRole(storeAppRole));
    }

    /**
     * 删除应用角色
     */
    @ApiOperation(value = "删除应用角色")
    @PreAuthorize("@ss.hasPermi('store:role:remove')")
    @Log(title = "应用角色", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppRoleService.deleteStoreAppRoleByIds(uuids));
    }
}
