package com.casi.project.tenant.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.tenant.domain.TenantAppUser;
import com.casi.project.tenant.service.ITenantAppUserService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 租户应用用户授权Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"租户应用用户授权"})
@RestController
@RequestMapping("/tenant/appUser")
public class TenantAppUserController extends BaseController
{
    @Autowired
    private ITenantAppUserService tenantAppUserService;

    /**
     * 查询租户应用用户授权列表
     */
    @ApiOperation(value = "查询租户应用用户授权列表")
    @PreAuthorize("@ss.hasPermi('tenant:appUser:list')")
    @GetMapping("/list")
    public TableDataInfo list(TenantAppUser tenantAppUser)
    {
        startPage();
        List<TenantAppUser> list = tenantAppUserService.selectTenantAppUserList(tenantAppUser);
        return getDataTable(list);
    }

    /**
     * 导出租户应用用户授权列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:appUser:export')")
    @Log(title = "租户应用用户授权", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TenantAppUser tenantAppUser)
    {
        List<TenantAppUser> list = tenantAppUserService.selectTenantAppUserList(tenantAppUser);
        ExcelUtil<TenantAppUser> util = new ExcelUtil<TenantAppUser>(TenantAppUser.class);
        return util.exportExcel(list, "appUser");
    }

    /**
     * 获取租户应用用户授权详细信息
     */
    @ApiOperation(value = "获取租户应用用户授权详细信息")
    @PreAuthorize("@ss.hasPermi('tenant:appUser:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(tenantAppUserService.selectTenantAppUserById(uuid));
    }

    /**
     * 新增租户应用用户授权
     */
    @ApiOperation(value = "新增租户应用用户授权")
    @PreAuthorize("@ss.hasPermi('tenant:appUser:add')")
    @Log(title = "租户应用用户授权", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TenantAppUser tenantAppUser)
    {
        return toAjax(tenantAppUserService.insertTenantAppUser(tenantAppUser));
    }

    /**
     * 修改租户应用用户授权
     */
    @ApiOperation(value = "修改租户应用用户授权")
    @PreAuthorize("@ss.hasPermi('tenant:appUser:edit')")
    @Log(title = "租户应用用户授权", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TenantAppUser tenantAppUser)
    {
        return toAjax(tenantAppUserService.updateTenantAppUser(tenantAppUser));
    }

    /**
     * 删除租户应用用户授权
     */
    @ApiOperation(value = "删除租户应用用户授权")
    @PreAuthorize("@ss.hasPermi('tenant:appUser:remove')")
    @Log(title = "租户应用用户授权", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(tenantAppUserService.deleteTenantAppUserByIds(uuids));
    }
}
