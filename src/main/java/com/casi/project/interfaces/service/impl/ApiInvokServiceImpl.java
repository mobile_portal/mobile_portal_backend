package com.casi.project.interfaces.service.impl;

import com.casi.common.constant.Constants;
import com.casi.common.utils.JsonUtils;
import com.casi.common.utils.StringUtils;
import com.casi.common.utils.http.HttpUtils;
import com.casi.framework.redis.RedisCache;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.interfaces.domain.ApiInvok;
import com.casi.project.interfaces.domain.DepartmentVo;
import com.casi.project.interfaces.domain.UserTemp;
import com.casi.project.interfaces.mapper.ApiInvokMapper;
import com.casi.project.interfaces.mapper.DeptTempMapper;
import com.casi.project.interfaces.mapper.UserTempMapper;
import com.casi.project.interfaces.service.ApiInvokService;
import com.casi.project.interfaces.service.WxUserService;
import com.casi.project.interfaces.utils.AccessTokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@SuppressWarnings("all")
public class ApiInvokServiceImpl implements ApiInvokService {
    private static Logger logger = LoggerFactory.getLogger(ApiInvokServiceImpl.class);


    @Autowired
    private ApiInvokMapper mapper;

    public ApiInvok getModelByKey(String appKey,String appSecret){
        return  mapper.selectByKey(appKey,appSecret);
    }

}
