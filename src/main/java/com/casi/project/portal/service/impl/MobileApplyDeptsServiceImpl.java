package com.casi.project.portal.service.impl;

import com.casi.common.utils.StringUtils;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.DoubleArray;
import com.casi.project.portal.domain.MobileApplyDepts;
import com.casi.project.portal.mapper.MobileApplyDeptsMapper;
import com.casi.project.portal.mapper.MobileApplyUsersMapper;
import com.casi.project.portal.service.MobileApplyDeptsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * 应用与部门关联表
 */
@Service
public class MobileApplyDeptsServiceImpl implements MobileApplyDeptsService {
    @Resource
    private MobileApplyDeptsMapper mobileApplyDeptsMapper;
    @Autowired
    private MobileApplyUsersMapper mobileApplyUsersMapper;


    /**
     * 添加应用与部门的关联关系
     */
    @Override
    @Transactional
    public AjaxResult addApplyIdsAndDeptIds(String[] applyIds, String[] deptIds) {
        if (null == applyIds || applyIds.length < 0) {
            return AjaxResult.error("缺失参数");
        }
        if (null == deptIds || deptIds.length < 0) {
            return AjaxResult.error("缺失参数");
        }
        //根据应用的id删除应用于部门的关联关系
        for (String applyId : applyIds){
            try {
                mobileApplyDeptsMapper.deleteByApplyId(applyId);
            }catch (Exception e){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
                return AjaxResult.error("应用授权组织机构失败");
            }
        }
        HashSet<MobileApplyDepts> tags = new HashSet<>();
        //遍历应用的id
        for (String applyId : applyIds) {
            //遍历组织机构的id
            for (String deptId : deptIds) {
                MobileApplyDepts mobileApplyDepts = new MobileApplyDepts();
                mobileApplyDepts.setApplyId(applyId);
                mobileApplyDepts.setDeptId(deptId);
                tags.add(mobileApplyDepts);
            }
        }
        if (tags.size() != 0) {
            int i = mobileApplyDeptsMapper.insertApplyIdsAndDeptIds(tags);
            if (i < 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
                return AjaxResult.error("应用授权组织机构失败");
            }
        }
        //现在开始操作应用与用户的关联表
        //根据应用id查询应用与用户的关联表查询出所有的部门id并且是去重的
        for (String applyId : applyIds){
            String[] s = mobileApplyUsersMapper.selectDeptIdsByApplyId(applyId);
        }

        return AjaxResult.success("应用授权组织结构成功");
    }


/*public AjaxResult addApplyIdsAndDeptIds(String[] applyIds, String[] deptIds) {
        if (null == applyIds || applyIds.length < 0) {
            return AjaxResult.error("缺失参数");
        }
        if (null == deptIds || deptIds.length < 0) {
            return AjaxResult.error("缺失参数");
        }
        // 新增部门与应用的关联关系
        HashSet<MobileApplyDepts> tags = new HashSet<>();
        //遍历应用的id
        for (String applyId : applyIds) {
            //遍历组织机构的id
            for (String deptId : deptIds) {
                //根据组织机构的id和应用的id查询
                MobileApplyDepts mobileApplyDepts1 = mobileApplyDeptsMapper.selectOneByApplyIdsAndDeptIds(applyId, deptId);
                if (null != mobileApplyDepts1) {
                    continue;
                }
                MobileApplyDepts mobileApplyDepts = new MobileApplyDepts();
                mobileApplyDepts.setApplyId(applyId);
                mobileApplyDepts.setDeptId(deptId);
                tags.add(mobileApplyDepts);
            }
        }
        if (tags.size() != 0) {
            int i = mobileApplyDeptsMapper.insertApplyIdsAndDeptIds(tags);
            if (i < 1) {
                return AjaxResult.error("应用授权组织机构失败");
            }
            return AjaxResult.success("应用授权组织结构成功");
        } else {
            return AjaxResult.error("该部门已授权过此应用");
        }
    }*/


    /**
     * 回显应用已授权的组织机构用于回显
     *
     * @param doubleArray
     * @return
     */
    @Override
    public AjaxResult findApplyIdsAndDeptIds(DoubleArray doubleArray) {
        if (StringUtils.isNull(doubleArray.getApplyIds()) || doubleArray.getApplyIds().length == 0) {
            return AjaxResult.error("请传应用ID");
        }
        List<HashMap<String, Object>> hashMaps = mobileApplyDeptsMapper.findApplyIdsAndDeptIds(doubleArray.getApplyIds());
        return AjaxResult.success("查询成功", hashMaps);
    }
    /*public AjaxResult findApplyIdsAndDeptIds(DoubleArray doubleArray) {
        if (StringUtils.isNull(doubleArray.getApplyIds()) || doubleArray.getApplyIds().length == 0) {
            return AjaxResult.error("请传应用ID");
        }
        List<HashMap<String, Object>> hashMaps = mobileApplyDeptsMapper.findApplyIdsAndDeptIds(doubleArray.getApplyIds());
        if (StringUtils.isNull(hashMaps) || hashMaps.size() == 0) {
            return AjaxResult.error("应用没有授权组织机构");
        }
        return AjaxResult.success("查询成功", hashMaps);
    }*/
}
