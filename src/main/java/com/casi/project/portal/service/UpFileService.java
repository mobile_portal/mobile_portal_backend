package com.casi.project.portal.service;

import com.casi.framework.web.domain.AjaxResult;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;



/**
 * @Auther shp
 * @data2020/4/2110:05
 * @description
 */
public interface UpFileService {

    /**
     * 支持前端上传文件
     * @param file
     * @return
     */
    AjaxResult uploadFile(@RequestParam("file") MultipartFile file);

    /**
     * 支持前端上传文件（新闻专用）
     * 上传文件时 会把文件信息存储到local_file 表中
     * 返回文件的id
     * @param file
     * @return
     */
    AjaxResult newUploadFile(@RequestParam("file") MultipartFile file);
}
