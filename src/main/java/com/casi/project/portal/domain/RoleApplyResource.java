package com.casi.project.portal.domain;

import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户和应用资源关联表 apply_user_resource
 * 
 * @author css
 */
@Data
public class RoleApplyResource
{
    /** 应用角色ID */
    private String applyRoleId;
    
    /** 应用菜单权限ID */
    private String appPermsId;

    /** 应用ID */
    private String mobileApplyId;


}
