package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.param.MobileApplyVersionNativeParam;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Auther shp
 * @data2020/4/2014:38
 * @description
 */



@Mapper
public interface MobileApplyVersionNativeMapper {


    @Insert("INSERT INTO mobile_apply_version(icon,screen_capture,version_number,version_describe,is_putaway,app_orIos,is_custom,install_package,packageName,to_url,mobile_home_menu_detailsId)VALUES(#{icon},#{screenCapture},#{versionNumber},#{versionDescribe},#{isPutaway},#{appOrIos},#{isCustom},#{installPackage},#{packageName},#{toUrl},#{mobileHomeMenuDetailsId})")
    Integer addApp(MobileApplyVersionNativeParam m);

}
