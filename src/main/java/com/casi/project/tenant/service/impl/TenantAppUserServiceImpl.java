package com.casi.project.tenant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.tenant.mapper.TenantAppUserMapper;
import com.casi.project.tenant.domain.TenantAppUser;
import com.casi.project.tenant.service.ITenantAppUserService;

/**
 * 租户应用用户授权Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class TenantAppUserServiceImpl implements ITenantAppUserService 
{
    @Autowired
    private TenantAppUserMapper tenantAppUserMapper;

    /**
     * 查询租户应用用户授权
     * 
     * @param uuid 租户应用用户授权ID
     * @return 租户应用用户授权
     */
    @Override
    public TenantAppUser selectTenantAppUserById(String uuid)
    {
        return tenantAppUserMapper.selectTenantAppUserById(uuid);
    }

    /**
     * 查询租户应用用户授权列表
     * 
     * @param tenantAppUser 租户应用用户授权
     * @return 租户应用用户授权
     */
    @Override
    public List<TenantAppUser> selectTenantAppUserList(TenantAppUser tenantAppUser)
    {
        return tenantAppUserMapper.selectTenantAppUserList(tenantAppUser);
    }

    /**
     * 新增租户应用用户授权
     * 
     * @param tenantAppUser 租户应用用户授权
     * @return 结果
     */
    @Override
    public int insertTenantAppUser(TenantAppUser tenantAppUser)
    {
        return tenantAppUserMapper.insertTenantAppUser(tenantAppUser);
    }

    /**
     * 修改租户应用用户授权
     * 
     * @param tenantAppUser 租户应用用户授权
     * @return 结果
     */
    @Override
    public int updateTenantAppUser(TenantAppUser tenantAppUser)
    {
        return tenantAppUserMapper.updateTenantAppUser(tenantAppUser);
    }

    /**
     * 批量删除租户应用用户授权
     * 
     * @param uuids 需要删除的租户应用用户授权ID
     * @return 结果
     */
    @Override
    public int deleteTenantAppUserByIds(String[] uuids)
    {
        return tenantAppUserMapper.deleteTenantAppUserByIds(uuids);
    }

    /**
     * 删除租户应用用户授权信息
     * 
     * @param uuid 租户应用用户授权ID
     * @return 结果
     */
    @Override
    public int deleteTenantAppUserById(String uuid)
    {
        return tenantAppUserMapper.deleteTenantAppUserById(uuid);
    }
}
