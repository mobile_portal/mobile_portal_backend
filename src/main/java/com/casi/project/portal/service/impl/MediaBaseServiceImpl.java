package com.casi.project.portal.service.impl;

import com.casi.project.portal.domain.MediaBase;
import com.casi.project.portal.mapper.MediaBaseMapper;
import com.casi.project.portal.service.MediaBaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author zhongzj
 * @Description:
 * @Param:
 * @Return:
 * @Create: 2019/11/27
 */
@Service
@Transactional
public class MediaBaseServiceImpl implements MediaBaseService {

    @Autowired
    private MediaBaseMapper baseMapper;

    @Override
    @Transactional(rollbackFor = {Exception.class,RuntimeException.class})
    public boolean updateBase(MediaBase base) {
        return 0 < baseMapper.update(base);
    }
}
