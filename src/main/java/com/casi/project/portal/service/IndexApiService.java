package com.casi.project.portal.service;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.*;
import com.casi.project.portal.domain.vo.NoticeVO;

import java.util.List;
import java.util.Map;

/**
 * @Auther shp
 * @data2020/4/1510:33
 * @description
 */
public interface IndexApiService  {


    /**
     * 返回办公/商店/服务的数据
     * @return
     */
    List<MobileHomeMenu> getAllMenu();

    /**
     * 在"我的应用"添加中出现的菜单
     * @return
     */
    List<MobileHomeMenu> getAddMyAppMenu();

    /**
     * 返回公告的数据
     * @return
     */
    List<NoticeVO> findNotice();


    /**
     * 返回轮播图数据
     * @return
     */
    List<MobileHomeLb> findMobileHomeLb();


    /**
     * 返回新闻资讯的数据
     * @return
     */

    List<MoblieNews> getNews();

    /**
     *移动端新闻资讯的展示
     */
    List<MediaArticle> queryNewsList(String limit);
    /**
     * 移动端新闻资讯详情接口
     */
    MediaArticle queryNewsById(Long id);
    /**
     * 获取更多应用
     * @param typeId
     * @return
     */
    List<MobileHomeMenuDetails> getMoreApply(Long typeId);

    /**
     * 主页搜索
     * @param appName
     * @return
     */
    List<MobileHomeMenuDetails> searchApp(String appName);


    /**
     * 隐藏应用
     * @param id
     * @return
     */
    Long hidden(Long id);

    /**
     * 根据pid 查询对应的添加界面
     * @param pid
     * @return
     */
    List<MobileHomeMenuDetails>  findAddApp(Long pid);



    /**
     * 显示应用
     * @param id
     * @return
     */

    AjaxResult show(String id);



    List<MobileHomeMenuDetails> getMyApp(String show);

    List<MobileHomeMenuDetails> getGroupApp();

    List<MobileHomeMenuDetails> getrRecommendedApp();

    List<MobileHomeMenuDetails> getAppDataListByParams(Map<String, Object> params);

    AjaxResult updateMenuShow(Map<String, Object> params);

    List<Map<String, Object>> getMyAppDetiles(Map<String, Object> params);
}


