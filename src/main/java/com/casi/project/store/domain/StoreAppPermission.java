package com.casi.project.store.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 应用权限对象 store_app_permission
 * 
 * @author casi
 * @date 2020-06-02
 */
public class StoreAppPermission extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private String appId;

    /** 角色编号 */
    @Excel(name = "角色编号")
    private String roleId;

    /** 资源编号 */
    @Excel(name = "资源编号")
    private String resourceId;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setRoleId(String roleId) 
    {
        this.roleId = roleId;
    }

    public String getRoleId() 
    {
        return roleId;
    }
    public void setResourceId(String resourceId) 
    {
        this.resourceId = resourceId;
    }

    public String getResourceId() 
    {
        return resourceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("appId", getAppId())
            .append("roleId", getRoleId())
            .append("resourceId", getResourceId())
            .toString();
    }
}
