package com.casi.project.store.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppTenantMapper;
import com.casi.project.store.domain.StoreAppTenant;
import com.casi.project.store.service.IStoreAppTenantService;

/**
 * 应用租户授权Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppTenantServiceImpl implements IStoreAppTenantService 
{
    @Autowired
    private StoreAppTenantMapper storeAppTenantMapper;

    /**
     * 查询应用租户授权
     * 
     * @param uuid 应用租户授权ID
     * @return 应用租户授权
     */
    @Override
    public StoreAppTenant selectStoreAppTenantById(String uuid)
    {
        return storeAppTenantMapper.selectStoreAppTenantById(uuid);
    }

    /**
     * 查询应用租户授权列表
     * 
     * @param storeAppTenant 应用租户授权
     * @return 应用租户授权
     */
    @Override
    public List<StoreAppTenant> selectStoreAppTenantList(StoreAppTenant storeAppTenant)
    {
        return storeAppTenantMapper.selectStoreAppTenantList(storeAppTenant);
    }

    /**
     * 新增应用租户授权
     * 
     * @param storeAppTenant 应用租户授权
     * @return 结果
     */
    @Override
    public int insertStoreAppTenant(StoreAppTenant storeAppTenant)
    {
        return storeAppTenantMapper.insertStoreAppTenant(storeAppTenant);
    }

    /**
     * 修改应用租户授权
     * 
     * @param storeAppTenant 应用租户授权
     * @return 结果
     */
    @Override
    public int updateStoreAppTenant(StoreAppTenant storeAppTenant)
    {
        return storeAppTenantMapper.updateStoreAppTenant(storeAppTenant);
    }

    /**
     * 批量删除应用租户授权
     * 
     * @param uuids 需要删除的应用租户授权ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppTenantByIds(String[] uuids)
    {
        return storeAppTenantMapper.deleteStoreAppTenantByIds(uuids);
    }

    /**
     * 删除应用租户授权信息
     * 
     * @param uuid 应用租户授权ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppTenantById(String uuid)
    {
        return storeAppTenantMapper.deleteStoreAppTenantById(uuid);
    }
}
