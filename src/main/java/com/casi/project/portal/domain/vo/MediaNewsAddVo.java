package com.casi.project.portal.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @Author zhongzj
 * @Description:总图文新增修改视图类
 * @Create: 2019/11/26
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MediaNewsAddVo {

    /**
     * 栏目id
     */
    private Long columnId;


    /**
     * 主键id
     */
    private Long id;
    /**
     * 素材类型（img:图片，voice:音频，video:视频，news:图文，file:文件，web:网页）
     */
    private String mediaType;
    /**
     * 状态（1：待提交，2：审核中，3：已审核，4：已发布，5：已退回）
     */
    private String status;
    /**
     * 审批流程（a:党群信息审批流程，b:精神宣贯审批流程，c:通知公告审批流程）
     */
    private String approveProcess;
    /**
     * 终端类别（1：微信公众，2：移动平台服务号，3：其他软件终端，4：硬件终端）
     */
    private String terminalType;
    /**
     * 微信公众号id集合
     */
    private String wxList;
    /**
     * 移动平台服务号id集合
     */
    private String ydList;
    /**
     * 其他软件终端id集合
     */
    private String otherList;
    /**
     * 硬件终端id集合
     */
    private String hardwareList;
    /**
     * 备注
     */
    private String remark;
    /**
     * 提交人
     */
    private String submitUser;
    /**
     * 提交时间
     */
    private Date submitTime;
    /**
     * 图文信息
     */
    private List<MediaArticleAddVo> articleList;




}
