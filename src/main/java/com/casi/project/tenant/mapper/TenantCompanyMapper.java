package com.casi.project.tenant.mapper;

import java.util.List;
import com.casi.project.tenant.domain.TenantCompany;

/**
 * 租户企业Mapper接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface TenantCompanyMapper 
{
    /**
     * 查询租户企业
     * 
     * @param uuid 租户企业ID
     * @return 租户企业
     */
    public TenantCompany selectTenantCompanyById(String uuid);

    /**
     * 查询租户企业列表
     * 
     * @param tenantCompany 租户企业
     * @return 租户企业集合
     */
    public List<TenantCompany> selectTenantCompanyList(TenantCompany tenantCompany);

    /**
     * 新增租户企业
     * 
     * @param tenantCompany 租户企业
     * @return 结果
     */
    public int insertTenantCompany(TenantCompany tenantCompany);

    /**
     * 修改租户企业
     * 
     * @param tenantCompany 租户企业
     * @return 结果
     */
    public int updateTenantCompany(TenantCompany tenantCompany);

    /**
     * 删除租户企业
     * 
     * @param uuid 租户企业ID
     * @return 结果
     */
    public int deleteTenantCompanyById(String uuid);

    /**
     * 批量删除租户企业
     * 
     * @param uuids 需要删除的数据ID
     * @return 结果
     */
    public int deleteTenantCompanyByIds(String[] uuids);
}
