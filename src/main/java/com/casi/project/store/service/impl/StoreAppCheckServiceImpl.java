package com.casi.project.store.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppCheckMapper;
import com.casi.project.store.domain.StoreAppCheck;
import com.casi.project.store.service.IStoreAppCheckService;

/**
 * 应用审核Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppCheckServiceImpl implements IStoreAppCheckService 
{
    @Autowired
    private StoreAppCheckMapper storeAppCheckMapper;

    /**
     * 查询应用审核
     * 
     * @param uuid 应用审核ID
     * @return 应用审核
     */
    @Override
    public StoreAppCheck selectStoreAppCheckById(String uuid)
    {
        return storeAppCheckMapper.selectStoreAppCheckById(uuid);
    }

    /**
     * 查询应用审核列表
     * 
     * @param storeAppCheck 应用审核
     * @return 应用审核
     */
    @Override
    public List<StoreAppCheck> selectStoreAppCheckList(StoreAppCheck storeAppCheck)
    {
        return storeAppCheckMapper.selectStoreAppCheckList(storeAppCheck);
    }

    /**
     * 新增应用审核
     * 
     * @param storeAppCheck 应用审核
     * @return 结果
     */
    @Override
    public int insertStoreAppCheck(StoreAppCheck storeAppCheck)
    {
        return storeAppCheckMapper.insertStoreAppCheck(storeAppCheck);
    }

    /**
     * 修改应用审核
     * 
     * @param storeAppCheck 应用审核
     * @return 结果
     */
    @Override
    public int updateStoreAppCheck(StoreAppCheck storeAppCheck)
    {
        return storeAppCheckMapper.updateStoreAppCheck(storeAppCheck);
    }

    /**
     * 批量删除应用审核
     * 
     * @param uuids 需要删除的应用审核ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppCheckByIds(String[] uuids)
    {
        return storeAppCheckMapper.deleteStoreAppCheckByIds(uuids);
    }

    /**
     * 删除应用审核信息
     * 
     * @param uuid 应用审核ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppCheckById(String uuid)
    {
        return storeAppCheckMapper.deleteStoreAppCheckById(uuid);
    }
}
