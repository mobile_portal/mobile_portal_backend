package com.casi.project.portal.controller;

import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.framework.web.domain.server.Sys;
import com.casi.project.portal.domain.DictionaryEnum;
import com.casi.project.portal.domain.MediaBase;
import com.casi.project.portal.domain.StatusEnum;
import com.casi.project.portal.domain.vo.MediaBaseForNewsVo;
import com.casi.project.portal.domain.vo.MediaNewsAddVo;
import com.casi.project.portal.service.MediaNewsService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:基础素材管理控制器
 * @Param:
 */
@RestController
//@RequestMapping("/portal/news")
@RequestMapping("/news")
@Api(tags = "新闻管理相关接口")
public class MediaNewsController extends BaseController {

    private static Logger logger = LoggerFactory.getLogger(MediaNewsController.class);

    @Autowired
    private MediaNewsService mediaNewsService;


    /**
     * @param
     * @return int
     * @Description: 图文列表获取
     */
    @GetMapping("list")
    @ApiOperation("查询新闻信息")
    // 状态（1：待提交，2：审核中，3：已审核，4：已发布，5：已退回）
    public AjaxResult queryList(MediaBase mediaBase) {

        AjaxResult ajaxResult = new AjaxResult();

        //调用service获取列表
        PageHelper.startPage(mediaBase.getPageNum(), mediaBase.getPageSize());

        List<MediaBaseForNewsVo> mediaBaseList = mediaNewsService.queryList(mediaBase);

        PageInfo<MediaBaseForNewsVo> pageInfo = new PageInfo<>(mediaBaseList);

        ajaxResult.put("code", 200);
        ajaxResult.put("msg", "查询成功");
        //返回data放入map中
        ajaxResult.put("data", pageInfo);
        return ajaxResult;
    }

    /**
     * @param
     * @return int
     * @Description: 素材详细信息获取
     */
    @ApiOperation("新闻详细信息获取")
    @GetMapping("detail")
    public AjaxResult queryDetail(MediaBase mediaBase) {

        AjaxResult ajaxResult = new AjaxResult();

        if (null == mediaBase.getId() || 0 == mediaBase.getId()) {
            ajaxResult.put("code", 500);
            ajaxResult.put("msg", "id不能为空");
            return ajaxResult;
        }

        //调用service获取列表
        MediaBase base = mediaNewsService.queryDetail(mediaBase.getId());

        if (null != base) {
            ajaxResult.put("code", 200);
            ajaxResult.put("msg", "查询成功");
            ajaxResult.put("data", base);
        } else {
            ajaxResult.put("code", 500);
            ajaxResult.put("msg", "查询失败");
        }
        return ajaxResult;
    }

    /**
     * @param
     * @return int
     * @Description: 保存素材
     */
    @ApiOperation("添加新闻信息")
    @PostMapping("/add")
    public AjaxResult add(@RequestBody MediaNewsAddVo params_one) {
        AjaxResult ajaxResult = new AjaxResult();
        if (params_one == null || params_one.getArticleList() == null) {
            ajaxResult.put("code", 500);
            ajaxResult.put("msg", "参数不能位空");
            return ajaxResult;
        }
        if (params_one.getArticleList().get(0).getFileId() == null || params_one.getArticleList().get(0).getFileId() == 0) {
            ajaxResult.put("code", 500);
            ajaxResult.put("msg", "文件还没有上传成功");
            return ajaxResult;
        }
        params_one.setStatus(StatusEnum.UNDER_REVIEW.toString());
        //调用service保存素材信息
        try {
            if (mediaNewsService.add(params_one, DictionaryEnum.Add.toString())) {
                ajaxResult.put("code", 200);
                ajaxResult.put("msg", "图文素材保存成功");
            } else {
                ajaxResult.put("code", 500);
                ajaxResult.put("msg", "图文素材保存失败 没有添加进去");
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.put("code", 500);
            ajaxResult.put("msg", "图文素材保存失败 代码异常请检查");
        }
        return ajaxResult;
    }

    /**
     * @param
     * @return int
     * @Description: 更新图文
     */
    @ApiOperation("更新新闻信息")
    @PostMapping("update")
    public AjaxResult update(@RequestBody MediaNewsAddVo newsAddVo, HttpServletRequest request) {
        AjaxResult ajaxResult = new AjaxResult();

        if (null == newsAddVo.getId() || 0 == newsAddVo.getId()) {
            ajaxResult.put("code", 500);
            ajaxResult.put("msg", "id不能为空");
            return ajaxResult;
        }


        //调用service更新素材信息
        try {
            if (mediaNewsService.update(newsAddVo, request)) {
                ajaxResult.put("code", 200);
                ajaxResult.put("msg", "图文更新成功");
            } else {
                ajaxResult.put("code", 500);
                ajaxResult.put("msg", "图文更新失败 没有修改成功");
            }
        } catch (Exception e) {
            e.printStackTrace();
            ajaxResult.put("code", 500);
            ajaxResult.put("msg", "图文更新失败 请检查代码");
        }
        return ajaxResult;
    }

    /**
     * @param
     * @return int
     * @Description: 删除素材
     */
    @ApiOperation("删除新闻信息")
    @GetMapping("/delete")
    public AjaxResult delete(MediaBase mediaBase) {

        //调用service删除数据库中素材信息
        AjaxResult ajaxResult = new AjaxResult();
        if (null == mediaBase.getId() || 0 == mediaBase.getId()) {
            ajaxResult.put("code", 500);
            ajaxResult.put("msg", "id不能为空");
            return ajaxResult;
        }
        try {
            if (mediaNewsService.delete(mediaBase)) {
                ajaxResult.put("code", 200);
                ajaxResult.put("msg", "图文删除成功");
            } else {
                ajaxResult.put("code", 500);
                ajaxResult.put("msg", "没有对应的新闻信息");
            }
        } catch (Exception e) {
            ajaxResult.put("code", 0);
            ajaxResult.put("msg", "图文删除失败 请检查代码");
        }
        return ajaxResult;
    }

    /**
     * 资讯管理审核接口
     */
    @ApiOperation(value = "资讯管理审核")
    @PostMapping("/auditNews")
    public AjaxResult auditNews(@RequestBody MediaBase mediaBase) {
        return mediaNewsService.auditNews(mediaBase);
    }

    /**
     * 资讯管理发布接口
     */
    @ApiOperation(value = "资讯管理发布")
    @PostMapping("/publishNews")
    public AjaxResult publishNews(@RequestBody MediaBase mediaBase) {
        return mediaNewsService.publishNews(mediaBase);
    }

    /**
     * 资讯管理退回接口
     */
    @GetMapping("/sendBackNews/{id}")
    public AjaxResult sendBackNews(@PathVariable("id") String id) {
        return mediaNewsService.sendBackNews(id);
    }

    /**
     * 删除素材
     */
    @ApiOperation("删除新闻信息")
    @GetMapping("/deleteById")
    public AjaxResult deleteById(MediaBase mediaBase) {
        return mediaNewsService.deleteById(mediaBase);
    }
}
