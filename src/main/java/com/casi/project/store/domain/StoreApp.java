package com.casi.project.store.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 应用详情对象 store_app
 * 
 * @author casi
 * @date 2020-06-02
 */
public class StoreApp extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 是否允许评分 */
    private Integer allowRate;

    /** 浏览数 */
    @Excel(name = "浏览数")
    private String views;

    /** 应用名称 */
    @Excel(name = "应用名称")
    private String app_name;

    /** 是否允许评论 */
    private Integer allowComment;

    /** 下载数 */
    @Excel(name = "下载数")
    private String dowloads;

    /** logo */
    private String logo;

    /** 供应商 */
    @Excel(name = "供应商")
    private String supplierId;

    /** 应用类别 */
    @Excel(name = "应用类别")
    private String categoryId;

    /** 开发者 */
    @Excel(name = "开发者")
    private String developerId;

    /** 最新版本 */
    @Excel(name = "最新版本")
    private String lastVersion;

    /** 应用介绍 */
    private String intro;

    /** 评分 */
    private Double rate;

    /** 状态 */
    @Excel(name = "状态")
    private String status;

    /** 是否推荐 */
    private Integer isRecommend;

    /** 是否公开 */
    @Excel(name = "是否公开")
    private String isPublic;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setAllowRate(Integer allowRate) 
    {
        this.allowRate = allowRate;
    }

    public Integer getAllowRate() 
    {
        return allowRate;
    }
    public void setViews(String views) 
    {
        this.views = views;
    }

    public String getViews() 
    {
        return views;
    }
    public void setApp_name(String app_name) 
    {
        this.app_name = app_name;
    }

    public String getApp_name() 
    {
        return app_name;
    }
    public void setAllowComment(Integer allowComment) 
    {
        this.allowComment = allowComment;
    }

    public Integer getAllowComment() 
    {
        return allowComment;
    }
    public void setDowloads(String dowloads) 
    {
        this.dowloads = dowloads;
    }

    public String getDowloads() 
    {
        return dowloads;
    }
    public void setLogo(String logo) 
    {
        this.logo = logo;
    }

    public String getLogo() 
    {
        return logo;
    }
    public void setSupplierId(String supplierId) 
    {
        this.supplierId = supplierId;
    }

    public String getSupplierId() 
    {
        return supplierId;
    }
    public void setCategoryId(String categoryId) 
    {
        this.categoryId = categoryId;
    }

    public String getCategoryId() 
    {
        return categoryId;
    }
    public void setDeveloperId(String developerId) 
    {
        this.developerId = developerId;
    }

    public String getDeveloperId() 
    {
        return developerId;
    }
    public void setLastVersion(String lastVersion) 
    {
        this.lastVersion = lastVersion;
    }

    public String getLastVersion() 
    {
        return lastVersion;
    }
    public void setIntro(String intro) 
    {
        this.intro = intro;
    }

    public String getIntro() 
    {
        return intro;
    }
    public void setRate(Double rate) 
    {
        this.rate = rate;
    }

    public Double getRate() 
    {
        return rate;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }
    public void setIsRecommend(Integer isRecommend) 
    {
        this.isRecommend = isRecommend;
    }

    public Integer getIsRecommend() 
    {
        return isRecommend;
    }
    public void setIsPublic(String isPublic) 
    {
        this.isPublic = isPublic;
    }

    public String getIsPublic() 
    {
        return isPublic;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("allowRate", getAllowRate())
            .append("views", getViews())
            .append("app_name", getApp_name())
            .append("allowComment", getAllowComment())
            .append("dowloads", getDowloads())
            .append("logo", getLogo())
            .append("supplierId", getSupplierId())
            .append("categoryId", getCategoryId())
            .append("developerId", getDeveloperId())
            .append("lastVersion", getLastVersion())
            .append("createTime", getCreateTime())
            .append("intro", getIntro())
            .append("createTime", getCreateTime())
            .append("rate", getRate())
            .append("status", getStatus())
            .append("isRecommend", getIsRecommend())
            .append("isPublic", getIsPublic())
            .toString();
    }
}
