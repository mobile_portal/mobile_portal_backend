package com.casi.project.screen.domain;

import lombok.Data;

/**
 * @Auther: lizhibin
 * @Date: 2020/5/11 18:21
 * @Description:
 */
@Data
public class DimUnit {
    public String orgcode; //组织编码

    public String orgname; //组织名称

    public String immeuporg;//上级单位编码

    public String uplevlname; //上级单位名称

    public String filltwoorg;//所属二级单位编码

    public  String secendDeptName;//所属二级单位名称

    public  String createDate;



}
