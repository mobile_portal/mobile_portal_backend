package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreApp;
import com.casi.project.store.service.IStoreAppService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用详情Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用详情"})
@RestController
@RequestMapping("/store/app")
public class StoreAppController extends BaseController
{
    @Autowired
    private IStoreAppService storeAppService;

    /**
     * 查询应用详情列表
     */
    @ApiOperation(value = "查询应用详情列表")
    @PreAuthorize("@ss.hasPermi('store:app:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreApp storeApp)
    {
        startPage();
        List<StoreApp> list = storeAppService.selectStoreAppList(storeApp);
        return getDataTable(list);
    }

    /**
     * 导出应用详情列表
     */
    @PreAuthorize("@ss.hasPermi('store:app:export')")
    @Log(title = "应用详情", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreApp storeApp)
    {
        List<StoreApp> list = storeAppService.selectStoreAppList(storeApp);
        ExcelUtil<StoreApp> util = new ExcelUtil<StoreApp>(StoreApp.class);
        return util.exportExcel(list, "app");
    }

    /**
     * 获取应用详情详细信息
     */
    @ApiOperation(value = "获取应用详情详细信息")
    @PreAuthorize("@ss.hasPermi('store:app:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppService.selectStoreAppById(uuid));
    }

    /**
     * 新增应用详情
     */
    @ApiOperation(value = "新增应用详情")
    @PreAuthorize("@ss.hasPermi('store:app:add')")
    @Log(title = "应用详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreApp storeApp)
    {
        return toAjax(storeAppService.insertStoreApp(storeApp));
    }

    /**
     * 修改应用详情
     */
    @ApiOperation(value = "修改应用详情")
    @PreAuthorize("@ss.hasPermi('store:app:edit')")
    @Log(title = "应用详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreApp storeApp)
    {
        return toAjax(storeAppService.updateStoreApp(storeApp));
    }

    /**
     * 删除应用详情
     */
    @ApiOperation(value = "删除应用详情")
    @PreAuthorize("@ss.hasPermi('store:app:remove')")
    @Log(title = "应用详情", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppService.deleteStoreAppByIds(uuids));
    }
}
