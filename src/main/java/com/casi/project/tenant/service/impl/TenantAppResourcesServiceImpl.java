package com.casi.project.tenant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.tenant.mapper.TenantAppResourcesMapper;
import com.casi.project.tenant.domain.TenantAppResources;
import com.casi.project.tenant.service.ITenantAppResourcesService;

/**
 * 租户应用资源Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class TenantAppResourcesServiceImpl implements ITenantAppResourcesService 
{
    @Autowired
    private TenantAppResourcesMapper tenantAppResourcesMapper;

    /**
     * 查询租户应用资源
     * 
     * @param uuid 租户应用资源ID
     * @return 租户应用资源
     */
    @Override
    public TenantAppResources selectTenantAppResourcesById(String uuid)
    {
        return tenantAppResourcesMapper.selectTenantAppResourcesById(uuid);
    }

    /**
     * 查询租户应用资源列表
     * 
     * @param tenantAppResources 租户应用资源
     * @return 租户应用资源
     */
    @Override
    public List<TenantAppResources> selectTenantAppResourcesList(TenantAppResources tenantAppResources)
    {
        return tenantAppResourcesMapper.selectTenantAppResourcesList(tenantAppResources);
    }

    /**
     * 新增租户应用资源
     * 
     * @param tenantAppResources 租户应用资源
     * @return 结果
     */
    @Override
    public int insertTenantAppResources(TenantAppResources tenantAppResources)
    {
        return tenantAppResourcesMapper.insertTenantAppResources(tenantAppResources);
    }

    /**
     * 修改租户应用资源
     * 
     * @param tenantAppResources 租户应用资源
     * @return 结果
     */
    @Override
    public int updateTenantAppResources(TenantAppResources tenantAppResources)
    {
        return tenantAppResourcesMapper.updateTenantAppResources(tenantAppResources);
    }

    /**
     * 批量删除租户应用资源
     * 
     * @param uuids 需要删除的租户应用资源ID
     * @return 结果
     */
    @Override
    public int deleteTenantAppResourcesByIds(String[] uuids)
    {
        return tenantAppResourcesMapper.deleteTenantAppResourcesByIds(uuids);
    }

    /**
     * 删除租户应用资源信息
     * 
     * @param uuid 租户应用资源ID
     * @return 结果
     */
    @Override
    public int deleteTenantAppResourcesById(String uuid)
    {
        return tenantAppResourcesMapper.deleteTenantAppResourcesById(uuid);
    }
}
