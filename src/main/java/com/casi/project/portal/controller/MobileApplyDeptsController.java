package com.casi.project.portal.controller;

import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.DoubleArray;
import com.casi.project.portal.service.MobileApplyDeptsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = { "应用授权管理" })
@RestController
@RequestMapping("/mobileApplyDepts")
public class MobileApplyDeptsController {
    @Autowired
    private MobileApplyDeptsService mobileApplyDeptsService;

    /**
     * 应用指定开发账户（添加应用与组织的关联关系）
     */
    @ApiOperation(value = "添加应用与组织的关联关系")
    @Log(title = "应用授权组织", businessType = BusinessType.INSERT)
    @PostMapping("/addApplyIdsAndDeptIds")
    public AjaxResult addApplyIdsAndDeptIds(@RequestBody DoubleArray doubleArray) {
        return mobileApplyDeptsService.addApplyIdsAndDeptIds(doubleArray.getApplyIds(), doubleArray.getDeptIds());
    }

    /**
     * 回显已经授权的组织机构
     *
     * @param doubleArray
     * @return
     */
    @ApiOperation(value = "回显已经授权的组织机构")
    @PostMapping("/findApplyIdsAndDeptIds")
    public AjaxResult findApplyIdsAndDeptIds(@RequestBody DoubleArray doubleArray) {
        return mobileApplyDeptsService.findApplyIdsAndDeptIds(doubleArray);
    }
}
