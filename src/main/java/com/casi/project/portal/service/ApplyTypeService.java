package com.casi.project.portal.service;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.ApplyIType;
import com.casi.project.portal.domain.vo.ApplyType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 应用类型
 * @author 18732
 */
public interface ApplyTypeService {

    /**
     * 新增应用类型
     *
     * @param applyType
     * @return
     */
    AjaxResult addApplyType(ApplyIType applyType);

    /**
     * 根据id回显应用类型具体详情
     *
     * @param id
     * @return
     */
    ApplyIType getApplyTypeId(String id);

    /**
     * 修改应用类型
     *
     * @param applyType
     * @return
     */
    AjaxResult updateApplyType(ApplyIType applyType);

    /**
     * 禁用启用应用类型
     *
     * @param id
     * @param type
     * @return
     */
    AjaxResult disableEnabledApplyType(String[] id, Integer type);

    /**
     * 查询应用类型下拉列表
     *
     * @return
     */
    public ArrayList<ApplyType> getApplyTypeList();

    /**
     * 查询应用类型列表
     *
     * @return
     */
    ArrayList<ApplyType> getApplyTypeListPage();

    /**
     * 查询首页menu名称下拉列表
     *
     * @return
     */
    List<HashMap<String, Object>> getHomePageList();
}
