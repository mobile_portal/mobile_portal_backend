package com.casi.project.portal.domain.thirdParty;

import lombok.Data;

/**
 *  第三方用户登录对象
 *
 */
@Data
public class ThirdPartyLoginBody
{

    private UserInfo userInfo;
    private Boolean admin;


}
