package com.casi.project.portal.domain;

import java.util.Date;

/**
 * @Description:单一图文实体类
 * @author zhongzj
 * @date 2019/11/16
 */


public class MediaArticle {
  /**
   * 主键id
   */
  private Long id;
  /**
   * 图文标题
   */
  private String title;
  /**
   * 封面图片绝对路径（这个暂时不用，用的是下面那个）
   */
  private String picDir;
  /**
   * 封面图片url
   */
  private String  picUrl;
  /**
   * 作者
   */
  private String author;
  /**
   * 摘要
   */
  private String digest;
  /**
   * 图文具体内容
   */
  private String content;
  /**
   * 是否显示封面（1：显示  0：不显示）
   */
  private String showCoverPic;
  /**
   * 多图文表主键id
   */
  private Long newsId;
  /**
   * 多图文中排序顺序
   */
  private Long newsIndex;
  /**
   * 封面文件名称
   */
  private String picName;
  /**
   * 封面图片id
   */
  private Long fileId;

  /**
   * 栏目id
   */
  private Long columnId;

  /**
   * 新闻跳转路径
   */
  private String newsUrl;

  /**
   * 创建时间
   */
  private String createTime;

  public MediaArticle() {
  }



  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getPicDir() {
    return  picDir;
  }

  public void setPicDir(String picDir) {
    this.picDir = picDir;
  }

  public String getPicUrl() {

    return  picUrl;
  }

  public void setPicUrl(String picUrl) {
    this.picUrl = picUrl;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getDigest() {
    return digest;
  }

  public void setDigest(String digest) {
    this.digest = digest;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getShowCoverPic() {
    return showCoverPic;
  }

  public void setShowCoverPic(String showCoverPic) {
    this.showCoverPic = showCoverPic;
  }

  public Long getNewsId() {
    return newsId;
  }

  public void setNewsId(Long newsId) {
    this.newsId = newsId;
  }

  public Long getNewsIndex() {
    return newsIndex;
  }

  public void setNewsIndex(Long newsIndex) {
    this.newsIndex = newsIndex;
  }

  public String getPicName() {
    return picName;
  }

  public void setPicName(String picName) {
    this.picName = picName;
  }

  public Long getFileId() {
    return fileId;
  }

  public void setFileId(Long fileId) {
    this.fileId = fileId;
  }

  public Long getColumnId() {
    return columnId;
  }

  public void setColumnId(Long columnId) {
    this.columnId = columnId;
  }

  public String getNewsUrl() {
    return newsUrl;
  }

  public void setNewsUrl(String newsUrl) {
    this.newsUrl = newsUrl;
  }

  @Override
  public String toString() {
    return "MediaArticle{" +
            "id=" + id +
            ", title='" + title + '\'' +
            ", picDir='" + picDir + '\'' +
            ", picUrl='" + picUrl + '\'' +
            ", author='" + author + '\'' +
            ", digest='" + digest + '\'' +
            ", content='" + content + '\'' +
            ", showCoverPic='" + showCoverPic + '\'' +
            ", newsId=" + newsId +
            ", newsIndex=" + newsIndex +
            ", picName='" + picName + '\'' +
            ", fileId=" + fileId +
            ", columnId=" + columnId +
            ", newsUrl='" + newsUrl + '\'' +
            ", createTime=" + createTime +
            '}';
  }

  public void setCreateTime(String createTime) {
    this.createTime = createTime;
  }

  public String getCreateTime() {
    return createTime;
  }
}
