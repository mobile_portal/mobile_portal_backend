package com.casi.project.portal.domain.vo;

import java.util.Date;

/**
 * @Author zhongzj
 * @Description:网页新增修改VO类
 * @Create: 2019/12/2
 */
public class MediaWebAddVo {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 素材类型（image:图片，voice:音频，video:视频，news:图文，file:文件，web:网页）
     */
    private String mediaType;
    /**
     * 状态（1：待提交，2：审核中，3：已审核，4：已发布，5：已退回）
     */
    private String status;
    /**
     * 审批流程
     */
    private String approveProcess;
    /**
     * 终端类别（1：微信公众，2：移动平台服务号，3：其他软件终端，4：硬件终端）
     */
    private String terminalType;
    /**
     * 微信公众号id集合
     */
    private String wxList;
    /**
     * 移动平台服务号id集合
     */
    private String ydList;
    /**
     * 其他软件终端id集合
     */
    private String otherList;
    /**
     * 硬件终端id集合
     */
    private String hardwareList;
    /**
     * 备注
     */
    private String remark;
    /**
     * 提交人
     */
    private String submitUser;
    /**
     * 提交时间
     */
    private Date submitTime;
    /**
     * 网页名称
     */
    private String name;
    /**
     * 网页地址
     */
    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApproveProcess() {
        return approveProcess;
    }

    public void setApproveProcess(String approveProcess) {
        this.approveProcess = approveProcess;
    }

    public String getTerminalType() {
        return terminalType;
    }

    public void setTerminalType(String terminalType) {
        this.terminalType = terminalType;
    }

    public String getWxList() {
        return wxList;
    }

    public void setWxList(String wxList) {
        this.wxList = wxList;
    }

    public String getYdList() {
        return ydList;
    }

    public void setYdList(String ydList) {
        this.ydList = ydList;
    }

    public String getOtherList() {
        return otherList;
    }

    public void setOtherList(String otherList) {
        this.otherList = otherList;
    }

    public String getHardwareList() {
        return hardwareList;
    }

    public void setHardwareList(String hardwareList) {
        this.hardwareList = hardwareList;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSubmitUser() {
        return submitUser;
    }

    public void setSubmitUser(String submitUser) {
        this.submitUser = submitUser;
    }

    public Date getSubmitTime() {
        return submitTime;
    }

    public void setSubmitTime(Date submitTime) {
        this.submitTime = submitTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
