package com.casi.project.portal.service;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.MobileNotice;
import com.casi.project.portal.domain.vo.NoticeVO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public interface NoticePcService {

    /**
     * 查询公告列表
     *
     * @return
     */
    public AjaxResult getNoticeList(HashMap<String,Object> map);

    AjaxResult addNotice(MobileNotice mobileNotice);

    /**
     * 根据ID查询公告具体详情
     *
     * @param
     * @return
     */
    public AjaxResult getNoticeId(NoticeVO noticeVO);
    /**
     * 审核公告数据回显
     *
     */
    public AjaxResult getNoticeById(NoticeVO noticeVO);
    /**
     * 审核公告数据回显接口
     */
    public Map<String,Object> getNoticeId1(MobileNotice notice);

    /**
     * 修改公共
     *
     * @param mobileNotice
     * @return
     */
    AjaxResult updateNotice(MobileNotice mobileNotice);

    /**
     * 删除公告
     *
     * @param id
     * @return
     */
    AjaxResult deleteNotice(String id);

    /**
     * 审核公告
     *
     * @param
     * @return
     */
    AjaxResult checkNoticeStatus(HashMap<String,Object> map);

    /**
     * 发布公告
     * @param id
     * @return
     */
    AjaxResult publishNoticeStatus(String [] id,Integer type);
}
