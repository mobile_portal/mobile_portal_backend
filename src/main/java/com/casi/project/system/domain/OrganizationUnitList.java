package com.casi.project.system.domain;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class OrganizationUnitList {
    private List<Institution> organizationUnitList;

    @Override
    public String toString() {
        return "OrganizationUnitList{" +
                "organizationUnitList=" + organizationUnitList +
                '}';
    }

    public List<Institution> getOrganizationUnitList() {
        return organizationUnitList;
    }

    public void setOrganizationUnitList(List<Institution> organizationUnitList) {
        this.organizationUnitList = organizationUnitList;
    }
}
