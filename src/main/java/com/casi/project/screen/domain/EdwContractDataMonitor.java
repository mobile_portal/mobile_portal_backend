package com.casi.project.screen.domain;

import lombok.Data;

/**
 * @Auther: lizhibin
 * @Date: 2020/5/9 11:42
 * @Description:
 */
@Data
public class EdwContractDataMonitor {



    /**
     * 合同所属单位编码
     */
    private String unitCode;

    /**
     * 合同所属单位名称
     */
    private String unitName;

    /**
     * 时间
     */
    private String todayDate;

    /**
     * 采集总数据量
     */
    private  Integer dataCollectTotal;
    /**
     * 今日采集总数据量
     */
    private  Integer dataCollectToday;


}
