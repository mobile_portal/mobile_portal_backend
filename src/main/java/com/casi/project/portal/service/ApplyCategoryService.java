package com.casi.project.portal.service;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.vo.ApplyCategory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 应用类别
 *
 * @author 18732
 */
public interface ApplyCategoryService {
    /**
     * 新增应用类别
     *
     * @param hashMap
     * @return
     */
    AjaxResult addApplyCategory(HashMap<String, Object> hashMap);

    /**
     * 回显应用类别
     *
     * @param id
     * @return
     */
    Map<String, Object> findApplyCategoryId(String id);

    /**
     * 修改应用类别
     *
     * @param hashMap
     * @return
     */
    public AjaxResult updateApplyCategory(HashMap<String, Object> hashMap);

    /**
     * 禁用启用应用类别
     *
     * @param id
     * @param type
     * @return
     */
    AjaxResult disableEnabledCategory(String[] id, Integer type);

    /**
     * 根据上级ID查询应用类别下拉列表
     *
     * @param id
     * @return
     */
    public ArrayList<ApplyCategory> getApplyCategoryId();

    /**
     * 查询应用类别列表
     *
     * @return
     */
    ArrayList<ApplyCategory> getApplyCategoryListPage();
}
