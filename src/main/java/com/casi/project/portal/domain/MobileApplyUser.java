package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * 应用类型
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MobileApplyUser implements Serializable {
    private Long applyId;
    private String userId;
    private String deptId;
    private Date createTime;


}
