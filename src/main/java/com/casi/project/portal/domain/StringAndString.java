package com.casi.project.portal.domain;

import lombok.Data;

/**
 * @author css
 * @date 2020/5/12 19:01
 */
@Data
public class StringAndString {

    private String mobileApplyId;
    private String userId;

}
