package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.ApplyDetails;
import com.casi.project.portal.domain.ApplyVersion;
import com.casi.project.portal.domain.MobileHomeMenuDetails;
import com.casi.project.system.domain.SysUserRole;
import org.apache.ibatis.annotations.Param;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 应用详细表操作
 *
 * @author 18732
 */
public interface ApplyDetailsMapper {

    /**
     * 判断应用类别名称是否唯一
     *
     * @param menuButtonName
     * @return
     */
    public int checkMenuButtonNameUnique(@Param("menuButtonName") String menuButtonName);

    /**
     * 添加应用明细
     *
     * @param applyDetails
     * @return
     */
    public int addApplyDetails(ApplyDetails applyDetails);

    /**
     * 回显应用明细
     *
     * @param id
     * @return
     */
    ApplyDetails findApplyDetailsId(@Param("id") String id);

    /**
     * 修改应用
     *
     * @param applyDetails1
     * @return
     */
    int updateApplyDetails(ApplyDetails applyDetails1);

    //    int deleteApplyDept(@Param("deptId") Long deptId, @Param("id") Long id);

    /**
     * 删除应用明细表
     *
     * @param id
     * @return
     */
    int deleteApplyDetails(@Param("id") String id);

    //删除应用范围表
    //int deleteApplication(@Param("id") Long id);

    /**
     * 禁用应用
     *
     * @param id
     * @param type
     * @return
     */
    int disableEnabledDetails(@Param("id") String id, @Param("type") Integer type);

    /**
     * 查询应用列表
     *
     * @param menuButtonName
     * @return
     */
    List<Map<String, Object>> getApplyList(@Param("menuButtonName") String menuButtonName);

    /**
     * 查询指定人员的应用列表
     *
     * @param menuButtonName
     * @param applyId
     * @return
     */
    Map<String, Object> getApplyListId(@Param("menuButtonName") String menuButtonName, @Param("applyId") String applyId);

    /**
     * 判断应用是否审核通过
     *
     * @param id
     * @return
     */
    Map<String, Object> selectCheckApplyStatus(@Param("id") String id);

    /**
     * 审核应用
     *
     * @param hashMap
     * @return
     */
    int checkApplyStatus(HashMap<String, Object> hashMap);

    /**
     * 上线
     *
     * @param id
     * @return
     */
    int upLineApply(@Param("id") String id);

    public int addApplyVersion(ApplyVersion applyVersion);

    /**
     * 判断应用范围表中是否存在应用id
     *
     * @param applyId
     * @return
     */
    int getIsApplyIdExists(@Param("applyId") Long applyId);

    /**
     * 根据应用id回显应用类型名称
     *
     * @param applyTypeId
     * @return
     */
    HashMap<String, Object> getApplyTypeName(@Param("applyTypeId") String applyTypeId);

    /**
     * 根据应用id回显应用类别名称
     *
     * @param buttonParentId
     * @return
     */
    HashMap<String, Object> getApplyCategoryName(@Param("buttonParentId") String buttonParentId);


    /**
     * 新增应用的轮播图到轮播图表中
     *
     * @param hashMap
     * @return
     */
    int addApplyCarousel(HashMap<String, Object> hashMap);

    /**
     * 查询应用封面轮播图的地址
     *
     * @param id
     * @return
     */
    HashMap<String, Object> getApplyLb(@Param("id") String id);

    /**
     * 修改轮播图
     *
     * @param applyDetails1
     * @return
     */
    int updateApplyLb(ApplyDetails applyDetails1);

    /**
     * 删除轮播图
     *
     * @param id1
     * @return
     */
    int deleteApplyLb(@Param("id1") String id1);

    /**
     * 查询轮播图个数
     */
    int selectApplyLbCount(@Param("id") String id);

    /**
     * 应用指定开发者账户——应用列表展示
     */
    public List<Map<String, Object>> selectAllApplyDetails();

    /**
     * 指定应用开发管理员
     *
     * @param userAndRole
     * @return
     */
    int addApplyDevelopAdminIsTraTor(@Param("userAndRole") List<SysUserRole> userAndRole);

    /**
     * 判断用户和角色id是否存在
     *
     * @param userId
     * @param roleId
     * @return
     */
    int selectIsUserRoleExists(@Param("userId") String userId, @Param("roleId") String roleId);

    /**
     * 查询用户名字
     *
     * @param userId
     * @return
     */
    String selectUserName(@Param("userId") String userId);

    /**
     * 查询用户和部门的应用id
     *
     * @param userId
     * @param deptId
     * @return
     */
    List<HashMap<String, Object>> getApplyId(@Param("userId") String userId, @Param("deptId") String deptId);

    /**
     * 判断用户和应用是否在黑名单中
     *
     * @param userId
     * @param applyId
     * @return
     */
    int selectIsBlackList(@Param("userId") String userId, @Param("applyId") String applyId);

    /**
     * 根据应用appKey和apkSecret获得应用的资源
     *
     * @param hashMap
     * @return
     */
    List<HashMap<String, Object>> getApplyResource(HashMap<String, Object> hashMap);


    /**
     * 根据应用appKey和apkSecret获得应用的用户id
     *
     * @param appKey
     * @param appSecret
     * @return
     */
    List<HashMap<String, Object>> getApplyUser(@Param("appKey") String appKey, @Param("appSecret") String appSecret);

    /**
     * 根据应用appKey，appSecret,token获得用户的用户权限
     *
     * @param appKey
     * @param appSecret
     * @param userId
     * @return
     */
    List<HashMap<String, Object>> getApplyUserPermission(@Param("appKey") String appKey, @Param("appSecret") String appSecret, @Param("userId") String userId);

    String selectApplyDetailsByMenuButtonName(@Param("menuButtonName") String menuButtonName);


    int selectApplyUsersCount(@Param("applyId") String id, @Param("userId") String userId);

    int addApplyUsersShow(@Param("applyId") String id, @Param("userId") String userId);

}
