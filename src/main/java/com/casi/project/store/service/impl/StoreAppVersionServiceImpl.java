package com.casi.project.store.service.impl;

import java.util.List;
import com.casi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppVersionMapper;
import com.casi.project.store.domain.StoreAppVersion;
import com.casi.project.store.service.IStoreAppVersionService;

/**
 * 应用版本Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppVersionServiceImpl implements IStoreAppVersionService 
{
    @Autowired
    private StoreAppVersionMapper storeAppVersionMapper;

    /**
     * 查询应用版本
     * 
     * @param uuid 应用版本ID
     * @return 应用版本
     */
    @Override
    public StoreAppVersion selectStoreAppVersionById(String uuid)
    {
        return storeAppVersionMapper.selectStoreAppVersionById(uuid);
    }

    /**
     * 查询应用版本列表
     * 
     * @param storeAppVersion 应用版本
     * @return 应用版本
     */
    @Override
    public List<StoreAppVersion> selectStoreAppVersionList(StoreAppVersion storeAppVersion)
    {
        return storeAppVersionMapper.selectStoreAppVersionList(storeAppVersion);
    }

    /**
     * 新增应用版本
     * 
     * @param storeAppVersion 应用版本
     * @return 结果
     */
    @Override
    public int insertStoreAppVersion(StoreAppVersion storeAppVersion)
    {
        return storeAppVersionMapper.insertStoreAppVersion(storeAppVersion);
    }

    /**
     * 修改应用版本
     * 
     * @param storeAppVersion 应用版本
     * @return 结果
     */
    @Override
    public int updateStoreAppVersion(StoreAppVersion storeAppVersion)
    {
        storeAppVersion.setUpdateTime(DateUtils.getNowDate());
        return storeAppVersionMapper.updateStoreAppVersion(storeAppVersion);
    }

    /**
     * 批量删除应用版本
     * 
     * @param uuids 需要删除的应用版本ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppVersionByIds(String[] uuids)
    {
        return storeAppVersionMapper.deleteStoreAppVersionByIds(uuids);
    }

    /**
     * 删除应用版本信息
     * 
     * @param uuid 应用版本ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppVersionById(String uuid)
    {
        return storeAppVersionMapper.deleteStoreAppVersionById(uuid);
    }
}
