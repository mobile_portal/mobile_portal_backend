package com.casi.project.portal.util;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author zhongzj
 * @Description:
 * @Create: 2019/10/14
 */
public class CommonApi {
    //图片上传路径(windows)
    public static final String UPLOAD_IMG_URL = "C:/Users/MECHREVO/Pictures/";
    //图片上传路径(linux)
    public static final String UPLOAD_IMG_URL_BASE = "/data/muicenter/files/";
    public static final String MM_API = "http://192.168.0.201:8096/files/";
    public static final String LOCAL_API = "http://192.168.31.29:8096/Pictures/";

    // token 接口
    public static final String TOKEN = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s";

    // 创建菜单
    public static final String MENU_CREATE = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=%s";

    // 创建个性化菜单
    public static final String MENU_ADDCONDITIONAL = "https://api.weixin.qq.com/cgi-bin/menu/addconditional?access_token=%s";

    // 删除菜单
    public static final String MENU_DELETE = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=%s";

    // 获取账号粉丝信息
    public static final String GET_FANS_INFO = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=zh_CN";

    // 获取账号粉丝列表
    public static final String GET_FANS_LIST = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s";

    // 获取批量素材
    public static final String GET_BATCH_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=%s";

    // 上传多媒体资料接口-临时
    public static final String UPLOAD_MEDIA = "http://api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s";

    // 上传永久素材：图文-临时
    public static final String UPLOAD_NEWS = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=%s";

    // 群发接口
    public static final String MASS_SEND = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=%s";

    // 上传永久图片素材
    public static final String UPLOAD_MATERIAL_IMG = "https://api.weixin.qq.com/cgi-bin/media/uploadimg?access_token=%s";

    // 新增其他类型永久素材
    public static final String ADD_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=%s&type=%s";

    // 新增永久图文素材
    public static final String ADD_NEWS_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/add_news?access_token=%s";

    // 根据media_id来获取永久素材
    public static final String GET_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/get_material?access_token=%s";
    //获取临时素材  GET
    public static final String GET_MEDIA="https://api.weixin.qq.com/cgi-bin/media/get?access_token=%s&media_id=%s";

    // 根据media_id来删除永久图文素材
    public static final String DELETE_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/del_material?access_token=%s";

    // 修改永久图文url
    public static final String UPDATE_NEWS_MATERIAL = "https://api.weixin.qq.com/cgi-bin/material/update_news?access_token=%s";

    //获取用户标签列表
    private static final String GET_USER_TAG = "https://api.weixin.qq.com/cgi-bin/tags/get?access_token=%s";

    //创建用户标签
    private static final String CREATE_USER_TAG = "https://api.weixin.qq.com/cgi-bin/tags/create?access_token=%s";

    //获取标签下粉丝列表
    private static final String GET_USER_LIST_BY_TAG="https://api.weixin.qq.com/cgi-bin/user/tag/get?access_token=%s";

    //删除用户标签
    private static final String DELETE_USER_TAG = "https://api.weixin.qq.com/cgi-bin/tags/delete?access_token=%s";

    /**
     * ###群发相关接口
     */
    /**
     * 临时素材接口-上传视频-POST
     */
    public static final String UPLOAD_VIDEO="https://api.weixin.qq.com/cgi-bin/media/uploadvideo?access_token=%s";

    /**
     * 根据标签进行群发-POST
     */
    public static final String MASS_TAG="https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=%s";

    /**
     * 根据openid列表进行群发-POST
     */
    public static final String MASS_OPENID="https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=%s";

    /**
     * 删除群发（图文和视频）-POST
     */
    public static final String MASS_DELETE="https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=%s";

    /**
     * 群发预览-POST
     */
    public static final String MASS_PREVIEW="https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=%s";

    /**
     * 查询群发消息发送状态-POST
     */
    public static final String MASS_STATUS="https://api.weixin.qq.com/cgi-bin/message/mass/get?access_token=%s";

    /**
     * 获取群发速度-POST
     */
    public static final String MASS_SPEED_GET="https://api.weixin.qq.com/cgi-bin/message/mass/speed/get?access_token=%s";

    /**
     * 设置群发速度-POST
     */
    public static final String MASS_SPEED_SET="https://api.weixin.qq.com/cgi-bin/message/mass/speed/set?access_token=%s";

    /**
     * ###统计分析相关接口
     */
    /**
     * 获取用户增减数据-7d-POST
     */
    public static final String GET_USER_SUMMARY="https://api.weixin.qq.com/datacube/getusersummary?access_token=%s";
    /**
     * 获取累计用户数据-7d-POST
     */
    public static final String GET_USER_CUMULATE="https://api.weixin.qq.com/datacube/getusercumulate?access_token=%s";

    /**
     * 获取图文群发每日数据-1d-POST
     */
    public static final String GET_ARTICLE_SUMMARY="https://api.weixin.qq.com/datacube/getarticlesummary?access_token=%s";
    /**
     * 获取图文群发总数据-1d-POST
     */
    public static final String GET_ARTICLE_TOTAL="https://api.weixin.qq.com/datacube/getarticletotal?access_token=%s";
    /**
     * 获取图文统计数据-3d-POST
     */
    public static final String GET_ARTICLE_READ="https://api.weixin.qq.com/datacube/getuserread?access_token=%s";
    /**
     * 获取图文统计分时数据-1d-POST
     */
    public static final String GET_ARTICLE_READHOUR="https://api.weixin.qq.com/datacube/getuserreadhour?access_token=%s";
    /**
     * 获取图文分享转发数据-7d-POST
     */
    public static final String GET_ARTICLE_SHARE="https://api.weixin.qq.com/datacube/getusershare?access_token=%s";
    /**
     * 获取图文分享转发分时数据-1d-POST
     */
    public static final String GET_ARTICLE_SHAREHOUR="https://api.weixin.qq.com/datacube/getusersharehour?access_token=%s";

    /**
     * 获取消息发送概况数据-7d-POST
     */
    public static final String GET_UPSTREAM_MSG="https://api.weixin.qq.com/datacube/getupstreammsg?access_token=%s";
    /**
     * 获取消息分送分时数据-1d-POST
     */
    public static final String GET_UPSTREAM_MSGHOUR="https://api.weixin.qq.com/datacube/getupstreammsghour?access_token=%s";
    /**
     * 获取消息发送周数据-30d-POST
     */
    public static final String GET_UPSTREAM_MSGWEEK="https://api.weixin.qq.com/datacube/getupstreammsgweek?access_token=%s";
    /**
     * 获取消息发送月数据-30d-POST
     */
    public static final String GET_UPSTREAM_MSGMONTH="https://api.weixin.qq.com/datacube/getupstreammsgmonth?access_token=%s";
    /**
     * 获取消息发送分布数据-15d-POST
     */
    public static final String GET_UPSTREAM_MSGDIST="https://api.weixin.qq.com/datacube/getupstreammsgdist?access_token=%s";
    /**
     * 获取消息发送分布周数据-30d-POST
     */
    public static final String GET_UPSTREAM_MSGDISTWEEK="https://api.weixin.qq.com/datacube/getupstreammsgdistweek?access_token=%s";
    /**
     * 获取消息发送分布月数据-30d-POST
     */
    public static final String GET_UPSTREAM_MSGDISTMONTH="https://api.weixin.qq.com/datacube/getupstreammsgdistmonth?access_token=%s";

    /**
     * 获取接口分析数据-30d-POST
     */
    public static final String GET_INTERFACE_SUMMARY="https://api.weixin.qq.com/datacube/getinterfacesummary?access_token=%s";
    /**
     * 获取接口分析分时数据-1d-POST
     */
    public static final String GET_INTERFACE_SUMMARYHOUR="https://api.weixin.qq.com/datacube/getinterfacesummaryhour?access_token=%s";


    //素材文件后缀
    public static Map<String,String> type_fix= new HashMap<>();
    public static Map<String,String> media_fix= new HashMap<>();
    //素材文件大小
    public static Map<String,Long> type_length= new HashMap<>();
    //统计图表数据
    public static Map<String,String[]> data_cube= new HashMap<>();
    static{
        type_fix.put("image","bmp|png|jpeg|jpg|gif");
        type_fix.put("voice","mp3|wma|wav|amr");
        type_fix.put("video","mp4");
        type_fix.put("thumb","jpg");

        media_fix.put("image","png|jpeg|jpg|gif");
        media_fix.put("voice","mp3|amr");
        media_fix.put("video","mp4");
        media_fix.put("thumb","jpg");

        type_length.put("image",new Long(2*1024*1024));
        type_length.put("voice",new Long(2*1024*1024));
        type_length.put("video",new Long(10*1024*1024));
        type_length.put("thumb",new Long(64*1024));

        data_cube.put("getusersummary", new String[]{"7",GET_USER_SUMMARY});
        data_cube.put("getusercumulate", new String[]{"7",GET_USER_CUMULATE});
        data_cube.put("getarticlesummary", new String[]{"1",GET_ARTICLE_SUMMARY});
        data_cube.put("getarticletotal", new String[]{"1",GET_ARTICLE_TOTAL});
        data_cube.put("getuserread", new String[]{"3",GET_ARTICLE_READ});
        data_cube.put("getuserreadhour", new String[]{"1",GET_ARTICLE_READHOUR});
        data_cube.put("getusershare", new String[]{"7",GET_ARTICLE_SHARE});
        data_cube.put("getusersharehour", new String[]{"1",GET_ARTICLE_SHAREHOUR});
    }

    // 获取token接口
    public static String getTokenUrl(String appId, String appSecret) {
        return String.format(TOKEN, appId, appSecret);
    }

    // 获取上传Media接口
    public static String getUploadMediaUrl(String token, String type) {
        return String.format(UPLOAD_MEDIA, token, type);
    }

    // 获取菜单创建接口
    public static String getMenuCreateUrl(String token) {
        return String.format(MENU_CREATE, token);
    }

    // 获取个性化菜单创建接口
    public static String getMenuAddconditionalUrl(String token) {
        return String.format(MENU_ADDCONDITIONAL, token);
    }

    // 获取菜单删除接口
    public static String getMenuDeleteUrl(String token) {
        return String.format(MENU_DELETE, token);
    }

    // 获取粉丝信息接口
    public static String getFansInfoUrl(String token, String openid) {
        return String.format(GET_FANS_INFO, token, openid);
    }

    // 获取粉丝列表接口
    public static String getFansListUrl(String token, String nextOpenId) {
        if (nextOpenId == null) {
            return String.format(GET_FANS_LIST, token);
        } else {
            return String.format(GET_FANS_LIST + "&next_openid=%s", token, nextOpenId);
        }
    }

    // 获取素材列表接口
    public static String getBatchMaterialUrl(String token) {
        return String.format(GET_BATCH_MATERIAL, token);
    }

    // 获取上传图文消息接口
    public static String getUploadNewsUrl(String token) {
        return String.format(UPLOAD_NEWS, token);
    }
    //获取用户标签列表接口
    public static String getUserTagList(String token) {
        return String.format(GET_USER_TAG, token);
    }

    //获取创建用户标签接口
    public static String getCreateUserTag(String token) {
        return String.format(CREATE_USER_TAG, token);
    }

    //获取标签下粉丝列表
    public static String getUserListByTag(String token) {
        return String.format(GET_USER_LIST_BY_TAG, token);
    }

    //获取删除用户标签接口
    public static String getDeleteUserTag(String token) {
        return String.format(DELETE_USER_TAG, token);
    }
    // 群发接口
    public static String getMassSendUrl(String token) {
        return String.format(MASS_SEND, token);
    }

    // 获取永久素材
    public static String getMaterial(String token) {
        return String.format(GET_MATERIAL, token);
    }

    // 删除永久图文素材
    public static String getDelMaterialURL(String token) {
        return String.format(DELETE_MATERIAL, token);
    }

    // 获取新增图文素材url
    public static String getNewsMaterialUrl(String token) {
        return String.format(ADD_NEWS_MATERIAL, token);
    }

    // 获取修改图文素材url
    public static String getUpdateNewsMaterialUrl(String token) {
        return String.format(UPDATE_NEWS_MATERIAL, token);
    }

    // 上传永久图片素材
    public static String getMaterialImgUrl(String token) {
        return String.format(UPLOAD_MATERIAL_IMG, token);
    }

    // 获取新增素材url
    public static String getMaterialUrl(String token, String type) {
        return String.format(ADD_MATERIAL, token, type);
    }
}
