package com.casi.project.system.domain;

import lombok.Data;

import java.util.List;

@Data
public class StringAndStringArray {

    private String roleId;

    private String[] userIds;

}
