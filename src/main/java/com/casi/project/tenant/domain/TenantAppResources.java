package com.casi.project.tenant.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 租户应用资源对象 tenant_app_resources
 * 
 * @author casi
 * @date 2020-06-02
 */
public class TenantAppResources extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 租户编号 */
    @Excel(name = "租户编号")
    private String tenantId;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private String appId;

    /** 资源编号 */
    @Excel(name = "资源编号")
    private String resourcesId;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setTenantId(String tenantId) 
    {
        this.tenantId = tenantId;
    }

    public String getTenantId() 
    {
        return tenantId;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setResourcesId(String resourcesId) 
    {
        this.resourcesId = resourcesId;
    }

    public String getResourcesId() 
    {
        return resourcesId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("tenantId", getTenantId())
            .append("appId", getAppId())
            .append("resourcesId", getResourcesId())
            .toString();
    }
}
