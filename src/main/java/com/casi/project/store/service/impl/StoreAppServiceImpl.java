package com.casi.project.store.service.impl;

import java.util.List;
import com.casi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.casi.project.store.mapper.StoreAppMapper;
import com.casi.project.store.domain.StoreApp;
import com.casi.project.store.service.IStoreAppService;

/**
 * 应用详情Service业务层处理
 * 
 * @author casi
 * @date 2020-06-02
 */
@Service
public class StoreAppServiceImpl implements IStoreAppService 
{
    @Autowired
    private StoreAppMapper storeAppMapper;

    /**
     * 查询应用详情
     * 
     * @param uuid 应用详情ID
     * @return 应用详情
     */
    @Override
    public StoreApp selectStoreAppById(String uuid)
    {
        return storeAppMapper.selectStoreAppById(uuid);
    }

    /**
     * 查询应用详情列表
     * 
     * @param storeApp 应用详情
     * @return 应用详情
     */
    @Override
    public List<StoreApp> selectStoreAppList(StoreApp storeApp)
    {
        return storeAppMapper.selectStoreAppList(storeApp);
    }

    /**
     * 新增应用详情
     * 
     * @param storeApp 应用详情
     * @return 结果
     */
    @Override
    public int insertStoreApp(StoreApp storeApp)
    {
        storeApp.setCreateTime(DateUtils.getNowDate());
        storeApp.setCreateTime(DateUtils.getNowDate());
        return storeAppMapper.insertStoreApp(storeApp);
    }

    /**
     * 修改应用详情
     * 
     * @param storeApp 应用详情
     * @return 结果
     */
    @Override
    public int updateStoreApp(StoreApp storeApp)
    {
        return storeAppMapper.updateStoreApp(storeApp);
    }

    /**
     * 批量删除应用详情
     * 
     * @param uuids 需要删除的应用详情ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppByIds(String[] uuids)
    {
        return storeAppMapper.deleteStoreAppByIds(uuids);
    }

    /**
     * 删除应用详情信息
     * 
     * @param uuid 应用详情ID
     * @return 结果
     */
    @Override
    public int deleteStoreAppById(String uuid)
    {
        return storeAppMapper.deleteStoreAppById(uuid);
    }
}
