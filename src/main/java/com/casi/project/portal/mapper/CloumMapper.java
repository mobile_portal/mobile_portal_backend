package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.Cloum;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @Auther shp
 * @data2020/4/2416:21
 * @description
 */

@Mapper
public interface CloumMapper {


    @Select("SELECT c.uuid,c.column_id as columnId ,c.column_name columnName,c.is_del isDel,c.create_time createTime,c.update_time updateTime FROM mobile_news_column c where c.is_del=#{isDel}  order by createTime desc")
    List<Cloum> findCloum(Cloum cloum);

    @Insert("INSERT INTO mobile_news_column(uuid,column_name) VALUES(#{uuid},#{columnName});")
    Integer addCloum(Cloum cloum);

    @Update("UPDATE mobile_news_column set column_id=#{columnId},column_name=#{columnName},update_time=#{updateTime} where uuid=#{uuid}")
    Integer editCloum(Cloum cloum);

    @Update("UPDATE mobile_news_column set is_del=#{isDel} where uuid=#{uuid}")
    Integer delCloum(Cloum cloum);


    @Select("SELECT  uuid,column_id columnId,column_name columnName,create_time createTime,update_time updateTime from mobile_news_column WHERE uuid=#{uuid}")
    Cloum findById(Cloum cloum);

}
