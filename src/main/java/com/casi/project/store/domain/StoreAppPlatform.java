package com.casi.project.store.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 应用平台表对象 store_app_platform
 * 
 * @author casi
 * @date 2020-06-02
 */
public class StoreAppPlatform extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 应用地址 */
    private String app_url;

    /** 应用大小 */
    private Integer app_size;

    /** 编码 */
    private String uuid;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private String appId;

    /** 平台类型 */
    @Excel(name = "平台类型")
    private String type;

    /** 唤醒地址 */
    @Excel(name = "唤醒地址")
    private String wakeUrl;

    public void setApp_url(String app_url) 
    {
        this.app_url = app_url;
    }

    public String getApp_url() 
    {
        return app_url;
    }
    public void setApp_size(Integer app_size) 
    {
        this.app_size = app_size;
    }

    public Integer getApp_size() 
    {
        return app_size;
    }
    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setWakeUrl(String wakeUrl) 
    {
        this.wakeUrl = wakeUrl;
    }

    public String getWakeUrl() 
    {
        return wakeUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("app_url", getApp_url())
            .append("app_size", getApp_size())
            .append("uuid", getUuid())
            .append("appId", getAppId())
            .append("type", getType())
            .append("wakeUrl", getWakeUrl())
            .toString();
    }
}
