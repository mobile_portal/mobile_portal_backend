package com.casi.project.interfaces.mapper;

import com.casi.project.interfaces.domain.ApiInvok;

public interface ApiInvokMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ApiInvok record);

    int insertSelective(ApiInvok record);

    ApiInvok selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ApiInvok record);

    int updateByPrimaryKey(ApiInvok record);

    ApiInvok selectByKey(String appKey,String appSecret);
}