package com.casi.project.store.service;

import java.util.List;
import com.casi.project.store.domain.StoreAppComment;

/**
 * 应用评论Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface IStoreAppCommentService 
{
    /**
     * 查询应用评论
     * 
     * @param uuid 应用评论ID
     * @return 应用评论
     */
    public StoreAppComment selectStoreAppCommentById(String uuid);

    /**
     * 查询应用评论列表
     * 
     * @param storeAppComment 应用评论
     * @return 应用评论集合
     */
    public List<StoreAppComment> selectStoreAppCommentList(StoreAppComment storeAppComment);

    /**
     * 新增应用评论
     * 
     * @param storeAppComment 应用评论
     * @return 结果
     */
    public int insertStoreAppComment(StoreAppComment storeAppComment);

    /**
     * 修改应用评论
     * 
     * @param storeAppComment 应用评论
     * @return 结果
     */
    public int updateStoreAppComment(StoreAppComment storeAppComment);

    /**
     * 批量删除应用评论
     * 
     * @param uuids 需要删除的应用评论ID
     * @return 结果
     */
    public int deleteStoreAppCommentByIds(String[] uuids);

    /**
     * 删除应用评论信息
     * 
     * @param uuid 应用评论ID
     * @return 结果
     */
    public int deleteStoreAppCommentById(String uuid);
}
