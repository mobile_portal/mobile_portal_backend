package com.casi.project.tenant.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.tenant.domain.TenantRole;
import com.casi.project.tenant.service.ITenantRoleService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 租户角色Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"租户角色"})
@RestController
@RequestMapping("/tenant/role")
public class TenantRoleController extends BaseController
{
    @Autowired
    private ITenantRoleService tenantRoleService;

    /**
     * 查询租户角色列表
     */
    @ApiOperation(value = "查询租户角色列表")
    @PreAuthorize("@ss.hasPermi('tenant:role:list')")
    @GetMapping("/list")
    public TableDataInfo list(TenantRole tenantRole)
    {
        startPage();
        List<TenantRole> list = tenantRoleService.selectTenantRoleList(tenantRole);
        return getDataTable(list);
    }

    /**
     * 导出租户角色列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:role:export')")
    @Log(title = "租户角色", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TenantRole tenantRole)
    {
        List<TenantRole> list = tenantRoleService.selectTenantRoleList(tenantRole);
        ExcelUtil<TenantRole> util = new ExcelUtil<TenantRole>(TenantRole.class);
        return util.exportExcel(list, "role");
    }

    /**
     * 获取租户角色详细信息
     */
    @ApiOperation(value = "获取租户角色详细信息")
    @PreAuthorize("@ss.hasPermi('tenant:role:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(tenantRoleService.selectTenantRoleById(uuid));
    }

    /**
     * 新增租户角色
     */
    @ApiOperation(value = "新增租户角色")
    @PreAuthorize("@ss.hasPermi('tenant:role:add')")
    @Log(title = "租户角色", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TenantRole tenantRole)
    {
        return toAjax(tenantRoleService.insertTenantRole(tenantRole));
    }

    /**
     * 修改租户角色
     */
    @ApiOperation(value = "修改租户角色")
    @PreAuthorize("@ss.hasPermi('tenant:role:edit')")
    @Log(title = "租户角色", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TenantRole tenantRole)
    {
        return toAjax(tenantRoleService.updateTenantRole(tenantRole));
    }

    /**
     * 删除租户角色
     */
    @ApiOperation(value = "删除租户角色")
    @PreAuthorize("@ss.hasPermi('tenant:role:remove')")
    @Log(title = "租户角色", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(tenantRoleService.deleteTenantRoleByIds(uuids));
    }
}
