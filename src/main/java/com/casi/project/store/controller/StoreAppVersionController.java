package com.casi.project.store.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.store.domain.StoreAppVersion;
import com.casi.project.store.service.IStoreAppVersionService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 应用版本Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"应用版本"})
@RestController
@RequestMapping("/store/version")
public class StoreAppVersionController extends BaseController
{
    @Autowired
    private IStoreAppVersionService storeAppVersionService;

    /**
     * 查询应用版本列表
     */
    @ApiOperation(value = "查询应用版本列表")
    @PreAuthorize("@ss.hasPermi('store:version:list')")
    @GetMapping("/list")
    public TableDataInfo list(StoreAppVersion storeAppVersion)
    {
        startPage();
        List<StoreAppVersion> list = storeAppVersionService.selectStoreAppVersionList(storeAppVersion);
        return getDataTable(list);
    }

    /**
     * 导出应用版本列表
     */
    @PreAuthorize("@ss.hasPermi('store:version:export')")
    @Log(title = "应用版本", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(StoreAppVersion storeAppVersion)
    {
        List<StoreAppVersion> list = storeAppVersionService.selectStoreAppVersionList(storeAppVersion);
        ExcelUtil<StoreAppVersion> util = new ExcelUtil<StoreAppVersion>(StoreAppVersion.class);
        return util.exportExcel(list, "version");
    }

    /**
     * 获取应用版本详细信息
     */
    @ApiOperation(value = "获取应用版本详细信息")
    @PreAuthorize("@ss.hasPermi('store:version:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(storeAppVersionService.selectStoreAppVersionById(uuid));
    }

    /**
     * 新增应用版本
     */
    @ApiOperation(value = "新增应用版本")
    @PreAuthorize("@ss.hasPermi('store:version:add')")
    @Log(title = "应用版本", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody StoreAppVersion storeAppVersion)
    {
        return toAjax(storeAppVersionService.insertStoreAppVersion(storeAppVersion));
    }

    /**
     * 修改应用版本
     */
    @ApiOperation(value = "修改应用版本")
    @PreAuthorize("@ss.hasPermi('store:version:edit')")
    @Log(title = "应用版本", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody StoreAppVersion storeAppVersion)
    {
        return toAjax(storeAppVersionService.updateStoreAppVersion(storeAppVersion));
    }

    /**
     * 删除应用版本
     */
    @ApiOperation(value = "删除应用版本")
    @PreAuthorize("@ss.hasPermi('store:version:remove')")
    @Log(title = "应用版本", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(storeAppVersionService.deleteStoreAppVersionByIds(uuids));
    }
}
