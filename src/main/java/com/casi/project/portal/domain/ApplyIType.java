package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 应用类型
 * @author 18732
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApplyIType implements Serializable {
    private String id;
    private String typeName;
    private Long homePageId;


}
