package com.casi.project.portal.service.impl;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.domain.param.MobileApplyVersionNativeParam;
import com.casi.project.portal.mapper.MobileApplyVersionNativeMapper;
import com.casi.project.portal.service.MobileApplyVersionNativeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Auther shp
 * @data2020/4/219:58
 * @description
 */


@Service
@Transactional(rollbackFor = Exception.class)
public class MobileApplyVersionNativeServiceImpl implements MobileApplyVersionNativeService {

    @Autowired
    private MobileApplyVersionNativeMapper mapper;

    @Override
    public AjaxResult addApp(MobileApplyVersionNativeParam m) {
        return AjaxResult.success(mapper.addApp(m)>0?"添加成功":"添加失败");
     }

}
