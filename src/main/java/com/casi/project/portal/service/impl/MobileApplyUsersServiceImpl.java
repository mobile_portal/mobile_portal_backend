package com.casi.project.portal.service.impl;

import com.alibaba.fastjson.JSON;
import com.casi.common.utils.StringUtils;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.interfaces.utils.HttpRequest;
import com.casi.project.portal.domain.DoubleArray;
import com.casi.project.portal.domain.MobileApplyDepts;
import com.casi.project.portal.domain.MobileApplyUser;
import com.casi.project.portal.domain.MobileApplyUsers;
import com.casi.project.portal.mapper.MobileApplyDeptsMapper;
import com.casi.project.portal.mapper.MobileApplyUsersMapper;
import com.casi.project.portal.service.MobileApplyUsersService;
import com.casi.project.portal.util.CollectionUtil;
import com.casi.project.system.domain.SysDept;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.mapper.SysDeptMapper;
import com.casi.project.system.mapper.SysUserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.RequestBody;

import javax.annotation.Resource;
import java.util.*;

@Service
public class MobileApplyUsersServiceImpl implements MobileApplyUsersService {
    @Resource
    private MobileApplyUsersMapper mobileApplyUsersMapper;

    @Autowired
    private SysUserMapper userMapper;

    @Autowired
    private SysDeptMapper deptMapper;
    @Autowired
    private MobileApplyDeptsMapper mobileApplyDeptsMapper;

    /**
     * 应用授权给用户
     */
    @Override
    @Transactional
    public AjaxResult addApplyIdsAndUserIds(String[] applyIds, String[] userIds) {
        if (null == applyIds || applyIds.length < 0) {
            return AjaxResult.error("缺失参数");
        }
        if (null == userIds || userIds.length < 0) {
            return AjaxResult.error("缺失参数");
        }
        String applyId = applyIds[0];
        //用于存储该应用没有授权过的用户
        HashSet<MobileApplyUsers> tags = new HashSet<>();
        //用于存储该应用授权过的用户id
        String s = "";
        //先根据应用的id和用户的id查询
        for(String userId : userIds){
            MobileApplyUsers mobileApplyUsers = mobileApplyUsersMapper.selectOneByApplyIdAndUserId(applyId, userId);
            if(StringUtils.isNull(mobileApplyUsers)){
                //如果为空则说明该应用没有授权过这个用户   则向应用与用户关联表中添加一条数据
                MobileApplyUsers mobileApplyUsers1 = new MobileApplyUsers();
                mobileApplyUsers1.setApplyId(applyId);
                mobileApplyUsers1.setUserId(userId);
                //根据用户id查询出该用户的部门id
                SysUser sysUser = userMapper.selectUserById(userId);
                mobileApplyUsers1.setDeptId(sysUser.getDeptId());
                tags.add(mobileApplyUsers1);
            }else{
                //说明该应用给这个用户授权过 只是后来又给取消掉了授权 那么重新让其授权只需将status由1变为0即可
                s +=userId+",";
            }
        }
        if(tags.size()!=0){
            //则说明tags里面有值则进行添加
            int i = mobileApplyUsersMapper.insertApplyIdsAndUserIds(tags);
            if(i<1){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
                return AjaxResult.error("应用授权用户失败");
            }
        }
        if (s.length() > 0) {
            //说明该应用授权过此用户   所以直接进行修改就可以  将status1变为0即可
            String substring = s.substring(0, s.length() - 1);
            String[] userIdss = substring.split(",");
            int i = mobileApplyUsersMapper.updateApplyIdsAndUserIds(applyId, userIdss);
            if(i<1){
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
                return AjaxResult.error("应用授权用户失败");
            }
        }
        return AjaxResult.success("应用授权用户成功");
        /*//根据应用的id删除该应用与用户的关联关系
        for (String applyId : applyIds) {
            try {
                mobileApplyUsersMapper.deleteMobileApplyUsersByApplyId(applyId);
            }catch (Exception e){
                return AjaxResult.error("应用授权失败");
            }
        }
        HashSet<MobileApplyUsers> tags = new HashSet<>();
        //遍历应用的id数组
        for (String applyId : applyIds) {
            //遍历用户的id数组
            for (String userId : userIds) {
                //通过用户的id查询出部门的id   在根据部门的id和应用的id查询此应用是否授权给这个部门
                MobileApplyDepts mobileApplyDepts = mobileApplyDeptsMapper.selectByApplyIdsAndDeptIdAndUserId(applyId, userId);
                //如果mobileApplyDepts不为空，则说名该用户所在的部门已经授权过此应用，因此在这个部门下面的人被授权过此应用
                if (null != mobileApplyDepts) {
                    continue;
                }
                MobileApplyUsers mobileApplyUsers = new MobileApplyUsers();
                mobileApplyUsers.setApplyId(applyId);
                mobileApplyUsers.setUserId(userId);
                tags.add(mobileApplyUsers);
            }
        }
        if (tags.size() != 0) {
            int i = mobileApplyUsersMapper.insertApplyIdsAndUserIds(tags);
            if (i < 0) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();//手动回滚
                return AjaxResult.error("应用授权用户失败");
            }
        }
        return AjaxResult.success("应用授权用户成功");*/
    }
    /*public AjaxResult addApplyIdsAndUserIds(String[] applyIds, String[] userIds) {
        if (null == applyIds || applyIds.length < 0) {
            return AjaxResult.error("缺失参数");
        }
        if (null == userIds || userIds.length < 0) {
            return AjaxResult.error("缺失参数");
        }
        HashSet<MobileApplyUsers> tags = new HashSet<>();
        //遍历应用的id数组
        for (String applyId : applyIds) {
            //遍历用户的id数组
            for (String userId : userIds) {
                //根据用户的id和应用的id查询
                MobileApplyUsers mobileApplyUsers1 = mobileApplyUsersMapper.selectOneByApplyIdAndUserId(applyId, userId);
                if (null != mobileApplyUsers1) {
                    continue;
                }
                MobileApplyUsers mobileApplyUsers = new MobileApplyUsers();
                mobileApplyUsers.setApplyId(applyId);
                mobileApplyUsers.setUserId(userId);
                tags.add(mobileApplyUsers);
            }
        }
        if (tags.size() != 0) {
            int i = mobileApplyUsersMapper.insertApplyIdsAndUserIds(tags);
            if (i < 0) {
                return AjaxResult.error("应用授权用户失败");
            }
            return AjaxResult.success("应用授权用户成功");
        } else {
            return AjaxResult.error("该用户们已添加过此应用不允许重复添加");
        }
    }*/

    /**
     * 回显已经授权的用户
     *
     * @param doubleArray
     * @return
     */
    @Override
    public AjaxResult findApplyIdsAndUserIds(DoubleArray doubleArray) {
        if (StringUtils.isNull(doubleArray.getApplyIds()) || doubleArray.getApplyIds().length == 0) {
            return AjaxResult.error("请传应用ID");
        }
        List<HashMap<String, Object>> userIds = mobileApplyUsersMapper.findApplyIdsAndUserIds(doubleArray.getApplyIds());
        if (StringUtils.isNull(userIds) || userIds.size() == 0) {
            return AjaxResult.error("应用没有授权用户");
        }
        return AjaxResult.success("查询成功", userIds);
    }

    @Override
    public List<SysUser> applyDisabledUserList(String applyId) {

        List<SysUser> sysUsers = new ArrayList<>();

        String[] userIds = mobileApplyUsersMapper.applyDisabledUserList(applyId);

        for (String userId : userIds) {
            SysUser sysUser = userMapper.selectUserById(userId);
            if (sysUser != null) {
                SysDept sysDept = deptMapper.selectDeptById(sysUser.getDeptId());
                if (sysDept != null) {
                    sysUser.setDeptName(sysDept.getDeptName());
                } else {
                    sysUser.setDeptName("");
                }
            }
            sysUsers.add(sysUser);
        }
        return sysUsers;
    }


    /**
     * 应用禁用用户
     */

    @Transactional
    @Override
    public AjaxResult addApplyToDisabledUserIds(String mobileApplyId, String userId) {
/*        if (StringUtils.isEmpty(mobileApplyId)) {
            return AjaxResult.error("请选择应用");
        }*/
        if (mobileApplyId == null) {
            return AjaxResult.error("请选择应用");
        }
        if (StringUtils.isEmpty(userId)) {
            return AjaxResult.error("请选择禁用用户");
        }
        int count = 0;

        int row = mobileApplyUsersMapper.selectDisabledByApplyIdAndUserId(mobileApplyId, userId);
        if (row > 0) {
            return AjaxResult.error("该用户已禁用");
        }
        count = mobileApplyUsersMapper.addApplyToDisabledUserIds(mobileApplyId, userId);


        if (count <= 0) {
            return AjaxResult.error("禁用用户失败,请重新选择");
        }
        return AjaxResult.success("禁用用户成功");
    }


    /**
     * 应用解除禁用用户
     */
    @Transactional
    @Override
    public AjaxResult romoveApplyToDisabledUserIds(String mobileApplyId, String userId) {
/*        if (StringUtils.isEmpty(mobileApplyId)) {
            return AjaxResult.error("请选择应用");
        }*/
        if (mobileApplyId == null) {
            return AjaxResult.error("请选择应用");
        }
        if (StringUtils.isEmpty(userId)) {
            return AjaxResult.error("请选择解除禁用的用户");
        }
        int count = mobileApplyUsersMapper.romoveApplyToDisabledUserIds(mobileApplyId, userId);
        if (count <= 0) {
            return AjaxResult.error("该用户未禁用应用");
        }

        return AjaxResult.success("解除禁用用户成功");
    }






    public static void main(String[] args) {
        String str1 = "1,2,3,4,5,6";
        String str2 = "5";
        String[] split = str1.split(",");
        String[] split1 = str2.split(",");
        System.out.println(split);
        System.out.println(split1);
        List<String> a = Arrays.asList(split);
        List<String> b = Arrays.asList(split1);
        //去重
        Collection<String> diffentNoDuplicate = CollectionUtil.getDiffentNoDuplicate(a, b);
        String[] strings = diffentNoDuplicate.toArray(new String[diffentNoDuplicate.size()]);
        System.out.println("去重"+Arrays.toString(strings));
        /*Collection<String> same = CollectionUtil.getSame(a, b);
        String[] string1 = same.toArray(new String[same.size()]);
        System.out.println("相同的"+Arrays.toString(string1));*/
    }
    /**
     *根据应用的id查询出该应用没有授权的用户信息
     */
    @Override
    public AjaxResult queryNotImpowerUsers(Map<String,Object> map) {
        if(StringUtils.isEmpty(map)){
            return AjaxResult.error("缺失参数");
        }
        if(StringUtils.isNull(map.get("pageNum")) || StringUtils.isNull(map.get("pageSize"))){
            return AjaxResult.error("请传入页数或每页多少条数");
        }
        if(StringUtils.isNull(map.get("applyId"))){
            return AjaxResult.error("请传应用的id");
        }
        Integer pageNum = Integer.parseInt(map.get("pageNum").toString());
        Integer pageSize = Integer.parseInt(map.get("pageSize").toString());
        String applyId = String.valueOf(map.get("applyId"));
        PageHelper.startPage(pageNum, pageSize);
        //查询应用与组织机构的关联表，看该应用都赋值了哪些部门
        String[] deptIds = mobileApplyDeptsMapper.selectMobileApplyDeptsGetDeptIdsByApplyId(applyId);
        //根据应用的id查询应用与用户的关联表   看该应用是否授权给过用户
        String[] strings = mobileApplyUsersMapper.selectUserIdsByApplyId(applyId);
        if(null !=deptIds && deptIds.length>0){
            //则说明这个应用给部门授权了
            if(strings != null && strings.length>0){
                //说明该应用给用户授权过
                //根据数组部门的id查询出所在这些部门的用户id
                String[] s1 = userMapper.selectUserGetUserIdsByDeptIds(deptIds);
                //根据应用的id查询出该应用被解除人的id
                String[] s2 = userMapper.selectMobileApplyUsersGetUserIdsbyApplyId(applyId);
                if(null != s1 && s1.length > 0){
                    if(null != s2 && s2.length > 0){
                        //说明该应用授权过部门 且s1为应用授权部门下面的所有人  也包括授权部门以后新加入的人的id   且该应用授权的人中有人被解除了授权 即为s2的值
                        List<String> a = Arrays.asList(s1);
                        List<String> b = Arrays.asList(s2);
                        //去重
                        Collection<String> diffentNoDuplicate = CollectionUtil.getDiffentNoDuplicate(a, b);
                        String[] userIds = diffentNoDuplicate.toArray(new String[diffentNoDuplicate.size()]);
                        //查询未授权的人员信息列表
                        List<Map<String, Object>> maps = userMapper.selectNotImpowerUsers(applyId,userIds);
                        PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(maps);
                        return AjaxResult.success("查询成功", mapPageInfo);
                    }else {
                        //说明该应用授权过部门 且s1为应用授权部门下面的所有人  也包括授权部门以后新加入的人的id 且没有人被取消授权
                        //查询未授权的人员信息列表
                        List<Map<String, Object>> maps = userMapper.selectNotImpowerUsers(applyId,s1);
                        PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(maps);
                        return AjaxResult.success("查询成功", mapPageInfo);
                    }
                }else{
                    //说明该应用授权过部门  但是部门下面没有人
                    if(null != s2 && s2.length > 0){
                        //该应用授权过用户  且该应用授权的人中有人被解除了授权
                    }else{

                    }
                    List<Map<String, Object>> maps = userMapper.selectUnauthorizedUsers();
                    PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(maps);
                    return AjaxResult.success("查询成功", mapPageInfo);
                }
            }else{
                //说明该应用没有给用户授权过
                return AjaxResult.success();
            }
        }else{
            //则说明应用没有给部门授权
            //所以只需要查询应用与用户关联表就可以了  应为可能是应用直接去给人授权的
            List<Map<String, Object>> maps = userMapper.selectNotImpowerUsers1(applyId);
            PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(maps);
            return AjaxResult.success("查询成功", mapPageInfo);
        }
    }
    /**
     * 根据应用的id查询已经授权的用户的信息
     */
    @Override
    public AjaxResult queryImpowerUsers(Map<String, Object> map) {
        if(StringUtils.isEmpty(map)){
            return AjaxResult.error("缺失参数");
        }
        if(StringUtils.isNull(map.get("pageNum")) || StringUtils.isNull(map.get("pageSize"))){
            return AjaxResult.error("请传入页数或每页多少条数");
        }
        if(StringUtils.isNull(map.get("applyIds"))){
            return AjaxResult.error("请传应用的id");
        }
        Integer pageNum = Integer.parseInt(map.get("pageNum").toString());
        Integer pageSize = Integer.parseInt(map.get("pageSize").toString());
        String applyId = String.valueOf(map.get("applyIds"));
        PageHelper.startPage(pageNum, pageSize);
        //查询应用与组织机构的关联表，看该应用都赋值了哪些部门
        String[] deptIds = mobileApplyDeptsMapper.selectMobileApplyDeptsGetDeptIdsByApplyId(applyId);
        if(null !=deptIds && deptIds.length>0){
            //则说明这个应用给部门授权了
            //根据数组部门的id查询出所在这些部门的用户id
            String[] s1 = userMapper.selectUserGetUserIdsByDeptIds(deptIds);
            //根据应用的id查询出该应用被解除人的id
            String[] s2 = userMapper.selectMobileApplyUsersGetUserIdsbyApplyId(applyId);
            if(null !=s1 && s1.length > 0){
                //说明该应用授权的部门下面有人
                if(null !=s2 && s2.length >0){
                    //说明该应用授权的人中有人被解除了授权
                    List<String> a = Arrays.asList(s1);
                    List<String> b = Arrays.asList(s2);
                    //去重
                    Collection<String> diffentNoDuplicate = CollectionUtil.getDiffentNoDuplicate(a, b);
                    String[] userIds = diffentNoDuplicate.toArray(new String[diffentNoDuplicate.size()]);
                    //查询已授权的人员列表  这里面包括那种已经授权了组织之后  又有人加入了这个组织  但是在应用与用户表里面没有新加入这个人的授权信息
                    List<Map<String, Object>> maps = userMapper.selectImpowerUsers(applyId,userIds);
                    PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(maps);
                    return AjaxResult.success("查询成功", mapPageInfo);
                }else{
                    //说明该应用授权给用户之后  没有用户被解除该应用的授权
                    List<Map<String, Object>> maps = userMapper.selectImpowerUsers(applyId,s1);
                    PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(maps);
                    return AjaxResult.success("查询成功", mapPageInfo);
                }
            }else{
                //说明该应用授权了部门  但是部门下面没有人  那么就是说没有授权该应用的人 那么直接给前段返回一个空的集合即可
                List<Map<String, Object>> maps = new ArrayList<>();
                PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(maps);
                return AjaxResult.success("查询成功", mapPageInfo);
            }
        }else{
            //则说明应用没有给部门授权
            //所以只需要查询应用与用户关联表就可以了  应为可能是应用直接去给人授权的
            List<Map<String, Object>> maps = userMapper.selectImpowerUsers1(applyId);
            PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(maps);
            return AjaxResult.success("查询成功", mapPageInfo);
        }
    }
    /**
     * 根据应用的id和用户的id（用户id可为多个）   解除该应用授权的用户
     */
    @Override
    public AjaxResult relieveUsersImpower(Map<String, Object> map) {
        if(StringUtils.isNull(map.get("applyId"))){
            return AjaxResult.error("请传应用的id");
        }
        if(StringUtils.isNull(map.get("userIds"))){
            return AjaxResult.error("用户id不能为空");
        }
        String applyId = String.valueOf(map.get("applyId"));
        String[] userIds = String.valueOf(map.get("userIds")).split(",");
        int i = mobileApplyUsersMapper.delectMobileApplyByApplyIdAndUserId(applyId, userIds);
        if(i<1){
            return AjaxResult.error("取消授权失败");
        }
        return AjaxResult.success("解除授权成功");
    }




    /*public static void main(String[] args) {
        //声明数组1
        String[] str1 = new String[]{"1","2"};

        //声明数组2
        String[] str2 = new String[]{"3","4"};

        //目标数组
        String[] str3 = new String[str1.length+str2.length];

        //将数组1放到目标数组中，参数为：
        // 1.将要复制的数组  2.从将要复制的数组的第几个元素开始  3.目标数组   4.将要放到目标数组的那个位置   5.复制多少个元素
        System.arraycopy(str1,0,str3,0,str1.length);

        //复制数组2到目标数组中
        System.arraycopy(str2,0,str3,str1.length,str2.length);

        //循环输出看
        for (int i = 0;i<str3.length;i++){
            System.out.println(str3[i]);
        }
    }*/
}
