package com.casi.project.portal.mapper;

import com.casi.project.portal.domain.vo.ApplyCategory;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * 应用类别
 *
 * @author 18732
 */
public interface ApplyCategoryMapper {

    /**
     * 新增应用类别
     *
     * @param hashMap
     * @return
     */
    public int addApplyCategory(HashMap<String, Object> hashMap);

    /**
     * 回显应用类别
     *
     * @param id
     * @return
     */
    Map<String, Object> findApplyCategoryId(@Param("id") String id);

    /**
     * 修改应用类别
     *
     * @param map
     * @return
     */
    public int updateApplyCategory(HashMap<String, Object> map);

    /**
     * 根据上级ID查询应用类别下拉列表
     *
     * @param id
     * @return
     */
    public ArrayList<ApplyCategory> getApplyCategoryId();

    /**
     * 禁用启用应用类别
     *
     * @param id
     * @param type
     * @return
     */
    int disableEnabledCategory(String id, Integer type);

    /***
     * 判断应用类别名字是否存在
     * @param menuName
     * @return
     */
    public int checkMenuNameUnique(@Param("menuName") String menuName);

    /**
     * 查询应用类别列表
     *
     * @return
     */
    ArrayList<ApplyCategory> getApplyCategoryListPage();
}
