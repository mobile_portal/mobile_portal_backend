package com.casi.project.portal.domain.vo;

/**
 * @Author zhongzj
 * @Description:
 * @Create: 2019/11/23
 */
public class MediaBaseForWebVo {
    /**
     * 主键id
     */
    private Long id;
    /**
     * 状态（1：待提交，2：审核中，3：已审核，4：已发布，5：已退回）
     */
    private String status;
    /**
     * 审批流程（a:党群信息审批流程，b:精神宣贯审批流程，c:通知公告审批流程）
     */
    private String approveProcess;
    /**
     * 网页名称
     */
    private String name;
    /**
     * 网页地址
     */
    private String url;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApproveProcess() {
        return approveProcess;
    }

    public void setApproveProcess(String approveProcess) {
        this.approveProcess = approveProcess;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
