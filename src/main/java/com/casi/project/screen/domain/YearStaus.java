package com.casi.project.screen.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther shp
 * @data2020/5/911:39
 * @description
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class YearStaus {

    private String name;

    private Long value;

}
