package com.casi.project.system.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.casi.common.utils.DateUtils;
import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.StringUtils;
import com.casi.framework.redis.RedisCache;
import com.casi.project.portal.domain.thirdParty.ThirdPartyLoginBody;
import com.casi.project.portal.util.MD5;
import com.casi.project.system.domain.vo.RouterVo;
import com.casi.project.system.service.ISysRoleService;
import com.casi.project.system.service.ISysUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.casi.common.constant.Constants;
import com.casi.common.utils.ServletUtils;
import com.casi.framework.security.LoginBody;
import com.casi.framework.security.LoginUser;
import com.casi.framework.security.service.SysLoginService;
import com.casi.framework.security.service.SysPermissionService;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.system.domain.SysMenu;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.service.ISysMenuService;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * 登录验证
 *
 * @author lzb
 */
@Api(tags = {"登录"})
@RestController
@CrossOrigin
public class SysLoginController {
    @Autowired
    private SysLoginService loginService;

    @Autowired
    private ISysMenuService menuService;

    @Autowired
    private SysPermissionService permissionService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private RedisCache redisCache;

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ISysUserService sysUserService;

    /**
     * 登录方法
     * @param loginBody-userName 用户名
     * @param loginBody-password 密码
     * @param loginBody-captcha  验证码
     * @param loginBody-uuid     唯一标识
     * @return 结果
     */
    @ApiOperation(value = "登录入口")
    @CrossOrigin
    @PostMapping(value = "/login", produces = "application/json")
    public AjaxResult login(@RequestBody LoginBody loginBody, HttpServletRequest request) {
        AjaxResult ajax = AjaxResult.success();
        // 管理员账号规定时长重置密码  7天
        SysUser sysUser = sysUserService.selectUserByUserName(loginBody.getUsername());
        if (sysUser != null) {
            if ("1".equals(sysUser.getUserId())) {
                Date passwordUpdateTime = sysUser.getUpdateTime();
                if (passwordUpdateTime == null) {
                    return AjaxResult.error("密码过期,请重置密码!");
                }
                Date nowDate = DateUtils.getNowDate();
                long time = nowDate.getTime() - passwordUpdateTime.getTime();
                long day = time / (1000 * 60 * 60 * 24);
                if (day > 7) {
                    return AjaxResult.error("请重置密码!");
                }
            }
        }
        // 生成令牌
        return loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
                loginBody.getUuid(), request);
    }

    // APP免登陆
    @CrossOrigin
    @PostMapping(value = "/appLogin", produces = "application/json")
    public AjaxResult appLogin(@RequestBody Map<String, Object> paraMap) {
        AjaxResult ajax = AjaxResult.success();
        if (paraMap.get("userId") != null) {
            // 生成令牌
            String token = loginService.appLogin(paraMap.get("userId").toString());
            ajax.put(Constants.TOKEN, token);
            System.out.println("token为===========================" + token);
        }
        return ajax;
    }

    /**
     * 第三方登录
     *
     * @param userName 用户名
     * @param mobile   手机号
     * @return 结果
     */
    @GetMapping(value = "/login")
    public AjaxResult login(String userName, String mobile) {
        AjaxResult ajax = AjaxResult.success();
        SysUser user = sysUserService.selectUserByUserName(userName);
        LoginUser loginUser = new LoginUser();
        loginUser.setUser(user);
        String token = loginService.appLogin(loginUser);
        redisCache.setCacheObject(Constants.TOKEN, token);
        ajax.put(Constants.TOKEN, "");
        return ajax;
    }

    @GetMapping(value = "/getToken")
    public AjaxResult getToken() {
        String token = (String) redisCache.getCacheObject(Constants.TOKEN);
        if (token == null) {
            token = "";
        }
        AjaxResult ajax = AjaxResult.success();
        ajax.put(Constants.TOKEN, token);
        return ajax;
    }

    /**
     * 第三方单点登录授权
     *
     * @param loginBody
     * @return
     */
    @PostMapping(value = "/casi/mobile/appAuthorize")
    public AjaxResult appAuthorize(@RequestBody LoginBody loginBody) {
        AjaxResult ajax = AjaxResult.success();
        if (loginBody == null) {
            return AjaxResult.error("参数不能为空!");
        }
        if (loginBody.getAppKey() == null) {
            return AjaxResult.error("AppKey不能为空!");
        }
        if (loginBody.getAppSecret() == null) {
            return AjaxResult.error("AppSecret不能为空!");
        }
        if (loginBody.getAppUserName() == null) {
            return AjaxResult.error("用户名不能为空!");
        }
        ThirdPartyLoginBody thirdPartyLoginBody = new ThirdPartyLoginBody();
        String url = "http://58.216.47.95:10454/idp_to_api/mobile/login";
        ResponseEntity<? extends ThirdPartyLoginBody> responseEntity = restTemplate.postForEntity(url, thirdPartyLoginBody, thirdPartyLoginBody.getClass());
        ThirdPartyLoginBody body = responseEntity.getBody();
        ajax = sysUserService.addThirdPartyInfo(loginBody.getAppUserName(), loginBody.getAppKey(), loginBody.getAppSecret(), body);
        return ajax;
    }

    /**
     * 获取用户信息
     *
     * @return 用户信息
     */
    @ApiOperation(value = "获取用户信息")
    @GetMapping("getInfo")
    public AjaxResult getInfo() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        SysUser user = loginUser.getUser();
        // 角色集合
     //   Set<String> roles = permissionService.getRolePermission(user);
        // 权限集合
        Set<String> permissions = permissionService.getMenuPermission(user);
        AjaxResult ajax = AjaxResult.success();
        ajax.put("user", user);
      //  ajax.put("roles", roles);
        ajax.put("permissions", permissions);
        return ajax;
    }

    /**
     * 获取路由信息
     *
     * @return 路由信息
     */
    @ApiOperation(value = "获取路由信息")
    @GetMapping("getRouters")
    public AjaxResult getRouters() {
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        // 用户信息
        SysUser user = loginUser.getUser();
        List<SysMenu> menus = menuService.selectMenuTreeByUserId(user.getUserId());
        // 无权限 默认页面
        if (StringUtils.isEmpty(menus)) {
            SysMenu menu = new SysMenu();
            menu.setMenuId("1");
            menu.setMenuName("系统管理");
            menu.setParentId("0");
            menu.setPath("/system");
            menu.setMenuType("M");
            menu.setIcon("system");
            menu.setRemark("系统管理目录");
            menu.setMenuCode("1");
            menu.setComponent("");
            menus.add(menu);
        }
        return AjaxResult.success(menuService.buildMenus(menus));
    }

    @PostMapping("adminResetPwd")
    public AjaxResult adminResetPwd(@RequestBody Map params) {
        AjaxResult ajax = loginService.adminResetPwd(params);
        return ajax;
    }
}
