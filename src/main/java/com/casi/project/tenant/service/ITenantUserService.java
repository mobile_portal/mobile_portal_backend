package com.casi.project.tenant.service;

import java.util.List;
import com.casi.project.tenant.domain.TenantUser;

/**
 * 租户账号Service接口
 * 
 * @author casi
 * @date 2020-06-02
 */
public interface ITenantUserService 
{
    /**
     * 查询租户账号
     * 
     * @param uuid 租户账号ID
     * @return 租户账号
     */
    public TenantUser selectTenantUserById(String uuid);

    /**
     * 查询租户账号列表
     * 
     * @param tenantUser 租户账号
     * @return 租户账号集合
     */
    public List<TenantUser> selectTenantUserList(TenantUser tenantUser);

    /**
     * 新增租户账号
     * 
     * @param tenantUser 租户账号
     * @return 结果
     */
    public int insertTenantUser(TenantUser tenantUser);

    /**
     * 修改租户账号
     * 
     * @param tenantUser 租户账号
     * @return 结果
     */
    public int updateTenantUser(TenantUser tenantUser);

    /**
     * 批量删除租户账号
     * 
     * @param uuids 需要删除的租户账号ID
     * @return 结果
     */
    public int deleteTenantUserByIds(String[] uuids);

    /**
     * 删除租户账号信息
     * 
     * @param uuid 租户账号ID
     * @return 结果
     */
    public int deleteTenantUserById(String uuid);
}
