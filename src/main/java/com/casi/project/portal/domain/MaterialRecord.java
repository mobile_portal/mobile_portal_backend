package com.casi.project.portal.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description:素材发布记录实体类
 * @author zhongzj
 * @date 2019/11/19
 */
public class MaterialRecord {

  /**
   * 主键id
   */
  private Long id;
  /**
   * 素材id
   */
  private Long baseId;
  /**
   * 终端id
   */
  private Long accountId;
  /**
   * 素材类型
   */
  private String mediaType;
  /**
   * 终端类型
   */
  private String terminalType;
  /**
   * 微信上传返回媒体id（仅微信有）
   */
  private String wxMediaId;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }


  public Long getBaseId() {
    return baseId;
  }

  public void setBaseId(Long baseId) {
    this.baseId = baseId;
  }


  public Long getAccountId() {
    return accountId;
  }

  public void setAccountId(Long accountId) {
    this.accountId = accountId;
  }


  public String getMediaType() {
    return mediaType;
  }

  public void setMediaType(String mediaType) {
    this.mediaType = mediaType;
  }


  public String getTerminalType() {
    return terminalType;
  }

  public void setTerminalType(String terminalType) {
    this.terminalType = terminalType;
  }


  public String getWxMediaId() {
    return wxMediaId;
  }

  public void setWxMediaId(String wxMediaId) {
    this.wxMediaId = wxMediaId;
  }

    /**
     * @Auther shp
     * @data2020/4/2414:34
     * @description
     */

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class LocalFile {

        private Long id;
        private String fileName;
        private String fileUrl;
        private Long size;
        private String name;


    }
}
