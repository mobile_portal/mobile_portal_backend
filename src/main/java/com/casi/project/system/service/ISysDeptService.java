package com.casi.project.system.service;

import java.util.List;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.framework.web.domain.TreeSelect;
import com.casi.project.system.domain.Institution;
import com.casi.project.system.domain.SysDept;
import org.springframework.web.multipart.MultipartFile;

/**
 * 部门管理 服务层
 * 
 * @author lzb
 */
public interface ISysDeptService
{
    /**
     * 查询部门管理数据
     * 
     * @param dept 部门信息
     * @return 部门信息集合
     */
    public List<SysDept> selectDeptList(SysDept dept);

    /**
     *  根据当前用户权限获取部门列表树
     */
    List<SysDept> selectDeptListByLoginIn();

    /**
     * 构建前端所需要树结构
     * 
     * @param depts 部门列表
     * @return 树结构列表
     */
    public List<SysDept> buildDeptTree(List<SysDept> depts);

    /**
     * 构建前端所需要下拉树结构
     * 
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    public List<TreeSelect> buildDeptTreeSelect(List<SysDept> depts);

    /**
     * 根据角色ID查询部门树信息
     * 
     * @param roleId 角色ID
     * @return 选中部门列表
     */
    public List<Integer> selectDeptListByRoleId(String roleId);

    /**
     * 根据部门ID查询信息
     * 
     * @param deptId 部门ID
     * @return 部门信息
     */
    public SysDept selectDeptById(String deptId);

    /**
     * 是否存在部门子节点
     * 
     * @param deptId 部门ID
     * @return 结果
     */
    public boolean hasChildByDeptId(String deptId);

    /**
     * 查询部门是否存在用户
     * 
     * @param deptId 部门ID
     * @return 结果 true 存在 false 不存在
     */
    public boolean checkDeptExistUser(String deptId);

    /**
     * 校验部门名称是否唯一
     * 
     * @param dept 部门信息
     * @return 结果
     */
    public String checkDeptNameUnique(String deptName);

    /**
     * 校验部门编码是否唯一
     */
    public String checkDeptCodeUnique(String deptCode);

    /**
     * 新增保存部门信息
     * 
     * @param dept 部门信息
     * @return 结果
     */
    public int insertDept(SysDept dept);

    /**
     * 修改保存部门信息
     * 
     * @param dept 部门信息
     * @return 结果
     */
    public int updateDept(SysDept dept);

    /**
     * 组织状态修改
     */
    public int changeStatus(SysDept dept);

    /**
     * 删除部门管理信息
     * 
     * @param deptId 部门ID
     * @return 结果
     */
    public int deleteDeptById(String deptId);

    /**
     * 获取组织列表
     * @param dept
     * @return
     */
    List<SysDept> selectOrganList();

    /**
     * 删除组织
     * @param deptId 组织ID
     * @return 结果
     */
    int deleteOrganById(String deptId);
    /**
     * 数据同步添加组织机构
     */
    public AjaxResult insertSysDeptOne(Institution institution);
    /**
     * 数据同步删除组织机构
     */
//    public AjaxResult deleteOne(Institution institution);
    /**
     * 熟不同步更新组织机构
     */
//    public AjaxResult updetaDeptOne(Institution institution);


}
