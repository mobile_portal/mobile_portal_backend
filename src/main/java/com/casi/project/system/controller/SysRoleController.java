package com.casi.project.system.controller;

import com.casi.common.constant.UserConstants;
import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.StringUtils;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.framework.web.page.TableDataInfo;
import com.casi.project.system.domain.RoleIds;
import com.casi.project.system.domain.StringAndStringArray;
import com.casi.project.system.domain.SysRole;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.domain.vo.StringArray;
import com.casi.project.system.mapper.SysRoleMenuMapper;
import com.casi.project.system.service.ISysMenuService;
import com.casi.project.system.service.ISysRoleService;
import com.casi.project.system.service.ISysUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 角色信息
 *
 * @author lzb
 */
@Api(tags = {"角色管理"})
@RestController
@RequestMapping("/system/role")
public class SysRoleController extends BaseController {
    @Autowired
    private ISysRoleService roleService;

    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;


    @Autowired
    private ISysUserService userService;

    @Autowired
    private ISysMenuService menuService;

    /**
     * 获取角色列表分页
     */
    @ApiOperation(value = "角色列表")
    @PreAuthorize("@ss.hasPermi('system:role:list')")
    @GetMapping("/list")
    public AjaxResult list(SysRole role, Integer pageNum, Integer pageSize) {
        logger.info("===============获取角色列表=============");
        if (null == pageNum || null == pageSize) {
            return AjaxResult.error("请传入页数或每页展示的条数");
        }
       /* if(null == role.getRoleType() || role.getRoleType().trim().equals("")){
            return AjaxResult.error("缺失参数");
        }
        if(!role.getRoleType().equals("0") && !role.getRoleType().equals("1")){
            return AjaxResult.error("参数错误");
        }*/
        PageHelper.startPage(pageNum, pageSize);
        List<SysRole> list = null;
        try {
            //   role.setRoleType("0");
            list = roleService.selectRoleList(role);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");

        }
        PageInfo<SysRole> noticeVOPageInfo = new PageInfo<>(list);
        return AjaxResult.success("查询成功", noticeVOPageInfo);
    }

    /**
     * 根据登录用户获取 系统角色列表
     */
    @ApiOperation(value = "根据登录用户获取系统角色列表")
    @GetMapping("/roleListByLogin")
    public AjaxResult roleListByLogin() {
        logger.info("===============获取角色列表=============");
        List<SysRole> list = null;
        AjaxResult ajax = new AjaxResult();
        try {
            // list = roleService.roleListByLogin();
            SysRole role = new SysRole();
            role.setRoleType("0");
            list = roleService.selectRoleList(role);
            ajax.put("msg", "操作成功");
            ajax.put("code", 200);
            ajax.put("data", list);

            // showPwdButton 前端根据这个值 控制是否展示重置密码按钮
            Boolean bool = true;
            if (SecurityUtils.getLoginUser().getUser().isAdmin()) {
                bool = false;
            } else if (StringUtils.isNotEmpty(list)) {
                for (SysRole sysRole : list) {
                    if (sysRole != null) {
                        String[] perms = menuService.selectPermsByRoleId(sysRole.getRoleId());
                        for (String perm : perms) {
                            if ("system:user:resetPwd".equals(perm)) {
                                bool = false;
                            }
                        }
                    }
                }
            }
            ajax.put("showPwdButton", bool);

        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            return AjaxResult.error("查询失败");

        }
        return ajax;
    }

    /**
     * 修改角色的时候根据角色id查询
     */
    @ApiOperation(value = "角色回显")
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @PostMapping("/queryOneByRoleId")
    public AjaxResult queryOneByRoleId(@RequestBody SysRole role) {
        logger.info("===============修改角色=============");
        SysRole sysRole = new SysRole();
        try {
            if (null == role) {
                return AjaxResult.error("角色内容不能为空");
            }
            if (null == role.getRoleId()) {
                return AjaxResult.error("角色id不能为空");
            }

            //系统角色多选框回显
            sysRole = roleService.selectRoleById(role.getRoleId());
            String[] longs = roleService.selectByRoleId(role.getRoleId());
            if (StringUtils.isNotEmpty(longs)) {
                sysRole.setMenuIds(longs);
            }

        } catch (Exception e) {
            logger.error("修改角色失败", e);
            return AjaxResult.error("修改角色失败");
        }
        return AjaxResult.success(sysRole);
    }


    /**
     * 平台角色回显  应用角色回显
     */
    @ApiOperation(value = "应用角色回显")
    @PostMapping("/queryApplyRoleOneByRoleId")
    public AjaxResult queryApplyRoleOneByRoleId(@RequestBody SysRole role) {
        logger.info("===============修改角色=============");
        SysRole sysRole;
        try {
            if (null == role) {
                return AjaxResult.error("角色内容不能为空");
            }
            if (null == role.getRoleId()) {
                return AjaxResult.error("角色id不能为空");
            }
            sysRole = roleService.selectApplyRoleById(role.getRoleId());
            String[] longs = roleService.selectApplyId(role.getRoleId());
            sysRole.setApplyId(longs);

        } catch (Exception e) {
            logger.error("修改角色失败", e);
            return AjaxResult.error("修改角色失败");
        }
        return AjaxResult.success(sysRole);
    }

    @Log(title = "角色管理", businessType = BusinessType.EXPORT)
    @PreAuthorize("@ss.hasPermi('system:role:export')")
    @GetMapping("/export")
    public AjaxResult export(SysRole role) {
        logger.info("===============角色数据模板导出=============");
        List<SysRole> list;
        ExcelUtil<SysRole> util;
        AjaxResult ajaxResult;
        try {
            list = roleService.selectRoleList(role);
            util = new ExcelUtil<SysRole>(SysRole.class);
            ajaxResult = util.exportExcel(list, "角色数据");
        } catch (Exception e) {
            logger.error("角色数据模板导出失败", e);
            return AjaxResult.error("角色数据模板导出失败");
        }
        return ajaxResult;
    }

    /**
     * 根据角色编号获取详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:role:query')")
    @GetMapping(value = "/{roleId}")
    public AjaxResult getInfo(@PathVariable String roleId) {
        logger.info("===============根据角色编号获取详细信息=============");
        SysRole sysRole;
        try {
            sysRole = roleService.selectRoleById(roleId);
        } catch (Exception e) {
            logger.error("根据角色编号获取详细信息失败", e);
            return AjaxResult.error("根据角色编号获取详细信息失败");
        }
        return AjaxResult.success(sysRole);
    }

    /**
     * 新增系统角色
     */
    @ApiOperation(value = "系统角色新增")
    @PreAuthorize("@ss.hasPermi('system:role:add')")
    @Log(title = "角色管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@Validated @RequestBody SysRole role) {
        logger.info("===============新增角色=============");
        int i;
        try {
            if (null == role) {
                return AjaxResult.error("角色不能为空");
            }
            // 系统角色标识
            role.setRoleType("0");
            if (StringUtils.isEmpty(role.getRoleName())) {
                return AjaxResult.error("角色名称不能为空");
            }
            if (null == role.getRoleCode() || "".equals(role.getRoleCode().trim())) {
                return AjaxResult.error("角色编码不能为空");
            }
            if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleCodeUnique(role.getRoleCode()))) {
                return AjaxResult.error("角色编码已存在");
            }
            // 上下級角色控制
            // roleService.isHavePerm(role);
            if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
                return AjaxResult.error("新增角色'" + role.getRoleName() + "'失败，角色名称已存在");
            }
            if (StringUtils.isEmpty(role.getMenuIds())){
                return AjaxResult.error("角色权限不能为空");
            }
            /*else if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
                return AjaxResult.error("新增角色'" + role.getRoleName() + "'失败，角色权限已存在");
            }*/
            role.setCreateBy(SecurityUtils.getUsername());
            i = roleService.insertRole(role);
        } catch (Exception e) {
            logger.error("新增角色失败", e);
            return AjaxResult.error("新增角色失败");
        }
        return toAjax(i);
    }


    /**
     * 新增应用角色
     */
    @ApiOperation(value = "应用角色新增")
    @PreAuthorize("@ss.hasPermi('system:role:add')")
    @Log(title = "角色管理", businessType = BusinessType.INSERT)
    @PostMapping("addApplyRole")
    public AjaxResult addApplyRole(@Validated @RequestBody SysRole role) {
        logger.info("===============新增角色=============");
        int i;
        try {
            if (role == null) {
                return AjaxResult.error("角色不能为空");
            }
            // 应用角色标识
            role.setRoleType("1");
            if (StringUtils.isEmpty(role.getRoleName())) {
                return AjaxResult.error("角色名称不能为空");
            }
            if (null == role.getRoleCode() || "".equals(role.getRoleCode().trim())) {
                return AjaxResult.error("角色编码不能为空");
            }
            if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleCodeUnique(role.getRoleCode()))) {
                return AjaxResult.error("角色编码已存在");
            }
            if (StringUtils.isEmpty(role.getMenuIds())){
                return AjaxResult.error("角色权限不能为空");
            }
            // roleService.isHavePerm(role);
            if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
                return AjaxResult.error("新增角色'" + role.getRoleName() + "'失败，角色名称已存在");
            }
            role.setCreateBy(SecurityUtils.getUsername());
            i = roleService.addApplyRole(role);
        } catch (Exception e) {
            logger.error("新增角色失败", e);
            return AjaxResult.error("新增角色失败");
        }
        return toAjax(i);
    }


    /**
     * 授权开发者角色
     */
    @ApiOperation(value = "授权开发者角色")
    @PreAuthorize("@ss.hasPermi('apply:role:list')")
    @Log(title = "角色管理", businessType = BusinessType.INSERT)
    @PostMapping("addDeveloper")
    public AjaxResult addDeveloper(@RequestBody StringArray userIds) {
        logger.info("===============授权开发者角色=============");
        AjaxResult ajax;
        try {
            if (userIds == null) {
                return AjaxResult.error("请勾选用户");
            }
            if (StringUtils.isEmpty(userIds.getUserIds())) {
                return AjaxResult.error("请勾选用户");
            }
            ajax = roleService.addDeveloper(userIds.getUserIds());
        } catch (Exception e) {
            logger.error("授权开发者角色失败", e);
            return AjaxResult.error("授权开发者角色失败");
        }
        return ajax;
    }


    /**
     * 开发者角色列表展示
     */
    @ApiOperation(value = "开发者角色列表展示")
    @PreAuthorize("@ss.hasPermi('apply:role:list')")
    @GetMapping("listDeveloper")
    public TableDataInfo listDeveloper() {
        logger.info("===============开发者角色列表展示=============");
        AjaxResult ajax;
        List<SysUser> users = new ArrayList<>();
        try {
            startPage();
            users = userService.selectDeveloperList();

        } catch (Exception e) {
            logger.error("开发者角色列表展示失败", e);
            TableDataInfo dataInfo = new TableDataInfo();
            dataInfo.setCode(500);
            dataInfo.setMsg("开发者角色列表展示失败");
            dataInfo.setTotal(0L);
            return dataInfo;
        }
        return getDataTable(users);
    }


    /**
     * 应用模块 关联角色列表展示
     */
    @ApiOperation(value = "应用角色关联列表展示")
    @PreAuthorize("@ss.hasPermi('apply:role:list')")
    @PostMapping("usersLinkedRoleList")
    public TableDataInfo usersLinkedRoleList(@RequestBody Map parms) {
        logger.info("===============关联角色列表展示=============");
        AjaxResult ajax;
        List<SysUser> users = new ArrayList<>();
        try {
            startPage();
            users = userService.linkedRoleList(parms);

        } catch (Exception e) {
            logger.error("关联角色列表展示失败", e);
            TableDataInfo dataInfo = new TableDataInfo();
            dataInfo.setCode(500);
            dataInfo.setMsg("关联角色列表展示失败");
            dataInfo.setTotal(0L);
            return dataInfo;
        }
        return getDataTable(users);
    }


    /**
     * 修改保存角色 没用到
     */
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@Validated @RequestBody SysRole role) {
        logger.info("===============修改保存角色=============");
        int i;
        try {
            roleService.checkRoleAllowed(role);
          //  roleService.isHavePerm(role);
            if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
                return AjaxResult.error("修改角色'" + role.getRoleName() + "'失败，角色名称已存在");
            } /*else if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
                return AjaxResult.error("修改角色'" + role.getRoleName() + "'失败，角色权限已存在");
            }*/
            role.setUpdateBy(SecurityUtils.getUsername());
            i = roleService.updateRole(role);
        } catch (Exception e) {
            logger.error("修改保存角色失败", e);
            return AjaxResult.error("修改保存角色失败");
        }
        return toAjax(i);
    }

    /**
     * 修改角色
     */
    @ApiOperation(value = "修改角色")
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("/addSysRole")
    public AjaxResult editRole(@RequestBody SysRole role) {
        logger.info("===============修改保存角色=============");
        int i;
        try {
            SysRole sysRole = roleService.selectRoleById(role.getRoleId());

            if (null == role) {
                return AjaxResult.error("角色不能为空");
            }
            if ("1".equals(role.getRoleId())) {
                return AjaxResult.error("不允许操作超级管理员角色");
            }
            if (StringUtils.isEmpty(role.getRoleName())) {
                return AjaxResult.error("角色名称不能为空");
            }
            if (StringUtils.isEmpty(role.getRoleCode())) {
                return AjaxResult.error("角色编码不能为空");
            }
            if (StringUtils.isEmpty(role.getMenuIds())){
                return AjaxResult.error("角色权限不能为空");
            }
            if (!role.getRoleCode().equals(sysRole.getRoleCode()) &&
                    UserConstants.NOT_UNIQUE.equals(roleService.checkRoleCodeUnique(role.getRoleCode()))) {
                return AjaxResult.error("角色编码已存在");
            }
            if (!role.getRoleName().equals(sysRole.getRoleName()) &&
                    UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role.getRoleName()))) {
                return AjaxResult.error("角色名称已存在");
            }
            roleService.checkRoleAllowed(role);
          //  roleService.isHavePerm(role);
            if (!role.getRoleName().equals(sysRole.getRoleName()) &&
                    UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
                return AjaxResult.error("角色名称已存在");
            }
            /*else if (!role.getRoleName().equals(sysRole.getRoleName()) &&
                    UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
                return AjaxResult.error("角色权限已存在");
            }*/
            role.setUpdateBy(SecurityUtils.getUsername());
            i = roleService.updateRole(role);
        } catch (Exception e) {
            logger.error("修改保存角色失败", e);
            return AjaxResult.error("修改保存角色失败");
        }
        return toAjax(i);
    }


    /**
     * 修改应用角色
     */
    @ApiOperation(value = "修改应用角色")
    @PreAuthorize("@ss.hasPermi('apply:role:list')")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PostMapping("/editApplyRole")
    public AjaxResult editApplyRole(@RequestBody SysRole role) {
        logger.info("===============修改保存角色=============");
        int i;
        try {
            SysRole sysRole = roleService.selectApplyRoleById(role.getRoleId());

            if (null == role) {
                return AjaxResult.error("角色不能为空");
            }
            if (StringUtils.isEmpty(role.getRoleName())) {
                return AjaxResult.error("角色名称不能为空");
            }
            if (StringUtils.isEmpty(role.getRoleCode())) {
                return AjaxResult.error("角色编码不能为空");
            }

            if (!role.getRoleCode().equals(sysRole.getRoleCode()) &&
                    UserConstants.NOT_UNIQUE.equals(roleService.checkRoleCodeUnique(role.getRoleCode()))) {
                return AjaxResult.error("角色编码已存在");
            }
            if (!role.getRoleName().equals(sysRole.getRoleName()) &&
                    UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role.getRoleName()))) {
                return AjaxResult.error("角色名称已存在");
            }

            roleService.checkRoleAllowed(role);
           // roleService.isHavePerm(role);
            if (!role.getRoleName().equals(sysRole.getRoleName()) &&
                    UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
                return AjaxResult.error("角色名称已存在");
            }
            /*else if (!role.getRoleName().equals(sysRole.getRoleName()) &&
                    UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
                return AjaxResult.error("角色权限已存在");
            }*/
            role.setUpdateBy(SecurityUtils.getUsername());
            i = roleService.editApplyRole(role);
        } catch (Exception e) {
            logger.error("修改保存角色失败", e);
            return AjaxResult.error("修改保存角色失败");
        }
        return toAjax(i);
    }

    /**
     * 修改保存数据权限
     */
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PutMapping("/dataScope")
    public AjaxResult dataScope(@RequestBody SysRole role) {
        logger.info("===============修改保存数据权限=============");
        int i;
        try {
            roleService.checkRoleAllowed(role);
          //  roleService.isHavePerm(role);
            i = roleService.authDataScope(role);
        } catch (Exception e) {
            logger.error("修改保存数据权限失败", e);
            return AjaxResult.error("修改保存数据权限失败");
        }
        return toAjax(i);
    }

    /**
     * 状态修改
     */
    @PreAuthorize("@ss.hasPermi('system:role:edit')")
    @Log(title = "角色管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody SysRole role) {
        logger.info("===============状态修改=============");
        int i;
        try {
            roleService.checkRoleAllowed(role);
         //   roleService.isHavePerm(role);
            role.setUpdateBy(SecurityUtils.getUsername());
            i = roleService.updateRoleStatus(role);

        } catch (Exception e) {
            logger.error("状态修改失败", e);
            return AjaxResult.error("状态修改失败");
        }
        return toAjax(i);
    }

    /**
     * 删除角色
     */
    @PreAuthorize("@ss.hasPermi('system:role:remove')")
    @Log(title = "角色管理", businessType = BusinessType.DELETE)
    @DeleteMapping("/{roleIds}")
    public AjaxResult remove(@PathVariable String[] roleIds) {
        logger.info("===============删除角色=============");
        int i;
        try {
            if (StringUtils.isEmpty(roleIds)) {
                return AjaxResult.error("请选择删除的角色");
            }
            for (String roleId : roleIds) {
                if ("1".equals(roleId)) {
                    return AjaxResult.error("不允许操作超级管理员角色");
                }
            }
          //  roleService.isHavePermByRoleId(roleIds);
            i = roleService.deleteRoleByIds(roleIds);
        } catch (Exception e) {
            logger.error("删除角色失败", e);
            return AjaxResult.error("删除角色失败");
        }
        return toAjax(i);
    }

    /**
     * 删除系统角色
     */
    @ApiOperation(value = "删除系统角色")
    @Log(title = "角色管理", businessType = BusinessType.DELETE)
    @PreAuthorize("@ss.hasPermi('system:role:remove')")
    @GetMapping("/delremove1")
    public AjaxResult removeSysRole(String roleId) {
        // public AjaxResult remove1(@RequestBody RoleIds roleIds) {
        logger.info("===============删除角色=============");
        int i;
        try {
            // List<String> roleIds1 = roleIds.getRoleIds();
            // String[] L = roleIds1.stream().toArray(String[]::new);
            //    roleService.isHavePermByRoleId(roleId);
            //  i = roleService.deleteRoleByIds(L);
            if (StringUtils.isEmpty(roleId)) {
                return AjaxResult.error("请选择删除的角色");
            }
            if ("1".equals(roleId)) {
                return AjaxResult.error("不允许操作超级管理员角色");
            }
            i = roleService.deleteRoleById(roleId);
        } catch (Exception e) {
            logger.error("删除角色失败", e);
            return AjaxResult.error("该角色下存在用户不可删除");
        }
        return toAjax(i);
    }

    /**
     * 获取角色选择框列表
     */
    @PreAuthorize("@ss.hasPermi('system:role:query')")
    @GetMapping("/optionselect")
    public AjaxResult optionselect() {
        logger.info("===============获取角色选择框列表=============");
        List<SysRole> sysRoles;
        try {
            sysRoles = roleService.selectRoleAll();
        } catch (Exception e) {
            logger.error("获取角色选择框列表失败", e);
            return AjaxResult.error("获取角色选择框列表失败");
        }
        return AjaxResult.success(sysRoles);
    }

    /**
     * 新增系统角色
     */
    @ApiOperation(value = "新增系统角色")
    @PreAuthorize("@ss.hasPermi('system:role:add')")
    @Log(title = "角色管理", businessType = BusinessType.INSERT)
    @PostMapping("/addRole")
    public AjaxResult addRole(@Validated @RequestBody SysRole role, String deptId) {
        logger.info("===============新增系统角色=============");
        int i;
        try {
         //   roleService.isHavePerm(role);
            if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
                return AjaxResult.error("新增角色'" + role.getRoleName() + "'失败，角色名称已存在");
            } /*else if (UserConstants.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
                return AjaxResult.error("新增角色'" + role.getRoleName() + "'失败，角色权限已存在");
            }*/
            role.setCreateBy(SecurityUtils.getUsername());
            i = roleService.addRole(role, deptId);
        } catch (Exception e) {
            logger.error("新增系统角色失败", e);
            return AjaxResult.error("新增系统角色失败");
        }
        return toAjax(i);
    }

    /**
     * 添加用户关联应用角色
     */
    @ApiOperation(value = "添加用户关联应用角色")
    @PreAuthorize("@ss.hasPermi('system:role:add')")
    @Log(title = "应用角色管理", businessType = BusinessType.INSERT)
    @PostMapping("userLinkedRole")
    public AjaxResult usersLinkedRole(@RequestBody StringAndStringArray stringAndStringArray) {
        logger.info("===============添加用户关联应用角色=============");
        AjaxResult ajax;
        try {
            if (stringAndStringArray == null) {
                return AjaxResult.error("请传入参数");
            }
            if (StringUtils.isEmpty(stringAndStringArray.getUserIds())) {
                return AjaxResult.error("请传入用户参数");
            }
            if (StringUtils.isEmpty(stringAndStringArray.getRoleId())) {
                return AjaxResult.error("请选择角色参数");
            }
            ajax = roleService.usersLinkedRole(stringAndStringArray.getRoleId(), stringAndStringArray.getUserIds());
        } catch (Exception e) {
            logger.error("添加用户关联应用角色失败", e);
            return AjaxResult.error("添加用户关联应用角色失败");
        }
        return ajax;
    }

}