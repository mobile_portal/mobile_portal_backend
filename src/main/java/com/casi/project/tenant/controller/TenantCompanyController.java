package com.casi.project.tenant.controller;

import java.util.List;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.casi.framework.aspectj.lang.annotation.Log;
import com.casi.framework.aspectj.lang.enums.BusinessType;
import com.casi.project.tenant.domain.TenantCompany;
import com.casi.project.tenant.service.ITenantCompanyService;
import com.casi.framework.web.controller.BaseController;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.common.utils.poi.ExcelUtil;
import com.casi.framework.web.page.TableDataInfo;

/**
 * 租户企业Controller
 * 
 * @author casi
 * @date 2020-06-02
 */
@Api(tags = {"租户企业"})
@RestController
@RequestMapping("/tenant/company")
public class TenantCompanyController extends BaseController
{
    @Autowired
    private ITenantCompanyService tenantCompanyService;

    /**
     * 查询租户企业列表
     */
    @ApiOperation(value = "查询租户企业列表")
    @PreAuthorize("@ss.hasPermi('tenant:company:list')")
    @GetMapping("/list")
    public TableDataInfo list(TenantCompany tenantCompany)
    {
        startPage();
        List<TenantCompany> list = tenantCompanyService.selectTenantCompanyList(tenantCompany);
        return getDataTable(list);
    }

    /**
     * 导出租户企业列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:company:export')")
    @Log(title = "租户企业", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(TenantCompany tenantCompany)
    {
        List<TenantCompany> list = tenantCompanyService.selectTenantCompanyList(tenantCompany);
        ExcelUtil<TenantCompany> util = new ExcelUtil<TenantCompany>(TenantCompany.class);
        return util.exportExcel(list, "company");
    }

    /**
     * 获取租户企业详细信息
     */
    @ApiOperation(value = "获取租户企业详细信息")
    @PreAuthorize("@ss.hasPermi('tenant:company:query')")
    @GetMapping(value = "/{uuid}")
    public AjaxResult getInfo(@PathVariable("uuid") String uuid)
    {
        return AjaxResult.success(tenantCompanyService.selectTenantCompanyById(uuid));
    }

    /**
     * 新增租户企业
     */
    @ApiOperation(value = "新增租户企业")
    @PreAuthorize("@ss.hasPermi('tenant:company:add')")
    @Log(title = "租户企业", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody TenantCompany tenantCompany)
    {
        return toAjax(tenantCompanyService.insertTenantCompany(tenantCompany));
    }

    /**
     * 修改租户企业
     */
    @ApiOperation(value = "修改租户企业")
    @PreAuthorize("@ss.hasPermi('tenant:company:edit')")
    @Log(title = "租户企业", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody TenantCompany tenantCompany)
    {
        return toAjax(tenantCompanyService.updateTenantCompany(tenantCompany));
    }

    /**
     * 删除租户企业
     */
    @ApiOperation(value = "删除租户企业")
    @PreAuthorize("@ss.hasPermi('tenant:company:remove')")
    @Log(title = "租户企业", businessType = BusinessType.DELETE)
	@DeleteMapping("/{uuids}")
    public AjaxResult remove(@PathVariable String[] uuids)
    {
        return toAjax(tenantCompanyService.deleteTenantCompanyByIds(uuids));
    }
}
