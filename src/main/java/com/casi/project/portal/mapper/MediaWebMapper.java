package com.casi.project.portal.mapper;


import com.casi.project.portal.domain.MediaWeb;

import java.util.List;

public interface MediaWebMapper {
    int insert(MediaWeb mediaWeb);

    int update(MediaWeb mediaWeb);

    int delete(Long id);

    MediaWeb queryById(Long id);

    List<MediaWeb> queryList(MediaWeb mediaWeb);
}
