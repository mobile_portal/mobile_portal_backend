package com.casi.project.system.mapper;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.casi.project.system.domain.SysRoleDept;
import org.apache.ibatis.annotations.Param;

/**
 * 角色与部门关联表 数据层
 * 
 * @author lzb
 */
public interface SysRoleDeptMapper
{
    /**
     * 通过角色ID删除角色和部门关联
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int deleteRoleDeptByRoleId(String roleId);

    /**
     * 批量删除角色部门关联信息
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteRoleDept(String[] ids);

    /**
     * 查询部门使用数量
     * 
     * @param deptId 部门ID
     * @return 结果
     */
    public int selectCountRoleDeptByDeptId(String deptId);

    /**
     * 批量新增角色部门信息
     * 
     * @param roleDeptList 角色部门列表
     * @return 结果
     */
    public int batchRoleDept(List<SysRoleDept> roleDeptList);
    /**
     * 根据角色id查询部门id
     */
    public List<SysRoleDept> selectByRoleId(String roleId);
    /**
     * 通过部门ID和角色的id删除部门和角色关联
     *
     */
    public int deleteRoleDeptByRoleIdsAndDeptIds(@Param("deptId") String deptId, @Param("roleIds") String[] roleIds);
}
