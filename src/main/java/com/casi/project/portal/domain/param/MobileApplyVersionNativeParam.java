package com.casi.project.portal.domain.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther shp
 * @data2020/4/2014:06
 * @description 原生应用对应的版本信息
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class MobileApplyVersionNativeParam{


    @ApiModelProperty("图标地址")
    private String icon;

    @ApiModelProperty("屏幕截图地址")
    private String screenCapture;


    @ApiModelProperty("版本号")
    private String versionNumber;

    @ApiModelProperty("版本描述")
    private String versionDescribe;

    @ApiModelProperty("0-上架，1-不上架")
    private Integer isPutaway;

    @ApiModelProperty("区分安卓或者ios 0为安卓 1为ios")
    private Integer appOrIos;

    @ApiModelProperty("0为自定义应用 1为系统应用")
    private Integer isCustom;

    @ApiModelProperty("安装包")
    private String installPackage;

    @ApiModelProperty("包名")
    private String packageName;

    @ApiModelProperty("启动跳转地址")
    private String toUrl;

    @ApiModelProperty("关联应用详细中的apply_version")
    private Long mobileHomeMenuDetailsId;

}
