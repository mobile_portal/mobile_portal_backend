package com.casi.project.interfaces.service.impl;

import com.casi.common.constant.Constants;
import com.casi.common.utils.JsonUtils;
import com.casi.common.utils.StringUtils;
import com.casi.common.utils.http.HttpUtils;
import com.casi.framework.redis.RedisCache;
import com.casi.framework.security.service.SysLoginService;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.interfaces.domain.ApiInvok;
import com.casi.project.interfaces.domain.DepartmentVo;
import com.casi.project.interfaces.domain.UserTemp;
import com.casi.project.interfaces.mapper.ApiInvokMapper;
import com.casi.project.interfaces.mapper.DeptTempMapper;
import com.casi.project.interfaces.mapper.UserTempMapper;
import com.casi.project.interfaces.service.WxUserService;
import com.casi.project.interfaces.utils.AccessTokenUtils;
import com.casi.project.system.domain.SysDept;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.mapper.SysDeptMapper;
import com.casi.project.system.mapper.SysUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
@SuppressWarnings("all")
public class WxUserServiceImpl implements WxUserService {
    private static Logger logger = LoggerFactory.getLogger(WxUserServiceImpl.class);

    @Autowired
    private RedisCache redisCache;
    @Autowired
    private AccessTokenUtils accutils;
    @Autowired
    private DeptTempMapper deptTempMapper;
    @Autowired
    private UserTempMapper userTempMapper;
    @Autowired
    private SysUserMapper sysUserMapper;
    @Autowired
    private SysDeptMapper sysDeptMapper;
    @Autowired
    private TokenService tokenService;

    @Autowired
    private SysLoginService loginService;

    @Autowired
    private ApiInvokMapper apiInvokMapper;

    /**
     * 从云信获取组织列表  本地同步云信组织
     *
     * @param idToken
     * @return
     */
    @Transactional
    @Override
    public AjaxResult getOrgList(String idToken, Integer parentId) {


        String accessToken = accutils.getAccessToken();
        String param = "access_token=" + accessToken;
        if (null != parentId) {
            param = param + "&id=" + parentId;
        }
        logger.info("accessToken======================"+accessToken);
        String resultOrgList = HttpUtils.sendGet(Constants.WX_DEPARTMANT_LIST_URL, param);
        if (StringUtils.isNotEmpty(resultOrgList)) {
            Map<Object, Object> map = JsonUtils.stringToCollect(resultOrgList);
            Integer code = (Integer) map.get("errcode");
            if (code == 0) {
                List<DepartmentVo> deptList = JsonUtils.getJsonToList(map.get("department").toString(), DepartmentVo.class);
                SysDept sysDepty = new SysDept();
                if (null != deptList && deptList.size() > 0) {
                    for (int i = 0; i < deptList.size(); i++) {
                        DepartmentVo vo1 = deptList.get(i);
                        Integer id = vo1.getId();
//                        Integer parentid = deptList.get(i).getParentid();
//                        DepartmentVo vo = deptTempMapper.selectByPrimaryKey(id);//查询出单挑的数据
                        //将查询出的id进行存储
//                        String parentid=vo.getId()+"";
                        //根据上面selectByPrimaryKey查询出来的数据再次查询查询他的全部数据
                        SysDept vo = sysDeptMapper.selectDeptById(String.valueOf(id));
                        SysDept sysDept = new SysDept();
                        //判断查询出来的数据不等于空查询的是`depe_temp`

                        //判断这个查询出来的值是否在sys_dept中存在如果存在那么走的就是修改如果不存在走的就是增加
                        StringBuffer str = new StringBuffer();
                        if (null != vo) {
                            sysDept.setDeptId(vo1.getId() + "");
                            sysDept.setParentId(vo1.getParentid() + "");
                            sysDept.setDeptName(vo1.getName());
                            String s = getchildrenMeun(str, String.valueOf(vo1.getParentid()), deptList);
                            sysDept.setAncestors(s);
                            sysDeptMapper.updateDept(sysDept);

                        }else{
                            deptTempMapper.insertSelective(vo1);
                            sysDept.setDeptId(vo1.getId()+"");
                            sysDept.setDeptName(vo1.getName());
                            sysDept.setParentId(vo1.getParentid()+"");
                            //拼接字符串
                            String s = getchildrenMeun(str,String.valueOf(vo1.getParentid()), deptList);
                            sysDept.setAncestors(s);
                            sysDeptMapper.insertDept(sysDept);
                        }
                    }
                }

                return AjaxResult.success(deptList);
            }else {
                return  AjaxResult.error("云信返回组织列表失败");
            }

        }else{
            return  AjaxResult.error("组织列表为空");
        }
    }

    public String getchildrenMeun(StringBuffer str,String id,List<DepartmentVo> allMeun){
        List<DepartmentVo> childrenList=new ArrayList<DepartmentVo>();

        if (id.equals("0")) {
            return id;
        }
        for (DepartmentVo info: allMeun){

            if (id.equals(String.valueOf(info.getId()))) {
                childrenList.add(info);
                str.append(info.getId()).append(",");
            }
        }

        for (DepartmentVo info:childrenList){
            getchildrenMeun(str,String.valueOf(info.getParentid()), allMeun);
         }
        if(childrenList!=null && childrenList.size()==0){

            return "";
        }

        String s = str.toString();
        if (s.length()==0) {
            return s;
        }

        String str1 = s.substring(0, s.length()-1);
        StringTokenizer st = new StringTokenizer(str1,",");

        List<String> strings = Arrays.asList(str1.split(","));
        Collections.reverse(strings);

        return ListToStr(strings);

    }

    public String ListToStr(List<String> t) {
        String str = "";
        int index=0;
        for(String strE :t){
            str += strE;
            index++;
            if (index == t.size()) {
                break;
            } else {
                str += ",";
            }

        }
        return str;
    }

    /**
     * 从云信获取组织列表  第三方调用
     *
     * @param idToken
     * @return
     */
    @Override
    public AjaxResult getOrgListForThirdParty(Integer parentId,Map<String,Object> params) {

        AjaxResult checkParamsRes = checkParams(params);
        Integer paramCode = (Integer)checkParamsRes.get("code");
        if (paramCode == 200){
            AjaxResult ajax = new AjaxResult();
            String accessToken = accutils.getAccessToken();
            String param = "access_token=" + accessToken;
            if (null != parentId) {
                param = param + "&id=" + parentId;
            }
            String resultOrgList = HttpUtils.sendGet(Constants.WX_DEPARTMANT_LIST_URL, param);

            if (StringUtils.isNotEmpty(resultOrgList)) {
                Map<Object, Object> map = JsonUtils.stringToCollect(resultOrgList);
                Integer code = (Integer) map.get("errcode");
                if (code == 0) {
                    List<DepartmentVo> deptList = JsonUtils.getJsonToList(map.get("department").toString(), DepartmentVo.class);
                    return AjaxResult.success(deptList);
                }
            }
            return  AjaxResult.error(9999,"云信获取用户数据失败");
        }

        return checkParamsRes;
    }

    /**
     * 从云信获取用户列表  本地同步云信用户数据
     * @param idToken
     * @return
     */
    @Override
    @Transactional
    public AjaxResult getUserList(String idToken,Integer orgId) {
        String accessToken = accutils.getAccessToken();
        String resultUserList = HttpUtils.sendGet(Constants.WX_USER_LIST_URL,"access_token="+accessToken+"&department_id="+orgId);
        logger.info("============  resultUserList ================="+resultUserList);
        logger.info("0000000000000000000000000 resultUserList 0000000000000000000000000"+resultUserList);
        if(StringUtils.isNotEmpty(resultUserList)){
            Map<Object, Object> map =JsonUtils.stringToCollect(resultUserList);
            Integer code = (Integer)map.get("errcode");
            if(code == 0){
                List<UserTemp> userList = JsonUtils.getJsonToList(map.get("userlist").toString(),UserTemp.class) ;

                if(null != userList && userList.size()>0 ){
                    for (int i = 0; i < userList.size(); i++) {
                        String  userId = userList.get(i).getUserId();
                        UserTemp model = userTempMapper.selectByUserId(userId);
                        SysUser model2 = sysUserMapper.selectByUserId(userId);//查询sys_user表中是否有该用户，有则修改，无则添加
                        if(null != model ){
                            userTempMapper.updateByPrimaryKeySelective(userList.get(i));
                        }else{
                            userTempMapper.insertSelective(userList.get(i));
                        }
                        SysUser sysUser = new SysUser();
                        sysUser.setUserId(userList.get(i).getUserId());
                      //  sysUser.setDeptUUID(userList.get(i).getDepartment());
                        sysUser.setNickName(userList.get(i).getName());
                        sysUser.setEmail(userList.get(i).getEmail());
                        sysUser.setPhonenumber(userList.get(i).getMobile());
                        sysUser.setUserCode(userList.get(i).getMobile());
                        String gender = userList.get(i).getGender();
                        if ("0".equals(gender)){
                            sysUser.setSex("1");
                        }else if ("1".equals(gender)){
                            sysUser.setSex("2");
                        }else {
                            sysUser.setSex("0");
                        }
                        String status = userList.get(i).getStatus();
                        if ("1".equals(status)){
                            sysUser.setStatus("0");
                        }else {
                            sysUser.setStatus("1");
                        }
                        sysUser.setSex(gender);
                        sysUser.setStatus(status);
                        sysUser.setCreateTime(new Date());
                        String deptUUID =userList.get(i).getDepartment();

                        //1.去除中括号
                        deptUUID = deptUUID.substring(1);
                        deptUUID = deptUUID.substring(0, deptUUID.length() - 1);

                        //2.将字符串通过 ， 分隔存入数组
                        String[] deptUUIDArr = deptUUID.split(",");

                        //3.判断
                        if(deptUUIDArr.length == 1) {//数组长度为1，则直接去该数
                            sysUser.setDeptId(deptUUID);
                        }else {//长度大于1，取出最大数
                            int [] newArr = new int[deptUUIDArr.length];//将字符串数组转换为Int数组，便于数据对比
                            for(int j=0; j<deptUUIDArr.length; j++) {
                                newArr[j] = Integer.parseInt(deptUUIDArr[j]);
                            }
                            //开始比较数据大小，获取到最大数
                            Arrays.sort(newArr);
                            sysUser.setDeptId( String.valueOf(newArr[newArr.length-1]) );
                        }

                        if(null != model2 ) {//向sys_user表执行修改
                            sysUserMapper.updateByPrimaryKeySelective(sysUser);
                        }else {//向sys_user表执行添加
                            sysUserMapper.insertSelective(sysUser);
                        }
                    }
                }
                return  AjaxResult.success("成功",userList);
            }else {
                return  AjaxResult.error("云信返回组织列表失败");
            }
        }
        return AjaxResult.error("云信未返回组织列表");
    }

    /**
     * 从云信获取用户列表  第三方调用
     *
     * @param idToken
     * @return
     */
    @Override
    public AjaxResult getUserListForThirdParty(Integer orgId,Map<String,Object> params) {

        AjaxResult checkParamsRes = checkParams(params);
        Integer paramCode = (Integer)checkParamsRes.get("code");
        if (paramCode == 200){
            AjaxResult ajax = new AjaxResult();
            String accessToken = accutils.getAccessToken();
            String resultUserList = HttpUtils.sendGet(Constants.WX_USER_LIST_URL, "access_token=" + accessToken + "&department_id=" + orgId);
            if (StringUtils.isNotEmpty(resultUserList)) {
                Map<Object, Object> map = JsonUtils.stringToCollect(resultUserList);
                Integer code = (Integer) map.get("errcode");
                if (code == 0) {
                    List<UserTemp> userList = JsonUtils.getJsonToList(map.get("userlist").toString(), UserTemp.class);
                    return  AjaxResult.success("成功",userList);
                }
            }
            return  AjaxResult.error(9999,"云信获取组织数据失败");
        }
        return checkParamsRes;
    }



    @Override
    public AjaxResult getWxUserList(Integer orgId) {
        return null;
    }

    @Override
    public AjaxResult getUserByUserId(String idToken,String userId) {
        if(StringUtils.isNotEmpty(idToken)){
            String oldUser = redisCache.getCacheObject(idToken);
            if(StringUtils.isNotEmpty(oldUser)){
                return  AjaxResult.success(oldUser);
            }
        }
        String accessToken = accutils.getAccessToken();
        String resultUser = HttpUtils.sendGet(Constants.WX_USER_URL,"access_token="+accessToken+"&userid="+userId);
        Map<Object, Object> map =JsonUtils.stringToCollect(resultUser);
        Integer code = (Integer)map.get("errcode");
        if(0 == code){
            redisCache.setCacheObject(idToken,resultUser,10,TimeUnit.MINUTES);
            return AjaxResult.success(resultUser);
        }else{
            return AjaxResult.error("云信获取用户失败");
        }

    }

    @Override
    public List<DepartmentVo> deptList() {
        return deptTempMapper.getAllDept();
    }

    @Override
    public String getUserByCode(String code) {
        String accessToken = accutils.getAccessToken();
        logger.info("accessToken===="+accessToken+"========code======"+code);
        String resultUser = HttpUtils.sendGet(Constants.WX_USERINFO_CODE,"access_token="+accessToken+"&code="+code);
        Map<Object, Object> map =JsonUtils.stringToCollect(resultUser);
        Integer errcode = (Integer)map.get("errcode");
        if(errcode == 0){
            String userid =  (String)map.get("UserId");
            logger.info("UserId===="+userid);
            String idToken ="";
            if(StringUtils.isNotEmpty(userid)){
                idToken = loginService.appLogin(userid);
                logger.info("idToken===="+idToken);
            }

           return idToken;

        }
        return "无权限";
    }

    // 检验 appKey 和 appSecret
    public AjaxResult checkParams(Map<String,Object> params){
        String appKey = String.valueOf(params.get("appKey"));
        if ("null".equals(appKey) || StringUtils.isEmpty(appKey)){
            return AjaxResult.error(9999,"appKey不能为空！！！");
        }
        String appSecret = String.valueOf(params.get("appSecret"));
        if ("null".equals(appSecret) || StringUtils.isEmpty(appSecret)){
            return AjaxResult.error(9999,"appSecret不能为空！！！");
        }
        ApiInvok apiInvok = apiInvokMapper.selectByKey(appKey,appSecret);
        if(null == apiInvok){
            return AjaxResult.error(9999,"appKey或者appSecret不正确！！！");
        }
        return AjaxResult.success();
    }
}
