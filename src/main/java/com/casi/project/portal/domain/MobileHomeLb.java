package com.casi.project.portal.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther shp
 * @data2020/4/1711:18
 * @description
 */


@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel
public class MobileHomeLb {

    @ApiModelProperty("主键")
    private String id;

    //@ApiModelProperty("轮播名称")
    private String  lbName;

    //@ApiModelProperty("排序地址")
    private String lbSoft;

    @ApiModelProperty("图片地址")
    private String  lbUrl;

    @ApiModelProperty("跳转应用地址")
    private String  lbAppUrl;


    //@ApiModelProperty("创建时间")
    private String  createTime;

    //@ApiModelProperty("修改时间")
    private String  updateTime;
    
    private Integer isIfream;

}
