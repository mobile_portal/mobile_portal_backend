package com.casi.project.portal.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @Auther shp
 * @data2020/4/1510:59
 * @description
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticeVO {
    private String id;
    private String title;
    private String checker;
    private String pushByUser;
    private String isPublish;
    private String auditStatus;
    private String pushTime;
    private String notices;
    private String deptName;
    private Boolean hasRead;
    private Long isShow;
    private String createTime;
    private String updateTime;

    private String read;

    private Date onTime;
    private Date offTime;

}
