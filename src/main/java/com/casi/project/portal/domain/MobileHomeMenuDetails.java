package com.casi.project.portal.domain;

import com.casi.common.constant.Constants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;

/**
 * @Auther shp
 * @data2020/4/1316:58
 * @description
 */

@ApiModel
public class MobileHomeMenuDetails {

    @ApiModelProperty("主键")
    private String id;
    @ApiModelProperty("菜单按钮名称")
    private String menuButtonName;
    @ApiModelProperty("菜单按钮图片")
    private String menuButtonIcon;
    @ApiModelProperty("菜单按钮跳转链接")
    private String menuButtonUrl;
    @ApiModelProperty("菜单按钮上级ID，外键mobile_home_menu表")
    private Long buttonParentId;
    @ApiModelProperty("0-显示,1-隐藏")
    private String isShow;
    @ApiModelProperty("按钮排序字段")
    private Integer buttonSort;
    @ApiModelProperty("创建时间")
    private Date createTime;
    @ApiModelProperty("更新时间")
    private Date updateTime;
    @ApiModelProperty("是否允许嵌入ifream:0-允许，1-不允许")
    private Integer isIfream;

    private String menuType;

    private String groupName;

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getMenuType() {
        return menuType;
    }

    public void setMenuType(String menuType) {
        this.menuType = menuType;
    }

    public MobileHomeMenuDetails() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenuButtonName() {
        return menuButtonName;
    }

    public void setMenuButtonName(String menuButtonName) {
        this.menuButtonName = menuButtonName;
    }

    public String getMenuButtonIcon() {
        return Constants.BASE_IMG_URL+menuButtonIcon;
    }

    public void setMenuButtonIcon(String menuButtonIcon) {
        this.menuButtonIcon = menuButtonIcon;
    }

    public String getMenuButtonUrl() {
        return menuButtonUrl;
    }

    public void setMenuButtonUrl(String menuButtonUrl) {
        this.menuButtonUrl = menuButtonUrl;
    }

    public Long getButtonParentId() {
        return buttonParentId;
    }

    public void setButtonParentId(Long buttonParentId) {
        this.buttonParentId = buttonParentId;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public Integer getButtonSort() {
        return buttonSort;
    }

    public void setButtonSort(Integer buttonSort) {
        this.buttonSort = buttonSort;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }


    public Integer getIsIfream() {
        return isIfream;
    }

    public void setIsIfream(Integer isIfream) {
        this.isIfream = isIfream;
    }
}
