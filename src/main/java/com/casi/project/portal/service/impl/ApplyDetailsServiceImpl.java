package com.casi.project.portal.service.impl;

import com.casi.common.constant.Constants;
import com.casi.common.utils.DateUtils;
import com.casi.common.utils.SecurityUtils;
import com.casi.common.utils.ServletUtils;
import com.casi.common.utils.StringUtils;
import com.casi.framework.security.LoginUser;
import com.casi.framework.security.service.TokenService;
import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.portal.controller.NoticeController;
import com.casi.project.portal.domain.ApplyDetails;
import com.casi.project.portal.domain.ApplyVersion;
import com.casi.project.portal.domain.MobileHomeMenuDetails;
import com.casi.project.portal.domain.UserAndRole;
import com.casi.project.portal.mapper.ApplyDetailsMapper;
import com.casi.project.portal.mapper.MobileApplyUserMapper;
import com.casi.project.portal.service.ApplyDetailsService;
import com.casi.project.portal.util.Tools;
import com.casi.project.system.domain.SysUser;
import com.casi.project.system.domain.SysUserRole;
import com.casi.project.system.mapper.SysDeptMapper;
import com.casi.project.system.mapper.SysRoleMapper;
import com.casi.project.system.mapper.SysUserMapper;
import com.casi.project.system.service.ISysUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.annotation.Resource;
import java.util.*;

/**
 * 应用明细
 *
 * @author 18732
 */
@Service
public class ApplyDetailsServiceImpl implements ApplyDetailsService {

    @Resource
    private ApplyDetailsMapper applyMapper;

    @Resource
    private ISysUserService iSysUserService;

    @Resource
    private SysDeptMapper sysDeptMapper;
    @Resource
    private SysUserMapper userMapper;

    @Autowired
    private TokenService tokenService;

    @Resource
    private MobileApplyUserMapper mobileApplyUserMapper;


    @Resource
    private SysRoleMapper sysRoleMapper;


    private static Logger logger = LoggerFactory.getLogger(NoticeController.class);

    /**
     * 添加应用明细
     *
     * @param applyDetails
     * @return
     */
    @Transactional
    @Override
    public AjaxResult addApplyDetails(ApplyDetails applyDetails) {
        if (applyDetails.getButtonParentId() == null) {
            return AjaxResult.error("上级应用类别buttonParentId不能为空");
        }
/*        if (applyDetails.getApplyTypeId() == null) {
            return AjaxResult.error("上级应用类型applyTypeId不能为空");
        }*/
        if (Tools.isEmpty(applyDetails.getMenuButtonName())) {
            return AjaxResult.error("应用名称不能为空");
        }
        if (Tools.isEmpty(applyDetails.getSynopsis())) {
            return AjaxResult.error("应用简介不能为空");
        }
        if (applyDetails.getButtonSort() == null || applyDetails.getButtonSort() == 0) {
            return AjaxResult.error("排序号不能为空");
        }
        if (applyDetails.getButtonSort() > Integer.MAX_VALUE) {
            return AjaxResult.error("排序号长度必须小于9位");
        }
        if (Tools.isEmpty(applyDetails.getMenuButtonIcon())) {
            return AjaxResult.error("应用图标不能为空");
        }
        if (Tools.isEmpty(applyDetails.getMenuButtonUrl())) {
            return AjaxResult.error("应用地址不能为空");
        }
        if (!applyDetails.getMenuButtonUrl().startsWith("https") && !applyDetails.getMenuButtonUrl().startsWith("http") && !applyDetails.getMenuButtonUrl().startsWith("local")) {
            return AjaxResult.error("请输入以http/local/https开头的移动端访问地址");
        }
        if (Tools.isEmpty(applyDetails.getAppKey())) {
            return AjaxResult.error("app_key不能为空");
        }
        if (Tools.isEmpty(applyDetails.getAppSecret())) {
            return AjaxResult.error("app_secret不能为空");
        }
        if (Tools.isEmpty(applyDetails.getLbUrl())) {
            return AjaxResult.error("应用了轮播图不能为空");
        }
        //判断应用名称是否唯一
        int count = applyMapper.checkMenuButtonNameUnique(applyDetails.getMenuButtonName());
        if (count > 0) {
            return AjaxResult.error("新增应用具体信息'" + applyDetails.getMenuButtonName() + "'失败，应用名称已存在");
        }
        LoginUser loginUser = tokenService.getLoginUser(ServletUtils.getRequest());
        if (StringUtils.isNull(loginUser)) {
            return AjaxResult.error("没有当前登录用户");
        }
        String userId = loginUser.getUser().getUserId();
        //根据用户id查询当前登录的用户名
        SysUser sysUser = userMapper.selectUserById(userId);
        if (StringUtils.isNull(sysUser)) {
            return AjaxResult.error("没有当前登录用户");
        }
        if (sysUser.isAdmin()) {
            applyDetails.setCreateBy(userId);
            applyDetails.setCheckStatus(1);
            int changeStatus = applyMapper.addApplyDetails(applyDetails);
            if (changeStatus < 1) {
                return AjaxResult.error("新增应用具体信息'" + applyDetails.getMenuButtonName() + "'失败");
            }
        } else {
            applyDetails.setCreateBy(userId);
            applyDetails.setCheckStatus(0);
            int rows = applyMapper.addApplyDetails(applyDetails);
            if (rows < 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return AjaxResult.error("新增应用具体信息'" + applyDetails.getMenuButtonName() + "'失败");
            }
        }
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put("homeMenuDetailsId", applyDetails.getId());
        hashMap.put("lbUrl", applyDetails.getLbUrl());
        int row = applyMapper.addApplyCarousel(hashMap);
        if (row < 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return AjaxResult.error("新增轮播图" + applyDetails.getMenuButtonName() + "失败");
        }

        // 在移动端展示
        String detailsId = applyMapper.selectApplyDetailsByMenuButtonName(applyDetails.getMenuButtonName());
        if (StringUtils.isNotEmpty(detailsId)) {
            int count1 = applyMapper.selectApplyUsersCount(detailsId, SecurityUtils.getLoginUser().getUser().getUserId());
            if (count1 < 1) {
                int i = applyMapper.addApplyUsersShow(detailsId, SecurityUtils.getLoginUser().getUser().getUserId());
            }
        }
        return AjaxResult.success("添加应用成功");
        //调用李元接口传入组织机构String[]id 和应用id      循环遍历组织机构的id查询拿到userId 然后插入到表中 用户id 应用id 和 组织机构id
//        return iSysUserService.insertApplyUser(applyDetails.getId(), applyDetails.getIds());
    }

    /**
     * 回显应用明细
     *
     * @param id
     * @return
     */
    @Override
    public AjaxResult findApplyDetailsId(String id) {
        if (id == null) {
            AjaxResult.error("请传要回显应用明细ID");
        }
        HashMap<String, Object> map = new HashMap<>();
        //查询应用明细
        ApplyDetails applyDetailsId = applyMapper.findApplyDetailsId(id);
        //回显应用范围名称 和id
        //List<Map<String, Object>> maps = sysDeptMapper.selectByApplyId(id);
        // 根据应用id回显应用类型名称
//        HashMap<String, Object> applyTypeName = applyMapper.getApplyTypeName(applyDetailsId.getApplyTypeId());
//        根据应用id回显应用类别名称
        HashMap<String, Object> applyCategoryName = applyMapper.getApplyCategoryName(applyDetailsId.getButtonParentId());
        //从目录获取文件回显应用图标
        String menuButtonIcon = applyDetailsId.getMenuButtonIcon();
        //保存不带Ip的图片
        applyDetailsId.setMenuButtonIconNoIp(menuButtonIcon);
        String baseImgUrl = Constants.BASE_IMG_URL + menuButtonIcon;
        applyDetailsId.setMenuButtonIcon(baseImgUrl);

        //获取应用封面的轮播图的地址
        HashMap<String, Object> applyLb = applyMapper.getApplyLb(id);
        //获取应用封面的了轮播图的地址
        if (StringUtils.isNotNull(applyLb)) {
            String lbUrl = (String) applyLb.get("lbUrl");
            String lbUrlIp = Constants.BASE_IMG_URL + lbUrl;
            applyLb.put("lbUrlIp", lbUrlIp);
        }
        map.put("applyDetailsId", applyDetailsId);
//        map.put("applyTypeName", applyTypeName);
        map.put("applyCategoryName", applyCategoryName);
        map.put("applyLb", applyLb);
        return AjaxResult.success("查询成功", map);
    }

    /**
     * 修改应用明细
     *
     * @param applyDetails1
     * @return
     */
    @Transactional
    @Override
    public AjaxResult updateApplyDetails(ApplyDetails applyDetails1) {
        if (null == applyDetails1.getId()) {
            return AjaxResult.error("请传要修改的应用明细ID");
        }
        if (Tools.isEmpty(applyDetails1.getMenuButtonName())) {
            return AjaxResult.error("应用名称不能为空");
        }
        if (Tools.isEmpty(applyDetails1.getSynopsis())) {
            return AjaxResult.error("应用简介不能为空");
        }
        if (applyDetails1.getButtonSort() == null || applyDetails1.getButtonSort() == 0) {
            return AjaxResult.error("排序号不能为空");
        }
        if (Tools.isEmpty(applyDetails1.getMenuButtonIcon())) {
            return AjaxResult.error("应用图标不能为空");
        }
        if (Tools.isEmpty(applyDetails1.getMenuButtonUrl())) {
            return AjaxResult.error("应用地址不能为空");
        }
        if (Tools.isEmpty(applyDetails1.getLbUrl())) {
            return AjaxResult.error("应用了轮播图不能为空");
        }
        //先更新应用明细表
        int rows = applyMapper.updateApplyDetails(applyDetails1);
        if (rows < 1) {
            return AjaxResult.error("修改应用失败");
        }
        int row = applyMapper.updateApplyLb(applyDetails1);
        if (row < 1) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return AjaxResult.error("修改应用失败");
        }
        return AjaxResult.success("修改应用成功");
    }

    /**
     * 删除应用明细
     *
     * @param id
     * @return
     */
    @Transactional
    @Override
    public AjaxResult deleteApplyDetails(String id) {
        if (StringUtils.isEmpty(id)) {
            return AjaxResult.error("请传要删除应用的id");
        }
//        Integer id = (Integer) hashMap.get("id");
//        //Integre转换成Long类型
//        Long id1 = id.longValue();
        int rows = applyMapper.deleteApplyDetails(id);
        if (rows < 1) {
            return AjaxResult.error("删除应用失败");
        }
        //删除轮播图
        int count = applyMapper.selectApplyLbCount(id);
        if (count > 0) {

            //删除轮播图
            int row = applyMapper.deleteApplyLb(id);
            if (row < 1) {
                TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
                return AjaxResult.error("删除应用失败");
            }
        }
        return AjaxResult.success("删除成功");
    }

    /**
     * 查询应用管理列表和条件模糊查询
     *
     * @param menuButtonName
     * @return
     */
    @Override
    public List<Map<String, Object>> getApplyList(String menuButtonName) {
        List<Map<String, Object>> applyList = applyMapper.getApplyList(menuButtonName);
        for (Map<String, Object> stringObjectMap : applyList) {
            if (stringObjectMap.get("create_time") != null) {
                Date updateTime = (Date) stringObjectMap.get("create_time");
                stringObjectMap.put("create_time", DateUtils.dateTimeHMS(updateTime));
            }
        }
        return applyList;
    }


    /**
     * 查询指定应用列表
     *
     * @param menuButtonName
     * @return
     */
    @Override
    public Map<String, Object> getApplyListId(String menuButtonName, String applyId) {
        Map<String, Object> applyList = applyMapper.getApplyListId(menuButtonName, applyId);
        if (applyList != null && applyList.get("create_time") != null) {
            Date updateTime = (Date) applyList.get("create_time");
            applyList.put("create_time", DateUtils.dateTimeHMS(updateTime));
        }
        return applyList;
    }

    /**
     * 判断用户和应用是否在黑名单中
     *
     * @param userId
     * @param applyId
     * @return
     */
    @Override
    public int selectIsBlackList(String userId, String applyId) {
        return applyMapper.selectIsBlackList(userId, applyId);
    }

    /**
     * 查询用户和部门的应用id
     *
     * @param userId
     * @param deptId
     * @return
     */
    @Override
    public List<HashMap<String, Object>> getApplyId(String userId, String deptId) {
        return applyMapper.getApplyId(userId, deptId);
    }

    /**
     * 禁用启用应用明细
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public AjaxResult disableEnabledDetails(String[] id, Integer type) {
        if (id == null) {
            return AjaxResult.error("请传要禁用应用类别的Id");
        }
        if (null == type) {
            return AjaxResult.error("请传要要禁用启用的值 0-启用 1-禁用");
        }
        if (type != 0 && type != 1) {
            return AjaxResult.error("请传0-启用,1-禁用这个值");
        }
        int rows = 0;
        try {
            for (int i = 0; i < id.length; i++) {
                rows = applyMapper.disableEnabledDetails(id[i], type);
            }
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("成功") : AjaxResult.error("失败");
    }

    /**
     * 审核应用
     *
     * @param id
     * @param type
     * @return
     */
    @Override
    public AjaxResult checkApplyStatus(String id, Integer type) {
        if (null == id) {
            return AjaxResult.error("请带来要审核应用的ID");
        }
        if (StringUtils.isNull(type)) {
            return AjaxResult.error("请传对应要审核的type值");
        }
        if (type != 1) {
            return AjaxResult.error("请type字段为1审核值");
        }
        int rows = 0;
        try {
            //根据审核id查询  :0-带审核,1-已审核
            Map<String, Object> map1 = applyMapper.selectCheckApplyStatus(id);
            int check_status = (int) map1.get("check_status");
            if (check_status == 1) {
                return AjaxResult.error(map1.get("menu_button_name") + "已经审核");
            }
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("id", id);
            hashMap.put("type", type);
            rows = applyMapper.checkApplyStatus(hashMap);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("审核成功") : AjaxResult.error("审核失败");
    }

    /**
     * 上线应用
     *
     * @param id
     * @return
     */
    @Override
    public AjaxResult upLineApply(String id) {
        if (null == id) {
            return AjaxResult.error("请带来要上线应用的ID");
        }
        Map<String, Object> map = applyMapper.selectCheckApplyStatus(id);
        int check_status = (int) map.get("check_status");
        if (check_status == 0) {
            return AjaxResult.error("该应用未审核通过,请审核");
        }
        int rows = applyMapper.upLineApply(id);
        return rows > 0 ? AjaxResult.success("上线成功") : AjaxResult.error("上线失败");
    }

    /**
     * 新增版本
     *
     * @param applyVersion
     * @return
     */
    @Override
    public AjaxResult addApplyVersion(ApplyVersion applyVersion) {
        if (null == applyVersion.getApplyVersion()) {
            return AjaxResult.error("请带来上级应用明细ID");
        }
        if (Tools.isEmpty(applyVersion.getIcon())) {
            return AjaxResult.error("请上传图标");
        }
        if (Tools.isEmpty(applyVersion.getVersionNumber())) {
            return AjaxResult.error("请输入版本号");
        }
        if (Tools.isEmpty(applyVersion.getVersionDescribe())) {
            return AjaxResult.error("请输入版本描述");
        }
        if (null == applyVersion.getApplyScope() || applyVersion.getApplyScope() == 0) {
            return AjaxResult.error("请输入应用范围");
        }
        if (Tools.isEmpty(applyVersion.getAppAddress())) {
            return AjaxResult.error("请输入移动端访问地址");
        }
        //     移动端访问地址：轻应用实际上是移动端的一个web地址，所以直接关联URL即可。
        //     支持http（s）/ftp/mms/local开头的网址，如果上传了离线资源包，访问地址必须以local开头
        String appAddress = applyVersion.getAppAddress();
        if (!applyVersion.getOffAddress().isEmpty()) {
            if (!appAddress.startsWith("local")) {
                return AjaxResult.error("请输入以local开头的移动端访问地址");
            }
        }
        if (!appAddress.startsWith("https") && !appAddress.startsWith("ftp") && !appAddress.startsWith("mms") && !appAddress.startsWith("local")) {
            return AjaxResult.error("请输入以http（s）/ftp/mms/local开头的移动端访问地址");
        }
        if (Tools.isEmpty(applyVersion.getTitleBarColor())) {
            return AjaxResult.error("请输入标题栏颜色");
        }
        if (Tools.isEmpty(applyVersion.getProgressBarColor())) {
            return AjaxResult.error("请输入进度栏颜色");
        }
        if (Tools.isEmpty(applyVersion.getPcAddress())) {
            return AjaxResult.error("请输入PC端访问地址");
        }
        //     PC端访问地址（应用范围内勾线了PC端），
        //      支持http（s）/ftp/mms/local开头的网址
        String pcAddress = applyVersion.getPcAddress();
        if (!pcAddress.startsWith("https") && !pcAddress.startsWith("ftp") && !pcAddress.startsWith("mms") && !pcAddress.startsWith("local")) {
            return AjaxResult.error("请输入以http（s）/ftp/mms/local开头的PC端访问地址");
        }
        if (Tools.isEmpty(applyVersion.getAdminAddress())) {
            return AjaxResult.error("请输入管理后台访问地址");
        }
        // PC端访问地址（应用范围内勾线了PC端），
        // 支持http（s）/ftp/mms/local开头的网址
        String adminAddress = applyVersion.getAdminAddress();
        if (!adminAddress.startsWith("https") && !adminAddress.startsWith("ftp") && !adminAddress.startsWith("mms") && !adminAddress.startsWith("local")) {
            return AjaxResult.error("请输入以http（s）/ftp/mms/local开头的管理后台访问地址");
        }
        if (Tools.isEmpty(applyVersion.getMessageAddress())) {
            return AjaxResult.error("请输入消息通知源地址");
        }
        int rows = 0;
        try {
            rows = applyMapper.addApplyVersion(applyVersion);
        } catch (Exception e) {
            e.printStackTrace();
            logger.info(e.toString(), e);
        }
        return rows > 0 ? AjaxResult.success("添加成功") : AjaxResult.error("添加失败");
    }

    /**
     * 应用指定开发者账户——应用列表展示
     */
    @Override
    public AjaxResult queryAllApply(Integer pageNum, Integer pageSize) {
        if (null == pageNum || null == pageSize) {
            return AjaxResult.error("缺失参数");
        }
        PageHelper.startPage(pageNum, pageSize);
        List<Map<String, Object>> maps = applyMapper.selectAllApplyDetails();
        PageInfo<Map<String, Object>> mapPageInfo = new PageInfo<>(maps);
        return AjaxResult.success("查询成功", mapPageInfo);
    }


    /**
     * 指定应用开发管理员
     *
     * @param
     * @param
     * @return
     */
    @Override
    public AjaxResult addApplyDevelopAdminIsTraTor(UserAndRole userAndRole) {
        if (StringUtils.isNull(userAndRole)) {
            return AjaxResult.error("参数为空");
        }
        if (StringUtils.isNull(userAndRole.getUserId())) {
            return AjaxResult.error("请传用户id");
        }
        if (StringUtils.isEmpty(userAndRole.getRoleId())) {
            return AjaxResult.error("请传角色id");
        }
        for (int i = 0; i < userAndRole.getUserId().length; i++) {
            int count = applyMapper.selectIsUserRoleExists(userAndRole.getUserId()[i], userAndRole.getRoleId());
            if (count > 0) {
                return AjaxResult.error("此用户" + applyMapper.selectUserName(userAndRole.getUserId()[i]) + "已经存在");
            }
        }
        String[] userIds = userAndRole.getUserId();
        ArrayList<SysUserRole> list = new ArrayList<>();
        for (String userId : userIds) {
            SysUserRole sysUserRole1 = new SysUserRole();
            sysUserRole1.setUserId(userId);
            sysUserRole1.setRoleId(userAndRole.getRoleId());
            list.add(sysUserRole1);
        }
        int rows = applyMapper.addApplyDevelopAdminIsTraTor(list);
        return rows > 0 ? AjaxResult.success("添加成功") : AjaxResult.error("添加失败");
    }


    /**
     * 根据应用appKey和appSecret获得应用的资源
     *
     * @param hashMap
     * @return
     */
    @Override
    public AjaxResult getApplyResource(HashMap<String, Object> hashMap) {
        if (StringUtils.isNull(hashMap.get("appKey"))) {
            return AjaxResult.error("appKey为空");
        }
        if (StringUtils.isNull(hashMap.get("appSecret"))) {
            return AjaxResult.error("appSecret为空");
        }
        List<HashMap<String, Object>> hashMaps = new ArrayList<>();
        try {
            hashMaps = applyMapper.getApplyResource(hashMap);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
        }
        return AjaxResult.success("查询成功", hashMaps);
    }


    /**
     * 根据应用appKey和appSecret获得应用的用户
     *
     * @param hashMap
     * @return
     */
    @Override
    public AjaxResult getApplyUser(HashMap<String, Object> hashMap) {
        if (StringUtils.isNull(hashMap.get("appKey"))) {
            return AjaxResult.error("appKey为空");
        }
        if (StringUtils.isNull(hashMap.get("appSecret"))) {
            return AjaxResult.error("appSecret为空");
        }
        String appKey = (String) hashMap.get("appKey");
        String appSecret = (String) hashMap.get("appSecret");
        List<HashMap<String, Object>> hashMaps = applyMapper.getApplyUser(appKey, appSecret);
        return AjaxResult.success("查询成功", hashMaps);
    }


    /**
     * 根据应用appKey，appSecret,token获得用户的用户权限
     *
     * @param hashMap
     * @return
     */
    @Override
    public AjaxResult getApplyUserPermission(HashMap<String, Object> hashMap) {
        if (StringUtils.isNull(hashMap.get("appKey"))) {
            return AjaxResult.error("appKey为空");
        }
        if (StringUtils.isNull(hashMap.get("appSecret"))) {
            return AjaxResult.error("appSecret为空");
        }
        SysUser loginUser = SecurityUtils.getLoginUser().getUser();
        if (StringUtils.isNull(loginUser)) {
            return AjaxResult.error("没有当前登录用户");
        }
        //获取用户的id
        String userId = loginUser.getUserId();
        //获取用户的权限
        List<HashMap<String, Object>> hashMaps = applyMapper.getApplyUserPermission((String) hashMap.get("appKey"), (String) hashMap.get("appSecret"), userId);
        return AjaxResult.success("查询成功", hashMaps);
    }

    /**
     * 根据应用appKey，appSecret 获得应用的所有角色
     */
    @Override
    public AjaxResult queryRolesByAppKeyAndAppSecret(ApplyDetails applyDetails) {
        if (StringUtils.isNull(applyDetails)) {
            return AjaxResult.error("缺失参数");
        }
        if (StringUtils.isEmpty(applyDetails.getAppKey())) {
            return AjaxResult.error("缺失参数");
        }
        if (StringUtils.isEmpty(applyDetails.getAppSecret())) {
            return AjaxResult.error("缺失参数");
        }
        List<Map<String, Object>> maps = sysRoleMapper.selectRolesByAppKeyAndAppSecret(applyDetails.getAppKey(), applyDetails.getAppSecret());
        return AjaxResult.success("查询成功", maps);
    }

    /**
     * 根据应用appKey，appSecret   获得应用的所有组织机构
     */
    @Override
    public AjaxResult queryDeptsByAppKeyAndAppSecret(ApplyDetails applyDetails) {
        if (StringUtils.isNull(applyDetails)) {
            return AjaxResult.error("缺失参数");
        }
        if (StringUtils.isEmpty(applyDetails.getAppKey())) {
            return AjaxResult.error("缺失参数");
        }
        if (StringUtils.isEmpty(applyDetails.getAppSecret())) {
            return AjaxResult.error("缺失参数");
        }
        List<Map<String, Object>> maps = sysDeptMapper.selectDeptsByAppKeyAndAppSecret(applyDetails.getAppKey(), applyDetails.getAppSecret());
        return AjaxResult.success("查询成功", maps);
    }

    /**
     * 根据应用appKey，appSecret   和用户token获得用户的角色
     */
    @Override
    public AjaxResult queryRoleByAppKeyAndAppSecretAndToken(ApplyDetails applyDetails) {
        if (StringUtils.isNull(applyDetails)) {
            return AjaxResult.error("缺失参数");
        }
        if (StringUtils.isEmpty(applyDetails.getAppKey())) {
            return AjaxResult.error("缺失参数");
        }
        if (StringUtils.isEmpty(applyDetails.getAppSecret())) {
            return AjaxResult.error("缺失参数");
        }
        //开始    从这起开始验证token闲在是已后台自己的token为例，如果不是已后台生产Token的方式在做替换
        /*if(StringUtils.isEmpty(applyDetails.getToken())){
            return AjaxResult.error("缺失参数");
        }*/
        // 获取用户的id
        String userId = SecurityUtils.getLoginUser().getUser().getUserId();
        if (StringUtils.isEmpty(userId)) {
            return AjaxResult.error("该用户不存在");
        }
        //结束了
        List<Map<String, Object>> maps = sysRoleMapper.selectRoleByAppKeyAndAppSecretAndToken(applyDetails.getAppKey(), applyDetails.getAppSecret(), userId);
        return AjaxResult.success("查询成功", maps);
    }
}