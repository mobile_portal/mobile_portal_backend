package com.casi.project.interfaces.service;

import com.casi.framework.web.domain.AjaxResult;
import com.casi.project.interfaces.domain.DepartmentVo;

import java.util.List;
import java.util.Map;

public interface WxUserService {


    AjaxResult getOrgList(String idToken,Integer parentId);

    AjaxResult getUserList(String idToken,Integer orgId);

    AjaxResult getUserListForThirdParty(Integer orgId,Map<String,Object> params);

    AjaxResult getOrgListForThirdParty(Integer orgId, Map<String,Object> params);

    AjaxResult getWxUserList(Integer orgId);

    AjaxResult getUserByUserId(String idToken,String userId);

    List<DepartmentVo> deptList();

    String getUserByCode(String code);

    // 检验 appKey 和 appSecret
    AjaxResult checkParams(Map<String,Object> params);
}
