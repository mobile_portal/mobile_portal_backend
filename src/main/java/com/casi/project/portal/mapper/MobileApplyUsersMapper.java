package com.casi.project.portal.mapper;



import com.casi.project.portal.domain.MobileApplyUsers;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;
import java.util.Set;

public interface MobileApplyUsersMapper {

    /**
     * 给用户授权应用
     */
    public int insertApplyIdsAndUserIds(@Param("tags") Set<MobileApplyUsers> tags);
    /**
     * 修改用户授权应用
     */
    public int updateApplyIdsAndUserIds(@Param("applyId") String applyId,@Param("userIdss")String[] userIdss);
    /**
     * 根据应用的id和用户的id查询
     */
    public MobileApplyUsers selectOneByApplyIdAndUserId(@Param("applyId") String applyId,@Param("userId") String userId);

    /**
     * 应用禁用用户
     */
    public int addApplyToDisabledUserIds(@Param("mobileApplyId")String mobileApplyId,@Param("userId") String userId);


    /**
     * 应用解除禁用用户
     */
    public int romoveApplyToDisabledUserIds(@Param("mobileApplyId")String mobileApplyId,@Param("userId") String userId);

    int selectDisabledByApplyIdAndUserId(@Param("mobileApplyId")String mobileApplyId,@Param("userId") String userId);

    /**
     * 回显已经授权的用户
     * @param applyIds
     * @return
     */
    List<HashMap<String, Object>> findApplyIdsAndUserIds(String[] applyIds);

    String[] applyDisabledUserList(String applyId);
    /**
     * 根据应用的id删除用户与应用的关联关系
     */
    public int deleteMobileApplyUsersByApplyId(String applyId);
    /**
     * 根据应用的id和用户的id逻辑删除应用与用户的关联关系
     */
    public int delectMobileApplyByApplyIdAndUserId(@Param("applyId") String applyId,@Param("userIds")String[] userIds);
    /**
     * 根据应用的id查询应用与用户表查询出这个应用对应的部门的id并去重
     */
    public String[] selectDeptIdsByApplyId(String applyId);
    /**
     * 根据应用的id查询应用与用户关联表  查看该应用是否授权给过用户
     */
    public String[] selectUserIdsByApplyId(String applyId);
}
