package com.casi.project.store.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.casi.framework.aspectj.lang.annotation.Excel;
import com.casi.framework.web.domain.BaseEntity;

/**
 * 应用资源对象 store_app_resource
 * 
 * @author casi
 * @date 2020-06-02
 */
public class StoreAppResource extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private String uuid;

    /** 应用编号 */
    @Excel(name = "应用编号")
    private String appId;

    /** 资源名称 */
    @Excel(name = "资源名称")
    private String name;

    /** 资源编码 */
    @Excel(name = "资源编码")
    private String code;

    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setAppId(String appId) 
    {
        this.appId = appId;
    }

    public String getAppId() 
    {
        return appId;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("uuid", getUuid())
            .append("appId", getAppId())
            .append("name", getName())
            .append("code", getCode())
            .toString();
    }
}
