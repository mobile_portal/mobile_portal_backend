package com.casi.project.portal.service.impl;

import com.casi.common.utils.file.FileUploadUtils;
import com.casi.framework.web.domain.AjaxResult;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.Arrays;

/**
 * @Auther shp
 * @data2020/4/2017:45
 * @description
 */


@Service
@Transactional(rollbackFor = Exception.class)
public class AddAppServiceImpl  {


    public AjaxResult uploadFile(MultipartFile file) {

        if (file.isEmpty()) {
            return AjaxResult.error("请上传一个文件");
        }

        AjaxResult ajaxResult = new AjaxResult();

        String upload = "";//输出一个相对地址
        try {
            upload = FileUploadUtils.upload(file);

            String[] arrs = {"jpg", "jpeg", "png"};
            if (useList(arrs, upload.substring(upload.indexOf(".") + 1))) {
                long size = file.getSize() / 1024;
                if (size > 1024) {
                    return AjaxResult.error("图片大小不能超过1mb");
                }
            }

            if (upload.endsWith(".apk")) {
                ajaxResult.put("appRoIos", 1);
            }
            if (upload.endsWith(".IPA")) {
                ajaxResult.put("appRoIos", 2);
            }

            ajaxResult.put("fileName", file.getOriginalFilename());
            ajaxResult.put("filePath", upload);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return ajaxResult;
    }


    public static boolean useList(String[] arr, String targetValue) {
        return Arrays.asList(arr).contains(targetValue);
    }

}

